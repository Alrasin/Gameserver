# Dereth Forever Server


**Dereth Forever is a custom, original server implementation for Asheron's Call built on C#.   The code base was originally forked from ACEmulator**

- MySQL and MariaDB are used as the database engine.
- Latest client supported.

***

## Disclaimer
**This project is for educational and non-commerical purposes only, use of the game client is for interoperability with the emulated server.**

- Asheron's Call was a registered trademark of Turbine, Inc. and WB Games Inc, but they abandoned it.
- Dereth Forever is not associated or affiliated in any way with Turbine, Inc. or WB Games Inc.

***
## Recommended Tools
* SQLYog [on Github](https://github.com/webyog/sqlyog-community/wiki/Downloads) (12.5.0 has known bugs that impact us, 12.5.1 or later recommended)
* Hex Editor (Hexplorer or 010 Editor are both good)
* ACLogView [on Github](https://github.com/ACEmulator/aclogview)
* StyleCop Visual Studio Extension [on visualstudio.com](https://marketplace.visualstudio.com/items?itemName=ChrisDahlberg.StyleCop)
* License Header Manager [on visualstudio.com](https://marketplace.visualstudio.com/items?itemName=StefanWenig.LicenseHeaderManager)
* Visual Studio 2017

## Getting Started

* Install MySQL
  - [MariaDB minimum required version - 10.2+](https://mariadb.org/download/) (preferred)
  - [MySQL minimum required version - 5.7.17+](https://dev.mysql.com/downloads/windows/installer/5.7.html)

## Automatic Database Setup on Intial Load:

* Copy `DerethForever\Config.json.example` to `Config.json` and modify settings, such as passwords and other server settings.
* Change 2 configuration variables in the server `Config.json` file, too enable Automatic deployment:
    - Set `EnableAutoRedeployment: true` in your Configuration:
        - Example:
            - Before:   ``"EnableAutoRedeployment": false,``
            - After:    ``"EnableAutoRedeployment": true,``
* Compile the Server and run the compiled binary too start the automated deplyoment process.
* Too prevent automatic database changes: Set `EnableAutoRedeployment` to false, if the previous deployment step was successful.

## Manual Database Setup:

* Create two databases named `df_auth`, and `df_shard`.
* Load the base scripts for their respective databases found in .\Database\Base\
* Load all incremental SQL updates found in the Database\Updates\Authentication sub directory in the order of oldest to newest.
* Load all incremental SQL updates found in the Database\Updates\Shard sub directory in the order of oldest to newest.
* Download the compressed version of the latest JSON release available, [found here](https://gitlab.com/DerethForever/GameWorld/tree/master/json-releases).  Extract the contents to a folder to be used in your configuration later on.
* Copy `DerethForever\Config.json.example` to `Config.json` and modify settings, such as passwords and other server settings.
* Copy `DerethForever.CmdLineLauncher\launcher_config.json.example` to `launcher_config.json` and modify your launcher settings to correspond with your DerethForever\config.json settings.
* Build and run DerethForever, DerethForever.CmdLineLauncher.
* Create your first account as an admin at the DF prompt - `accountcreate testaccount testpassword 5`
* Launch AC with the CmdLineLauncher or directly with this command: `acclient.exe -a testaccount -h 127.0.0.1:9000 -glsticketdirect null`

## Running the Server

* ONLY After succsfully compiling the server: run the `DerethForever.exe` binary from the target output path. Possible location: `$(Project Directory)\Source\DerethForever\bin\x64\Debug\DerethForever.exe`

## API

Once running, you can browse the APIs by adding "/swagger" to the hosted endpoints.  For example, the game server api defaults to run on port 8000, and you could browse to http://localhost:8000/swagger for the documentation.

## Contributions

* Contributions in the form of issues and pull requests are welcomed and encouraged.
* Do not work directly on `master` - the preferred way to contribute is to work on a new branch of the repo and submit a pull request on Bitbucket.
* Code style information can be found on the [Wiki](https://gitlab.com/DerethForever/Gameserver/wikis/Coding-Style-Standards).

## Bug Reports

* Please use [our Trello Board](https://trello.com/b/Lrp0GoSW/server) provided by Trello to send in bug reports by creating a new issue.

## FAQ

#### 1. StyleCop.MSBuild.targets not found
* _Problem_
> When opening the solution, you get a "The imported project "{project path}\DerethForever\Source\packages\StyleCop.MSBuild.5.0.0\build\StyleCop.MSBuild.targets" was not found. Confirm that the path in the <Import> declaration is correct, and that the file exists on disk" error.
* _Solution_
> Right click "Solution 'DerethForever'" in the Solution Explorer and select "Restore Nuget Packages".  After it restores, right click "DerethForever (load failed)" and select "Reload Project."

#### 2. Startup projects are not set / working
* _Problem_
> When you first load the solution and try to "run" the server, you may get a popup that says "A project with Output Type of Class Library cannot be started directly."
* _Solution_
> 1) Right click the Solution in Visual Studio ("Solution 'DerethForever' (X projects)"), and select "Set StartUp Projects".
> 2) The following projects should have "Start" in the "Action" column: DerethForever, DerethForever.CmdLineLauncher.

#### 3. DerethForever application throws "Access is denied" errors
* _Problem_ When you first run the application, you may get an error message:
```cs
Unhandled Exception: System.Reflection.TargetInvocationException: Exception has been thrown by the target of an invocation. ---> System.Net.HttpListenerException: Access is denied
```
* _Solution_
In an administrative Windows command prompt add port 8000 (or whatever you configure it to in config.json) to netsh's urlacl table: 
```powershell
netsh http add urlacl url=http://+:8000/ user=<YOUR USER ACCOUNT>
```


## Other Resources
* [Protocol documentation](http://ac.zegeger.net/protocol/) (Recommended)
* [Skunkworks Protocol documentation](http://skunkworks.sourceforge.net/protocol/Protocol.php) (outdated)
* [Virindi Protocol XML documentation](http://www.virindi.net/junk/messages_annotated_final.xml)

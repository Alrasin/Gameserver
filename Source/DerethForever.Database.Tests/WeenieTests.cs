/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.IO;
using DerethForever.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Entity.Enum.Properties;

namespace DerethForever.Database.Tests
{
    [TestClass]
    public class WeenieTests
    {
        private static WorldJsonDatabase _worldDb;

        private const uint TestWeenieId = 2;

        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            _worldDb = new WorldJsonDatabase(@"..\..\..\..\..\..\GameWorld\json");
            CreateTestingWeenie();
        }

        public static DataObject CreateTestingWeenie()
        {
            DataObject dataWeenie = _worldDb.GetWeenie(TestWeenieId) ?? new DataObject
            {
                DataObjectId = TestWeenieId,
                WeenieClassId = TestWeenieId,
                UserModified = true,
                DataObjectDescriptionFlags = (uint)ObjectDescriptionFlag.None
            };

            // 2 ints
            dataWeenie.Age = 1;
            dataWeenie.ArmorLevel = 1;

            // 2 int64s
            dataWeenie.AvailableExperience = 1;
            dataWeenie.TotalExperience = 1;

            // 2 strings
            dataWeenie.Name = "fubar";
            dataWeenie.Title = "the bold";

            // 2 floats
            dataWeenie.Shade = 1f;
            dataWeenie.Friction = 1f;

            // 2 data ids
            dataWeenie.CombatTableDID = 1;
            dataWeenie.ClothingBaseDID = 1;

            // 2 instance ids
            dataWeenie.AllowedActivator = 1;
            dataWeenie.LastUnlockerIID = 1;

            // 2 booleans
            dataWeenie.Afk = true;
            dataWeenie.IsGagged = true;

            _worldDb.SaveWeenie(dataWeenie);
            return dataWeenie;
        }

        [TestMethod]
        public void SaveWeenie_NoChanges_HasNoDbChanges()
        {
            // setup guarantees it exists
            DataObject weenie = _worldDb.GetWeenie(TestWeenieId);

            // save to force it to go through the update logic
            _worldDb.SaveWeenie(weenie);

            // fetch again for validation
            weenie = _worldDb.GetWeenie(TestWeenieId);

            Assert.IsTrue(weenie.IntProperties.Count >= 2);
            Assert.IsTrue(weenie.Int64Properties.Count >= 2);
            Assert.IsTrue(weenie.StringProperties.Count >= 2);
            Assert.IsTrue(weenie.DoubleProperties.Count >= 2);
            Assert.IsTrue(weenie.DataIdProperties.Count >= 2);
            Assert.IsTrue(weenie.InstanceIdProperties.Count >= 2);
            Assert.IsTrue(weenie.BoolProperties.Count >= 2);
        }
    }
}

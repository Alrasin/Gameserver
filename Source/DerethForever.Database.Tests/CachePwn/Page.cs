/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;

namespace DerethForever.Database.Tests.CachePwn
{
    public class Page
    {
        [JsonProperty("authorAccount")]
        public string AuthorAccount { get; set; }

        [JsonProperty("authorId")]
        public uint? AuthorId { get; set; }

        [JsonProperty("authorName")]
        public string AuthorName { get; set; }

        [JsonProperty("ignoreAuthor")]
        public byte? IgnoreAutor_Binder { get; set; }

        public bool? IgnoreAuthor
        {
            get { return ((IgnoreAutor_Binder == null) ? (bool?)null : (IgnoreAutor_Binder.Value == 0 ? true : false)); }
            set { IgnoreAutor_Binder = (value == null) ? (byte?)null : (value.Value ? (byte)0 : (byte)1); }
        }

        [JsonProperty("pageText")]
        public string PageText { get; set; }
    }
}

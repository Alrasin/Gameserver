/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.Reflection;

namespace DerethForever.Database.Tests
{
    public class WeenieJson
    {
        public string Label { get; set; }

        public uint Timestamp { get; set; }

        public uint WeenieType { get; set; }

        public uint WCID { get; set; }

        public Dictionary<string, int> IntValues { get; set; }

        public Dictionary<string, bool> BoolValues { get; set; }

        public Dictionary<string, double> DoubleValues { get; set; }

        public Dictionary<string, string> StringValues { get; set; }

        public Dictionary<string, uint> DIDValues { get; set; }

        public Dictionary<string, float> SpellCastingProbability { get; set; }

        public uint[] EventFilters { get; set; }

        public Dictionary<string, PositionJson> PositionValues { get; set; }

        public AttributesJson Attributes { get; set; }

        public Dictionary<string, BodyPartJson> BodyParts { get; set; }

        public Dictionary<string, List<EmoteJson>> Emotes { get; set; }

        public List<CreateItemJson> CreateList { get; set; }

        public List<GeneratorJson> Generators { get; set; }

        public Extra1Json Extra1 { get; set; }

        public List<ExtraItem3Json> Extra2Items { get; set; }

        public List<ExtraItem2Json> Extra3Items { get; set; }
    }

    public class EmoteJson
    {
        public uint Category { get; set; }

        public uint EmoteCategoryId
        {
            get { return Category; }
        }

        public float Probability { get; set; }

        public uint? ClassID { get; set; }

        public uint? VendorType { get; set; }

        public uint? ClassId
        {
            get { return ClassID; }
        }

        public string Quest { get; set; }

        public uint? Style { get; set; }

        public uint? Substyle { get; set; }

        public float? MinHealth { get; set; }

        public float? MaxHealth { get; set; }

        public List<EmoteActionJson> EmoteActions { get; set; }
    }

    public class EmoteActionJson
    {
        public uint Type { get; set; }

        public uint EmoteTypeId
        {
            get { return Type; }
        }

        public float Delay { get; set; }

        public float Extent { get; set; }

        public string Message { get; set; } = "";

        public uint? Min { get; set; }

        public uint? Minimum
        {
            get { return Min; }
        }

        public uint? Max { get; set; }

        public uint? Maximum
        {
            get { return Max; }
        }

        public uint? WealthRating { get; set; }

        public uint? TreasureClass { get; set; }

        public uint? TreasureType { get; set; }

        public uint? Stat { get; set; }

        public long? Min64 { get; set; }

        public long? Minimum64
        {
            get { return Min64; }
        }

        public long? Max64 { get; set; }

        public long? Maximum64
        {
            get { return Max64; }
        }

        public long? Display { get; set; }

        public double? Percent { get; set; }

        public ItemJson Item { get; set; }

        public ulong? Amount64 { get; set; }

        public uint? Amount { get; set; }

        public ulong? HeroXp64 { get; set; }

        public int? Motion { get; set; }

        public uint? SpellId { get; set; }

        public uint? Sound { get; set; }

        public string TestString { get; set; } = "";

        public uint? PScript { get; set; }

        public uint? PhysicsScript
        {
            get { return PScript; }
        }

        public PositionJson Frame { get; set; }
    }

    public class ItemJson
    {
        public uint WCID { get; set; }

        public uint? CP_WeenieClassId
        {
            get { return WCID; }
        }

        public uint? Palette { get; set; }

        public uint? CP_Palette
        {
            get { return Palette; }
        }

        public float? Shade { get; set; }

        public float? CP_Shade
        {
            get { return Shade; }
        }

        public uint? Destination { get; set; }

        public uint? CP_Destination
        {
            get { return Destination; }
        }

        public int? StackSize { get; set; }

        public int? CP_StackSize
        {
            get { return StackSize; }
        }

        public bool? TryToBond { get; set; }

        public bool? CP_TryToBond
        {
            get { return TryToBond; }
        }
    }
    public class CreateItemJson
    {
        public long WCID { get; set; }

        public uint? WeenieClassId
        {
            get { return (uint?)WCID; }
        }

        public uint? Palette { get; set; }

        public float? Shade { get; set; }

        public uint? Destination { get; set; }

        public int? StackSize { get; set; }

        public bool? TryToBond { get; set; }
    }

    public class BodyPartJson
    {
        public long DType { get; set; }

        public long DVal { get; set; }

        public double DVar { get; set; }

        public ArmorValuesJson ArmorValues { get; set; }

        public long BH { get; set; }

        public ZonesJson SD { get; set; }
    }

    public class ZonesJson
    {
        public long HLF { get; set; }

        public long MLF { get; set; }

        public double LLF { get; set; }

        public long HRF { get; set; }

        public long MRF { get; set; }

        public double LRF { get; set; }

        public long HLB { get; set; }

        public long MLB { get; set; }

        public double LLB { get; set; }

        public long HRB { get; set; }

        public long MRB { get; set; }

        public double LRB { get; set; }
    }

    public class ArmorValuesJson
    {
        public long BaseArmor { get; set; }

        public long ArmorVsSlash { get; set; }

        public long ArmorVsPierce { get; set; }

        public long ArmorVsBludgeon { get; set; }

        public long ArmorVsCold { get; set; }

        public long ArmorVsFire { get; set; }

        public long ArmorVsAcid { get; set; }

        public long ArmorVsElectric { get; set; }

        public long ArmorVsNether { get; set; }
    }

    public class AttributesJson
    {
        public AttributeJson Strength { get; set; }

        public AttributeJson Endurance { get; set; }

        public AttributeJson Quickness { get; set; }

        public AttributeJson Coordination { get; set; }

        public AttributeJson Focus { get; set; }

        public AttributeJson Self { get; set; }

        public AttributeJson Health { get; set; }

        public AttributeJson Stamina { get; set; }

        public AttributeJson Mana { get; set; }
    }

    public class AttributeJson
    {
        public long? Current { get; set; }

        public long a { get; set; }

        public long InitLevel { get; set; }

        public long b { get; set; }
    }

    public class GeneratorJson
    {
        public double Probability { get; set; }

        public long Type { get; set; }

        public long Delay { get; set; }

        public long InitCreate { get; set; }

        public long MaxNum { get; set; }

        public long WhenCreate { get; set; }

        public long WhereCreate { get; set; }

        public long StackSize { get; set; }

        public long Shade { get; set; }

        public long PalleteTypeId { get; set; }

        public PositionJson Position { get; set; }

        public long Slot { get; set; }
    }

    public class PositionJson
    {
        public long ObjCellID { get; set; }

        public OriginJson Origin { get; set; }

        public AnglesJson Angles { get; set; }
    }

    public class OriginJson
    {
        public float X { get; set; }

        public float Y { get; set; }

        public long Z { get; set; }
    }

    public class AnglesJson
    {
        public long W { get; set; }

        public long X { get; set; }

        public long Y { get; set; }

        public long Z { get; set; }
    }

    public class Extra1Json
    {
        public uint unknown { get; set; }

        public List<ExtraItem3Json> Items { get; set; }
    }

    public class ExtraItem3Json
    {
        public uint Component1 { get; set; }

        public uint Component2 { get; set; }

        public uint Component3 { get; set; }
    }

    public class ExtraItem2Json
    {
        public uint Component1 { get; set; }

        public uint Component2 { get; set; }
    }

    public class PropertyCopier<TParent, TChild> where TParent : class where TChild : class
    {
        public static void Copy(TParent parent, TChild child)
        {
            PropertyInfo[] parentProperties = parent.GetType().GetProperties();
            PropertyInfo[] childProperties = child.GetType().GetProperties();
            foreach (PropertyInfo parentProperty in parentProperties)
            {
                foreach (PropertyInfo childProperty in childProperties)
                {
                    if (parentProperty.Name == childProperty.Name &&
                        parentProperty.PropertyType == childProperty.PropertyType)
                    {
                        childProperty.SetValue(child, parentProperty.GetValue(parent));
                        break;
                    }
                }
            }
        }
    }
}

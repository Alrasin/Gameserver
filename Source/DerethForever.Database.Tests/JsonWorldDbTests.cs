/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using DerethForever.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DerethForever.Database.Tests
{
    [TestClass]
    public class JsonWorldDbTests
    {
        private static WorldJsonDatabase db = null;
        private static WeenieObjectInstance incompleteTestObject = null;
        // This landblock seemed to have alot of instance objects, please change it too a test landblock!
        private static ushort testLandblockId = (ushort)0x00E5;

        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            // copy config.json
            File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"),
                ".\\Config.json", true);

            ConfigManager.Initialize();
            db = new WorldJsonDatabase(ConfigManager.Config.Database.World.Folder);

            incompleteTestObject = new WeenieObjectInstance()
            {
                Landblock = 0,
                WeenieClassId = 7096,
                LandblockRaw = 15009492,
                PositionX = (float)67.6808,
                PositionY = (float)-277.741,
                PositionZ = (float)-6.20983,
                PreassignedGuid = 1879999999,                
                RotationX = 0,
                RotationY = 0,
                RotationZ = 0,
                RotationW = 0
            };
        }

        [TestMethod]
        public void SpawnMaps_UpdateInstance()
        {
            db.UpdateInstance(testLandblockId, incompleteTestObject);
            db.RemoveInstance(testLandblockId, incompleteTestObject);
        }

        [TestMethod]
        public void SpawnMaps_Searches()
        {
            SearchSpawnMapsCriteria testSearch = new SearchSpawnMapsCriteria() { PartialDungeonName = "Keep" };
            var results = db.SearchSpawnMaps(testSearch);
            testSearch = new SearchSpawnMapsCriteria() { WeenieClassId = 7096 };
            results = db.SearchSpawnMaps(testSearch);
            testSearch = new SearchSpawnMapsCriteria() { InstanceId = 1879999999 };
            results = db.SearchSpawnMaps(testSearch);
        }

        [TestMethod]
        public void CutWorldRelease()
        {
            db.CutWorldRelease(ReleaseType.Partial);
        }
    }
}

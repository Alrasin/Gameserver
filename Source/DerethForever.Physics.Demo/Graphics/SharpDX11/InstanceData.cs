﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using BulletSharp;
using System.Runtime.InteropServices;

namespace DerethForever.Physics.Demo.SharpDX11
{
    [StructLayout(LayoutKind.Sequential)]
    public struct InstanceData
    {
        public Matrix WorldTransform;
        public uint Color;

        public static readonly int SizeInBytes = Marshal.SizeOf(typeof(InstanceData));
    }

    public class InstanceDataList
    {
        private const int InitialCapacity = 16;
        private int _nextIndex;
        private InstanceData[] _array;
        private int _capacityAtLastClear = 1;

        public InstanceData[] Array => _array;
        public int Count => _nextIndex;

        public InstanceDataList()
        {
            _array = new InstanceData[InitialCapacity];
        }

        public void Add(ref Matrix transform, uint color)
        {
            EnsureCapacity();
            _array[_nextIndex].WorldTransform = transform;
            _array[_nextIndex].Color = color;
            _nextIndex++;
        }

        public void Add(uint color)
        {
            EnsureCapacity();
            _array[_nextIndex].WorldTransform = Matrix.Identity;
            _array[_nextIndex].Color = color;
            _nextIndex++;
        }

        public void Clear()
        {
            // If array was filled to less than half,
            // then allocate half the memory.
            if (_nextIndex < (_capacityAtLastClear >> 1) && _array.Length > InitialCapacity)
            {
                System.Array.Resize(ref _array, _array.Length >> 1);
            }

            _nextIndex = 0;
            _capacityAtLastClear = _array.Length;
        }

        private void EnsureCapacity()
        {
            if (_array.Length <= _nextIndex)
            {
                System.Array.Resize(ref _array, _array.Length << 1);
            }
        }
    }
}

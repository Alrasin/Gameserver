/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace DerethForever.Physics.Demo
{
    public class GraphicsLibraryManager
    {
        static LibrarySelection librarySelection;

        const string SettingsFilename = "settings.xml";

        public static string GraphicsLibraryName { get; set; }

        public static bool ExitWithReload { get; set; }

        static string[] supportedLibraries = {
            "SharpDX (DirectX 11)", "SharpDX (DirectX 10)", "SlimDX (DirectX 9)", "OpenTK (OpenGL)", "MonoGame (OpenGL)" };
        public static string[] GetSupportedLibraries()
        {
            return supportedLibraries;
        }

        public static bool IsLibraryAvailable(string library)
        {
            return library == "SharpDX (DirectX 11)";
                // assemblyNames = new[] { "DerethForever.Physics.Demo.SharpDX11", "SharpDX", "SharpDX.Direct3D11", "SharpDX.Direct3D11.Effects", "SharpDX.DXGI", "SharpDX.D3DCompiler" };
            //    return true;
            //else
            //    return false;

            //    case "SharpDX (DirectX 10)":
            //        assemblyNames = new[] { "DerethForever.Physics.Demo.SharpDX", "SharpDX", "SharpDX.Direct3D10", "SharpDX.DXGI", "SharpDX.D3DCompiler" };
            //        break;
            //    case "SlimDX (DirectX 9)":
            //        assemblyNames = new[] { "DerethForever.Physics.Demo.SlimDX", "SlimDX, Version=4.0.13.43, Culture=neutral, PublicKeyToken=b1b0c32fd1ffe4f9" };
            //        break;
            //    case "OpenTK (OpenGL)":
            //        assemblyNames = new[] { "DerethForever.Physics.Demo.OpenTK", "OpenTK" };
            //        break;
            //    case "MonoGame (OpenGL)":
            //        assemblyNames = new[] { "DerethForever.Physics.Demo.MonoGame" };
            //        break;
            //    default:
            //        return false;
            //}

            //try
            //{
            //    foreach (string assemblyName in assemblyNames)
            //    {
            //        Assembly.Load(assemblyName);
            //    }
            //}
            //catch (Exception)
            //{
            //    return false;
            //}

            //return true;
        }

        public static Graphics GetGraphics(DerethDemo demo)
        {
            //Type graphicsType;
            //Assembly assembly;
            //switch (GraphicsLibraryName)
            //{
            //    case "SharpDX (DirectX 11)":
            //        assembly = Assembly.Load("DerethForever.Physics.Demo.SharpDX11");
            //        graphicsType = typeof(SharpDX11.SharpDX11Graphics); // assembly.GetType("DerethForever.Physics.Demo.SharpDX11.SharpDX11Graphics");
            //        break;
            //    case "SharpDX (DirectX 10)":
            //        assembly = Assembly.Load("DerethForever.Physics.Demo.SharpDX");
            //        graphicsType = assembly.GetType("DerethForever.Physics.Demo.SharpDX.SharpDXGraphics");
            //        break;
            //    case "SlimDX (DirectX 9)":
            //        assembly = Assembly.Load("DerethForever.Physics.Demo.SlimDX");
            //        graphicsType = assembly.GetType("DerethForever.Physics.Demo.SlimDX.SlimDXGraphics");
            //        break;
            //    case "OpenTK (OpenGL)":
            //        assembly = Assembly.Load("DerethForever.Physics.Demo.OpenTK");
            //        graphicsType = assembly.GetType("DerethForever.Physics.Demo.OpenTK.OpenTKGraphics");
            //        break;
            //    case "MonoGame (OpenGL)":
            //        assembly = Assembly.Load("DerethForever.Physics.Demo.MonoGame");
            //        graphicsType = assembly.GetType("DerethForever.Physics.Demo.MonoGame.MonoGameGraphics");
            //        break;
            //    default:
            //        return null;
            //}
            SharpDX11.SharpDX11Graphics graphics = new SharpDX11.SharpDX11Graphics(demo);
            return graphics; // graphicsType.InvokeMember(null, BindingFlags.CreateInstance, null, null, new[] { demo }) as Graphics;
        }

        static XmlElement GetSettingsDocumentRoot()
        {
            XmlDocument settings = new XmlDocument();
            XmlElement root;
            try
            {
                settings.Load(SettingsFilename);
                root = settings.DocumentElement;
            }
            catch (FileNotFoundException)
            {
                // Create an empty settings file
                root = settings.CreateElement("settings");
                settings.AppendChild(root);
                settings.Save(SettingsFilename);
            }

            return root;
        }

        public static void Run(DerethDemo demo)
        {
            Application.EnableVisualStyles();

            // Check if BulletSharp exists
            try
            {
                Assembly.Load("BulletSharp");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "BulletSharp Error!");
                return;
            }


            // Load settings
            XmlElement root = GetSettingsDocumentRoot();

            XmlNodeList l = root.GetElementsByTagName("graphicsframework");
            if (l.Count == 1)
            {
                XmlElement graphics = l.Item(0) as XmlElement;
                GraphicsLibraryName = graphics.GetAttribute("value");
            }

            if (GraphicsLibraryName == null)
            {
                if (!SelectLibrary())
                {
                    return;
                }
            }

            demo.Run();
            while (ExitWithReload)
            {
                ExitWithReload = false;
                if (!SelectLibrary())
                {
                    return;
                }
                demo.Run();
            }
        }

        public static bool SelectLibrary()
        {
            librarySelection = new LibrarySelection();
            librarySelection.ShowDialog();
            librarySelection.Dispose();
            librarySelection = null;
            return GraphicsLibraryName != null;
        }

        // Called by Graphics to indicate that the library was loaded successfully
        public static void LibraryStarted()
        {
            XmlElement root = GetSettingsDocumentRoot();

            XmlElement graphics;
            XmlNodeList l = root.GetElementsByTagName("graphicsframework");
            if (l.Count == 1)
            {
                graphics = l.Item(0) as XmlElement;
            }
            else
            {
                graphics = root.OwnerDocument.CreateElement("graphicsframework");
            }

            graphics.SetAttribute("value", GraphicsLibraryName);
            root.AppendChild(graphics);
            root.OwnerDocument.Save(SettingsFilename);
        }
    }
}

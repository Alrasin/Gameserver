/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using BulletSharp;
using DerethForever.DatLoader;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Physics.Demo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Physics
{
    public class LandblockDemo : DerethDemo
    {
        private const float _offsetXZ = -24480f;
        private const float _offsetY = -500;
        private const int _vertsPerBlockXZ = 9;
        private const float _disancePerVertex = 24f;

        // create 125 (5x5x5) dynamic objects
        const int ArraySizeX = 5, ArraySizeY = 5, ArraySizeZ = 5;
        Vector3 startPosition = new Vector3(0, 2, 0);

        private uint _landblockId;

        public LandblockDemo(uint landblockId)
        {
            // lifestone = 0x20002EE
            _landblockId = landblockId;
        }

        protected override void OnInitialize()
        {
            Freelook.Eye = new Vector3(0, 25, 0);
            Freelook.Target = new Vector3(10, 5, 10);

            Graphics.SetFormText("Dereth Forever - Bullet Physics Demo");
        }

        protected override void OnInitializePhysics()
        {
            // collision configuration contains default setup for memory, collision setup
            CollisionConf = new DefaultCollisionConfiguration();
            Dispatcher = new CollisionDispatcher(CollisionConf);

            Broadphase = new DbvtBroadphase();

            World = new DiscreteDynamicsWorld(Dispatcher, Broadphase, null, CollisionConf);
            World.Gravity = new Vector3(0, -9.81f, 0);

            //CreateGround();
            // LoadObject(_singleObject, _objectName);

            LoadLandlock();
        }
        
        private void LoadObject(uint objectId, Entity.Position offset)
        {
            Quaternion placement = new Quaternion(offset.RotationX, -offset.RotationZ, offset.RotationY, offset.RotationW);
            var fullTransform = Matrix.RotationQuaternion(placement) * Matrix.Translation(offset.PositionX - 96, offset.PositionZ, offset.PositionY - 96);

            if (objectId > 0x02000000)
            {
                SetupModel model = SetupModel.ReadFromDat(objectId); // lifestone

                // model.spheres and model.cylspheres are the physics objects
                // model.parts has the rendered objects, which are also the physics if spheres and cylspheres are empty
                CompoundShape fullObject = new CompoundShape();

                for (int i = 0; i < model.Parts.Count; i++)
                {
                    var part = GfxObj.ReadFromDat(model.Parts[i]);
                    BvhTriangleMeshShape shape = GetShape(part);

                    if (shape == null)
                        continue;

                    float x = model.PlacementFrames[0].AnimFrame.Locations[i].PositionX;
                    float y = model.PlacementFrames[0].AnimFrame.Locations[i].PositionZ;
                    float z = model.PlacementFrames[0].AnimFrame.Locations[i].PositionY;

                    float qx = model.PlacementFrames[0].AnimFrame.Locations[i].RotationX;
                    float qy = -model.PlacementFrames[0].AnimFrame.Locations[i].RotationZ;
                    float qz = model.PlacementFrames[0].AnimFrame.Locations[i].RotationY;
                    float qw = model.PlacementFrames[0].AnimFrame.Locations[i].RotationW;

                    fullObject.AddChildShape(Matrix.RotationQuaternion(new Quaternion(qx, qy, qz, qw)) * Matrix.Translation(x, y, z), shape);
                }
                
                LocalCreateRigidBody(0f, fullTransform, fullObject, CollisionFlags.StaticObject);
                CollisionShapes.Add(fullObject);
            }
            else
            {
                var part = GfxObj.ReadFromDat(objectId);

                var shape = GetShape(part);
                if (shape == null)
                    return;
                
                LocalCreateRigidBody(0f, fullTransform, shape, CollisionFlags.StaticObject);
                CollisionShapes.Add(shape);
            }
        }

        public BvhTriangleMeshShape GetShape(IPhysicsPart part)
        {
            Vector3[] vertices = new Vector3[part.VertexArray.Vertices.Count];
            for (short v = 0; v < part.VertexArray.Vertices.Count; v++)
            {
                vertices[v] = new Vector3(part.VertexArray.Vertices[v].X, part.VertexArray.Vertices[v].Z, part.VertexArray.Vertices[v].Y);

            }

            var trimesh = new TriangleMesh();

            if (part.PhysicsPolygons?.Count > 0)
            {
                for (ushort p = 0; p < part.PhysicsPolygons.Count; p++)
                {
                    var poly = part.PhysicsPolygons[p];

                    // create trifans from the polygon
                    for (ushort pt = 3; pt <= poly.NumPts; pt++)
                    {
                        Vector3 vertex0 = vertices[poly.VertexIds[0]];
                        Vector3 vertex1 = vertices[poly.VertexIds[pt - 1]];
                        Vector3 vertex2 = vertices[poly.VertexIds[pt - 2]];

                        if (poly.NegSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex1, ref vertex2);

                        if (poly.PosSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex2, ref vertex1);
                    }
                }

            }
            else if (part.Polygons?.Count > 0)
            {
                for (ushort p = 0; p < part.Polygons.Count; p++)
                {
                    var poly = part.Polygons[p];

                    // create trifans from the polygon
                    for (ushort pt = 3; pt <= poly.NumPts; pt++)
                    {
                        Vector3 vertex0 = vertices[poly.VertexIds[0]];
                        Vector3 vertex1 = vertices[poly.VertexIds[pt - 1]];
                        Vector3 vertex2 = vertices[poly.VertexIds[pt - 2]];

                        if (poly.NegSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex1, ref vertex2);

                        if (poly.PosSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex2, ref vertex1);
                    }
                }
            }

            if (trimesh.NumTriangles > 0)
            {
                BvhTriangleMeshShape shape = new BvhTriangleMeshShape(trimesh, true);

                return shape;
            }

            return null;
        }

        public void LoadLandlock()
        {
            // not a whole lot to do here - we should probably just load on demand
            // we'll load rithwic as a proof of concept though (C98C)
            

            var info = CLandblockInfo.ReadFromDat(_landblockId);
            var cellInfo = CellLandblock.ReadFromDat(_landblockId);

            GenerateBvhGround(cellInfo);

            // info will have the cell count.  theory: this applies to dungeons only.  skip for now
            for (uint i = 0; i < info.NumCells; i++)
            {
                uint cellId = _landblockId + 0x0100 + i;

                // cells are loaded via EnvCell.  the tangled web continues
                if (!DatManager.CellDat.AllFiles.ContainsKey(cellId))
                    break;

                var cell = EnvCell.ReadFromDat(cellId);

                // get the cell placement + rotations
                Quaternion cellPlacement = new Quaternion(cell.Position.RotationX, -cell.Position.RotationZ, cell.Position.RotationY, cell.Position.RotationW);
                var cellTransform = Matrix.RotationQuaternion(cellPlacement) * Matrix.Translation(cell.Position.PositionX - 96, cell.Position.PositionZ, cell.Position.PositionY - 96);

                DatLoader.FileTypes.Environment env = DatLoader.FileTypes.Environment.ReadFromDat(cell.EnvironmentId);
                var cellKeys = env.Cells.Keys.ToArray();

                CompoundShape cellShape = new CompoundShape();

                // these are things like the floors of upstairs/downstairs in a building
                for (uint j = 0; j < env.Cells.Count; j++)
                {
                    var envCell = env.Cells[cellKeys[j]];
                    var shape = GetShape(envCell);

                    cellShape.AddChildShape(Matrix.Identity, shape);

                    LocalCreateRigidBody(0f, cellTransform, cellShape, CollisionFlags.StaticObject);
                    CollisionShapes.Add(cellShape);
                }

                // these are re-usable objects just like appear on the landblock at large
                for (int j = 0; j < cell.StabList.Count; j++)
                {
                    var stab = cell.StabList[j];
                    LoadObject(stab.Model, stab.Position);
                }
            }

            for (int b = 0; b < info.Buildings.Count; b++)
            {
                var building = info.Buildings[b];

                LoadObject(building.ModelId, building.Frame);                
            }

            for (int i = 0; i < info.ObjectIds.Count; i++)
            {
                uint objectId = info.ObjectIds[i];

                LoadObject(objectId, info.ObjectFrames[i]);
            }

            // load scenery
            for (uint x = 0; x < 9; x++)
            {
                for (uint y = 0; y < 9; y++)
                {
                    uint rawTerrain = cellInfo.Terrain[x * 9 + y];
                    uint terrainType = (rawTerrain >> 2) & 0x1f;         // copy pasta from others.  not sure why this is correct
                    uint terrainSceneType = (rawTerrain >> 11) & 0x1f;   // copy pasta from others.  not sure why this is correct


                }
            }
        }
        
        private void GenerateBvhGround(CellLandblock cellInfo)
        {
            int vertStride = Vector3.SizeInBytes;
            int indexStride = 3 * sizeof(int);
            const int totalVerts = _vertsPerBlockXZ * _vertsPerBlockXZ;
            const int totalTriangles = 2 * (_vertsPerBlockXZ - 1) * (_vertsPerBlockXZ - 1);

            TriangleIndexVertexArray vertexArray = new TriangleIndexVertexArray();
            IndexedMesh mesh = new IndexedMesh();
            mesh.Allocate(totalTriangles, totalVerts, indexStride, vertStride, PhyScalarType.Int32, PhyScalarType.Single);

            using (var data = mesh.LockVerts())
            {
                for (int i = 0; i < _vertsPerBlockXZ; i++)
                {
                    for (int j = 0; j < _vertsPerBlockXZ; j++)
                    {
                        float height = cellInfo.HeightMap[i * 9 + j];

                        data.Write((i - 4f) * 24);
                        data.Write(height * 2);
                        data.Write((j - 4f) * 24);

                        //data.Write(i * 24 - 96);
                        //data.Write(height);
                        //data.Write(j * 24 - 96);
                    }
                }
            }

            int index = 0;
            IntArray idata = mesh.TriangleIndices;
            for (int i = 0; i < _vertsPerBlockXZ - 1; i++)
            {
                for (int j = 0; j < _vertsPerBlockXZ - 1; j++)
                {
                    idata[index++] = j * _vertsPerBlockXZ + i;
                    idata[index++] = j * _vertsPerBlockXZ + i + 1;
                    idata[index++] = (j + 1) * _vertsPerBlockXZ + i + 1;

                    idata[index++] = j * _vertsPerBlockXZ + i;
                    idata[index++] = (j + 1) * _vertsPerBlockXZ + i + 1;
                    idata[index++] = (j + 1) * _vertsPerBlockXZ + i;
                }
            }

            vertexArray.AddIndexedMesh(mesh);
            CollisionShape groundShape = new BvhTriangleMeshShape(vertexArray, true);

            // Collection.Add(groundShape);

            //create ground object
            RigidBody ground = LocalCreateRigidBody(0, Matrix.Identity, groundShape, CollisionFlags.StaticObject);
            ground.UserObject = "Ground";

        }
    }
}

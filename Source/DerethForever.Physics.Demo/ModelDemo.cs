/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using BulletSharp;
using DerethForever.DatLoader;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Physics.Demo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Physics
{
    public class ModelDemo : DerethDemo
    {
        private const float _offsetXZ = -24480f;
        private const float _offsetY = -500;
        private const int _vertsPerBlockXZ = 9;
        private const float _disancePerVertex = 24f;

        // create 125 (5x5x5) dynamic objects
        const int ArraySizeX = 5, ArraySizeY = 5, ArraySizeZ = 5;
        Vector3 startPosition = new Vector3(5, 2, 0);

        private uint _modelId;

        public ModelDemo(uint modelId)
        {
            _modelId = modelId;
        }

        protected override void OnInitialize()
        {
            Freelook.Eye = new Vector3(0, 3, 5);
            Freelook.Target = new Vector3(0, 2, 0);

            Graphics.SetFormText("Dereth Forever - Bullet Physics Demo");
        }

        protected override void OnInitializePhysics()
        {
            // collision configuration contains default setup for memory, collision setup
            CollisionConf = new DefaultCollisionConfiguration();
            Dispatcher = new CollisionDispatcher(CollisionConf);

            Broadphase = new DbvtBroadphase();

            World = new DiscreteDynamicsWorld(Dispatcher, Broadphase, null, CollisionConf);
            World.Gravity = new Vector3(0, -9.81f, 0);

            // GenerateBvhGround();
            LoadObject(_modelId, new Entity.Position());
        }

        private void LoadObject(uint objectId, Entity.Position offset)
        {
            Quaternion placement = new Quaternion(offset.RotationX, -offset.RotationZ, offset.RotationY, offset.RotationW);
            var fullTransform = Matrix.RotationQuaternion(placement) * Matrix.Translation(offset.PositionX, offset.PositionZ, offset.PositionY);

            if (objectId > 0x02000000)
            {
                SetupModel model = SetupModel.ReadFromDat(objectId); // lifestone

                // model.spheres and model.cylspheres are the physics objects
                // model.parts has the rendered objects, which are also the physics if spheres and cylspheres are empty
                CompoundShape fullObject = new CompoundShape();

                for (int i = 0; i < model.Parts.Count; i++)
                {
                    BvhTriangleMeshShape shape = GetPart(model.Parts[i]);

                    if (shape == null)
                        continue;

                    float x = model.PlacementFrames[0].AnimFrame.Locations[i].PositionX;
                    float y = model.PlacementFrames[0].AnimFrame.Locations[i].PositionZ;
                    float z = model.PlacementFrames[0].AnimFrame.Locations[i].PositionY;

                    float qx = model.PlacementFrames[0].AnimFrame.Locations[i].RotationX;
                    float qy = -model.PlacementFrames[0].AnimFrame.Locations[i].RotationZ;
                    float qz = model.PlacementFrames[0].AnimFrame.Locations[i].RotationY;
                    float qw = model.PlacementFrames[0].AnimFrame.Locations[i].RotationW;

                    fullObject.AddChildShape(Matrix.RotationQuaternion(new Quaternion(qx, qy, qz, qw)) * Matrix.Translation(x, y, z), shape);
                }

                LocalCreateRigidBody(0f, fullTransform, fullObject, CollisionFlags.StaticObject);
                CollisionShapes.Add(fullObject);
            }
            else
            {
                var part = GetPart(objectId);
                if (part == null)
                    return;

                LocalCreateRigidBody(0f, fullTransform, part, CollisionFlags.StaticObject);
                CollisionShapes.Add(part);
            }
        }

        public BvhTriangleMeshShape GetPart(uint partId)
        {
            var part = GfxObj.ReadFromDat(partId);

            Vector3[] vertices = new Vector3[part.VertexArray.Vertices.Count];
            for (short v = 0; v < part.VertexArray.Vertices.Count; v++)
            {
                vertices[v] = new Vector3(part.VertexArray.Vertices[v].X, part.VertexArray.Vertices[v].Z, part.VertexArray.Vertices[v].Y);

            }

            var trimesh = new TriangleMesh();

            if (part.PhysicsPolygons?.Count > 0)
            {
                for (ushort p = 0; p < part.PhysicsPolygons.Count; p++)
                {
                    var poly = part.PhysicsPolygons[p];

                    // create trifans from the polygon
                    for (ushort pt = 3; pt <= poly.NumPts; pt++)
                    {
                        Vector3 vertex0 = vertices[poly.VertexIds[0]];
                        Vector3 vertex1 = vertices[poly.VertexIds[pt - 1]];
                        Vector3 vertex2 = vertices[poly.VertexIds[pt - 2]];

                        if (poly.NegSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex1, ref vertex2);

                        if (poly.PosSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex2, ref vertex1);
                    }
                }

            }
            else if (part.Polygons?.Count > 0)
            {
                for (ushort p = 0; p < part.Polygons.Count; p++)
                {
                    var poly = part.Polygons[p];

                    // create trifans from the polygon
                    for (ushort pt = 3; pt <= poly.NumPts; pt++)
                    {
                        Vector3 vertex0 = vertices[poly.VertexIds[0]];
                        Vector3 vertex1 = vertices[poly.VertexIds[pt - 1]];
                        Vector3 vertex2 = vertices[poly.VertexIds[pt - 2]];

                        if (poly.NegSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex1, ref vertex2);

                        if (poly.PosSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex2, ref vertex1);
                    }
                }
            }

            if (trimesh.NumTriangles > 0)
            {
                BvhTriangleMeshShape shape = new BvhTriangleMeshShape(trimesh, true);

                return shape;
            }

            return null;
        }
    }
}

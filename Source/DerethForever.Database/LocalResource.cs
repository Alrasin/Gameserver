/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.Common;
using System.IO;

namespace DerethForever.Database
{
    public class LocalResource : IRedeploySource
    {
        /// <summary>
        /// Scoures a directory and finds relevante data on the local system that originated from a remote API.
        /// </summary>
        /// <param name="directoryPath"></param>
        private List<RemoteResource> ParseLocalDatabase(string directoryPath)
        {
            List<RemoteResource> resources = null;

            var actualPath = Path.GetFullPath(Path.Combine(directoryPath));

            if (Directory.Exists(actualPath))
            {
                // Instance the new object
                resources = new List<RemoteResource>();

                var files = from file in Directory.EnumerateFiles(actualPath, "*.*", SearchOption.AllDirectories) where !file.Contains(".txt") select new { File = file };

                foreach (var file in files)
                {
                    var path = Path.GetDirectoryName(file.File);
                    var searchPath = path.Replace(actualPath, string.Empty).Replace(@"\", @"/");
                    var fileName = Path.GetFileName(file.File);
                    var item = Redeploy.GetDatabaseNameAndResourceType(searchPath, fileName);
                    if (item.Item1.Length > 0)
                    {
                        resources.Add(new RemoteResource() { FileName = fileName, FilePath = file.File, SourcePath = searchPath, SourceUri = file.File, DatabaseName = item.Item1, Type = item.Item2 });
                    }
                }
            }

            return resources;
        }

        /// <summary>
        /// Get the data files from local disk.
        /// </summary>
        /// <returns></returns>
        public List<RemoteResource> SyncFileList()
        {
            var databaseFiles = ParseLocalDatabase(ConfigManager.Config.ContentServer.LocalContentPath);
            return databaseFiles;
        }
    }
}

/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.Common;
using log4net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Net;
using Newtonsoft.Json;

namespace DerethForever.Database
{
    public class GitlabResource : IRedeploySource
    {
        private ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<RemoteResource> SyncFileList()
        {
            log.Info("Downloading All Gitlab Content.");

            Redeploy.SetToken();
            var allDownloadedFiles = GetDatabaseScripts(ConfigManager.Config.ContentServer.GitlabDatabaseUrl);
            var worldDb = GetWorldJsonRelease();

            // Append to downloads
            if (worldDb != null)
            {
                allDownloadedFiles.Add(worldDb);
            }
            else
            {
                log.Error("Error downloading worldfile!");
                return null;
            }

            var contentDataPath = Path.GetFullPath(Path.Combine(ConfigManager.Config.ContentServer.LocalContentPath, "filehashes.json"));
            // Save download data to json?
            File.WriteAllText(contentDataPath, JsonConvert.SerializeObject(allDownloadedFiles));

            return allDownloadedFiles;
        }

        /// <summary>
        /// Downloads the database scripts from an API Endpoint
        /// </summary>
        private List<RemoteResource> GetDatabaseScripts(string url)
        {
            if (Redeploy.RemaingApiCalls > 0 || (DateTime.Now >= Redeploy.ApiResetTime.Value))
            {
                // Check to see if the input is usable and the data path is valid
                if (url?.Length > 0)
                {
                    List<RemoteResource> databaseScriptFiles = new List<RemoteResource>();
                    var contentFolderPath = Path.GetFullPath(ConfigManager.Config.ContentServer.LocalContentPath);

                    // Zip and retain backups in a seporate folder.
                    if (ConfigManager.Config.ContentServer.KeepDataBackups)
                    {
                        Redeploy.BackupContentFolder();
                    }

                    // static database folder translation
                    // All of the database scripts currently reside under this folder:
                    var lameDatabasePath = Path.Combine(contentFolderPath, "Database\\");

                    // Start fresh and wipe content database directory:
                    if (Directory.Exists(lameDatabasePath))
                        Redeploy.DeleteFolderContents(lameDatabasePath);

                    // Directories collected from the API
                    List<string> directoryUrls = new List<string>();
                    directoryUrls.Add(url);
                    // Recurse api and collect all downloads
                    while (directoryUrls.Count > 0)
                    {
                        var currentUrl = directoryUrls.LastOrDefault();
                        var content = RetrieveWebString(currentUrl);
                        var repoFiles = content != null ? JArray.Parse(content) : null;
                        if (repoFiles?.Count > 0)
                        {
                            foreach (var file in repoFiles)
                            {
                                var search = file["path"].ToString();
                                if (search.Contains("Database"))
                                {
                                    if (file["type"].ToString() == "tree")
                                    {
                                        // Fix the url from the path
                                        string newUrl = currentUrl.SubstringBefore('=') + "=" + search;
                                        directoryUrls.Add(newUrl);
                                        // directoryUrls.Add(file["url"].ToString());
                                        Redeploy.CheckLocalDataPath(Path.Combine(contentFolderPath, file["path"].ToString()));
                                    }
                                    else
                                    {
                                        var fileName = file["name"].ToString();
                                        var filePath = Path.GetFullPath(Path.Combine(contentFolderPath, file["path"].ToString()));
                                        var info = Redeploy.GetDatabaseNameAndResourceType(search, fileName);
                                        var databaseName = info.Item1;
                                        var localType = info.Item2;
                                        var fileUrl = currentUrl.SubstringBefore('=').Replace("tree?path", "files/") + WebUtility.UrlEncode(search) + "?ref=" + ConfigManager.Config.ContentServer.GitlabWorldReleaseBranch;
                                        // Change Source URL, remove download_url
                                        databaseScriptFiles.Add(new RemoteResource()
                                        {
                                            DatabaseName = databaseName,
                                            Type = localType,
                                            SourceUri = fileUrl,
                                            SourcePath = file["path"].ToString(),
                                            FileName = fileName,
                                            FilePath = filePath,
                                            Hash = file["id"].ToString()
                                        });
                                    }
                                }
                            }
                        }
                        // Cancel because the string was caught in exception
                        if (repoFiles == null)
                        {
                            log.Error($"No files found within {currentUrl}");
                            return null;
                        }
                        // Remove the parsed url
                        directoryUrls.Remove(currentUrl);
                    }
                    // Download the files from the downloads tuple
                    foreach (var download in databaseScriptFiles)
                    {
                        // If we cannot Retrieve content, return false for failure
                        if (!RetrieveWebContent(download.SourceUri, download.FilePath))
                            log.Error($"Trouble downloading {download.SourceUri} : {download.FilePath}");
                    }
                    return databaseScriptFiles;
                }
                log.Error($"Invalid Url provided, please check configuration.");
                return null;
            }

            log.Error($"You have exhausted your Remote Server API limt. Please wait till {Redeploy.ApiResetTime}");
            return null;
        }

        /// <summary>
        /// Custom Gitlab API File download function
        /// </summary>
        /// <param name="url">Url to API Endpoint</param>
        /// <param name="destinationFilePath">Local filestystem download destination</param>
        private bool RetrieveWebContent(string url, string destinationFilePath)
        {
            using (WebClient webClient = new WebClient())
            {
                WebClient w = new WebClient();
                if (Redeploy.CurrentToken?.Length > 0)
                {
                    w.Headers.Add($"PRIVATE-TOKEN: {Redeploy.CurrentToken}");
                }

                var folder = Path.GetDirectoryName(destinationFilePath);

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                else
                {
                    // Delete file if it already exists or rename path?
                    if (File.Exists(destinationFilePath))
                        File.Delete(destinationFilePath);
                }

                string contentData = string.Empty;
                // Get content localtion from server:
                try
                {
                    contentData = (string)JObject.Parse((w.DownloadString(url))).SelectToken("content");
                }
                catch (WebException error)
                {
                    // nothing
                    log.Error($"Error Downloading Content Data from {url}: {error.Message}");
                    Redeploy.ToggleDownloadFailure();
                    return false;
                }

                // write the content too file:
                File.WriteAllBytes(destinationFilePath, Convert.FromBase64String(contentData));

                if (File.Exists(destinationFilePath))
                    return true;
            }
            Redeploy.ToggleDownloadFailure();
            log.Error($"Troubles downloading {url} {destinationFilePath}");
            return false;
        }

        /// <summary>
        /// Retreieves a string from a web location.
        /// </summary>
        private string RetrieveWebString(string updateUrl)
        {
            using (WebClient webClient = new WebClient())
            {
                var result = string.Empty;
                WebClient w = new WebClient();
                try
                {
                    // User Agent
                    w.Headers.Add("User-Agent", Redeploy.ApiUserAgent);
                    if (Redeploy.CurrentToken?.Length > 0)
                    {
                        w.Headers.Add($"PRIVATE-TOKEN: {Redeploy.CurrentToken}");
                    }
                    result = w.DownloadString(updateUrl);
                }
                catch (WebException e)
                {
                    if (e.Response != null)
                    {
                        Redeploy.ToggleDownloadFailure();
                        Redeploy.CaptureWebHeaderData(e.Response.Headers);
                        if (e.Message.Contains("Unauthorized"))
                        {
                            log.Error($"Please check your Gitlab Token, in the server config: {e.Message}");
                        }
                        else
                        {
                            log.Error($"Could not download from the web: {e.Message}");
                        }
                        return null;
                    }
                    else
                        throw;
                }
                catch (Exception e)
                {
                    log.Error($"Error returned: {e.Message}");
                    return null;
                }
                Redeploy.CaptureWebHeaderData(w.ResponseHeaders);
                return (result);
            }
        }

        /// <summary>
        /// Retrieves the World Release from Gitlab.
        /// </summary>
        /// <remarks>This function assumes that the release archive is a zip file.</remarks>
        private RemoteResource GetWorldJsonRelease()
        {
            RemoteResource jsonReleaseFile = null;

            var contentFolderPath = Path.GetFullPath(ConfigManager.Config.ContentServer.LocalContentPath);

            try
            {
                using (WebClient webClient = new WebClient())
                {
                    WebClient w = new WebClient();

                    // Add token, if available
                    if (Redeploy.CurrentToken?.Length > 0)
                    {
                        w.Headers.Add($"PRIVATE-TOKEN: {Redeploy.CurrentToken}");
                    }

                    // Get the release info pointer, based on the config settings
                    string pointer = ConfigManager.Config.ContentServer.GitlabWorldArchivePointer + "?ref=" + ConfigManager.Config.ContentServer.GitlabWorldReleaseBranch;

                    // Parse the API info about the Current World Release Archive:
                    var releaseInfo = JObject.Parse(Redeploy.DecodeBase64String(JObject.Parse(w.DownloadString(pointer)).GetValue("content").ToString()));
                    // Extract data
                    var sourcePath = releaseInfo.GetValue("ArchivePath").ToString();
                    var contentHash = releaseInfo.GetValue("Hash").ToString();
                    // Validate file paths
                    var sourceUrl = ConfigManager.Config.ContentServer.GitlabWorldArchiveBaseUrl + sourcePath.Replace(@"\", "%2F").Replace(@"/", "%2F") + "?ref=" + ConfigManager.Config.ContentServer.GitlabWorldReleaseBranch;
                    var storedReleaseFileName = Path.GetFileName(sourcePath);
                    var storedContentPath = Path.Combine(contentFolderPath, storedReleaseFileName);
                    var download = false;

                    // Check hash against stored archive
                    if (File.Exists(storedContentPath))
                    {
                        // Get hash and compare
                        var localFileHash = Redeploy.CalculateFileChecksum(storedContentPath);
                        if (localFileHash != contentHash)
                        {
                            download = true;
                            // Hash is different, backup or delete
                            if (ConfigManager.Config.ContentServer.KeepDataBackups)
                            {
                                var backupPath = Path.GetFullPath(ConfigManager.Config.ContentServer.LocalBackupPath);
                                File.Move(storedContentPath, backupPath);
                            }
                            else
                            {
                                // Just remove.
                                File.Delete(storedContentPath);
                            }
                        }
                    }
                    else
                    {
                        // download missing content
                        download = true;
                    }

                    // Only download if we need too:
                    if (download)
                    {
                        // Make sure we can write to disk
                        Redeploy.CheckLocalDataPath(Path.GetDirectoryName(storedContentPath));
                        // Download latest archive
                        if (RetrieveWebContent(sourceUrl, storedContentPath)) {
                            // Save Release Info
                            var storedReleaseInfo = Path.Combine(Path.GetFullPath(ConfigManager.Config.ContentServer.LocalContentPath), @"stored_world_release.json");
                            File.WriteAllText(storedReleaseInfo, JsonConvert.SerializeObject(releaseInfo));
                        }
                    }

                    long storedArchiveFileSize = 0;
                    // gg: This may be unneeded at this point:
                    if (File.Exists(storedContentPath))
                        storedArchiveFileSize = new FileInfo(storedContentPath).Length;

                    // Saves file info, too use elsewhere.
                    jsonReleaseFile = new RemoteResource() {
                        DatabaseName = Redeploy.DefaultWorldDatabaseName,
                        SourceUri = sourceUrl,
                        SourcePath = sourcePath,
                        FilePath = storedContentPath,
                        FileName = storedReleaseFileName,
                        FileSize = storedArchiveFileSize,
                        Type = RemoteResourceType.WorldReleaseJsonFile,
                        Hash = contentHash
                    };
                }
            }
            catch (Exception error)
            {
                log.Error($"Trouble capturing metadata from the Gitlab API. {error.ToString()}");
                return null;
            }

            return jsonReleaseFile;
        }
    }
}

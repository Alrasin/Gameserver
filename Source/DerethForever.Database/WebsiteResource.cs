/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DerethForever.Common;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DerethForever.Database
{
    public class WebsiteResource : IRedeploySource
    {
        private ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Download the world release too the content folder, along with a release info file (in json format)
        private Uri _worldDbReleaseInfoUrl;

        /// <summary>
        /// Captured world release info
        /// </summary>
        private RemoteResource worldRelease;

        public List<RemoteResource> SyncFileList()
        {
            // Only download over HTTP or HTTPS
            if (!Uri.TryCreate((ConfigManager.Config.ContentServer.ContentServerBaseUrl + ConfigManager.Config.ContentServer.CurrentReleaseInfo), UriKind.Absolute, out Uri websiteUri)
                || (websiteUri.Scheme != Uri.UriSchemeHttp && websiteUri.Scheme != Uri.UriSchemeHttps))
            {
                log.Error($"Invalid uri in CurrentReleaseInfo: {ConfigManager.Config.ContentServer.ContentServerBaseUrl + ConfigManager.Config.ContentServer.CurrentReleaseInfo}. Must contain an http or https protocol specification, in the url.");
                return null;
            }

            _worldDbReleaseInfoUrl = websiteUri;

            // Must download world release from the website, only.
            worldRelease = GetWorldJsonRelease();

            if (worldRelease != null)
            {
                // Save hashes for comparison in the future
                var contentDataPath = Path.GetFullPath(Path.Combine(ConfigManager.Config.ContentServer.LocalContentPath, $"{System.Reflection.MethodBase.GetCurrentMethod().DeclaringType}.json"));
                // Save download data to json?
                File.WriteAllText(contentDataPath, JsonConvert.SerializeObject(worldRelease));
            } else
                return null;

            return new List<RemoteResource>() { worldRelease };
        }

        /// <summary>
        /// Retrieves the World Release from Gitlab.
        /// </summary>
        /// <remarks>This function assumes that the release archive is a zip file.</remarks>
        private RemoteResource GetWorldJsonRelease()
        {
            RemoteResource jsonReleaseFile = null;

            var contentFolderPath = Path.GetFullPath(ConfigManager.Config.ContentServer.LocalContentPath);

            using (WebClient webClient = new WebClient())
            {
                WebClient w = new WebClient();

                // Add token, if available
                if (Redeploy.CurrentToken?.Length > 0)
                {
                    w.Headers.Add($"PRIVATE-TOKEN: {Redeploy.CurrentToken}");
                }

                // Get the release info pointer, based on the config settings
                string pointer = _worldDbReleaseInfoUrl.ToString();
                JObject webReleaseInfo = null;

                try
                {
                    // Parse the API info about the Current World Release Archive:
                    webReleaseInfo = JObject.Parse(w.DownloadString(pointer));
                }
                catch (WebException e)
                {
                    // Send errors to the log
                    log.Error($"Download error: {e.Message}");
                    return null;
                }

                // Extract data
                var sourcePath = webReleaseInfo.GetValue("ArchivePath").ToString();
                var contentHash = webReleaseInfo.GetValue("Hash").ToString();
                // Validate file paths
                var sourceUrl = _worldDbReleaseInfoUrl.GetLeftPart(UriPartial.Authority) + '/' + sourcePath;
                var storedReleaseFileName = Path.GetFileName(sourcePath);
                var storedContentPath = Path.Combine(contentFolderPath, storedReleaseFileName);
                var download = false;

                // Check hash against stored archive
                if (File.Exists(storedContentPath))
                {
                    // Get hash and compare
                    var localFileHash = Redeploy.CalculateFileChecksum(storedContentPath);
                    if (localFileHash != contentHash)
                    {
                        download = true;
                        // Hash is different, backup or delete
                        if (ConfigManager.Config.ContentServer.KeepDataBackups)
                        {
                            var backupPath = Path.GetFullPath(ConfigManager.Config.ContentServer.LocalBackupPath);
                            File.Move(storedContentPath, backupPath);
                        }
                        else
                        {
                            // Just remove.
                            File.Delete(storedContentPath);
                        }
                    }
                }
                else
                {
                    // download missing content
                    download = true;
                }

                // Only download if we need too:
                if (download)
                {
                    try
                    {
                        // Make sure we can write
                        Redeploy.CheckLocalDataPath(Path.GetDirectoryName(storedContentPath));
                        // Download latest archive
                        w.DownloadFile(sourceUrl, storedContentPath);
                    }
                    catch (WebException e)
                    {
                        // Send errors to the log
                        log.Error($"Download error: {e.Message}");
                        return null;
                    }

                    // Bomb out if file is missing or save the downloaded information if successful
                    if (File.Exists(storedContentPath)) {
                        // Save Release Info
                        var storedReleaseInfoPath = Path.Combine(Path.GetFullPath(ConfigManager.Config.ContentServer.LocalContentPath), @"stored_world_release.json");
                        File.WriteAllText(storedReleaseInfoPath, JsonConvert.SerializeObject(webReleaseInfo));
                    } else
                    {
                        log.Error("Download did not finish correctly. Please check your config settings.");
                        return null;
                    }
                }

                long storedArchiveFileSize = 0;
                // gg: This may be unneeded at this point:
                if (File.Exists(storedContentPath))
                    storedArchiveFileSize = new FileInfo(storedContentPath).Length;

                // Saves file info, too use elsewhere.
                jsonReleaseFile = new RemoteResource()
                {
                    DatabaseName = Redeploy.DefaultWorldDatabaseName,
                    SourceUri = sourceUrl,
                    SourcePath = sourcePath,
                    FilePath = storedContentPath,
                    FileName = storedReleaseFileName,
                    FileSize = storedArchiveFileSize,
                    Type = RemoteResourceType.WorldReleaseJsonFile,
                    Hash = contentHash
                };
            }

            return jsonReleaseFile;
        }
    }
}

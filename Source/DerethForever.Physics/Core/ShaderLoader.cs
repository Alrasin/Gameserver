/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using SharpDX.D3DCompiler;
using System.IO;
using System.Reflection;

namespace DerethForever.Physics.Core
{
    public class ShaderLoader
    {
        public static ShaderBytecode FromResource(Assembly assembly, string sourceName, ShaderFlags flags)
        {
            return FromResource(assembly, new[] { sourceName }, flags);
        }

        public static ShaderBytecode FromResource(Assembly assembly, string[] sourceNames, ShaderFlags flags)
        {
            string assemblyName = assembly.GetName().Name;
            string shaderSource = "";
            foreach (string source in sourceNames)
            {
                using (var stream = assembly.GetManifestResourceStream(string.Format("{0}.{1}", assemblyName, source)))
                //using (var stream = assembly.GetManifestResourceStream(source))
                {
                    StreamReader reader = new StreamReader(stream);
                    shaderSource += reader.ReadToEnd();
                }
            }
            return ShaderBytecode.Compile(shaderSource, "fx_5_0", flags);
        }
    }
}

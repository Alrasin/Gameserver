/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Diagnostics;
using Microsoft.Owin.Hosting;

using DerethForever.Api.Common;
using DerethForever.Api.Controllers;
using DerethForever.Common;
using log4net;
using DerethForever.DatLoader;
using DerethForever.Database;

namespace DerethForever.Api.Host
{
    public class Program
    {
        private static readonly ILog log = LogManager.GetLogger("ApiHost");

        public static void Main(string[] args)
        {
            Console.Title = @"Dereth Forever API Host";
            log.Warn("Starting Dereth Forever API Host...");
            Initialize();

            if (!string.IsNullOrWhiteSpace(ConfigManager.Config.ApiServer?.ListenUrl))
            {
                // Get the bind address and port from config:
                var server = WebApp.Start<Startup>(url: ConfigManager.Config.ApiServer.ListenUrl);
                log.Info($"Dereth Forever API listening at {ConfigManager.Config.ApiServer.ListenUrl}");
            }
            else
            {
                log.Error("There was an error in your API configuration.");
            }
            Console.ReadLine(); // need a readline or the error flashes without being seen
        }

        public static void Initialize()
        {
            ServiceConfig.LoadServiceConfig();
            DatManager.Initialize();
            DatabaseManager.Initialize(false);
        }
    }
}

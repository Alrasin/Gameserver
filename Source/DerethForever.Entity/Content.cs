/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using DerethForever.Common;
using DerethForever.Entity.Enum;

using MySql.Data.MySqlClient;
using Newtonsoft.Json.Converters;

namespace DerethForever.Entity
{
    /// <summary>
    /// ACE-constructed concept that allows for association of resources into a logical grouping
    /// for ease of finding information about some content and editing the associated resources
    /// </summary>
    [DbTable("ace_content")]
    public class Content : ChangeTrackedObject
    {
        public Content()
        {
        }

        /// <summary>
        /// content identifier.
        /// </summary>
        [JsonProperty("contentGuid")]
        public Guid? ContentGuid { get; set; }

        [JsonIgnore]
        [DbField("contentGuid", (int)MySqlDbType.Binary, Update = false, IsCriteria = true)]
        public byte[] ContentId_Binder
        {
            get { return ContentGuid.Value.ToByteArray(); }
            set { ContentGuid = new Guid(value); }
        }

        /// <summary>
        /// user-defined name for the content that is searchable
        /// </summary>
        [JsonProperty("contentName")]
        [DbField("contentName", (int)MySqlDbType.Text)]
        public string ContentName { get; set; }

        /// <summary>
        /// logical grouping for what type of content this is
        /// </summary>
        [JsonProperty("contentType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ContentType? ContentType { get; set; }

        [JsonIgnore]
        [DbField("contentType", (int)MySqlDbType.Byte)]
        public uint? ContentType_Binder
        {
            get { return (uint?)ContentType; }
            set { ContentType = (value == null ? (ContentType?)null : (ContentType)value); }
        }

        /// <summary>
        /// This is a mocked property that will set a flag in the database any time this object is altered.  this flag
        /// will allow us to detect objects that have changed post-installation and generate changesetss
        /// </summary>
        [JsonIgnore]
        [DbField("userModified", (int)MySqlDbType.Bit)]
        public virtual bool UserModified
        {
            get { return true; }
            set { } // method intentionally not implemented
        }

        private DateTime? _lastModified;

        [JsonProperty("lastModified")]
        public DateTime? LastModified
        {
            get { return _lastModified; }
            set
            {
                _lastModified = value;
                IsDirty = true;
            }
        }

        [JsonIgnore]
        [DbField("lastModifiedDate", (int)MySqlDbType.Int64)]
        public long? LastModified_Binder
        {
            get { return LastModified == null ? (long?)null : LastModified.Value.Ticks; }
            set { LastModified = value == null ? (DateTime?)null : new DateTime(value.Value); }
        }

        [JsonProperty("modifiedBy")]
        [DbField("modifiedBy", (int)MySqlDbType.Text)]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// list of landblock upper words (ie, landblock x/y) containing dungeons associated with this quest.  please note, other content
        /// may be associated with them as well
        /// </summary>
        [JsonProperty("associatedLandblocks")]
        public List<ContentLandblock> AssociatedLandblocks { get; set; } = new List<ContentLandblock>();

        /// <summary>
        /// list of weenies that are associated with this content.  NPCs, items turned in, found, or otherwise
        /// involved in this quest.  could also be traps, levers, or other static objects in the dungeon.
        /// also includes spawn generators associated with the quests.
        /// </summary>
        [JsonProperty("weenies")]
        public List<ContentWeenie> Weenies { get; set; } = new List<ContentWeenie>();

        /// <summary>
        /// list of URLs linking the content in other places (acpedia, asheron.wikia, etc)
        /// </summary>
        [JsonProperty("externalResources")]
        public List<ContentResource> ExternalResources { get; set; } = new List<ContentResource>();

        /// <summary>
        /// list of associated content
        /// </summary>
        [JsonProperty("associatedContent")]
        public List<ContentLink> AssociatedContent { get; set; } = new List<ContentLink>();

        [JsonProperty("changelog")]
        public List<ChangelogEntry> Changelog { get; set; } = new List<ChangelogEntry>();

        [JsonProperty("comments")]
        public string Comments { get; set; }

        public override void SetDirtyFlags()
        {
            base.SetDirtyFlags();

            AssociatedContent.ForEach(c => c.SetDirtyFlags());
            ExternalResources.ForEach(r => r.SetDirtyFlags());
            Weenies.ForEach(w => w.SetDirtyFlags());
            AssociatedLandblocks.ForEach(l => l.SetDirtyFlags());
        }

        public override void ClearDirtyFlags()
        {
            base.ClearDirtyFlags();

            AssociatedContent.ForEach(c => c.ClearDirtyFlags());
            ExternalResources.ForEach(r => r.ClearDirtyFlags());
            Weenies.ForEach(w => w.ClearDirtyFlags());
            AssociatedLandblocks.ForEach(l => l.ClearDirtyFlags());
        }
    }
}

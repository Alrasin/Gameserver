/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using MySql.Data.MySqlClient;

namespace DerethForever.Entity
{
    [DbTable("vw_ace_weenie_class")]
    public class CachedWeenieClass
    {
        [DbField("aceObjectId", (int)MySqlDbType.UInt32, IsCriteria = true)]
        public uint DataObjectId { get; set; }

        [DbField("name", (int)MySqlDbType.String, ListGet = true)]
        public string Name { get; set; }

        [DbField("weenieClassId", (int)MySqlDbType.UInt32, IsCriteria = true)]
        public uint WeenieClassId { get; set; }

        [DbField("weenieClassDescription", (int)MySqlDbType.String, ListGet = true)]
        public string WeenieClassDescription { get; set; }

        [DbField("itemType", (int)MySqlDbType.Int32, ListGet = true)]
        public int ItemType { get; set; }
    }
}

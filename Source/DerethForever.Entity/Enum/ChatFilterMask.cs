﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// The ChatFilterMask identifies types of messages that are squelched or filtered (/messagetypes).<para />
    /// Used with F7B0 01F4: Game Event -> Squelch and Filter List
    /// </summary>
    [Flags]
    public enum ChatFilterMask
    {
        Speech          = 0x00000004,
        Tell            = 0x00000008,
        Combat          = 0x00000040,
        Magic           = 0x00000080,
        Emote           = 0x00001000,
        Appraisal       = 0x00010000,
        Spellcasting    = 0x00020000,
        Allegiance      = 0x00040000,
        Fellowship      = 0x00080000,
        Combat_Enemy    = 0x00200000,
        Combat_Self     = 0x00400000,
        Recall          = 0x00800000,
        Craft           = 0x01000000,
        Salvaging       = 0x02000000,
        AllMessageTypes = ~0
    }
}

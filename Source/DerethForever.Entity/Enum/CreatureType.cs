﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    public enum CreatureType : uint
    {
        Invalid,
        Olthoi,
        Banderling,
        Drudge,
        Mosswart,
        Lugian,
        Tumerok,
        Mite,
        Tusker,
        PhyntosWasp,
        Rat,
        Auroch,
        Cow,
        Golem,
        Undead,
        Gromnie,
        Reedshark,
        Armoredillo,
        Fae,
        Virindi,
        Wisp,
        Knathtead,
        Shadow,
        Mattekar,
        Mumiyah,
        Rabbit,
        Sclavus,
        ShallowsShark,
        Monouga,
        Zefir,
        Skeleton,
        Human,
        Shreth,
        Chittick,
        Moarsman,
        OlthoiLarvae,
        Slithis,
        Deru,
        FireElemental,
        Snowman,
        Unknown,
        Bunny,
        LightningElemental,
        Rockslide,
        Grievver,
        Niffis,
        Ursuin,
        Crystal,
        HollowMinion,
        Scarecrow,
        Idol,
        Empyrean,
        Hopeslayer,
        Doll,
        Marionette,
        Carenzi,
        Siraluun,
        AunTumerok,
        HeaTumerok,
        Simulacrum,
        AcidElemental,
        FrostElemental,
        Elemental,
        Statue,
        Wall,
        AlteredHuman,
        Device,
        Harbinger,
        DarkSarcophagus,
        Chicken,
        GotrokLugian,
        Margul,
        BleachedRabbit,
        NastyRabbit,
        GrimacingRabbit,
        Burun,
        Target,
        Ghost,
        Fiun,
        Eater,
        Penguin,
        Ruschk,
        Thrungus,
        ViamontianKnight,
        Remoran,
        Swarm,
        Moar,
        EnchantedArms,
        Sleech,
        Mukkir,
        Merwart,
        Food,
        ParadoxOlthoi,
        Harvest,
        Energy,
        Apparition,
        Aerbax,
        Touched,
        BlightedMoarsman,
        GearKnight,
        Gurog,
        Anekshay
    }
}

/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    public enum PaletteTemplate : uint
    {
        Undefined,
        AquaBlue,
        Blue,
        BluePurple,
        Brown,
        DarkBlue,
        DeepBrown,
        DeepGreen,
        Green,
        Grey,
        LightBlue,
        Maroon,
        Navy,
        Purple,
        Red,
        RedPurple,
        Rose,
        Yellow,
        YellowBrown,
        Copper,
        Silver,
        Gold,
        Aqua,
        DarkAquaMetal,
        DarkBlueMetal,
        DarkCopperMetal,
        DarkGoldMetal,
        DarkGreenMetal,
        DarkPurpleMetal,
        DarkRedMetal,
        DarkSilverMetal,
        LightAquaMetal,
        LightBlueMetal,
        LightCopperMetal,
        LightGoldMetal,
        LightGreenMetal,
        LightPurpleMetal,
        LightRedMetal,
        LightSilverMetal,
        Black,
        Bronze,
        SandyYellow,
        DarkBrown,
        LightBrown,
        TanRed,
        PaleGreen,
        Tan,
        PastyYellow,
        SnowyWhite,
        RuddyYellow,
        RuddierYellow,
        MidGrey,
        DarkGrey,
        BlueDullSilver,
        YellowPaleSilver,
        BrownBlueDark,
        BrownBlueMed,
        GreenSilver,
        BrownGreen,
        YellowGreen,
        PalePurple,
        White,
        RedBrown,
        GreenBrown,
        OrangeBrown,
        PaleGreenBrown,
        PaleOrange,
        GreenSlime,
        BlueSlime,
        YellowSlime,
        PurpleSlime,
        DullRed,
        GreyWhite,
        MediumGrey,
        DullGreen,
        Olivegreen,
        Orange,
        BlueGreen,
        Olive,
        Lead,
        Iron,
        LiteGreen,
        PinkPurple,
        Amber,
        DyeDarkGreen,
        DyeDarkRed,
        DyeDarkYellow,
        DyeBotched,
        DyeWinterBlue,
        DyeWinterGreen,
        DyeWinterSilver,
        DyeSpringBlue,
        DyeSpringPurple,
        DyeSpringBlack,
    }
}

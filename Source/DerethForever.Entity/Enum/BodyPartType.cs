/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// client source: 
    /// ----- (005D0EC0) --------------------------------------------------------
    /// unsigned int __cdecl BodyPartEnumMapper::BodyPartToString(const int bp, char* buf, const unsigned int size)
    /// </summary>
    public enum BodyPartType
    {
        Undefined           = -1,
        Head                = 0,
        Chest               = 1,
        Abdomen             = 2,
        UpperArm            = 3,
        LowerArm            = 4,
        Hand                = 5,
        UpperLeg            = 6,
        LowerLeg            = 7,
        Foot                = 8,
        Horn                = 9,
        FrontLeg            = 10,
        // 11 is missing in client
        FrontFoot           = 12,
        RearLeg             = 13,
        // 14 is missing in client
        RearFoot            = 15,
        Torso               = 16,
        Tail                = 17,
        Arm                 = 18,
        Leg                 = 19,
        Claw                = 20,
        Wings               = 21,
        Breath              = 22,
        Tentacle            = 23,
        UpperTentacle       = 24,
        LowerTentacle       = 25,
        Cloak               = 26
    }
}

﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    public enum WeenieType : int
    {
        Undef,
        Generic,
        Clothing,
        MissileLauncher,
        Missile,
        Ammunition,
        MeleeWeapon,
        Portal,
        Book,
        Coin,
        Creature,
        Admin,
        Vendor,
        HotSpot,
        Corpse,
        Cow,
        AI,
        Machine,
        Food,
        Door,
        Chest,
        Container,
        Key,
        Lockpick,
        PressurePlate,
        LifeStone,
        Switch,
        PKModifier,
        Healer,
        LightSource,
        Allegiance,
        UNKNOWN__GUESSEDNAME32, // NOTE: Missing 1
        SpellComponent,
        ProjectileSpell,
        Scroll,
        Caster,
        Channel,
        ManaStone,
        Gem,
        AdvocateFane,
        AdvocateItem,
        Sentinel,
        GSpellEconomy,
        LSpellEconomy,
        CraftTool,
        LScoreKeeper,
        GScoreKeeper,
        GScoreGatherer,
        ScoreBook,
        EventCoordinator,
        Entity,
        Stackable,
        HUD,
        House,
        Deed,
        SlumLord,
        Hook,
        Storage,
        BootSpot,
        HousePortal,
        Game,
        GamePiece,
        SkillAlterationDevice,
        AttributeTransferDevice,
        Hooker,
        AllegianceBindstone,
        InGameStatKeeper,
        AugmentationDevice,
        SocialManager,
        Pet,
        PetDevice,
        CombatPet        
    }
}

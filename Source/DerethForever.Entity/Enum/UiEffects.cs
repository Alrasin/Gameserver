/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    public enum UiEffects : uint
    {
        Undef           = 0x0000,
        Magical         = 0x0001,
        Poisoned        = 0x0002,
        BoostHealth     = 0x0004,
        BoostMana       = 0x0008,
        BoostStamina    = 0x0010,
        Fire            = 0x0020,
        Default         = 0x0021,
        Lightning       = 0x0040,
        Frost           = 0x0080,
        Acid            = 0x0100,
        Bludgeoning     = 0x0200,
        Slashing        = 0x0400,
        Piercing        = 0x0800,
        Nether          = 0x1000
    }
}

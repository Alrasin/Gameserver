/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

namespace DerethForever.Entity.Enum
{
    public enum RegenerationLocation
    {
        Undef            = 0x000,
        OnTop            = 0x001,
        Scatter          = 0x002,
        Specific         = 0x004,
        Contain          = 0x008,
        Wield            = 0x010,
        Shop             = 0x020,
        Checkpoint       = 0x038,
        Treasure         = 0x040,
        OnTopTreasure    = 0x041,
        ScatterTreasure  = 0x042,
        SpecificTreasure = 0x044,
        ContainTreasure  = 0x048,
        WieldTreasure    = 0x050,
        ShopTreasure     = 0x060
    }
}

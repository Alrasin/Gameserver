﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.ComponentModel;
namespace DerethForever.Entity.Enum.Properties
{
    public enum PropertyString : ushort
    {
        // properties marked as ServerOnly are properties we never saw in PCAPs, from here:
        // http://ac.yotesfan.com/ace_object/not_used_enums.php
        // source: @OptimShi
        // description attributes are used by the weenie editor for a cleaner display name
        Undef                           = 0,
        Name                            = 1,
        /// <summary>
        /// default "Adventurer"
        /// </summary>
        Title                           = 2,
        Sex                             = 3,
        HeritageGroup                   = 4,
        Template                        = 5,
        AttackersName                   = 6,
        Inscription                     = 7,
        [Description("Scribe Name")]
        ScribeName                      = 8,
        VendorsName                     = 9,
        Fellowship                      = 10,
        MonarchsName                    = 11,
        LockCode                        = 12,
        KeyCode                         = 13,
        Use                             = 14,
        ShortDesc                       = 15,
        LongDesc                        = 16,
        ActivationTalk                  = 17,
        UseMessage                      = 18,
        ItemHeritageGroupRestriction    = 19,
        PluralName                      = 20,
        MonarchsTitle                   = 21,
        ActivationFailure               = 22,
        ScribeAccount                   = 23,
        TownName                        = 24,
        CraftsmanName                   = 25,
        UsePkServerError                = 26,
        ScoreCachedText                 = 27,
        ScoreDefaultEntryFormat         = 28,
        ScoreFirstEntryFormat           = 29,
        ScoreLastEntryFormat            = 30,
        ScoreOnlyEntryFormat            = 31,
        ScoreNoEntry                    = 32,
        Quest                           = 33,
        GeneratorEvent                  = 34,
        PatronsTitle                    = 35,
        HouseOwnerName                  = 36,
        QuestRestriction                = 37,
        AppraisalPortalDestination      = 38,
        TinkerName                      = 39,
        ImbuerName                      = 40,
        HouseOwnerAccount               = 41,
        DisplayName                     = 42,
        DateOfBirth                     = 43,
        ThirdPartyApi                   = 44,
        KillQuest                       = 45,
        Afk                             = 46,
        AllegianceName                  = 47,
        AugmentationAddQuest            = 48,
        KillQuest2                      = 49,
        KillQuest3                      = 50,
        UseSendsSignal                  = 51,

        [Description("Gear Plating Name")]
        GearPlatingName                 = 52
        // values over 9000 are ones that we have added and should not be sent to the client
    }

    public static class PropertyStringExtensions
    {
        public static string GetDescription(this PropertyString prop)
        {
            var description = EnumHelper.GetAttributeOfType<DescriptionAttribute>(prop);
            return description?.Description ?? prop.ToString();
        }
    }
}
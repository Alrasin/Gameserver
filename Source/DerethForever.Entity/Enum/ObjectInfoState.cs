using System;

namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// pulled from the PDB with the microsoft-pdb tool
    /// </summary>
    [Flags]
    public enum ObjectInfoState
    {
        Default         = 0x0000,
        Contact         = 0x0001,
        OnWalkable      = 0x0002,
        IsViewer        = 0x0004,
        PathClipped     = 0x0008,
        FreeRotate      = 0x0010,
        Missing         = 0x0020,
        PerfectClip     = 0x0040,
        IsImpenetrable  = 0x0080,
        IsPlayer        = 0x0100,
        EdgeSlide       = 0x0200,
        IgnoreCreatures = 0x0400,
        IsPK            = 0x0800,
        IsPkLite        = 0x1000
    }
}

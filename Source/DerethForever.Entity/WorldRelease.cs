/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Entity
{
    /// <summary>
    /// Database archive of the World
    /// </summary>
    public class WorldRelease
    {
        /// <summary>
        /// Server Name Responsible for creating the World Release
        /// </summary>
        public string OriginName { get; set; }
        /// <summary>
        /// CDN or Server URL for the download.
        /// </summary>
        public string ContentUrl { get; set; }
        /// <summary>
        /// URL for the WorldRelease info download (created from this object).
        /// </summary>
        public string ResourceInfoUrl { get; set; }
        /// <summary>
        /// Timestamp when release was cut
        /// </summary>
        public string ReleaseDateTime { get; set; }
        /// <summary>
        /// Release version string
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// Filename of the Archive
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Path to the Archive
        /// </summary>
        public string ArchivePath { get; set; }
        /// <summary>
        /// SHA256 Calulcated Hash of the Archive
        /// </summary>
        public string Hash { get; set; }
        /// <summary>
        /// Full or Partial type
        /// </summary>
        public ReleaseType Type { get; set; }
        /// <summary>
        /// Previous Release
        /// </summary>
        public string PreviousRelease { get; set; }
    }
}

/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using DerethForever.Common;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    [DbTable("ace_position", HasAutoGeneratedId = true, AutoGeneratedIdColumn = "positionId")]
    public class DataObjectPropertiesPosition : BaseDataProperty, ICloneable
    {
        // private variables for things that mean "this changed".
        // position type and position id are _relatively_ immutable
        private Position _position = new Position();

        [JsonProperty("landblock")]
        [DbField("landblockRaw", (int)MySqlDbType.UInt32)]
        public uint Cell
        {
            get { return _position.Cell; }
            set
            {
                _position.Cell = value;
                IsDirty = true;
            }
        }

        [JsonProperty("positionType")]
        [DbField("positionType", (int)MySqlDbType.UInt16, Update = false, IsCriteria = true)]
        public ushort DbPositionType { get; set; }

        [JsonProperty("positionId")]
        [DbField("positionId", (int)MySqlDbType.UInt32, Insert = false, Update = false)]
        public uint? PositionId { get; set; }

        [JsonProperty("x")]
        [DbField("posX", (int)MySqlDbType.Float)]
        public float PositionX
        {
            get { return _position.PositionX; }
            set
            {
                _position.PositionX = value;
                IsDirty = true;
            }
        }

        [JsonProperty("y")]
        [DbField("posY", (int)MySqlDbType.Float)]
        public float PositionY
        {
            get { return _position.PositionY; }
            set
            {
                _position.PositionY = value;
                IsDirty = true;
            }
        }

        [JsonProperty("z")]
        [DbField("posZ", (int)MySqlDbType.Float)]
        public float PositionZ
        {
            get { return _position.PositionZ; }
            set
            {
                _position.PositionZ = value;
                IsDirty = true;
            }
        }

        [JsonProperty("qw")]
        [DbField("qW", (int)MySqlDbType.Float)]
        public float RotationW
        {
            get { return _position.RotationW; }
            set
            {
                _position.RotationW = value;
                IsDirty = true;
            }
        }

        [JsonProperty("qx")]
        [DbField("qX", (int)MySqlDbType.Float)]
        public float RotationX
        {
            get { return _position.RotationX; }
            set
            {
                _position.RotationX = value;
                IsDirty = true;
            }
        }

        [JsonProperty("qy")]
        [DbField("qY", (int)MySqlDbType.Float)]
        public float RotationY
        {
            get { return _position.RotationY; }
            set
            {
                _position.RotationY = value;
                IsDirty = true;
            }
        }

        [JsonProperty("qz")]
        [DbField("qZ", (int)MySqlDbType.Float)]
        public float RotationZ
        {
            get { return _position.RotationZ; }
            set
            {
                _position.RotationZ = value;
                IsDirty = true;
            }
        }

        [JsonIgnore]
        public Position Position
        {
            get { return _position; }
            set
            {
                _position = value;
                IsDirty = true;
            }
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}

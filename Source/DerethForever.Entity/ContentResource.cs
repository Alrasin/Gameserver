﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Common;

using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    [DbTable("ace_content_resource")]
    public class ContentResource : ChangeTrackedObject
    {
        public ContentResource()
        {
            IsDirty = true;
            HasEverBeenSavedToDatabase = false;
        }

        [JsonProperty("contentResourceGuid")]
        public Guid? ContentResourceGuid { get; set; }

        [JsonIgnore]
        [DbField("contentResourceGuid", (int)MySqlDbType.Binary, Update = false, IsCriteria = true)]
        public byte[] ContentResourceGuid_Binder
        {
            get { return ContentResourceGuid.Value.ToByteArray(); }
            set { ContentResourceGuid = new Guid(value); }
        }

        /// <summary>
        /// content identifier
        /// </summary>
        [JsonIgnore]
        public Guid ContentGuid { get; set; }

        [JsonIgnore]
        [DbField("contentGuid", (int)MySqlDbType.Binary, Update = false, IsCriteria = true, ListGet = true, ListDelete = true)]
        public byte[] ContentId_Binder
        {
            get { return ContentGuid.ToByteArray(); }
            set { ContentGuid = new Guid(value); }
        }

        private string _name;

        [JsonProperty("name")]
        [DbField("name", (int)MySqlDbType.Text)]
        public string Name
        {
            get { return _name; }
            set { _name = value; IsDirty = true; }
        }

        private string _resourceUri;

        [JsonProperty("resourceUri")]
        [DbField("resourceUri", (int)MySqlDbType.Text)]
        public string ResourceUri
        {
            get { return _resourceUri; }
            set { _resourceUri = value; IsDirty = true; }
        }

        private string _comment;

        [JsonProperty("comment")]
        [DbField("comment", (int)MySqlDbType.Text)]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; IsDirty = true; }
        }
    }
}

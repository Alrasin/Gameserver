/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity
{
    public class CharacterPositionExtensions
    {
        private enum StarterTown : uint
        {
            Holtburg    = 0,
            Shoushi     = 1,
            Yaraq       = 2,
            Sanamar     = 3
        }

        public static Position StartingPosition(uint startArea)
        {
            uint landblockID;

            switch (startArea)
            {
                case (uint)StarterTown.Shoushi:
                    landblockID = 2130903469;
                    break;
                case (uint)StarterTown.Yaraq:
                    landblockID = 2349072813;
                    break;
                case (uint)StarterTown.Sanamar:
                    landblockID = 1912799661;
                    break;
                default:
                    landblockID = 2248343981;
                    break;
            }

            var startingPosition = new Position(landblockID, 12.3199f, -28.482f, 0.0049999995f, 0.0f, 0.0f, -0.9408059f, -0.3389459f);

            return startingPosition;
        }

        public static Position InvalidPosition(uint characterId)
        {
            var invalidPosition = new Position();
            invalidPosition.LandblockId = new LandblockId();
            return invalidPosition;
        }
    }
}
﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    /// <summary>
    ///  simple class that implements IDirty to make inheriting from it easy
    /// </summary>
    public class ChangeTrackedObject : IDirty
    {
        [JsonProperty("isDirty")]
        public virtual bool IsDirty { get; set; } = false;

        [JsonIgnore]
        public virtual bool HasEverBeenSavedToDatabase { get; set; } = false;

        [JsonProperty("isNew")]
        public bool IsNew
        {
            get { return !HasEverBeenSavedToDatabase; }
            set { HasEverBeenSavedToDatabase = !value; }
        }

        public virtual void ClearDirtyFlags()
        {
            this.IsDirty = false;
            this.HasEverBeenSavedToDatabase = true;
        }

        public virtual void SetDirtyFlags()
        {
            this.IsDirty = true;
            this.HasEverBeenSavedToDatabase = false;
        }
    }
}

/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Entity
{
    [DbTable("managed_world")]
    public class ManagedWorld
    {
        [JsonProperty("worldGuid")]
        public Guid? WorldGuid { get; set; }

        [JsonIgnore]
        [DbField("worldGuid", (int)MySqlDbType.Binary, IsCriteria = true, Update = false)]
        public byte[] WorldGuid_Binder
        {
            get { return WorldGuid?.ToByteArray(); }
            set { WorldGuid = (value != null) ? new Guid(value) : (Guid?)null; }
        }

        [JsonProperty("accountGuid")]
        public Guid? AccountGuid { get; set; }

        [JsonIgnore]
        [DbField("accountGuid", (int)MySqlDbType.Binary, ListGet = true, ListDelete = true)]
        public byte[] AccountGuid_Binder
        {
            get { return AccountGuid == null ? (byte[])null : AccountGuid.Value.ToByteArray(); }
            set { AccountGuid = value == null ? (Guid?)null : new Guid(value); }
        }

        [JsonProperty("serverName")]
        [DbField("serverName", (int)MySqlDbType.Text)]
        public string ServerName { get; set; }

        [JsonProperty("address")]
        [DbField("address", (int)MySqlDbType.Text)]
        public string Address { get; set; }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
namespace DerethForever.ClientLib.Enum
{
    public enum TerrainType
    {
        BarrenRock           = 0x0,
        Grassland            = 0x1,
        Ice                  = 0x2,
        LushGrass            = 0x3,
        MarshSparseSwamp     = 0x4,
        MudRichDirt          = 0x5,
        ObsidianPlain        = 0x6,
        PackedDirt           = 0x7,
        PatchyDirt           = 0x8,
        PatchyGrassland      = 0x9,
        SandYellow           = 0xA,
        SandGrey             = 0xB,
        SandRockStrewn       = 0xC,
        SedimentaryRock      = 0xD,
        SemiBarrenRock       = 0xE,
        Snow                 = 0xF,
        WaterRunning         = 0x10,
        WaterStandingFresh   = 0x11,
        WaterShallowSea      = 0x12,
        WaterShallowStillSea = 0x13,
        WaterDeepSea         = 0x14,
        Reserved21           = 0x15,
        Reserved22           = 0x16,
        Reserved23           = 0x17,
        Reserved24           = 0x18,
        Reserved25           = 0x19,
        Reserved26           = 0x1A,
        Reserved27           = 0x1B,
        Reserved28           = 0x1C,
        Reserved29           = 0x1D,
        Reserved30           = 0x1E,
        Reserved31           = 0x1F,
        RoadType             = 0x20,
    }
}

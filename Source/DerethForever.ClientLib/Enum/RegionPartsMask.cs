/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Enum
{
    [Flags]
    public enum RegionPartsMask
    {
        Sound       = 1,   // 0x001
        Scene       = 2,   // 0x002

        /// <summary>
        /// unused
        /// </summary>
        Terrain     = 4,   // 0x004

        /// <summary>
        /// unused
        /// </summary>
        Encounter   = 8,   // 0x008
        Sky         = 16,  // 0x010

        /// <summary>
        /// unused
        /// </summary>
        Fog         = 64,  // 0x040

        /// <summary>
        /// unused
        /// </summary>
        DistanceFog = 128, // 0x080

        /// <summary>
        /// unused
        /// </summary>
        Map         = 256, // 0x100
        Misc        = 512, // 0x200
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;

namespace DerethForever.ClientLib.Enum
{
    /// <summary>
    /// These are new - not from client.
    /// </summary>
    [Flags]
    public enum MovementParamsFlags
    {
        CanWalk               = 0x00001,
        CanRun                = 0x00002,
        CanSideStep           = 0x00004,
        CanWalkBackwards      = 0x00008,
        CanCharge             = 0x00010,
        FailWalk              = 0x00020,
        UseFinalHeading       = 0x00040,
        Sticky                = 0x00080,
        MoveAway              = 0x00100,
        MoveTowards           = 0x00200,
        UseSpheres            = 0x00400,
        SetHoldKey            = 0x00800,
        Autonomous            = 0x01000,
        ModifyRawState        = 0x02000,
        ModifyInterpretedSate = 0x04000,
        CancelMoveTo          = 0x08000,
        StopCompletly         = 0x10000,
        DisableJumpDuringLink = 0x20000
    }
}

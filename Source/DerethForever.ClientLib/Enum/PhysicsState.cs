/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;

namespace DerethForever.ClientLib.Enum
{
    [Flags]
    public enum PhysicsState
    {
        Static                      = 0x00000001,
        Unused1                     = 0x00000002,
        Ethereal                    = 0x00000004,
        ReportCollision             = 0x00000008,
        IgnoreCollision             = 0x00000010,
        NoDraw                      = 0x00000020,
        Missile                     = 0x00000040,
        Pushable                    = 0x00000080,
        AlignPath                   = 0x00000100,
        PathClipped                 = 0x00000200,
        Gravity                     = 0x00000400,
        LightingOn                  = 0x00000800,
        ParticleEmitter             = 0x00001000,
        Unused2                     = 0x00002000,
        Hidden                      = 0x00004000,
        ScriptedCollision           = 0x00008000,
        HasPhysicsBsp               = 0x00010000,
        Inelastic                   = 0x00020000,
        HasDefaultAnim              = 0x00040000,
        HasDefaultScript            = 0x00080000,
        Cloaked                     = 0x00100000,
        ReportCollisionAsEnviroment = 0x00200000,
        EdgeSlide                   = 0x00400000,
        Sledding                    = 0x00800000,
        Frozen                      = 0x01000000,
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;

namespace DerethForever.ClientLib.Enum
{
    /// <summary>
    /// pulled from the PDB with the microsoft-pdb tool
    /// </summary>
    [Flags]
    public enum ObjectInfoState
    {
        Default         = 0x0000,
        Contact         = 0x0001,
        OnWalkable      = 0x0002,
        IsViewer        = 0x0004,
        PathClipped     = 0x0008,
        FreeRotate      = 0x0010,
        Missing         = 0x0020,
        PerfectClip     = 0x0040,
        IsImpenetrable  = 0x0080,
        IsPlayer        = 0x0100,
        EdgeSlide       = 0x0200,
        IgnoreCreatures = 0x0400,
        IsPK            = 0x0800,
        IsPkLite        = 0x1000
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    public class MotionData
    {
        // +0x000 __VFN_table : Ptr32
        // +0x004 __VFN_table : Ptr32
        // +0x008 hash_next        : Ptr32 HashBaseData<unsigned long>
        // +0x010 num_anims        : UChar
        // +0x030 bitfield         : UChar

        /// <summary>
        /// +0x00c id               : Uint4B
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// +0x014 anims            : Ptr32 AnimData
        /// </summary>
        public List<AnimationData> Animations;

        /// <summary>
        /// +0x018 velocity         : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Velocity;

        /// <summary>
        /// +0x024 omega            : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Omega;

        /// <summary>
        /// +0x030 bitfield         : UChar
        /// </summary>
        public byte Bitfield { get; set; }


        /// <summary>
        /// ----- (00526570) --------------------------------------------------------
        /// int __thiscall MotionData::UnPack(MotionData*this, void** addr, unsigned int size)
        /// acclient.c 341814
        /// </summary>
        public static MotionData Unpack(BinaryReader reader, uint id)
        {
            MotionData md = new MotionData();

            md.Id = id;

            // I am sure there is a better way to do this.
            byte numAnims = reader.ReadByte();
            md.Bitfield = reader.ReadByte();
            byte bitfield2 = reader.ReadByte();
            byte junk = reader.ReadByte();

            if (numAnims > 0)
            {
                md.Animations = new List<AnimationData>();
                for (byte i = 0; i < numAnims; i++)
                {
                    AnimationData animData = AnimationData.Unpack(reader);                   
                    md.Animations.Add(animData);
                }
            }

            if ((bitfield2 & 1) > 0)
                md.Velocity = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

            if ((bitfield2 & 2) > 0)
                md.Omega = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

            return md;
        }
    }
}

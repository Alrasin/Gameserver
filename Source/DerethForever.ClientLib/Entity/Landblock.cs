/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.DatUtil;
using DerethForever.ClientLib.Enum.LandDefs;
using log4net;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CLandBlock in the client.  the client version of this implemented dual inheritance of
    /// DBObj and CLandblockStruct.  we've implemented LandblockStruct as an interface with
    /// extension methods in order to give the illusion of multiple inheritance here in C#
    /// </summary>
    public class Landblock : DatabaseObject, LandblockStruct
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// intentionally private.  Landblocks are only valid if loaded from the dat file via Unpack.
        /// </summary>
        private Landblock()
        {
        }

        /// <summary>
        /// one and only way to get landblocks.
        /// </summary>
        public static Landblock Load(uint landblockId)
        {
            uint fileId = ((landblockId & 0xFFFF0000) | 0x0000FFFF);
            Landblock lb = CellDatReader.Current.Unpack<Landblock>(fileId);

            lb.Generate();
            lb.InitializeStaticObjects();
            lb.InitializeBuildings();
            lb.GetLandScenes();

            return lb;
        }

        /// <summary>
        /// land_uvs - static 4-element array
        /// </summary>
        public static UvMapping[] LandUvMapping = new UvMapping[4];

        /// <summary>
        /// =00000000`00844acc use_encounter_info : Int4B
        /// </summary>
        public static bool UseEncounterInfo;

        /// <summary>
        /// 0x038 vertex_lighting : Ptr32 RGBColor
        /// </summary>
        public List<RgbColor> VertexLighting { get; set; } = new List<RgbColor>();

        /// <summary>
        /// heading index into LandDefs.Direction array
        /// 0x03c trans_dir : LandDefs::Direction
        /// </summary>
        public Direction TransDirection { get; set; }

        /// <summary>
        /// 0x040 side_vertex_count
        /// </summary>
        public int SideVertexCount { get; set; } = 9;

        /// <summary>
        /// 0x044 side_polygon_count
        /// </summary>
        public int SidePolygonCount { get; set; }

        /// <summary>
        /// 0x048 side_cell_count
        /// </summary>
        public int SideCellCount { get; set; }

        /// <summary>
        /// 0x04C water_type
        /// </summary>
        public Enum.WaterType WaterType { get; set; }

        /// <summary>
        /// Heightmap.  index is x + y*row.
        /// 0x050 height : Ptr32 UChar
        /// </summary>
        public byte[,] HeightMap { get; set; } = new byte[9, 9];

        /// <summary>
        /// terrain map
        /// 0x054 terrain : Ptr32 Uint2B
        /// </summary>
        public ushort[,] Terrain { get; set; } = new ushort[9, 9];

        /// <summary>
        /// 0x058 vertex_array : CVertexArray
        /// </summary>
        public VertexArray VertexArray { get; set; } = new VertexArray();

        /// <summary>
        /// 0x080 polygons
        /// </summary>
        public List<Polygon> Polygons { get; set; } = new List<Polygon>();

        /// <summary>
        /// 0x084 num_surface_strips : Uint4B
        /// </summary>
        public uint NumSurfaceStrips => (uint)SurfaceStrips.Count;

        /// <summary>
        /// 0x088 surface_strips : Ptr32 CSurfaceTriStrips
        /// </summary>
        public List<SurfaceTriStrip> SurfaceStrips { get; set; } = new List<SurfaceTriStrip>();

        /// <summary>
        /// 0x08C block_surface_index : uint4B
        /// </summary>
        public uint BlockSurfaceIndex { get; set; }

        /// <summary>
        /// deviation from client: 2-D array instead of single array with calculated index
        /// 0x090 lcell : Ptr32 CLandCell
        /// </summary>
        public LandblockCell[,] LandblockCells { get; set; } = new LandblockCell[8, 8];

        /// <summary>
        /// direction of the triangle cut across this landblock.
        /// 0x094 SWtoNEcut : Ptr32 Int4B
        /// </summary>
        public bool[,] SouthWestToNorthEastCut { get; set; } = new bool[8, 8];

        /// <summary>
        /// 0x098 block_cord : SqCoord
        /// </summary>
        public SqCoord BlockCoord;

        /// <summary>
        /// 0x0a0 block_frame : Frame
        /// </summary>
        public Frame BlockFrame;

        /// <summary>
        /// 0x0e0 max_zval : Float
        /// </summary>
        public float MaxZ;

        /// <summary>
        /// 0x034 min_zval : Float
        /// </summary>
        public float MinZ;

        /// <summary>
        /// 0x0e8 dyn_objs_init_done : Int4B
        /// </summary>
        public bool DynamicObjectsInitialized;

        /// <summary>
        /// 0x0ec lbi_exists : Int4B
        /// </summary>
        public bool LandblockInfoExists;

        /// <summary>
        /// heading index into LandDefs.Direction array
        /// 0x0f0 dir : LandDefs::Direction
        /// </summary>
        public Direction Direction;

        /// <summary>
        /// 0x0f4 closest : SqCoord
        /// </summary>
        public SqCoord Closest;

        /// <summary>
        /// 0x0fc in_view : BoundingType
        /// </summary>
        public Enum.BoundingType InView;

        /// <summary>
        /// 0x100 lbi
        /// </summary>
        public LandblockInfo LandblockInfo;

        /// <summary>
        /// 0x104 num_static_objects : Uint4B
        /// </summary>
        public uint NumStaticObjects => (uint)StaticObjects.Count;

        /// <summary>
        /// 0x108 static_objects : DArray&lt;CPhysicsObj *&gt;
        /// </summary>
        public List<PhysicsObject> StaticObjects { get; set; } = new List<PhysicsObject>();

        /// <summary>
        /// 0x118 num_buildings : Uint4B
        /// </summary>
        public uint NumBuildings => (uint)Buildings.Count;

        /// <summary>
        /// 0x11c buildings : Ptr32 Ptr32 CBuildingObj
        /// </summary>
        public List<Building> Buildings { get; set; } = new List<Building>();

        /// <summary>
        /// 0x120 stab_num : Uint4B
        /// </summary>
        public uint StabNum => (uint)StabList.Count;

        /// <summary>
        /// 0x124 stablist : Ptr32 Uint4B
        /// </summary>
        public List<uint> StabList = new List<uint>();

        /// <summary>
        /// 0x128 draw_array : Ptr32 Ptr32 CLandCell
        /// </summary>
        public List<LandblockCell> DrawArray = new List<LandblockCell>();
        
        /// <summary>
        /// 0x128 draw_array : Ptr32 Ptr32 CLandCell
        /// </summary>
        public uint DrawArraySize => (uint)DrawArray.Count;

        /// <summary>
        /// //----- (0052F310) --------------------------------------------------------
        /// int __thiscall CLandBlock::UnPack(CLandBlock*this, void** addr, unsigned int size)
        /// acclient.c 351521
        /// </summary>
        public static Landblock Unpack(BinaryReader reader, DatReader datReader)
        {
            Landblock lb = new Landblock();

            lb.Id = reader.ReadUInt32();
            uint hasLbi = reader.ReadUInt32();
            lb.LandblockInfoExists = (hasLbi != 0);

            // this is an implied cast into LandblockStruct
            lb.Unpack(reader);

            if (lb.LandblockInfoExists)
            {
                uint infoFile = (lb.Id & 0xFFFF0000) | 0xFFFE;
                lb.LandblockInfo = datReader.Unpack<LandblockInfo>(infoFile);
            }
            return lb;

        }

        /// <summary>
        /// //----- (00530A40) --------------------------------------------------------
        /// void __thiscall CLandBlock::init_static_objs(CLandBlock *this, LongNIValHash(unsigned long) *hash)
        /// acclient.c 352787
        /// </summary>
        private void InitializeStaticObjects()
        {
            if (StaticObjects.Count > 0)
                return; // already initialized

            if (LandblockInfo == null)
                return;

            for (int i = 0; i < LandblockInfo.NumObjects; i++)
            {
                var obj = PhysicsObject.MakeObject(LandblockInfo.ObjectIds[i], 0, false);
                if (obj != null)
                {
                    Position p = new Position();
                    p.CellId = Id;
                    p.Frame = new Frame(LandblockInfo.ObjectFrames[i]); // we have classes here instead of a struct, have to new it up
                    
                    Vector3 loc = p.Frame.Origin;
                    uint cellId = Id;

                    bool mapped = LandDefs.AdjustToOutside(ref cellId, ref loc);
                    var cell = GetLandCell(mapped ? cellId : 0);

                    if (cell != null)
                    {
                        obj.AddObjectToCell(cell, new Frame(p.Frame));
                        AddStaticObject(obj);
                    }
                    else
                    {
                        throw new Exception($"Unable to place static object id {LandblockInfo.ObjectIds[i]}");
                    }
                }
                else
                {
                    throw new Exception($"Unable to load static object id {LandblockInfo.ObjectIds[i]}");
                }
            }

            for (int x = 0; x < SideCellCount; x++)
            {
                for (int y = 0; y < SideCellCount; y++)
                {
                    // apply restrictions ~ client line 352878
                }
            }

            // client checks a constant here, i'm skipping it
        }

        /// <summary>
        /// //----- (00530460) --------------------------------------------------------
        /// void __thiscall CLandBlock::get_land_scenes(CLandBlock *this)
        /// acclient.c 352530
        /// </summary>
        private void GetLandScenes()
        {
            // this implementation was taken largely from our older DerethPhysics implementation rather than
            // a straight RE of the client code.  Copy/pasta is much easier than REing all this.

            if (PortalDatReader.Current.CurrentRegion == null || SideVertexCount < 1)
                    return;

            var region = PortalDatReader.Current.CurrentRegion;
            var blockX = (Id >> 24) * 8;
            var blockY = (Id >> 16 & 0xFF) * 8;
            
            for (uint x = 0; x < SideVertexCount; x++)
            {
                for (uint y = 0; y < SideVertexCount; y++)
                {
                    uint cellX = blockX + x;
                    uint cellY = blockY + y;

                    uint rawTerrain = Terrain[x, y];
                    uint terrainType = (rawTerrain >> 2) & 0x1f;
                    uint terrainSceneType = (rawTerrain >> 11) & 0x1f;

                    var terrain = region.TerrainInfo.TerrainTypes[(int)terrainType];
                    var sceneInfo = terrain.SceneTypes[(int)terrainSceneType];
                    var scenes = sceneInfo.Scenes;
                    
                    uint subScene = (uint)(SceneryPsuedoRandom(cellX, cellY, Prng.Seeds.SceneSubIndex) * scenes.Count);
                    if (subScene >= scenes.Count)
                        subScene = 0;

                    if (scenes.Count == 0 || subScene > (scenes.Count + 1))
                        continue;

                    uint sceneId = scenes[(int)subScene];
                    if (sceneId == 0)
                        continue;

                    Scene scene = PortalDatReader.Current.Unpack<Scene>(sceneId);

                    double sceneRot = SceneryPsuedoRandom(cellX, cellY, Prng.Seeds.SceneRotation);

                    for (uint i = 0; i < scene.Objects.Count; i++)
                    {
                        if (i >= 300) // max objects
                            break;
                        
                        var sceneObject = scene.Objects[(int)i];
                        
                        if (sceneObject.WeenieObject > 0)
                            continue;

                        double freq = SceneryPsuedoRandom(cellX, cellY, Prng.Seeds.Frequency + i);
                        if (sceneObject.Freq <= freq)
                        {
                            log.Debug($"{Id} - scenery eliminated as noise");
                            continue;
                        }

                        Vector3 loc = Vector3.Zero;
                        sceneObject.Place(cellX, cellY, i, ref loc, sceneRot);
                        loc.X += x * 24.0f;
                        loc.Y += y * 24.0f;
                        
                        if (!(loc.X >= 0f && loc.Y >= 0f && loc.X < 192f && loc.Y < 192f && !IsRoad(loc)))
                        {
                            log.Debug($"{Id} - scenery out of bounds or on road.");
                            continue;
                        }

                        Position p = new Position();
                        p.CellId = Id;
                        p.Frame.Origin = loc;

                        p.CellId = p.GetOutsideCellId();
                        var cell = GetLandCell(p.CellId);

                        Polygon walkable = null;
                        if (cell.HasBuilding())
                        {
                            log.Debug($"{Id} - scenery overlaps building.");
                            continue;
                        }

                        if (!cell.FindTerrainPolygon(loc, ref walkable))
                        {
                            log.Debug($"{Id} - scenery could not be placed on polygon.");
                            continue;
                        }

                        if (!sceneObject.CheckSlope(walkable.Plane.Normal.Z))
                        {
                            log.Debug($"{Id} - polygon slope outside tolerance.");
                            continue;
                        }

                        var surfacePlane = walkable.Plane;
                        surfacePlane.SetHeight(ref loc);
                        Frame placementFrame = null;

                        if (sceneObject.Alignment != 0)
                            placementFrame = sceneObject.Align(surfacePlane, loc);
                        else
                            placementFrame = sceneObject.GetRandomPlacement(cellX, cellY, i, ref loc);

                        var physObj = PhysicsObject.MakeObject(sceneObject.ObjectId, 0, false);
                        physObj.SetInitialFrame(placementFrame);

                        if (!physObj.IsWithinBlock())
                        {
                            log.Debug($"{Id} - scenery outside bounds after randomization");
                            continue;
                        }

                        physObj.AddObjectToCell(cell, placementFrame);
                        var scale = (float)sceneObject.RandomScale(cellX, cellY, i);
                        physObj.SetScaleStatic(scale);
                        AddStaticObject(physObj);
                    }
                }
            }
        }

        public static double SceneryPsuedoRandom(uint cellX, uint cellY, uint seed)
        {
            uint ival = 0x6C1AC587 * cellY - 0x421BE3BD * cellX - seed * (0x5111BFEF * cellY * cellX + 0x70892FB7);

            return ((double)ival / (double)uint.MaxValue);
        }

        /// <summary>
        /// //----- (0052FFF0) --------------------------------------------------------
        /// int __thiscall CLandBlock::on_road(CLandBlock *this, AC1Legacy::Vector3 *obj_vector)
        /// acclient.c 352238
        /// </summary>
        public bool IsRoad(Vector3 loc)
        {
            const float roadHalfWidth = 5.0f;
            const float diagonalTolerance = 7.07f; // 5.0f * sqrt(2)

            Vector3 copy = loc;
            float max = 19f;
            int tileX = (int)(copy.X / 24.0f);
            int tileY = (int)(copy.Y / 24.0f);

            float xf = ((loc.X / 24.0f) % 1f) * 24;
            float yf = ((loc.Y / 24.0f) % 1f) * 24;

            bool r0 = IsRoadTile(tileX, tileY);
            bool r1 = IsRoadTile(tileX + 1, tileY);
            bool r2 = IsRoadTile(tileX, tileY + 1);
            bool r3 = IsRoadTile(tileX + 1, tileY + 1);

            // no roads anywhere.  most common case
            if (!r0 && !r1 && !r2 && !r3)
                return false;

            // all roads.  rare, but short circuit because it's easy
            if (r0 && r1 && r2 && r3)
                return true;

            // x,y
            if (r0)
            {
                if (r1 && (yf < roadHalfWidth)) // road along the bottom row
                    return true;
                if (r2 && (xf < roadHalfWidth)) // road up the left side
                    return true;

                // this check is technically not in the client code as i was able to decipher it.  however,
                // it yields results consitent with what is seen when logged in.  i used the diagonal road
                // from rithwic to eastham to test extensively
                if (r3 && Math.Abs(xf - yf) < diagonalTolerance) // up the middle of the diagonal 
                    return true;

                if ((xf < roadHalfWidth) && (yf < roadHalfWidth)) // this is part of a diagonal opposite our search direction
                    return true;

                return false; // this block is a road, but the points on it are not
            }

            // x+1, y
            if (r1)
            {
                // no sense in checking r0, already covered r1 && r0

                if (r3 && (xf > (max))) // up the right side
                    return true;

                if (r2 && xf > max && yf > max) // diagonal, and we're in the corner of it
                    return true;

                if ((xf > max) && (yf < roadHalfWidth)) // part of a diagonal not in this grid
                    return true;
            }

            // x, y+1
            if (r2)
            {
                // no sense in checking r0, as r0 && r2 and r1 && r2 are already coverd

                if (r3 && (yf > max)) // road across the top
                    return true;

                if ((xf < roadHalfWidth) && (yf > max)) // part of a diagonal not in this grid
                    return true;
            }

            // x+1, y+1
            if (r3)
            {
                if ((xf > max) && (yf > max)) // part of a diagonal not in this grid
                    return true;
            }

            return false;
        }

        /// <summary>
        /// no client reference.  just wrapping the "& 0x3" in something simple as it's done a lot
        /// </summary>
        private bool IsRoadTile(int tileX, int tileY)
        {
            return (Terrain[tileX, + tileY] & 0x3) > 0;
        }

        /// <summary>
        /// //----- (0052FD80) --------------------------------------------------------
        /// void __thiscall CLandBlock::init_buildings(CLandBlock *this)
        /// acclient.c 352114
        /// </summary>
        private void InitializeBuildings()
        {
            uint maxStab = 0;

            // i THINK this is a basic dungeon check
            if (SideCellCount != 8)
                return;

            if (LandblockInfo == null)
                return;

            for (int i = 0; i < LandblockInfo.NumBuildings; i++)
            {
                var bInfo = LandblockInfo.Buildings[i];
                var b = Building.MakeBuilding(bInfo.Id, bInfo.Portals, bInfo.NumLeaves);
                Position p = new Position() { CellId = Id, Frame = new Frame(bInfo.Frame) };
                Vector3 loc = p.Frame.Origin;
                uint cellId = Id;
                bool adjusted = LandDefs.AdjustToOutside(ref cellId, ref loc);
                p.Frame.Origin = loc; // copy it back to the frame

                var cell = GetLandCell(adjusted ? cellId : 0);
                if (cell != null)
                {
                    p.CellId = cellId;
                    b.SetInitialFrame(p.Frame);
                    b.AddToCell((SortCell)cell);
                    Buildings.Add(b);
                    b.AddToStabList(StabList, ref maxStab);
                }
                else
                {
                    throw new Exception($"Unable to load or place building id {LandblockInfo.Buildings[i].Id}");
                }
            }
        }

        /// <summary>
        /// client says the return is an int, but it's really a pointer.  in practice, it is cast as
        /// an ObjectCell, so we'll return that for now.  suspect that it should be LandblockCell.
        /// //----- (0052FD20) --------------------------------------------------------
        /// signed int __thiscall CLandBlock::get_landcell(CLandBlock *this, unsigned int cell_id)
        /// acclient.c 352098
        /// </summary>
        public LandblockCell GetLandCell(uint cellId)
        {
            int x = 0;
            int y = 0;

            LandDefs.GidToLCoord(cellId, ref x, ref y);

            var cell = LandblockCells[x & 7, y & 7];

            if (cell.Id == cellId)
                return cell;

            return null;
        }

        /// <summary>
        /// //----- (0052F910) --------------------------------------------------------
        /// void __thiscall CLandBlock::add_static_object(CLandBlock *this, CPhysicsObj *object)
        /// acclient.c 351857
        /// </summary>
        public void AddStaticObject(PhysicsObject physicsObject)
        {
            StaticObjects.Add(physicsObject);
        }
    }
}

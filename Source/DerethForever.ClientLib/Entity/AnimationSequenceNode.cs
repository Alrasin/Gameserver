/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AnimSequenceNode in the client
    /// </summary>
    public class AnimationSequenceNode
    {
        /// <summary>
        /// +0x000 __VFN_table : Ptr32
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// 0x004 dllist_next : Ptr32 DLListData
        /// </summary>
        public AnimationSequenceNode Next { get; set; }

        /// <summary>
        /// 0x008 dllist_prev : Ptr32 DLLIstData
        /// </summary>
        public AnimationSequenceNode Previous { get; set; }

        /// <summary>
        /// 0x00c anim : Ptr32 Animation
        /// </summary>
        public Animation Animation { get; set; }

        /// <summary>
        /// +0x010 framerate        : Float
        /// </summary>
        public float Framerate { get; set; }

        /// <summary>
        /// +0x014 low_frame        : Int4B
        /// </summary>
        public int LowFrame { get; set; }

        /// <summary>
        /// +0x018 high_frame       : Int4B
        /// </summary>
        public int HighFrame { get; set; }

        /// <summary>
        /// ----- (00525F40) --------------------------------------------------------
        /// int __thiscall AnimSequenceNode::UnPack(AnimSequenceNode*this, void** addr, unsigned int size)
        /// acclient.c 341257
        /// </summary>
        public static AnimationSequenceNode Unpack(BinaryReader reader)
        {
            AnimationSequenceNode asq = new AnimationSequenceNode();
            asq.Id = reader.ReadUInt32();
            asq.LowFrame = reader.ReadInt32();
            asq.HighFrame = reader.ReadInt32();
            asq.Framerate = reader.ReadSingle();
            throw new NotImplementedException();
            // return asq;
        }

        /// <summary>
        /// //----- (00525C40) --------------------------------------------------------
        /// int __thiscall AnimSequenceNode::get_part_frame(AnimSequenceNode *this, int fn)
        /// acclient.c 340996
        /// </summary>
        public AnimationFrame GetPartFrame(float value)
        {
            if (value > 0 && value < Animation.PartFrames.Count)
                return Animation.PartFrames[(int)value];
            return null;
        }

        /// <summary>
        /// ----- (00525CB0) --------------------------------------------------------
        /// double __thiscall AnimSequenceNode::get_ending_frame(AnimSequenceNode*this)
        /// acclient.c 341028
        /// </summary>
        public float GetEndingFrame()
        {
            if (Framerate > 0.0f)
                return HighFrame + 1 - Constants.TOLERANCE;
            return LowFrame;
        }

        /// <summary>
        /// ----- (00525BE0) --------------------------------------------------------
        /// void __thiscall AnimSequenceNode::multiply_framerate(AnimSequenceNode*this, float multiplier)
        /// acclient.c 349968
        /// </summary>
        public void MultiplyFrameRate(float multiplier)
        {
            if (multiplier < 0.0f)
            {
                int hold = LowFrame;
                LowFrame = HighFrame;
                HighFrame = hold;
            }
            Framerate *= multiplier;
        }

        /// <summary>
        /// ----- (00525C80) --------------------------------------------------------
        /// double __thiscall AnimSequenceNode::get_starting_frame(AnimSequenceNode*this)
        /// acclient.c 341016
        /// </summary>
        public float GetStartingFrame()
        {
            if (Framerate < 0.0f)
                return (HighFrame + 1) - Constants.TOLERANCE;
            return LowFrame;
        }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    public class PhysicsScript
    {
        // I think this is all memory management stuff for the old smart array and we may not need it - needs review
        // +0x000 __VFN_table : Ptr32
        // +0x004 m_dataCategory   : Uint4B
        // +0x008 m_bLoaded        : Bool
        // +0x010 m_timeStamp      : Float
        // +0x018 m_pNext          : Ptr32 DBObj
        // +0x01c m_pLast          : Ptr32 DBObj
        // +0x020 m_pMaintainer    : Ptr32 DBOCache
        // +0x024 m_numLinks       : Int4B
        // +0x028 m_DID            : IDClass<_tagDataID,32,0>
        // +0x02c m_AllowedInFreeList : Bool
        // +0x030 __VFN_table : Ptr32


        public uint Id { get; set; }

        /// <summary>
        /// +0x038 script_data      : OldSmartArray<PhysicsScriptData *>
        /// </summary>
        public List<PhysicsScriptData> ScriptData { get; set; }

        public PhysicsScript()
        {
            ScriptData = new List<PhysicsScriptData>();
        }

        public static PhysicsScript Unpack(BinaryReader reader)
        {
            PhysicsScript obj = new PhysicsScript();
            obj.Id = reader.ReadUInt32();

            uint numScriptData = reader.ReadUInt32();
            for (uint i = 0; i < numScriptData; i++)
                obj.ScriptData.Add(PhysicsScriptData.Unpack(reader));
            return obj;
        }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AmbientSTBDesc in the client
    /// </summary>
    public class AmbientSoundTable
    {
        /// <summary>
        /// private because unpack is the only valid source of this class
        /// </summary>
        private AmbientSoundTable()
        {
        }

        /// <summary>
        /// 0x000 stb_id : IDClass
        /// </summary>
        public uint SoundTableId;

        /// <summary>
        /// 0x004 stb_not_found : Int4B
        /// </summary>
        public bool NotFound;

        /// <summary>
        /// 0x008 ambient_sounds : AC1Legacy::SmartArray(AmbientSoundDesc *)
        /// </summary>
        public List<AmbientSound> AmbientSounds = new List<AmbientSound>();

        /// <summary>
        /// 0x014 sound_table : Ptr32 CSoundTable
        /// </summary>
        public uint SoundTable;

        /// <summary>
        /// 0x018 play_count : Uint4B
        /// </summary>
        public uint PlayCount;

        /// <summary>
        /// //----- (005518F0) --------------------------------------------------------
        /// int __thiscall AmbientSTBDesc::UnPack(AmbientSTBDesc *this, void **addr, unsigned int *size)
        /// acclient.c 384535
        /// </summary>
        public static AmbientSoundTable Unpack(BinaryReader reader)
        {
            AmbientSoundTable s = new AmbientSoundTable();

            s.SoundTableId = reader.ReadUInt32();

            uint num = reader.ReadUInt32();
            for (uint i = 0; i < num; i++)
                s.AmbientSounds.Add(AmbientSound.Unpack(reader));

            return s;
        }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <inheritdoc />
    /// <summary>
    /// This reads an "indoor" cell from the client_cell.dat. This is mostly dungeons, but can also be a building interior.
    /// An EnvironmentCell is designated by starting 0x0100 (whereas all landcells are in the 0x0001 - 0x003E range.
    /// CEnvCell from the client
    /// </summary>
    public class EnvironmentCell : ObjectCell
    {
        /// <summary>
        /// +0x0f8 num_surfaces     : Uint4B
        /// </summary>
        public int NumberSurfaces { get; set; }

        /// <summary>
        /// +0x108 num_portals      : Uint4B
        /// </summary>
        public uint NumberPortals
        {
            get { return (uint) Portals.Count; }
        }

        /// <summary>
        /// +0x10c portals          : Ptr32 CCellPortal
        /// </summary>
        public List<CellPortal> Portals { get; set; }

        /// <summary>
        /// +0x110 num_static_objects : Uint4B
        /// </summary>
        public uint NumberStaticObjects
        {
            get { return (uint)StaticObjectIDs.Count; }
        }

        /// <summary>
        /// +0x114 static_object_ids : Ptr32 IDClass<_tagDataID,32,0>
        /// </summary>
        public List<uint> StaticObjectIDs { get; set; }

        /// <summary>
        /// +0x118 static_object_frames : Ptr32 Frame
        /// </summary>
        public List<AnimationFrame> StaticObjectFrames { get; set; }

        /// <summary>
        /// +0x11c static_objects   : Ptr32 Ptr32 CPhysicsObj
        /// </summary>
        public List<PhysicsObject> StaticObjects { get; set; }

        /// <summary>
        /// +0x120 light_array      : Ptr32 RGBColor
        /// We probably don't need this for the server
        /// </summary>
        public List<ushort> LightArray { get; set; }

        /// <summary>
        /// +0x124 incell_timestamp : Int4B
        /// </summary>
        public int InCellTimestamp { get; set; }

        /// <summary>
        /// I am a bit fuzzy here they pass in a double pointer to the unpack method
        /// pack_bitfield = *(_DWORD *)*addr; - it is then used as a flag field in several operations.
        /// acclient.c 349193
        /// </summary>
        public uint Flags { get; set; }

        // 0x08000000 textures (which contains degrade/quality info to reference the specific 0x06000000 graphics)
        public List<uint> Textures { get; set; } = new List<uint>();

        // the 0x0D000000 model of the pre-fab dungeon block
        public uint EnvironmentId { get; set; }

        /// <summary>
        /// +0x100 structure        : Ptr32 CCellStruct
        /// </summary>
        public ushort CellStructure { get; set; }

        /// <summary>
        /// +0x10c portals          : Ptr32 CCellPortal
        /// </summary>
        public List<CellPortal> CellPortals { get; set; } = new List<CellPortal>();

        public List<ushort> VisibleBlocks { get; set; } = new List<ushort>();

        /// <summary>
        /// ----- (0052D470) --------------------------------------------------------
        /// int __thiscall CEnvCell::UnPack(CEnvCell *this, void **addr, unsigned int size)
        /// TODO: Need to find out what is passed into unpack in the double pointer.
        /// acclient.c 349135
        /// </summary>
        public static EnvironmentCell Unpack(BinaryReader reader)
        {
            EnvironmentCell c = new EnvironmentCell();
            c.Id = reader.ReadUInt32();
            c.Flags = reader.ReadUInt32();

            uint extraCellId = reader.ReadUInt32();

            byte numTextures = reader.ReadByte();
            byte numPortals = reader.ReadByte();
            ushort numVisibleBlocks = reader.ReadUInt16();

            for (uint i = 0; i < numTextures; i++)
                c.Textures.Add(0x08000000u + reader.ReadUInt16());

            c.EnvironmentId = (0x0D000000u + reader.ReadUInt16());
            c.CellStructure = reader.ReadUInt16();

            c.Position = Position.UnpackAsFrame(reader, c.Id);

            for (uint i = 0; i < numPortals; i++)
            {
                // reference: 349209 of acclient.c
                CellPortal cp = CellPortal.Unpack(reader, c.Id & 0xFFFF0000);
                c.CellPortals.Add(cp);
            }

            for (uint i = 0; i < numVisibleBlocks; i++)
            {
                c.VisibleBlocks.Add(reader.ReadUInt16());
            }

            if ((c.Flags & 2) != 0)
            {
                uint numObjects = reader.ReadUInt32();

                for (uint i = 0; i < numObjects; i++)
                {
                    c.Stabs.Add(Stab.Unpack(reader, c.Id));
                }
            }

            if ((c.Flags & 8) != 0)
            {
                c.RestrictionObject = reader.ReadUInt32();
            }

            // client line 349382
            // *((_DWORD *)v5 + 72) = operator new[](12 * *(_DWORD *)(*((_DWORD *)v5 + 64) + 36));
            // as best I can tell, this is myLandblock_ allocation

            c.CalcClipPlanes();

            return c;
        }

        /// <summary>
        /// //----- (0052C100) --------------------------------------------------------
        /// signed int __thiscall CEnvCell::find_collisions(CEnvCell *this, CTransition *transition)
        /// acclient.c 347810
        /// </summary>
        public override TransitionState FindCollisions(Transition trans)
        {
            // this was the vftable[5] call
            TransitionState result = FindEnvironmentCollisions(trans);

            if (result == TransitionState.Ok)
                result = FindObjectCollisions(trans);

            return result;
        }

        /// <summary>
        /// //----- (0052C130) --------------------------------------------------------
        /// signed int __thiscall CEnvCell::find_env_collisions(CEnvCell *this, CTransition *transition)
        /// acclient.c 347823
        /// </summary>
        public override TransitionState FindEnvironmentCollisions(Transition trans)
        {
            // TODO implement EnvironmentCell.FindEnvironmentCollisions
            // note, this is where we start involving BSP trees and other fun stuff

            return TransitionState.Ok;
        }

        /// <summary>
        /// //----- (0052D0F0) --------------------------------------------------------
        /// void __thiscall CEnvCell::calc_clip_planes(CEnvCell *this)
        /// acclient.c 348871
        /// </summary>
        private void CalcClipPlanes()
        {
            if (ClipPlanes == null)
            {
                ClipPlanes = new List<ClipPlane>();
            }

            if (NumberPortals <= 0)
                return;
            foreach (CellPortal portal in Portals)
            {
                ClipPlane cp = new ClipPlane
                {
                    Plane = portal.Portal.Plane
                };
                if (portal.PortalSide)
                    cp.Side = Sidedness.Positive;
                else
                    cp.Side = Sidedness.Negative;
                ClipPlanes.Add(cp);
            }
        }
    }
}

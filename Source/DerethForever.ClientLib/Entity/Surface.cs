/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CSurface in the client
    /// </summary>
    public class Surface : DatabaseObject, GraphicsResource
    {
        /// <summary>
        /// private because all valid instances come from Unpack (for now)
        /// </summary>
        private Surface()
        {
        }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x008 m_bIsLost : Bool
        /// </summary>
        public bool IsLost { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x010 m_TimeUsed : Float
        /// </summary>
        public float TimeUsed { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x018 m_FrameUsed : Uint4B
        /// </summary>
        public uint FrameUsed { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x01c m_bIsThrashable : Bool
        /// </summary>
        public bool IsThrashable { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x01d m_AutoRestore : Bool
        /// </summary>
        public bool AutoRestore { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x020 m_nResourceSize
        /// </summary>
        public uint ResourceSize { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x024 m_ListIndex
        /// </summary>
        public uint ListIndex { get; set; }

        /// <summary>
        /// 0x058 type : Uint4B
        /// </summary>
        public SurfaceType Type { get; set; }

        /// <summary>
        /// 0x05c handler : SurfaceHandlerEnum
        /// </summary>
        public SurfaceHandler Handler { get; set; }

        /// <summary>
        /// 0x060 color_value : Uint4B
        /// </summary>
        public uint ColorValue { get; set; }

        /// <summary>
        /// 0x064 solid_index : Int4B
        /// </summary>
        public int SolidIndex { get; set; }

        /// <summary>
        /// 0x068 indexed_texture_id : IDClass
        /// </summary>
        public uint IndexedTextureId { get; set; }

        /// <summary>
        /// 0x06c base1map : Ptr32 ImgTex
        /// </summary>
        public object BaseMap { get; set; }

        /// <summary>
        /// 0x070 base1pal : Ptr32 Palette
        /// </summary>
        public Palette BasePalette { get; set; }

        /// <summary>
        /// 0x074 translucency : Float
        /// </summary>
        public float Translucency { get; set; }

        /// <summary>
        /// 0x078 Luminosity : Float
        /// </summary>
        public float Luminosity { get; set; }

        /// <summary>
        /// 0x07c diffuse : Float
        /// </summary>
        public float Diffuse { get; set; }

        /// <summary>
        /// 0x080 orig_texture_id : IDClass
        /// </summary>
        public uint OriginalTextureId { get; set; }

        /// <summary>
        /// 0x084 orig_palette_id : IDClass
        /// </summary>
        public uint OriginalPaletteId { get; set; }

        /// <summary>
        /// 0x088 orig_luminosity : Float
        /// </summary>
        public float OriginalLuminosity { get; set; }

        /// <summary>
        /// 0x08c orig_diffuse : Float
        /// </summary>
        public float OriginalDiffuse { get; set; }

        /// <summary>
        /// CSurface doesn't have a literal unpack method.  we're adding one for 
        /// consistency purpose.  logic here was REed from CSurface::Serialize
        /// 
        /// //----- (005365F0) --------------------------------------------------------
        /// void __thiscall CSurface::Serialize(CSurface *this, Archive *io_archive)
        /// acclient.c 358266
        /// </summary>
        public static Surface Unpack(BinaryReader reader)
        {
            Surface s = new Surface();

            s.Type = (SurfaceType)reader.ReadUInt32();

            if (s.Type.HasFlag(SurfaceType.BaseImage) && s.Type.HasFlag(SurfaceType.BaseClipMap))
            {
                s.OriginalTextureId = reader.ReadUInt32();
                s.OriginalPaletteId = reader.ReadUInt32();
            }
            else
            {
                s.ColorValue = reader.ReadUInt32();
            }

            s.Translucency = reader.ReadSingle();
            s.Luminosity = reader.ReadSingle();
            s.Diffuse = reader.ReadSingle();

            return s;
        }
    }
}

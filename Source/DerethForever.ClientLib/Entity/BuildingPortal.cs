/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CBldPortal from the client
    /// </summary>
    public class BuildingPortal
    {
        public CellPortalState Flags { get; set; }

        /// <summary>
        ///   +0x000 portal_side      : Int4B
        /// </summary>
        public bool PortalSide
        {
            get { return Flags.HasFlag(CellPortalState.PortalSide); }
        }

        /// <summary>
        /// +0x004 other_cell_id    : Uint4B
        /// </summary>
        public uint OtherCellId { get; set; }

        /// <summary>
        /// +0x008 other_portal_id  : Int4B
        /// </summary>
        public uint OtherPortalId { get; set; }

        /// <summary>
        /// +0x00c exact_match      : Int4B
        /// </summary>
        public bool ExactMatch
        {
            get { return Flags.HasFlag(CellPortalState.ExactMatch); }
        }

        /// <summary>
        /// +0x014 stab_list        : Ptr32 Uint4B
        /// </summary>
        public List<uint> StabList { get; set; } = new List<uint>();

        /// <summary>
        /// +0x010 num_stabs        : Uint4B
        /// </summary>
        public uint NumStabs
        {
            get { return (uint)(StabList?.Count ?? 0); }
        }

        /// <summary>
        /// +0x018 sidedness        : Float
        /// </summary>
        public float Sidedness { get; set; }

        /// <summary>
        /// //----- (0053BC40) --------------------------------------------------------
        /// int __thiscall CBldPortal::UnPack(CBldPortal *this, unsigned int block_mask, void **addr, unsigned int size
        /// acclient.c 362499
        /// </summary>
        public static BuildingPortal Unpack(BinaryReader reader, uint blockMask)
        {
            BuildingPortal bp = new BuildingPortal
            {
                Flags = (CellPortalState)reader.ReadUInt16(),
                OtherCellId = blockMask + reader.ReadUInt16(),
                OtherPortalId = blockMask + reader.ReadUInt16()
            };

            uint numStabs = reader.ReadUInt16();
            for (int i = 0; i < numStabs; i++)
                bp.StabList.Add(blockMask + reader.ReadUInt16());

            // align to dword boundary
            if (numStabs % 2 != 0)
                reader.ReadBytes(2);

            return bp;
        }

        /// <summary>
        /// //----- (0053BB60) --------------------------------------------------------
        /// void __thiscall CBldPortal::add_to_stablist(CBldPortal *this, unsigned int **block_stab_list, unsigned int *max_size, unsigned int *stab_num)
        /// acclient.c 362423
        /// </summary>
        public void AddToStabList(List<uint> stabList, ref uint maxStabs)
        {
            // TODO: verify and test this and build a unit test
            // All of this code in the client may be unnecessary - this may be c++
            // memory stuff - it looked to me that all I needs was to add the second list
            // to the end of the first
            // question for Behemoth.
            foreach (uint t in stabList)
            {
                StabList.Add(t);
            }
        }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.Linq;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class MotionState
    {
        /// <summary>
        /// +0x000 style            : Uint4B
        /// </summary>
        public MotionCommand Style { get; set; }

        /// <summary>
        /// +0x004 substate         : Uint4B
        /// </summary>
        public MotionCommand SubState { get; set; }

        /// <summary>
        /// +0x008 substate_mod     : Float
        /// </summary>
        public float SubStateMod { get; set; }

        /// <summary>
        ///    +0x00c modifier_head    : Ptr32 MotionList
        /// </summary>
        public LinkedList<Motion> Modifiers { get; set; }

        /// <summary>
        /// +0x010 action_head      : Ptr32 MotionList
        /// </summary>
        public LinkedList<Motion> Actions { get; set; }

        // ***********************  RE: Notes *************************
        // The following methods in the client are just direct add, delete and update from our lists.   I am leaving those out
        // add_action, add_modifier_no_check, clear_actions, clear_modifiers, remove_action_head, remove_modifier all have straight forward
        // c# supported equivalents.   If those lists should be private, then we could add the wrapper methods if needed.

        /// <summary>
        /// ----- (00525FD0) --------------------------------------------------------
        ///void __thiscall MotionState::MotionState(MotionState*this)
        /// acclient.c 341303
        /// </summary>
        public MotionState()
        {
            SubStateMod = Constants.DefaultSpeed;
            Modifiers = new LinkedList<Motion>();
            Actions = new LinkedList<Motion>();
        }

        /// <summary>
        /// ----- (00526340) --------------------------------------------------------
        /// int __thiscall MotionState::add_modifier(MotionState*this, unsigned int modifier, float speed_mod)
        /// acclient.c 341619
        /// </summary>
        public bool AddModifier(MotionCommand modifier, float speedMod)
        {
            if (SubState == modifier)
                return false;

            if (Modifiers.Any(mod => mod.Id == modifier))
                return false;

            Modifiers.AddFirst(new Motion(modifier, speedMod));
            return true;
        }
    }
}

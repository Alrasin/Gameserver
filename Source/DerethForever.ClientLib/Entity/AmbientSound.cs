/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AmbientSoundDesc in the client
    /// </summary>
    public class AmbientSound
    {
        /// <summary>
        /// private because unpack is the only valid source of this class
        /// </summary>
        private AmbientSound()
        {
        }

        /// <summary>
        /// 0x000 stype : SoundType
        /// </summary>
        public Enum.SoundType SoundType;

        /// <summary>
        /// 0x004 is_continuous : Int4B
        /// </summary>
        public bool IsContinuous;

        /// <summary>
        /// 0x008 volume : Float
        /// </summary>
        public float Volume;

        /// <summary>
        /// 0x00c base_chance : Float
        /// </summary>
        public float BaseChance;

        /// <summary>
        /// 0x010 min_rate : Float
        /// </summary>
        public float MinRate;

        /// <summary>
        /// 0x014 max_rate : Float
        /// </summary>
        public float MaxRate;

        /// <summary>
        /// this method does not explicitly exist in the client, but is inlined with 
        /// it's parent's (AmbientSoundTable) unpack, lines 384573 - 384624
        /// </summary>
        public static AmbientSound Unpack(BinaryReader reader)
        {
            AmbientSound s = new AmbientSound();
            
            s.SoundType = (Enum.SoundType)reader.ReadUInt32();
            s.Volume = reader.ReadSingle();
            s.BaseChance = reader.ReadSingle();
            s.MinRate = reader.ReadSingle();
            s.MaxRate = reader.ReadSingle();

            return s;
        }
    }
}

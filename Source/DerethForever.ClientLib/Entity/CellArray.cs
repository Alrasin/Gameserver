/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.Linq;

namespace DerethForever.ClientLib.Entity
{
    public class CellArray
    {
        /// <summary>
        /// +0x000 added_outside    : Int4B
        /// </summary>
        public bool AddedOutside { get; set; } // int in the client

        /// <summary>
        /// +0x004 do_not_load_cells : Int4B
        /// </summary>
        public bool DoNotLoadCells { get; set; } // int in the client

        /// <summary>
        ///  +0x00c cells            : DArray<CELLINFO>
        /// </summary>
        public List<CellInfo> Cells;

        public int NumCells
        {
            get { return Cells?.Count ?? 0; }
        }

        /// <summary>
        /// ----- (006B4FF0) --------------------------------------------------------
        /// void __thiscall CELLARRAY::add_cell(CELLARRAY*this, const unsigned int cell_id, CObjCell *cell)
        /// acclient.c 718896
        /// </summary>
        public void AddCell(uint cellId, ObjectCell cell)
        {
            Cells.Add(new CellInfo
            {
                ObjectCell = cell,
                CellId = cellId
            });
        }

        /// <summary>
        /// ----- (006B4E80) --------------------------------------------------------
        /// void __thiscall CELLARRAY::remove_cell(CELLARRAY*this, const unsigned int index)
        /// acclient.c 718762
        /// </summary>
        /// <param name="cellId"></param>
        public void RemoveCell(uint cellId)
        {
            Cells.Remove(Cells.Single(r => r.CellId == cellId));
        }
    }
}

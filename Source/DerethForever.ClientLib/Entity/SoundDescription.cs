/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CSoundDesc in the client
    /// </summary>
    public class SoundDescription
    {
        /// <summary>
        /// private because Unpack is the only valid source of this class
        /// </summary>
        private SoundDescription()
        {   
        }

        /// <summary>
        /// 0x000 stb_desc : AC1Legacy::SmartArray(AmbientSTBDesc *)
        /// </summary>
        public List<AmbientSoundTable> SoundTables = new List<AmbientSoundTable>();

        /// <summary>
        /// //----- (005028D0) --------------------------------------------------------
        /// int __thiscall CSoundDesc::UnPack(CSoundDesc *this, void **addr, unsigned int *size)
        /// acclient.c 303694
        /// </summary>
        public static SoundDescription Unpack(BinaryReader reader)
        {
            SoundDescription sd = new SoundDescription();

            uint num = reader.ReadUInt32();
            for (uint i = 0; i < num; i++)
                sd.SoundTables.Add(AmbientSoundTable.Unpack(reader));

            return sd;
        }
    }
}

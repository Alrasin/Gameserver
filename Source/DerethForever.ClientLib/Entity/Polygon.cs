/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CPolygon in the client
    /// </summary>
    public class Polygon
    {
        /// <summary>
        /// +0x000 vertices         : Ptr32 Ptr32 CVertex
        /// </summary>
        public List<Vector3> Vertices { get; set; } = new List<Vector3>();

        /// <summary>
        /// +0x004 vertex_ids       : Ptr32 Uint2B
        /// </summary>
        public List<short> VertexIds { get; set; } = new List<short>();

        /// <summary>
        /// +0x008 screen           : Ptr32 Ptr32 Vec2Dscreen
        /// </summary>
        public object Screen { get; set; }

        /// <summary>
        /// in practice, this is the index of this element in the parent Polygon array/list
        /// +0x00c poly_id          : Int2B
        /// </summary>
        public ushort PolygonId { get; set; }

        /// <summary>
        /// +0x00e num_pts          : UChar
        /// </summary>
        public byte NumPts { get; set; }

        /// <summary>
        /// Whether it has that textured/bumpiness to it
        /// +0x00f stippling        : Char
        /// </summary>
        public byte Stippling { get; set; }

        /// <summary>
        /// +0x010 sides_type       : Int4B
        /// </summary>
        public int SidesType { get; set; }

        /// <summary>
        /// +0x014 pos_uv_indices   : Ptr32 Char
        /// </summary>
        public List<byte> PosUVIndices { get; set; } = new List<byte>();

        /// <summary>
        /// +0x018 neg_uv_indices   : Ptr32 Char
        /// </summary>
        public List<byte> NegUVIndices { get; set; } = new List<byte>();

        /// <summary>
        /// +0x01c pos_surface      : Uint2B
        /// </summary>
        public ushort PosSurface { get; set; }

        /// <summary>
        /// +0x01e neg_surface      : Uint2B
        /// </summary>
        public ushort NegSurface { get; set; }

        /// <summary>
        /// field insted of property because struct shenanigans
        /// +0x020 plane            : Plane
        /// </summary>
        public Plane Plane;

        /// <summary>
        /// //----- (00538650) --------------------------------------------------------
        /// int __thiscall CPolygon::UnPack(CPolygon *this, void **addr, unsigned int size)
        /// </summary>
        public static Polygon Unpack(BinaryReader reader)
        {
            Polygon obj = new Polygon();

            obj.PolygonId = reader.ReadUInt16();
            obj.NumPts = reader.ReadByte();
            obj.Stippling = reader.ReadByte();
            obj.SidesType = reader.ReadInt32();
            obj.PosSurface = reader.ReadUInt16();
            obj.NegSurface = reader.ReadUInt16();

            for (short i = 0; i < obj.NumPts; i++)
                obj.VertexIds.Add(reader.ReadInt16());

            if ((obj.Stippling & 4) == 0)
            {
                for (short i = 0; i < obj.NumPts; i++)
                    obj.PosUVIndices.Add(reader.ReadByte());
            }

            if (obj.SidesType == 2 && ((obj.Stippling & 8) == 0))
            {
                for (short i = 0; i < obj.NumPts; i++)
                    obj.NegUVIndices.Add(reader.ReadByte());
            }

            if (obj.SidesType == 1)
            {
                obj.NegSurface = obj.PosSurface;
                obj.NegUVIndices = obj.PosUVIndices;
            }

            return obj;
        }

        /// <summary>
        /// //----- (005383D0) --------------------------------------------------------
        /// void __thiscall CPolygon::make_plane(CPolygon *this)
        /// acclient.c 359628
        /// </summary>
        public void MakePlane()
        {
            // takes 3 vertices to make a plane
            if (Vertices.Count < 3)
                return;

            Plane = Plane.CreateFromVertices(Vertices[0], Vertices[1], Vertices[2]);

            //Vector3 anchorV = Vertices[0];
            //Vector3 normal = Vector3.Zero;

            //for (int i = 1; i < Vertices.Count - 1; i++)
            //{
            //    var v1 = Vertices[i] - anchorV;
            //    var v2 = Vertices[i + 1] - anchorV;

            //    normal = normal + Vector3.Cross(v1, v2);
            //}

            //normal.Normalize();

            //float distance = 0.0f;
            //Vertices.ForEach(v => distance += Vector3.Dot(normal, v));

            //Plane = new Plane(normal, distance);
        }

        /// <summary>
        /// //----- (00538060) --------------------------------------------------------
        /// int __thiscall CPolygon::point_in_poly2D(CPolygon *this, AC1Legacy::Vector3 *point, Sidedness side)
        /// acclient.c 359420
        /// </summary>
        public bool PointInPoly2D(Vector3 point, Sidedness side)
        {
            // short circuit not in the client, but necessary for us
            if (Vertices?.Count < 1)
                return false;

            int j = 0;
            for (int i = NumPts - 1; i >= 0; i--)
            {
                var vert0 = Vertices[j];
                var vert1 = Vertices[i];

                // client does some really screwball math here.  we probably should take the time
                // to RE the geometry of what it's doing, but copy/pasta for now

                float y = vert0.Y - vert1.Y;
                float x = vert1.X - vert0.X;

                float temp = -(y * vert1.X) - x * vert1.Y + x * point.Y + y * point.X;

                if (side != Sidedness.Positive)
                {
                    if (temp < 0f)
                        return false;
                }
                else if (temp > 0f)
                    return false;

                j = i;
            }

            return true;
        }
    }
}

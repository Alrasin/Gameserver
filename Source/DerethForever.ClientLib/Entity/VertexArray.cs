/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CVertexArray in the client
    /// </summary>
    public class VertexArray
    {
        /// <summary>
        /// because all valid instances come from either Unpack or other internal functions
        /// </summary>
        internal VertexArray()
        {
        }

        /// <summary>
        /// +0x000 vertex_memory    : Ptr32 Void
        /// </summary>
        public object VertexMemory { get; set; }

        /// <summary>
        /// +0x004 bbox             : BBox
        /// </summary>
        public BoundingBox BoundingBox { get; set; }

        /// <summary>
        /// +0x01c vertex_type      : VertexType
        /// </summary>
        public VertexType VertexType { get; set; }

        /// <summary>
        /// +0x020 num_vertices     : Uint4B
        /// </summary>
        public int NumVertices
        {
            get { return Vertices.Length; }
        }

        /// <summary>
        /// +0x024 vertices         : Ptr32 CVertex
        /// </summary>
        public Vertex[] Vertices { get; set; }

        public Vector3 GetGroundVertex(uint x, uint y)
        {
            uint index = x * 8 + y;
            return Vertices[(int)index].Vector;
        }

        /// <summary>
        /// VertexArray has both an Unpack and a Serialize.  The serialize, however, just
        /// calls Unpack, so we'll stick with unpack.
        /// 
        /// //----- (0053C030) --------------------------------------------------------
        /// int __thiscall CVertexArray::UnPack(CVertexArray *this, void **addr, unsigned int size)
        /// acclient.c 362802
        /// </summary>
        public static VertexArray Unpack(BinaryReader reader)
        {
            VertexArray va = new VertexArray();

            va.VertexType = (VertexType)reader.ReadUInt32();
            var count = reader.ReadUInt32();
            va.Vertices = new Vertex[count];

            for (int i = 0; i < count; i++)
                va.Vertices[i] = Vertex.Unpack(reader);

            va.BoundingBox = BoundingBox.CreateBoundingBox(va);

            return va;
        }
    }
}

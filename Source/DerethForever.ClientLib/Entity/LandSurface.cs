/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// LandSurf in the client
    /// </summary>
    public class LandSurface
    {
        /// <summary>
        /// 0x000 pal_shift : Ptr32 PalShift
        /// </summary>
        public PaletteShift PaletteShift = null;

        /// <summary>
        /// 0x004 tex_merge : Ptr32 TexMerge
        /// </summary>
        public TextureMerge TextureMerge;

        /// <summary>
        /// data type unknown.  8 bytes in the type dump, so ulong was used as a placeholder
        /// 0x008 surf_info : Ptr32 LongNIValHash(SurfInfo *)
        /// </summary>
        public ulong SurfaceInfo;

        /// <summary>
        /// 0x00c num_lsurf : Uint4B
        /// </summary>
        public uint NumLandSurfaces => (uint)Surfaces.Count;

        /// <summary>
        /// 0x010 lsurf : Ptr32 Ptr32 CSurface
        /// </summary>
        public List<Surface> Surfaces = new List<Surface>();

        /// <summary>
        /// 0x014 num_unique_surfaces : Uint4B
        /// </summary>
        public uint NumUniqueSurfaces;

        /// <summary>
        /// 0x018 num_block_surfs : Uint4B
        /// </summary>
        public uint NumBlockSurfaces;

        /// <summary>
        /// 0x01c block_surf_array : AC1Legacy::SmartArray(CSurface *)
        /// </summary>
        public List<Surface> BlockSurfaces = new List<Surface>();

        /// <summary>
        /// 0x028 curr_tex : Ptr32 UChar (void pointer?)
        /// </summary>
        public uint CurrentTexture;

        /// <summary>
        /// private because unpack is the only source of this class
        /// </summary>
        private LandSurface()
        {
        }

        /// <summary>
        /// //----- (00502C20) --------------------------------------------------------
        /// int __thiscall LandSurf::UnPack(LandSurf *this, void **addr, unsigned int *size)
        /// acclient.c 303985
        /// </summary>
        public static LandSurface Unpack(BinaryReader reader)
        {
            LandSurface ls = new LandSurface();

            uint hasPaletteShift = reader.ReadUInt32();

            // this is always 0 for the current AC dat files, so why bother?
            if (hasPaletteShift != 0)
                throw new NotImplementedException();
            
            ls.TextureMerge = TextureMerge.Unpack(reader);

            return ls;
        }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BBox in the client
    /// </summary>
    public struct BoundingBox
    {
        /// <summary>
        /// +0x000 m_vMin           : Vector3
        /// </summary>
        public Vector3 Minimum;

        /// <summary>
        /// +0x00c m_vMax           : Vector3
        /// </summary>
        public Vector3 Maximum;

        /// <summary>
        /// //----- (0053BF10) --------------------------------------------------------
        /// void __thiscall CVertexArray::BuildBoundingBox(CVertexArray *this)
        /// acclient.c 362715
        /// </summary>
        public static BoundingBox CreateBoundingBox(VertexArray va)
        {
            BoundingBox bb = new BoundingBox();
            va.Vertices.ToList().ForEach(v => bb.AdjustBox(v.Vector));
            return bb;
        }

        /// <summary>
        /// //----- (00534150) --------------------------------------------------------
        /// void __thiscall BBox::AdjustBBox(BBox *this, Vector3 *vc)
        /// acclient.c 356169
        /// </summary>
        public void AdjustBox(Vector3 v)
        {
            if (v.X < Minimum.X)
                Minimum.X = v.X;
            if (v.Y < Minimum.Y)
                Minimum.Y = v.Y;
            if (v.Z < Minimum.Z)
                Minimum.Z = v.Z;

            if (v.X > Maximum.X)
                Maximum.X = v.X;
            if (v.Y > Maximum.Y)
                Maximum.Y = v.Y;
            if (v.Z > Maximum.Z)
                Maximum.Z = v.Z;
        }
    }
}

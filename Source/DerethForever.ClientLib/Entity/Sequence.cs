/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class Sequence
    {
        // TODO: We are passing values on the create in the client but not sure how that works - this could be wrong Coral Golem
        /// <summary>
        /// +0x000 __VFN_table : Ptr32
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// +0x004 anim_list        : DLList<AnimSequenceNode>
        /// </summary>
        public LinkedList<AnimationSequenceNode> AnimationList { get; set; }

        /// <summary>
        /// +0x00c first_cyclic     : Ptr32 AnimSequenceNode
        /// </summary>
        public AnimationSequenceNode FirstCyclic { get; set; } = null;

        /// <summary>
        /// +0x010 velocity         : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Velocity { get; set; } = Vector3.Zero;

        /// <summary>
        /// 0x01c omega : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Omega { get; set; } = Vector3.Zero;

        /// <summary>
        /// 0x028 hook_obj : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject HookObject { get; set; } = null;

        /// <summary>
        /// +0x030 frame_number     : Float
        /// </summary>
        public float FrameNumber { get; set; }

        /// <summary>
        /// 0x038 curr_anim : Ptr32 AnimSequenceNode
        /// </summary>
        public AnimationSequenceNode CurrentAnimation { get; set; }

        /// <summary>
        /// 0x03c placement_frame : Ptr32 AnimFrame
        /// </summary>
        public AnimationFrame PlacementFrame { get; set; }

        /// <summary>
        /// 0x040 placement_frame_id : Uint4B
        /// </summary>
        public Placement PlacementFrameId { get; set; }

        public Sequence()
        {
            Velocity = Vector3.Zero;
            Omega = Vector3.Zero;
            AnimationList = new LinkedList<AnimationSequenceNode>();
        }

        public Sequence(uint id): this()
        {
            // We need to check this for sure
            // this->vfptr = (PackObjVtbl *)&CSequence::`vftable';
            Id = id;
        }

        /// <summary>
        /// //----- (005259D0) --------------------------------------------------------
        /// int __thiscall CSequence::UnPack(CSequence *this, void **addr, unsigned int size)
        /// acclient.c 340783
        /// </summary>
        public static Sequence Unpack(BinaryReader reader)
        {
            // TODO implement Sequence.Unpack
            return null;
        }

        /// <summary>
        /// //----- (00525B80) --------------------------------------------------------
        /// void __thiscall CSequence::update(CSequence *this, long double quantum, Frame* retval)
        /// acclient.c 340951
        /// </summary>
        public void Update(double quantum, Frame frame)
        {
            if (AnimationList?.First != null)
            {
                this.UpdateInternal(quantum, FirstCyclic, FrameNumber, ref frame);
                Apricot();
            }
            else if (frame != null)
            {
                ApplyPhysics(frame, quantum, quantum < 0);
            }
        }

        /// <summary>
        /// //----- (005249B0) --------------------------------------------------------
        /// void __thiscall CSequence::set_placement_frame(CSequence *this, AnimFrame *_placement_frame, unsigned int _id)
        /// acclient.c 339767
        /// </summary>
        public void SetPlacementFrame(AnimationFrame frame, Placement placement)
        {
            PlacementFrame = frame;
            PlacementFrameId = placement;
        }

        /// <summary>
        /// //----- (00524820) --------------------------------------------------------
        /// void __thiscall CSequence::set_object(CSequence *this, CPhysicsObj *_phys_obj)
        /// </summary>
        public void SetObject(PhysicsObject physicsObject)
        {
            HookObject = physicsObject;
        }

        /// <summary>
        /// //----- (00524DC0) --------------------------------------------------------
        /// void __thiscall CSequence::clear_animations(CSequence *this)
        /// acclient.c 340102
        /// </summary>
        public void ClearAnimations()
        {
            // TODO implement Sequence.ClearAnimations
        }

        /// <summary>
        /// //----- (00525510) --------------------------------------------------------
        /// void __thiscall CSequence::append_animation(CSequence *this, AnimData *new_data)
        /// acclient.c 340590
        /// </summary>
        public void AppendAnimation(AnimationData newData)
        {
            // TODO implement Sequence.AppendAnimation
        }

        /// <summary>
        /// //----- (005255D0) --------------------------------------------------------
        /// void __thiscall CSequence::update_internal(CSequence *this, long double quantum, AnimSequenceNode** _curr_anim, long double* _frame_number, Frame* retval)
        /// acclient.c 340659
        /// </summary>
        private void UpdateInternal(double quantum, AnimationSequenceNode animationNode, float frameNumber, ref Frame retVal)
        {
            // TODO implement UpdateInternal
        }

        /// <summary>
        /// //----- (00524B40) --------------------------------------------------------
        /// void __thiscall CSequence::apricot(CSequence *this)
        /// acclient.c 339893
        /// </summary>
        private void Apricot()
        {
            AnimationSequenceNode node = AnimationList.First.Value;
            while (!node.Equals(CurrentAnimation))
            {
                if (node.Equals(FirstCyclic))
                    break;
                AnimationList.Remove(node);
                node = AnimationList.First.Value;
            }
        }

        /// <summary>
        /// //----- (00524970) --------------------------------------------------------
        /// AnimFrame *__thiscall CSequence::get_curr_animframe(CSequence *this)
        /// acclient.c 00524970
        /// </summary>
        public AnimationFrame GetCurrentAnimationFrame()
        {
            if (CurrentAnimation == null)
                return PlacementFrame;

            return CurrentAnimation.GetPartFrame(FrameNumber);
        }

        /// <summary>
        /// //----- (00524AB0) --------------------------------------------------------
        /// void __thiscall CSequence::apply_physics(CSequence*this, Frame* frame, long double quantum, long double sign)
        /// acclient.c 339860
        /// </summary>
        private void ApplyPhysics(Frame frame, double quantum, bool forceNegative)
        {
            double scalar = Math.Abs(quantum);
            scalar *= forceNegative ? -1 : 1;

            frame.Origin.X *= (Velocity.X * (float)scalar);
            frame.Origin.Y *= (Velocity.Y * (float)scalar);
            frame.Origin.Z *= (Velocity.Z * (float)scalar);

            // TODO finish Sequence.ApplyPhysics
        }

        /// <summary>
        /// ----- (00524D50) --------------------------------------------------------
        /// void __thiscall CSequence::clear_physics(CSequence*this)
        /// acclient.c 340086
        /// </summary>
        public void ClearPhysics()
        {
            Velocity = Vector3.Zero;
            Omega = Vector3.Zero;
        }

        /// <summary>
        /// ----- (00524E40) --------------------------------------------------------
        /// void __thiscall CSequence::remove_cyclic_anims(CSequence*this)
        /// acclient.c 340154
        /// </summary>
        public void RemoveCyclicAnimations()
        {
            AnimationSequenceNode node = FirstCyclic;
            while (node != null)
            {
                if (CurrentAnimation != null && CurrentAnimation.Equals(node))
                {
                    CurrentAnimation = node.Previous;
                    FrameNumber = CurrentAnimation?.GetEndingFrame() ?? 0.0f;
                }
                AnimationSequenceNode next = node.Next;
                AnimationList.Remove(node);
                node = next;
            }

            if (AnimationList.Last != null)
                FirstCyclic = AnimationList.Last.Value;
            else
                FirstCyclic = null;

        }

        /// <summary>
        /// ----- (00524900) --------------------------------------------------------
        /// void __thiscall CSequence::subtract_physics(CSequence*this, AC1Legacy::Vector3* v, AC1Legacy::Vector3* o)
        /// acclient.c 339725
        /// </summary>
        public void SubtractPhysics(Vector3 velocity, Vector3 omega)
        {
            Velocity -= velocity;
            Omega -= omega;
        }

        /// <summary>
        /// ----- (00524940) --------------------------------------------------------
        /// void __thiscall CSequence::multiply_cyclic_animation_fr(CSequence*this, float multiplier)
        /// acclient.c 339736
        /// </summary>
        public void MultiplyCyclicAnimationFrameRate(float rate)
        {
            if (FirstCyclic == null)
                return;

            foreach (AnimationSequenceNode af in AnimationList)
            {
                if (af.Equals(FirstCyclic))
                    af.MultiplyFrameRate(rate);
            }
        }

        /// <summary>
        /// ----- (005248C0) --------------------------------------------------------
        /// void __thiscall CSequence::combine_physics(CSequence*this, AC1Legacy::Vector3* v, AC1Legacy::Vector3* o)
        /// </summary>
        public void CombinePhysics(Vector3 velocity, Vector3 omega)
        {
            Velocity += velocity;
            Omega += omega;
        }

        /// <summary>
        ///----- (00524BE0) --------------------------------------------------------
        /// void __thiscall CSequence::remove_link_animations(CSequence*this, unsigned int n)
        /// acclient.c 339954
        /// </summary>
        public void RemoveLinkAnimations(uint amount)
        {
            for (int i = 0; i < amount; i++)
            {
                if (FirstCyclic.Previous == null)
                    return;

                if (CurrentAnimation.Equals(FirstCyclic.Previous))
                {
                    CurrentAnimation = FirstCyclic;

                    if (CurrentAnimation != null)
                        FrameNumber = CurrentAnimation.GetStartingFrame();
                }
                AnimationList.Remove(FirstCyclic.Previous);
            }
        }

    }
}

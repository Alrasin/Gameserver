/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CObjCell in the client
    /// </summary>
    public abstract class ObjectCell : DatabaseObject
    {
        /// <summary>
        /// +0x03c num_shadow_parts : Uint4B
        /// </summary>
        public uint NumShadowParts
        {
            // Not sure if this is right - please verify Coral Golem
            get { return (uint)(ShadowPartList?.Count ?? 0); }
        }

        /// <summary>
        /// 0x040 shadow_part_list : DArray(CShadowPart *)
        /// </summary>
        public List<ShadowPart> ShadowPartList { get; set; } = new List<ShadowPart>();

        /// <summary>
        /// +0x050 water_type       : LandDefs::WaterType
        /// </summary>
        public WaterType WaterType { get; set; }

        /// <summary>
        /// +0x054 pos              : Position
        /// </summary>
        public Position Position { get; set; } = new Position();

        /// <summary>
        /// +0x09c num_objects      : Uint4B
        /// </summary>
        public uint NumObjects
        {
            get { return (uint)ObjectList.Count; }
        }

        /// <summary>
        /// +0x0a0 object_list      : DArray(CPhysicsObj *)
        /// </summary>
        public List<PhysicsObject> ObjectList { get; set; } = new List<PhysicsObject>();

        /// <summary>
        /// +0x0b0 num_lights       : Uint4B
        /// </summary>
        public uint NumLights
        {
            // Not sure if this is right - please verify Coral Golem
            get { return (uint)(LightList?.Count ?? 0); }
        }

        /// <summary>
        /// +0x0b4 light_list       : DArray(LIGHTOBJ const *)
        /// </summary>
        public List<LightObject> LightList { get; set; } = new List<LightObject>();

        /// <summary>
        /// +0x0c4 num_shadow_objects : Uint4B
        /// </summary>
        public uint NumShadowObjects
        {
            // Not sure if this is right - please verify Coral Golem
            get { return (uint)(ShadowObjectList?.Count ?? 0); }
        }

        /// <summary>
        /// +0x0c8 shadow_object_list : DArray(CShadowObj *)
        /// </summary>
        public List<ShadowObject> ShadowObjectList { get; set; } = new List<ShadowObject>();

        /// <summary>
        /// +0x0d8 restriction_obj  : Uint4B
        /// </summary>
        public uint RestrictionObject { get; set; } // pointer to something

        /// <summary>
        /// +0x0dc clip_planes      : Ptr32 Ptr32 ClipPlaneList
        /// </summary>
        public List<ClipPlane> ClipPlanes { get; set; } = new List<ClipPlane>(); // double pointer

        /// <summary>
        /// +0x0e0 num_stabs        : Uint4B
        /// </summary>
        public uint NumStabs
        {
            // Not sure if this is right - please verify Coral Golem
            get { return (uint)(Stabs?.Count ?? 0); }
        }

        /// <summary>
        /// +0x0e4 stab_list        : Ptr32 Uint4B
        /// </summary>
        public List<Stab> Stabs { get; set; } = new List<Stab>();

        /// <summary>
        /// +0x0e8 seen_outside     : Int4B
        /// </summary>
        public int SeenOutside { get; set; } // boolean?

        /// <summary>
        /// +0x0ec voyeur_table     : Ptr32 LongNIValHash(GlobalVoyeurInfo)
        /// </summary>
        public List<Voyeur> VoyeurTable { get; set; } = new List<Voyeur>();

        /// <summary>
        /// +0x0f0 myLandBlock_     : Ptr32 CLandBlock
        /// </summary>
        public Landblock MyLandblock { get; set; }

        /// <summary>
        /// doesn't actually exist on CObjCell because it's abstract and C++ is fun that way
        /// </summary>
        public abstract TransitionState FindCollisions(Transition trans);

        /// <summary>
        /// //----- (0052B750) --------------------------------------------------------
        /// signed int __thiscall CObjCell::find_obj_collisions(CObjCell*this, CTransition* transition)
        /// acclient.c 347142
        /// </summary>
        public virtual TransitionState FindObjectCollisions(Transition trans)
        {
            if (trans.SpherePath.InsertType != SpherePathInsertType.InitialPlacementInsert)
                return TransitionState.Ok;

            foreach (var shadowObject in ShadowObjectList)
            {
                if (shadowObject is null)
                    continue;

                // TODO continue here after creating shadow object properties necessary to finish
            }

            return TransitionState.Ok;
        }

        /// <summary>
        /// doesn't actually exist on CObjCell because it's abstract and C++ is fun that way
        /// </summary>
        public virtual TransitionState FindEnvironmentCollisions(Transition trans)
        {
            return TransitionState.Invalid;
        }

        /// <summary>
        /// //----- (0052BF60) --------------------------------------------------------
        /// void __thiscall CObjCell::add_object(CObjCell *this, CPhysicsObj *_object)
        /// acclient.c 347703
        /// </summary>
        public virtual void AddObject(PhysicsObject item)
        {
            ObjectList.Add(item);

            if (item.Id > 0 && item.Parent == null && !item.State.HasFlag(PhysicsState.Hidden))
            {
                VoyeurTable.ForEach(v =>
                {
                    if (v.ObjectId == item.Id || v.ObjectId == 0)
                        return;

                    // PhysicsObject voyeur = GetObjectA(v.ObjectId);
                    if (true) // if (voyeur != null)
                    {
                        DetectionInfo di = new DetectionInfo(item.Id, DetectionType.Entered);

                        // todo ObjectCell.AddObject does broadcasting to voyeurs
                        // voyeur.ReceiveDetectionUpdate(di);
                    }
                });
            }
        }

        /// <summary>
        /// ----- (0052B280) --------------------------------------------------------
        /// void __thiscall CObjCell::add_shadow_object(CObjCell*this, CShadowObj* _object, unsigned int num_shadow_cells)
        /// acclient.c 346805
        /// </summary>
        public virtual void AddShadowObject(ShadowObject shadowObject)
        {
            ShadowObjectList.Add(shadowObject);
            shadowObject.Cell = this;
        }

        /// <summary>
        /// ----- (0052C040) --------------------------------------------------------
        /// void __thiscall CObjCell::remove_object(CObjCell*this, CPhysicsObj* _object)
        /// acclient.c 347765
        /// </summary>
        public virtual void RemoveObject(PhysicsObject physicsObject)
        {
            ObjectList.Remove(physicsObject);
            UpdateAllVoyeur(physicsObject, DetectionType.Left);
        }

        /// <summary>
        /// ----- (0052BD80) --------------------------------------------------------
        /// void __thiscall CObjCell::update_all_voyeur(CObjCell*this, CPhysicsObj*object, DetectionType type)
        /// acclient.c 347569
        /// </summary>
        public virtual void UpdateAllVoyeur(PhysicsObject physicsObject, DetectionType detectionType)
        {
            if (physicsObject.Id == 0 || physicsObject.Parent != null || VoyeurTable == null)
                return;

            if (physicsObject.State.HasFlag(PhysicsState.Hidden) && detectionType == DetectionType.Entered)
                return;

            foreach (Voyeur voy in VoyeurTable)
            {
                if (voy.ObjectId != physicsObject.Id && voy.ObjectId != 0)
                {
                    PhysicsObject voyeur = physicsObject.GetObjectA(voy.ObjectId);
                    if (voyeur == null)
                        continue;

                    DetectionInfo info = new DetectionInfo(physicsObject.Id, detectionType);
                    voyeur.ReceiveDetectionUpdate(info);
                }
            }
        }

        /// <summary>
        /// ----- (0052B6D0) --------------------------------------------------------
        /// signed int __thiscall CObjCell::check_entry_restrictions(CObjCell*this, CTransition* transition)
        /// </summary>
        public virtual TransitionState CheckEntryRestrictions(Transition transition)
        {
            ObjectInfo objInfo = transition.ObjectInfo;

            if (objInfo.PhysicsObject == null)
                return TransitionState.Collided;

            if (objInfo.PhysicsObject.WeenieObject == null)
                return TransitionState.Ok;

            if (objInfo.ObjectInfoState.HasFlag(ObjectInfoState.Contact))
            {
                if (RestrictionObject != 0)
                {
                    PhysicsObject restrictionObject = objInfo.PhysicsObject.GetObjectA(RestrictionObject);
                    if (restrictionObject == null)
                        return TransitionState.Collided;
                    // TODO: more checks here - need to understand the vftable 
                }
            }
            return TransitionState.Ok;
        }

    }
}

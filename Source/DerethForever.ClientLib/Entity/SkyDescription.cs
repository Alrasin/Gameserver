/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SkyDesc in the client
    /// </summary>
    public class SkyDescription
    {
        /// <summary>
        /// private because the only valid source for SkyDescription is the dat, which is handled
        /// here in Unpack
        /// </summary>
        private SkyDescription()
        {
        }

        /// <summary>
        /// +0x000 present_day_group : Uint4B
        /// </summary>
        public uint PresentDayGroup { get; set; }

        // TODO: Behemoth - Should these be doubles instead of longs?   Coral Golem
        /// <summary>
        /// +0x008 tick_size        : Float
        /// </summary>
        public ulong TickSize { get; set; }

        /// <summary>
        /// +0x010 light_tick_size  : Float
        /// </summary>
        public ulong LightTickSize { get; set; }

        /// <summary>
        /// +0x018 day_groups       : AC1Legacy::SmartArray<DayGroup*>
        /// </summary>
        public List<DayGroup> DayGroups = new List<DayGroup>();

        /// <summary>
        /// //----- (00501CD0) --------------------------------------------------------
        /// int __thiscall SkyDesc::UnPack(SkyDesc *this, void **addr, unsigned int *size)
        /// acclient.c 302822
        /// </summary>
        public static SkyDescription Unpack(BinaryReader reader)
        {
            SkyDescription sd = new SkyDescription();
            sd.TickSize = reader.ReadUInt64();
            sd.LightTickSize = reader.ReadUInt64();

            // client has an align_ptr here, but this should never matter as reading
            // unit64 values will not break alignment

            uint num = reader.ReadUInt32();
            for (uint i = 0; i < num; i++)
                sd.DayGroups.Add(DayGroup.Unpack(reader));

            return sd;
        }

        /// <summary>
        /// ----- (00500E10) --------------------------------------------------------
        /// void __thiscall SkyDesc::CalcPresentDayGroup(SkyDesc*this)
        /// acclient.c 301664
        /// </summary>
        /// <returns></returns>
        public SkyDescription CalculatePresentDayGroup()
        {
            // This may not be needed for physics but could be needed for stuff like
            // the graveyard where things change depending on the day part.   Coral Golem.
            throw new NotImplementedException();
        }
    }
}

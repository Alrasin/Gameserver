/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class MotionTable
    {
        // Don't think we need these - used for list management?

        //+0x004 m_dataCategory   : Uint4B
        //+0x008 m_bLoaded        : Bool
        //+0x010 m_timeStamp      : Float
        //+0x018 m_pNext          : Ptr32 DBObj
        //+0x01c m_pLast          : Ptr32 DBObj
        //+0x020 m_pMaintainer    : Ptr32 DBOCache
        //+0x024 m_numLinks       : Int4B
        //+0x028 m_DID            : IDClass<_tagDataID,32,0>
        //+0x02c m_AllowedInFreeList : Bool
        //+0x030 __VFN_table : Ptr32

        /// <summary>
        /// +0x000 __VFN_table : Ptr32
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// +0x038 style_defaults   : LongNIValHash<unsigned long>
        /// </summary>
        public Dictionary<MotionCommand, MotionCommand> StyleDefaults { get; set; }

        /// <summary>
        /// +0x050 cycles           : LongHash<MotionData>
        /// </summary>
        public Dictionary<uint, MotionData> Cycles { get; set; }

        /// <summary>
        /// +0x068 modifiers        : LongHash<MotionData>
        /// </summary>
        public Dictionary<uint, MotionData> Modifiers { get; set; }

        /// <summary>
        /// +0x068 modifiers        : LongHash<MotionData>
        /// </summary>
        public Dictionary<uint, Dictionary<uint, MotionData>> Links { get; set; }

        /// <summary>
        /// +0x068 modifiers        : LongHash<MotionData>
        /// </summary>
        public MotionCommand DefaultStyle { get; set; }

        /// <summary>
        /// ----- (004F94E0) --------------------------------------------------------
        /// void __thiscall CMotionTable::CMotionTable(CMotionTable*this)
        /// acclient.c 292831
        /// </summary>
        public MotionTable()
        {
            Cycles = new Dictionary<uint, MotionData>();
            Modifiers = new Dictionary<uint, MotionData>();
        }

        /// <summary>
        /// ----- (00523E90) --------------------------------------------------------
        /// int __thiscall CMotionTable::DoObjectMotion(CMotionTable*this, unsigned int motion, MotionState* curr_state, CSequence* sequence, float speed_mod, unsigned int* num_anims)
        /// acclient.c 339023
        /// </summary>
        public bool DoObjectMotion(MotionCommand motion, ref MotionState currState, ref Sequence sequence, float speedMod, ref uint numAnims)
        {
            return GetObjectSequence(motion, ref currState, ref sequence, speedMod, ref numAnims, false);
        }
        /// <summary>
        /// ----- (005238C0) --------------------------------------------------------
        /// int __thiscall CMotionTable::UnPack(CMotionTable*this, void** addr, unsigned int size)
        /// acclient.c 338604
        /// </summary>
        public static MotionTable Unpack(BinaryReader reader)
        {
            MotionTable mt = new MotionTable();

            mt.Id = reader.ReadUInt32();
            mt.DefaultStyle = (MotionCommand)reader.ReadUInt32();



            uint numStyleDefaults = reader.ReadUInt32();
            if (numStyleDefaults > 0)
            {
                mt.StyleDefaults = new Dictionary<MotionCommand, MotionCommand>();
                for (uint i = 0; i < numStyleDefaults; i++)
                    mt.StyleDefaults.Add((MotionCommand)reader.ReadUInt32(), (MotionCommand)reader.ReadUInt32());
            }

            uint numCycles = reader.ReadUInt32();
            if (numCycles > 0)
            {
                mt.Cycles = new Dictionary<uint, MotionData>();
                for (uint i = 0; i < numCycles; i++)
                {
                    uint key = reader.ReadUInt32();
                    MotionData md = MotionData.Unpack(reader, mt.Id);

                    mt.Cycles.Add(key, md);
                }
            }

            uint numModifiers = reader.ReadUInt32();

            if (numModifiers > 0)
            {
                mt.Modifiers = new Dictionary<uint, MotionData>();
                for (uint i = 0; i < numModifiers; i++)
                {
                    uint key = reader.ReadUInt32();
                    MotionData md = MotionData.Unpack(reader, mt.Id);
                    mt.Modifiers.Add(key, md);
                }
            }

            uint numLinks = reader.ReadUInt32();

            if (numLinks > 0)
            {
                mt.Links = new Dictionary<uint, Dictionary<uint, MotionData>>();
                for (uint i = 0; i < numLinks; i++)
                {
                    uint firstKey = reader.ReadUInt32();
                    uint numSubLinks = reader.ReadUInt32();

                    if (numSubLinks > 0)
                    {
                        Dictionary<uint, MotionData> sublinks = new Dictionary<uint, MotionData>();
                        for (uint j = 0; j < numSubLinks; j++)
                        {
                            uint subKey = reader.ReadUInt32();
                            MotionData md = MotionData.Unpack(reader, mt.Id);
                            sublinks.Add(subKey, md);
                        }
                        mt.Links.Add(firstKey, sublinks);
                    }
                }
            }

            return mt;
        }

        /// <summary>
        /// ----- (00522710) --------------------------------------------------------
        /// int __thiscall CMotionTable::get_link(CMotionTable*this, unsigned int style, unsigned int substate, float substate_speed, unsigned int motion, float speed)
        /// acclient.c 337585
        /// </summary>
        public MotionData GetLink(MotionCommand style, MotionCommand substate, float substateSpeed, MotionCommand motion, float speed)
        {
            MotionCommand first = motion;
            MotionCommand second = substate;
            uint styleKey = (uint)style << 16;

            if (substateSpeed >= 0.0f && speed >= 0.0f)
            {
                first = substate;
                second = motion;
            }

            styleKey |= (uint)first & 0xFFFFF;
            Links.TryGetValue(styleKey, out Dictionary<uint, MotionData> link);

            if (link == null)
                return null;

            link.TryGetValue((uint)second, out MotionData motionData);
            if (motionData != null)
                return motionData;

            styleKey = (uint)style << 16;
            Links.TryGetValue(styleKey, out link);

            if (link == null)
                return null;

            link.TryGetValue((uint)second, out motionData);
            return motionData;
        }

        /// <summary>
        /// ----- (00522860) --------------------------------------------------------
        /// int __thiscall CMotionTable::GetObjectSequence(CMotionTable*this, unsigned int motion, MotionState* curr_state, CSequence* sequence, float speed_mod, unsigned int* num_anims, int stop_modifiers)
        /// acclient.c 337641
        /// </summary>
        public bool GetObjectSequence(MotionCommand motion, ref MotionState currState, ref Sequence sequence, float speedMod, ref uint numAnimations, bool stopModifiers)
        {
            if (currState.Style == 0 || currState.SubState == 0)
                return false;

            MotionData motionData = null;
            MotionData motionData2 = null;

            StyleDefaults.TryGetValue(currState.Style, out MotionCommand subState);

            if (motion == subState && !stopModifiers && (subState & MotionCommand.ModifierMask) != 0)
                return true;

            if ((motion & MotionCommand.StyleMask) != 0)
            {
                if (currState.Style == motion)
                    return true;

                if (subState != currState.SubState)
                    motionData = GetLink(currState.Style, currState.SubState, currState.SubStateMod, subState, speedMod);

                if (subState != 0)
                {
                    Cycles.TryGetValue(((uint)motion << 16) | ((uint)subState & 0xFFFFFF), out MotionData cycles);
                    if (cycles != null)
                    {
                        if ((cycles.Bitfield & 1) != 0)
                            currState.Modifiers.Clear();

                        MotionData link = GetLink(currState.Style, subState, 1.0f, DefaultStyle, 1.0f);
                        if (link != null && currState.Style != motion)
                        {
                            StyleDefaults.TryGetValue(DefaultStyle, out MotionCommand defaultStyle);
                            motionData2 = GetLink(DefaultStyle, defaultStyle, 1.0f, motion, 1.0f);
                        }

                        sequence.ClearPhysics();
                        sequence.RemoveCyclicAnimations();

                        AddMotion(sequence, motionData, speedMod);
                        AddMotion(sequence, link, speedMod);
                        AddMotion(sequence, motionData2, speedMod);
                        AddMotion(sequence, cycles, speedMod);

                        currState.SubState = subState;
                        currState.Style = motion;
                        currState.SubStateMod = speedMod;

                        ReModify(sequence, currState);

                        numAnimations = (uint)((motionData?.Animations.Count ?? 0) + (link?.Animations.Count ?? 0) + (motionData2?.Animations.Count ?? 0) + (cycles.Animations.Count));
                        return true;
                    }
                }
            }

            if ((motion & MotionCommand.SubStateMask) != 0)
            {
                MotionCommand motionId = motion & (MotionCommand)0xFFFFFF;

                Cycles.TryGetValue((uint)currState.Style << 16 | (uint)motionId, out motionData);
                if (motionData == null)
                    Cycles.TryGetValue((uint)DefaultStyle << 16 | (uint)motionId, out motionData);
                if (motionData != null)
                {
                    if (IsAllowed(motion, motionData, currState))
                    {
                        if (motion == currState.SubState && sequence.AnimationList.Count > 0 && Math.Sign(speedMod) == Math.Sign(currState.SubStateMod))
                        {
                            ChangeCycleSpeed(sequence, motionData, currState.SubStateMod, speedMod);
                            SubtractMotion(sequence, motionData, currState.SubStateMod);
                            CombineMotion(sequence, motionData, speedMod);
                            currState.SubStateMod = speedMod;
                            return true;
                        }

                        if ((motionData.Bitfield & 1) != 0)
                            currState.Modifiers.Clear();

                        MotionData link = GetLink(currState.Style, currState.SubState, currState.SubStateMod, motion, speedMod);

                        if (link == null || Math.Sign(speedMod) != Math.Sign(currState.SubStateMod))
                        {
                            StyleDefaults.TryGetValue(currState.Style, out MotionCommand defaultMotion);
                            link = GetLink(currState.Style, currState.SubState, currState.SubStateMod, defaultMotion,1.0f);
                            motionData2 = GetLink(currState.Style, defaultMotion, 1.0f, motion, speedMod);
                        }
                        sequence.ClearPhysics();
                        sequence.RemoveCyclicAnimations();

                        if (motionData2 != null)
                        {
                            AddMotion(sequence, link, currState.SubStateMod);
                            AddMotion(sequence, motionData2, speedMod);
                        }
                        else
                        {
                            float newSpeedMod = speedMod;
                            if (currState.SubStateMod < 0.0f && speedMod > 0.0f)
                                newSpeedMod *= -1.0f;
                            AddMotion(sequence, link, newSpeedMod);
                        }

                        AddMotion(sequence, motionData, speedMod);

                        if (currState.SubState != motion && (currState.SubState & MotionCommand.ModifierMask) != 0)
                        {
                            StyleDefaults.TryGetValue(currState.Style, out MotionCommand defaultMotion);
                            if (defaultMotion != motion)
                                currState.Modifiers.AddFirst(new Motion(currState.SubState, currState.SubStateMod));
                        }

                        currState.SubStateMod = speedMod;
                        currState.SubState = motion;
                        ReModify(sequence, currState);
                        numAnimations = (uint)(motionData.Animations.Count + (link?.Animations.Count ?? 0) + (motionData2?.Animations.Count ?? 0));

                        return true;
                    }
                }
            }

            if ((motion & MotionCommand.ActionMask) != 0)
            {
                uint cycleKey = ((uint)currState.Style << 16) | ((uint)subState & 0xFFFFFF);
                Cycles.TryGetValue(cycleKey, out motionData);
                if (motionData != null)
                {
                    MotionData link = GetLink(currState.Style, subState, currState.SubStateMod, motion, speedMod);
                    if (link != null)
                    {
                        currState.Actions.AddLast(new Motion(motion, speedMod));
                        sequence.ClearPhysics();
                        sequence.RemoveCyclicAnimations();

                        AddMotion(sequence, link, speedMod);
                        AddMotion(sequence, motionData, currState.SubStateMod);
                        ReModify(sequence, currState);

                        numAnimations = (uint)link.Animations.Count;
                        return true;
                    }

                    StyleDefaults.TryGetValue(currState.Style, out subState);
                    motionData = GetLink(currState.Style, currState.SubState, currState.SubStateMod, subState, 1.0f);
                    if (motionData != null)
                    {
                        link = GetLink(currState.Style, subState, 1.0f, motion, speedMod);
                        if (link != null && Cycles.TryGetValue(cycleKey, out MotionData cycles))
                        {
                            motionData2 = GetLink(currState.Style, subState, 1.0f, currState.SubState, currState.SubStateMod);
                            currState.Actions.AddLast(new Motion(motion, speedMod));
                            sequence.ClearPhysics();
                            sequence.RemoveCyclicAnimations();
                            AddMotion(sequence, motionData, 1.0f);
                            AddMotion(sequence, link, speedMod);
                            AddMotion(sequence, motionData2, 1.0f);
                            AddMotion(sequence, cycles, currState.SubStateMod);
                            ReModify(sequence, currState);
                            numAnimations = (uint)(motionData.Animations.Count + link.Animations.Count + (motionData2 == null ? 0 : motionData.Animations.Count));
                            return true;
                        }
                    }
                }

            }

            if ((motion & MotionCommand.ModifierMask) != 0)
            {
                uint styleKey = (uint) currState.Style << 16;
                Cycles.TryGetValue(styleKey | ((uint)currState.SubState & 0xFFFFFF), out MotionData cycles);
                if (motionData != null && (cycles != null && (motionData.Bitfield & 1) == 0))
                {
                    Modifiers.TryGetValue(styleKey | (uint)motion, out motionData);
                    Modifiers.TryGetValue((uint)motion & 0xFFFFFF, out motionData2);
                    if (motionData != null || motionData2 != null)
                    {
                        StopSequenceMotion(motion, 1.0f, ref currState, ref sequence, out numAnimations);
                        if (!currState.AddModifier(motion, speedMod))
                            return false;
                    }
                    CombineMotion(sequence, motionData, speedMod);
                    return true;
                }
            }
                return false;
        }

        /// <summary>
        /// ----- (005224B0) --------------------------------------------------------
        /// void __cdecl add_motion(CSequence* sequence, MotionData* data, float speed_mod)
        /// acclient.c 337431
        /// </summary>
        public void AddMotion(Sequence sequence, MotionData motionData, float speed)
        {
            if (motionData == null) return;

            sequence.Velocity = (motionData.Velocity * speed);
            sequence.Omega = (motionData.Omega * speed);

            foreach (AnimationData ad in motionData.Animations)
                sequence.AppendAnimation(new AnimationData(ad, speed));
        }

        /// <summary>
        /// ----- (005222E0) --------------------------------------------------------
        /// void __thiscall CMotionTable::re_modify(CMotionTable*this, CSequence* sequence, MotionState* state)
        /// acclient.c 337286
        /// </summary>
        public void ReModify(Sequence sequence, MotionState state)
        {
            uint numAnimations = 0;
            while (state.Modifiers.Count > 0)
            {
                LinkedListNode<Motion> modifier = state.Modifiers.First;
                state.Modifiers.RemoveFirst();
                GetObjectSequence((MotionCommand)modifier.Value.Id, ref state, ref sequence, modifier.Value.SpeedMod, ref numAnimations, false);
            }
        }

        /// <summary>
        /// ----- (005226C0) --------------------------------------------------------
        /// MotionData* __thiscall CMotionTable::is_allowed(CMotionTable*this, unsigned int motion, MotionData* mdata, MotionState* state)
        /// </summary>
        public bool IsAllowed(MotionCommand motion, MotionData motionData, MotionState state)
        {
            if (motionData == null)
                return false;

            if ((motionData.Bitfield & 2) == 0 || motion == state.SubState)
                return true;

            StyleDefaults.TryGetValue(state.Style, out MotionCommand style);
            return (style == state.SubState);
        }

        /// <summary>
        /// ----- (00522600) --------------------------------------------------------
        /// void __cdecl subtract_motion(CSequence* sequence, MotionData* data, float speed_mod)
        /// acclient.c 337506
        /// </summary>
        public void SubtractMotion(Sequence sequence, MotionData motionData, float speed)
        {
            if (motionData == null)
                return;
            sequence.SubtractPhysics(motionData.Velocity * speed, motionData.Omega * speed);
        }

        /// <summary>
        /// ----- (00522290) --------------------------------------------------------
        /// void __cdecl change_cycle_speed(CSequence* sequence, MotionData* data, float old_speed, float new_speed)
        /// </summary>
        public void ChangeCycleSpeed(Sequence sequence, MotionData motionData, float oldSpeed, float newSpeed)
        {
            if (Math.Abs(oldSpeed) <= Constants.TOLERANCE)
                if (Math.Abs(newSpeed) < Constants.TOLERANCE)
                    sequence.MultiplyCyclicAnimationFrameRate(0.0f);
                else
                    sequence.MultiplyCyclicAnimationFrameRate(newSpeed / oldSpeed);
        }

        /// <summary>
        /// ----- (00522580) --------------------------------------------------------
        /// void __cdecl combine_motion(CSequence* sequence, MotionData* data, float speed_mod)
        /// </summary>
        public void CombineMotion(Sequence sequence, MotionData motionData, float speed)
        {
            if (motionData == null)
                return;
            sequence.CombinePhysics(motionData.Velocity * speed,
                motionData.Omega * speed);
        }

        /// <summary>
        /// ----- (00522FC0) --------------------------------------------------------
        /// int __thiscall CMotionTable::StopSequenceMotion(CMotionTable*this, unsigned int motion, float speed, MotionState* curr_state, CSequence* sequence, unsigned int* num_anims)
        /// acclient.c 337912
        /// </summary>
        public bool StopSequenceMotion(MotionCommand motion, float speed, ref MotionState currState, ref Sequence sequence, out uint numAnims)
        {
            numAnims = 0;
            if ((motion & MotionCommand.SubStateMask) != 0 && currState.SubState == motion)
            {
                StyleDefaults.TryGetValue(currState.Style, out MotionCommand style);
                GetObjectSequence(style, ref currState, ref sequence, 1.0f, ref numAnims, true);
                return true;
            }
            if ((motion & MotionCommand.ModifierMask) == 0)
                return false;

            foreach (Motion modifier in currState.Modifiers)
            {
                if (modifier.Id == motion)
                {
                    MotionCommand key = (MotionCommand)(((uint)modifier.Id << 16) | ((uint)motion & 0xFFFFFF));
                    if (!Modifiers.TryGetValue((uint)motion & 0xFFFFFF, out MotionData motionData))
                        return false;

                    SubtractMotion(sequence, motionData, modifier.SpeedMod);
                    currState.Modifiers.Remove(modifier);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// ----- (005230A0) --------------------------------------------------------
        /// int __thiscall CMotionTable::SetDefaultState(CMotionTable*this, MotionState* state, CSequence* sequence, unsigned int* num_anims)
        /// acclient.c 337970
        /// </summary>
        public bool SetDefaultState(MotionState state, Sequence sequence, ref uint numAnims)
        {
            if (!StyleDefaults.TryGetValue(DefaultStyle, out MotionCommand defaultSubState))
                return false;

            state.Modifiers.Clear();
            state.Actions.Clear();

            uint cycle = ((uint)DefaultStyle << 16) | ((uint)defaultSubState & 0xFFFFFF);

            if (!Cycles.TryGetValue(cycle, out MotionData motionData))
                return false;

            numAnims = (uint)motionData.Animations.Count;
            state.Style = DefaultStyle;
            state.SubState = defaultSubState;
            state.SubStateMod = Constants.DefaultSpeed;

            sequence.ClearPhysics();
            sequence.ClearAnimations();

            AddMotion(sequence, motionData, state.SubStateMod);
            return true;
        }

        /// ----- (00523ED0) --------------------------------------------------------
        /// int __thiscall CMotionTable::StopObjectCompletely(CMotionTable*this, MotionState* curr_state, CSequence* sequence, unsigned int* num_anims)
        /// acclient.c 339029
        /// </summary>
        public bool StopObjectCompletely(MotionState currState, Sequence sequence, ref uint numAnims)
        {
            LinkedListNode<Motion> node = currState.Modifiers.First;
            float speed = Constants.DefaultSpeed;
            bool retval = false;
            while (node != null)
            {
                Motion motion = node.Value;
                speed = motion.SpeedMod;
                if (StopSequenceMotion(motion.Id, motion.SpeedMod, ref currState, ref sequence, out numAnims))
                    retval = true;
                node = currState.Modifiers.First;
            }
            if (!StopSequenceMotion(currState.SubState, speed, ref currState,  ref sequence, out numAnims))
                return retval;
            return true;
        }
    }
}

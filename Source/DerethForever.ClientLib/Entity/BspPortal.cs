/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BSPPORTAL in the client
    /// </summary>
    public class BspPortal : BspNode
    {
        internal BspPortal()
        {
            Type = BspNodeType.Port;
        }

        /// <summary>
        /// 0x038 num_portals : Uint4B
        /// </summary>
        public uint NumPortals
        {
            get { return (uint)Portals.Count; }
        }

        /// <summary>
        /// 0x03c in_portals : Ptr32 Ptr32 CPortalPoly
        /// </summary>
        public List<PortalPolygon> Portals { get; set; } = new List<PortalPolygon>();

        /// <summary>
        /// //----- (0053DB70) --------------------------------------------------------
        /// int __thiscall BSPPORTAL::UnPackPortal(BSPPORTAL *this, void **addr, unsigned int size)
        /// acclient.c 364646
        /// </summary>
        public void UnpackPort(BinaryReader reader, BspTreeType treeType, List<Polygon> polygons)
        {
            SplittingPlane = reader.ReadPlane();

            PositiveNode = UnpackChild(reader, treeType, polygons);
            NegativeNode = UnpackChild(reader, treeType, polygons);

            if (treeType == BspTreeType.Drawing)
            {
                Sphere = Sphere.Unpack(reader);

                uint numPolys = reader.ReadUInt32();
                uint numPortals = reader.ReadUInt32();

                for (int i = 0; i < numPolys; i++)
                {
                    ushort polyIndex = reader.ReadUInt16();
                    InPolygons.Add(polygons[polyIndex]);
                }

                for (int i = 0; i < numPortals; i++)
                    Portals.Add(PortalPolygon.Unpack(reader, polygons));
            }
        }
    }
}

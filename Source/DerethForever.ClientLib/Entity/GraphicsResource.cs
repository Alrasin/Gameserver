/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// GraphicsResource in the client.  objects that inherit from GraphicsResource typically demonstrate
    /// multiple inheritance, so this is being implemented as an interface, with methods provided via 
    /// extension methods.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1302:InterfaceNamesMustBeginWithI", Justification = "hack to make it look like multiple inheritance")]
    public interface GraphicsResource
    {
        /// <summary>
        /// +0x008 m_bIsLost : Bool
        /// </summary>
        bool IsLost { get; set; }

        /// <summary>
        /// +0x010 m_TimeUsed : Float
        /// </summary>
        float TimeUsed { get; set; }

        /// <summary>
        /// +0x018 m_FrameUsed : Uint4B
        /// </summary>
        uint FrameUsed { get; set; }

        /// <summary>
        /// +0x01c m_bIsThrashable : Bool
        /// </summary>
        bool IsThrashable { get; set; }

        /// <summary>
        /// +0x01d m_AutoRestore : Bool
        /// </summary>
        bool AutoRestore { get; set; }

        /// <summary>
        /// +0x020 m_nResourceSize
        /// </summary>
        uint ResourceSize { get; set; }

        /// <summary>
        /// +0x024 m_ListIndex
        /// </summary>
        uint ListIndex { get; set; }
    }
}

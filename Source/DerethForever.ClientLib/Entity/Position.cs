/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    public class Position
    {
        /// <summary>
        /// +0x004 objcell_id       : Uint4B
        /// </summary>
        public uint CellId { get; set; }

        /// <summary>
        /// +0x008 frame            : Frame
        /// </summary>
        public Frame Frame { get; set; } = new Frame();

        public Position()
        {
        }

        public static Position Unpack(BinaryReader reader)
        {
            Position p = new Position
            {
                CellId = reader.ReadUInt32(),
                Frame = Frame.Unpack(reader)
            };
            return p;
        }

        public static Position UnpackAsFrame(BinaryReader reader, uint cellId)
        {
            Position p = new Position
            {
                CellId = cellId,
                Frame = Frame.Unpack(reader)
            };
            return p;
        }

        /// <summary>
        /// ----- (00424AB0) --------------------------------------------------------
        /// void __thiscall Position::Position(Position*this)
        /// acclient.c 96611
        /// </summary>
        public Position(Position p) : this()
        {
            CellId = p.CellId;
        }

        /// <summary>
        /// //----- (00509F60) --------------------------------------------------------
        /// AC1Legacy::Vector3* __thiscall Position::get_offset(Position*this, AC1Legacy::Vector3* result, Position* p)
        /// acclient.c 311690
        /// </summary>
        /// <param name="target"></param>
        public Vector3 GetOffset(Position target)
        {
            Vector3 offset = Vector3.Zero;

            if (this.CellId != target.CellId)
                offset = GetBlockOffset(this.CellId, target.CellId);

            offset.X += target.Frame.Origin.X - this.Frame.Origin.X;
            offset.Y += target.Frame.Origin.Y - this.Frame.Origin.Y;
            offset.Z += target.Frame.Origin.Z - this.Frame.Origin.Z;

            return offset;
        }

        /// <summary>
        /// calculates distances between the specified landcells.  the client takes makes 2 functions
        /// out of this, one left shifting the result 3 bits and the other multiplying by 24.  instead,
        /// we will just multiply by 192 and save the shift operation.
        ///
        /// //----- (0043E630) --------------------------------------------------------
        /// AC1Legacy::Vector3* __cdecl LandDefs::get_block_offset(AC1Legacy::Vector3* result, unsigned int cell_from, unsigned int cell_to)
        /// acclient.c 123110
        /// </summary>
        public static Vector3 GetBlockOffset(uint originBlock, uint targetBlock)
        {
            uint originX = (originBlock & 0xFF000000) >> 24;
            uint originY = (originBlock & 0x00FF0000) >> 16;
            uint targetX = (targetBlock & 0xFF000000) >> 24;
            uint targetY = (targetBlock & 0x00FF0000) >> 16;
            float deltaX = (targetX - originX) * 192.0f;
            float deltaY = (targetY - originY) * 192.0f;

            return new Vector3(deltaX, deltaY, 0f);
        }

        /// <summary>
        /// //----- (004527B0) --------------------------------------------------------
        /// unsigned int __thiscall Position::get_outside_cell_id(Position *this)
        /// acclient.c 143774
        /// </summary>
        public uint GetOutsideCellId()
        {
            Vector3 v = Frame.Origin;
            uint cellId = CellId;

            bool adjusted = LandDefs.AdjustToOutside(ref cellId, ref v);
            return (adjusted ? cellId : 0);
        }

        /// <summary>
        /// ----- (005A9520) --------------------------------------------------------
        /// double __thiscall Position::heading(Position*this, Position* p)
        /// </summary>
        /// <param name="position"></param>
        /// <returns>returns heading in degrees</returns>
        public float Heading(Position position)
        {
            Vector3 dir = GetOffset(position);
            dir.Z = 0.0f;
            return dir.GetHeading(false);
        }

        /// <summary>
        /// ----- (0050C420) --------------------------------------------------------
        /// AC1Legacy::Vector3* __thiscall Position::globaltolocalvec(Position*this, AC1Legacy::Vector3* result, AC1Legacy::Vector3* v)
        /// </summary>
        public Vector3 GlobalToLocalVector(Vector3 point)
        {
            return Frame.GlobalToLocalVector(point);
        }

        /// <summary>
        /// ----- (00456220) --------------------------------------------------------
        /// AC1Legacy::Vector3* __thiscall Position::localtoglobalvec(Position*this, AC1Legacy::Vector3* result, AC1Legacy::Vector3* v)
        /// acclient.c 147165
        /// </summary>
        public Vector3 LocalToGlobal(Vector3 point)
        {
            return Frame.LocalToGlobal(point);
        }

    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    public class AttackCone
    {
        /// <summary>
        /// +0x000 part_index       : Int4B
        /// </summary>
        public uint PartIndex { get; set; }

        /// <summary>
        /// +0x004 left             : Vec2D
        /// </summary>
        public Vector2 Left;

        /// <summary>
        /// +0x00c right            : Vec2D
        /// </summary>
        public Vector2 Right;

        /// <summary>
        /// +0x014 radius           : Float
        /// </summary>
        public float Radius { get; private set; }

        /// <summary>
        /// +0x018 height           : Float
        /// </summary>
        public float Height { get; private set; }

        /// <summary>
        /// ----- (00527060) --------------------------------------------------------
        /// int __thiscall AttackCone::UnPack(AttackCone *this, void **addr, unsigned int size)
        /// acclient.c 342622
        /// </summary>
        public static AttackCone Unpack(BinaryReader reader)
        {
            AttackCone a = new AttackCone
            {
                PartIndex = reader.ReadUInt32(),
                Left =
                {
                    X = reader.ReadSingle(),
                    Y = reader.ReadSingle()
                },
                Right =
                {
                    X = reader.ReadSingle(),
                    Y = reader.ReadSingle()
                },
                Radius = reader.ReadSingle(),
                Height = reader.ReadSingle()
            };
            return a;
        }
    }
}

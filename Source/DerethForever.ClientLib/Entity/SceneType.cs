/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CSceneType in the client
    /// </summary>
    public class SceneType
    {
        /// <summary>
        /// +0x000 scene_name       : PStringBase<char>
        /// </summary>
        public string SceneName;

        /// <summary>
        /// +0x004 scenes           : SmartArray<IDClass<_tagDataID,32,0>,1>
        /// </summary>
        public List<uint> Scenes = new List<uint>();

        /// <summary>
        /// +0x010 sound_table_desc : Ptr32 AmbientSTBDesc
        /// </summary>
        public AmbientSoundTable AmbientSoundTable;

        /// <summary>
        /// private because all valid instances come from unpack
        /// </summary>
        internal SceneType()
        {
        }

        /// <summary>
        /// //----- (005032C0) --------------------------------------------------------
        /// int __thiscall CSceneType::unpack(CSceneType *this, void **addr, unsigned int *size)
        /// acclient.c 304485
        /// </summary>
        public static SceneType Unpack(BinaryReader reader)
        {
            SceneType st = new SceneType();

            uint num = reader.ReadUInt32();
            for (uint i = 0; i < num; i++)
                st.Scenes.Add(reader.ReadUInt32());

            return st;
        }
    }
}

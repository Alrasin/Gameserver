/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CHILDLIST in the client
    /// </summary>
    public class ChildList
    {
        /// <summary>
        /// 0x000 num_objects : Uint2B
        /// </summary>
        public ushort NumObjects => (ushort)Objects.Count;

        /// <summary>
        /// 0x004 objects : SArray(CPhysicsObj *)
        /// </summary>
        public List<PhysicsObject> Objects { get; set; } = new List<PhysicsObject>();

        /// <summary>
        /// 0x00c frames : SArray(Frame)
        /// </summary>
        public List<Frame> Frames { get; set; } = new List<Frame>();

        /// <summary>
        /// 0x014 part_numbers : SArray(unsigned long)
        /// </summary>
        public List<uint> PartNumbers { get; set; } = new List<uint>();

        /// <summary>
        /// 0x01c location_ids : SArray(unsigned long)
        /// </summary>
        public List<uint> LocationIds { get; set; } = new List<uint>();
    }
}

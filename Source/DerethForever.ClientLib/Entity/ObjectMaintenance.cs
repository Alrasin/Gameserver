/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using DerethForever.ClientLib.Integration;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// acclient!CObjectMaint
    /// </summary>
    public class ObjectMaintenance
    {
        /// <summary>
        /// 0x010 is_active        : Int4B
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// +0x014 lost_cell_table  : IntrusiveHashTable<unsigned long,CLostCell *,0>
        /// </summary>
        public Dictionary<uint, LostCell> LostCellTable { get; set; }

        /// <summary>
        /// +0x084 object_table     : LongHash<CPhysicsObj>
        /// </summary>
        public Dictionary<uint, PhysicsObject> ObjectTable { get; set; }

        /// <summary>
        /// +0x09c null_object_table : LongHash<CPhysicsObj>
        /// </summary>
        public List<PhysicsObject> NullObjectTable { get; set; }

        /// <summary>
        /// +0x0b4 weenie_object_table : LongHash<CWeenieObject>
        /// </summary>
        public Dictionary<uint, IWorldObject> WeenieObjectTable { get; set; }

        /// <summary>
        /// +0x0cc null_weenie_object_table : LongHash<CWeenieObject>
        /// </summary>
        public List<IWorldObject> NullWeenieObjectTable { get; set; }

        /// <summary>
        /// +0x0e4 visible_object_table : HashSet<unsigned long>
        /// </summary>
        public List<int> VisibileObjectTable { get; set; }

        /// <summary>
        /// +0x158 destruction_object_table : HashTable<unsigned long,double,0>
        /// </summary>
        public Dictionary<uint, double> DestructionObjectTable { get; set; }

        /// <summary>
        /// +0x1cc object_inventory_table : LongHash<CObjectInventory>
        /// </summary>
        public Dictionary<uint, int> ObjectInventoryTable { get; set; }

        /// <summary>
        /// +0x1e4 object_destruction_queue : AC1Legacy::PQueueArray<double>
        /// </summary>
        public Queue<double> ObjectDestructionQueue { get; set; }

        // TODO: Add methods - just stubbed the properties
    }
}

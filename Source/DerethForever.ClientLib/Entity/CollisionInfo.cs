/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    public class CollisionInfo
    {
        /// <summary>
        /// +0x000 last_known_contact_plane_valid : Int4B
        /// </summary>
        public bool LastKnownContactPlaneValid; // int32 in client

        /// <summary>
        /// +0x004 last_known_contact_plane : Plane
        /// </summary>
        public Plane LastKnownContactPlane;

        /// <summary>
        ///  +0x014 last_known_contact_plane_is_water : Int4B
        /// </summary>
        public bool LastKnownContactPlaneIsWater;

        /// <summary>
        /// +0x018 contact_plane_valid : Int4B
        /// </summary>
        public bool ContactPlaneValid; // int32 in client

        /// <summary>
        /// +0x01c contact_plane    : Plane
        /// </summary>
        public Plane ContactPlane;

        /// <summary>
        /// +0x02c contact_plane_cell_id : Uint4B
        /// </summary>
        public uint ContactPlaneCellId;

        /// <summary>
        /// +0x030 last_known_contact_plane_cell_id : Uint4B
        /// </summary>
        public uint LastKnownContactPlaneCellId;

        /// <summary>
        /// +0x034 contact_plane_is_water : Int4B
        /// </summary>
        public bool ContactPlaneIsWater;

        /// <summary>
        /// +0x038 sliding_normal_valid : Int4B
        /// </summary>
        public bool SlidingNormalValid; // int32 in client

        /// <summary>
        /// +0x03c sliding_normal   : AC1Legacy::Vector3
        /// </summary>
        public Vector3 SlidingNormal;

        /// <summary>
        /// +0x048 collision_normal_valid : Int4B
        /// </summary>
        public bool CollisionNormalValid; // int32 in client

        /// <summary>
        /// +0x04c collision_normal : AC1Legacy::Vector3
        /// </summary>
        public Vector3 CollisionNormal;

        /// <summary>
        /// +0x058 adjust_offset    : AC1Legacy::Vector3
        /// </summary>
        public Vector3 AdjustOffset;

        /// <summary>
        /// +0x064 num_collide_object : Uint4B
        /// </summary>
        public int NumCollisionObjects // use list.Count instead?
        {
            get { return (CollisionObjects?.Count ?? 0); }
        }

        /// <summary>
        /// +0x068 collide_object   : DArray<CPhysicsObj const *>
        /// </summary>
        public List<PhysicsObject> CollisionObjects = new List<PhysicsObject>();

        /// <summary>
        /// +0x078 last_collided_object : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject LastCollidedObject;

        /// <summary>
        /// +0x07c collided_with_environment : Int4B
        /// </summary>
        public bool CollidedWithEnvironment; // int32 in client

        /// <summary>
        /// +0x080 frames_stationary_fall : Int4B
        /// </summary>
        public int FramesStationaryFall; // int32 in client

        /// <summary>
        /// //----- (00509D60) --------------------------------------------------------
        /// void __thiscall COLLISIONINFO::init(COLLISIONINFO*this)
        /// acclient.c 311567
        /// </summary>
        public CollisionInfo()
        {
            CollisionObjects = new List<PhysicsObject>();
        }

        /// <summary>
        /// ----- (0050A000) --------------------------------------------------------
        /// void __thiscall COLLISIONINFO::set_collision_normal(COLLISIONINFO*this, AC1Legacy::Vector3* normal)
        /// acclient.c 311726
        /// </summary>
        public void SetCollisionNormal(Vector3 normal)
        {
            CollisionNormalValid = true;
            CollisionNormal = normal;
            if (NormalizeCheckSmall(ref normal))
                CollisionNormal = Vector3.Zero;
        }

        /// <summary>
        /// ----- (00452460) --------------------------------------------------------
        /// int __thiscall AC1Legacy::Vector3::normalize_check_small(AC1Legacy::Vector3*this)
        /// acclient.c 143622
        /// </summary>
        public static bool NormalizeCheckSmall(ref Vector3 vector3)
        {
            float dist = vector3.Length();
            if (dist > Constants.TOLERANCE)
            {
                vector3 *= 1.0f / dist;
                return false;
            }
            return true;
        }
    }
}

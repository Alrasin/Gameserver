/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public struct Sphere
    {
        /// <summary>
        /// must be a field because struct shenanigans.
        /// +0x000 center           : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Center;

        /// <summary>
        /// +0x00c radius           : Float
        /// </summary>
        public float Radius { get; set; }

        public Sphere(Vector3 origin, float radius)
        {
            Center = origin;
            Radius = radius;
        }

        /// <summary>
        /// ----- (0051FD20) --------------------------------------------------------
        /// int __thiscall CSphere::UnPack(CSphere*this, void** addr, unsigned int size)
        /// acclient.c 334268
        /// </summary>
        public static Sphere Unpack(BinaryReader datReader)
        {
            Sphere obj = new Sphere();

            obj.Center = new Vector3(datReader.ReadSingle(), datReader.ReadSingle(), datReader.ReadSingle());
            obj.Radius = datReader.ReadSingle();
            return obj;
        }

        /// <summary>
        /// ----- (005344A0) --------------------------------------------------------
        /// BOOL __thiscall CSphere::intersects(CSphere*this, CSphere* s)
        /// I had to look this up - the decompiled code made no sense.
        /// </summary>
        public bool Intersects(Sphere sphere)
        {
            Vector3 offset = Center - sphere.Center;
            float reach = Radius + sphere.Radius;
            float distance = offset.LengthSquared() - reach * reach;

            if (Constants.TOLERANCE > distance)
                return true;

            return false;
        }

        /// <summary>
        /// ----- (005368F0) --------------------------------------------------------
        /// double __stdcall CSphere::find_time_of_collision(AC1Legacy::Vector3* movement, AC1Legacy::Vector3* disp, const float radsum)
        /// acclient.c 358481
        /// </summary>
        public double FindTimeOfCollision(Vector3 movement, Vector3 disp, float radSum)
        {
            // TODO: Not sure how to name these variables.
            float lengthSquared = movement.LengthSquared(); // v4
            float v5 = -Vector3.Dot(disp, movement);
            float v6 = disp.LengthSquared() - (radSum * radSum);
            float v7 = (v5 * v5) - (v6 * lengthSquared);

            if (v6 < Constants.TOLERANCE || lengthSquared < Constants.TOLERANCE || v7 < 0.0)
                return -1;

            double v8 = Math.Sqrt(v7);

            if ((v5 - v8) < 0.0)
                return (v8 + v5) / lengthSquared; // sign flip

            return (v5 - v8) / lengthSquared;
        }

        /// <summary>
        /// ----- (00537900) --------------------------------------------------------
        /// signed int __thiscall CSphere::step_sphere_up(CSphere*this, CTransition* transition, CSphere* check_pos, AC1Legacy::Vector3* disp, float radsum)
        /// acclient.c 359072
        /// </summary>
        public TransitionState StepSphereUp(ref Transition transition, ref Sphere checkPos, ref Vector3 disp, float radsum)
        {
            radsum += Constants.TOLERANCE;
            if (transition.ObjectInfo.StepUpHeight < radsum - disp.Z)
                return SlideSphere(ref transition.ObjectInfo, ref transition.SpherePath, ref transition.CollisionInfo, ref disp, radsum, 0);


            float z = transition.SpherePath.GlobalCurrentCenter[0].Z - Center.Z; // v7
            float y = transition.SpherePath.GlobalCurrentCenter[0].Y - Center.Y; // v8
            Vector3 collisionNormal;
            collisionNormal.X = transition.SpherePath.GlobalCurrentCenter[0].X - Center.X;
            collisionNormal.Y = y;
            collisionNormal.Z = z;

            if (transition.StepUp(collisionNormal))
                return TransitionState.Ok;

            return transition.SpherePath.StepUpSlide(ref transition.ObjectInfo, ref transition.CollisionInfo);

        }

        /// <summary>
        /// ----- (00536F40) --------------------------------------------------------
        /// signed int __thiscall CSphere::slide_sphere(CSphere*this, OBJECTINFO*object, SPHEREPATH* path, COLLISIONINFO* collisions, AC1Legacy::Vector3* disp, float radsum, int sphere_num)
        /// acclient.c 358709
        /// </summary>
        public TransitionState SlideSphere(ref ObjectInfo objectInfo, ref SpherePath spherePath, ref CollisionInfo collisionInfo, ref Vector3 disp, float radsum, int sphereNum)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ----- (00537440) --------------------------------------------------------
        /// signed int __thiscall CSphere::slide_sphere(CSphere*this, SPHEREPATH* path, COLLISIONINFO* collisions, AC1Legacy::Vector3* collision_normal, AC1Legacy::Vector3* curr_pos)
        /// acclient.c 358899
        /// </summary>
        public TransitionState SlideSphere(SpherePath spherePath, CollisionInfo collisionInfo, Vector3 collisionNormal, Vector3 currentPosition)
        {
            SpherePath path = spherePath;
            CollisionInfo collisions = collisionInfo;

            if (collisionNormal.Equals(Vector3.Zero))
            {
                Vector3 halfOffset = (currentPosition - Center) * 0.5f;
                path.AddOffsetToCheckPosition(halfOffset);
                return TransitionState.Adjusted;
            }

            collisions.SetCollisionNormal(collisionNormal);
            Vector3 blockOffset = LandDefs.GetBlockOffset(path.CurrentPosition.CellId, path.CheckPosition.CellId);
            Vector3 change = blockOffset + (Center - currentPosition);

            Plane contactPlane;
            if (collisions.ContactPlaneValid)
                contactPlane = collisions.ContactPlane;
            else
                contactPlane = collisions.LastKnownContactPlane;

            Vector3 direction = Vector3.Cross(collisionNormal, contactPlane.Normal);
            float directionLengthSquared = direction.LengthSquared();

            if (directionLengthSquared >= Constants.TOLERANCE)
            {
                float diff = Vector3.Dot(direction, change);
                float inverseDirectionLengthSquared = 1.0f / directionLengthSquared;
                Vector3 offset = direction * diff * inverseDirectionLengthSquared;
                float offsetLengthSquared = offset.LengthSquared();
                if (offsetLengthSquared < Constants.TOLERANCE)
                    return TransitionState.Collided;

                offset -= change;
                path.AddOffsetToCheckPosition(offset);
                return TransitionState.Slid;
            }
            if (Vector3.Dot(collisionNormal, contactPlane.Normal) >= 0.0f)
            {
                float diff = Vector3.Dot(collisionNormal, change);
                Vector3 offset = -collisionNormal * diff;
                path.AddOffsetToCheckPosition(offset);
                return TransitionState.Slid;
            }

            collisionNormal = -change;
            if (!CollisionInfo.NormalizeCheckSmall(ref collisionNormal))
                collisions.SetCollisionNormal(collisionNormal);

            return TransitionState.Ok;
        }
    }
}

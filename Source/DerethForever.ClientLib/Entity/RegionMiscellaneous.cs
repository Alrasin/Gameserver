/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// RegionMisc in the client
    /// </summary>
    public class RegionMiscellaneous
    {
        /// <summary>
        /// 0x000 version : Uint4B
        /// </summary>
        public uint Version;

        /// <summary>
        /// 0x004 game_map : IDClass
        /// </summary>
        public uint GameMapId;

        /// <summary>
        /// 0x008 autotest_map : IDClass
        /// </summary>
        public uint AutoTestMapId;

        /// <summary>
        /// 0x00c autotest_map_size : Uint4B
        /// </summary>
        public uint AutoTestMapSize;

        /// <summary>
        /// 0x010 clear_cell : IDClass
        /// </summary>
        public uint ClearCellId;

        /// <summary>
        /// 0x014 clear_monster : IDClass
        /// </summary>
        public uint ClearMonsterId;

        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private RegionMiscellaneous()
        {
        }

        /// <summary>
        /// //----- (004FEBC0) --------------------------------------------------------
        /// int __thiscall RegionMisc::UnPack(RegionMisc *this, void **addr, unsigned int *size)
        /// acclient.c 299098
        /// </summary>
        public static RegionMiscellaneous Unpack(BinaryReader reader)
        {
            RegionMiscellaneous rm = new RegionMiscellaneous();

            rm.Version = reader.ReadUInt32();
            rm.GameMapId = reader.ReadUInt32();
            rm.AutoTestMapId = reader.ReadUInt32();
            rm.AutoTestMapSize = reader.ReadUInt32();
            rm.ClearCellId = reader.ReadUInt32();
            rm.ClearMonsterId = reader.ReadUInt32();

            return rm;
        }
    }
}

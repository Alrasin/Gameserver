/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// dt acclient!OBJECTINFO **** Notice the case
    /// </summary>
    public class ObjectInfo
    {
        /// <summary>
        /// +0x000 object           : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObject;

        /// <summary>
        /// +0x004 state            : Int4B
        /// </summary>
        public ObjectInfoState ObjectInfoState;

        /// <summary>
        /// +0x008 scale            : Float
        /// </summary>
        public float Scale;

        /// <summary>
        /// +0x00c step_up_height   : Float
        /// </summary>
        public float StepUpHeight;

        /// <summary>
        /// +0x010 step_down_height : Float
        /// </summary>
        public float StepDownHeight;

        /// <summary>
        /// +0x014 ethereal         : Int4B
        /// </summary>
        public bool Ethereal; // int32 in client

        /// <summary>
        /// +0x018 step_down        : Int4B
        /// </summary>
        public bool StepDown; // int32 in client

        /// <summary>
        /// +0x01c targetID         : Uint4B
        /// </summary>
        public uint TargetId;

        public ObjectInfo()
        {
            PhysicsObject = new PhysicsObject();
        }

        /// <summary>
        /// this constructor is a substitute for the following method in the client:
        /// //----- (0050CF30) --------------------------------------------------------
        /// void __thiscall OBJECTINFO::init(OBJECTINFO* this, CPhysicsObj* _object, int object_state)
        /// acclient.c 314118
        /// </summary>
        public ObjectInfo(PhysicsObject physicsObject, ObjectInfoState state)
        {
            PhysicsObject = physicsObject;
            ObjectInfoState = state;
            Scale = physicsObject.Scale;
            StepUpHeight = physicsObject.GetStepUpHeight();
            StepDownHeight = physicsObject.GetStepDownHeight();
            Ethereal = physicsObject.State.HasFlag(PhysicsState.Ethereal);
            StepDown = !physicsObject.State.HasFlag(PhysicsState.Missile);

            // Note: Instance is our call to the interface that will give us world object (CWeenieObject) Info
            if (physicsObject?.Instance == null)
                return;

            if (physicsObject.Instance.IsImpenetrable())
                ObjectInfoState |= ObjectInfoState.IsImpenetrable;

            if (physicsObject.Instance.IsPlayer())
                ObjectInfoState |= ObjectInfoState.IsPlayer;

            if (physicsObject.Instance.IsPK())
                ObjectInfoState |= ObjectInfoState.IsPK;

            if (physicsObject.Instance.IsPKLite())
                ObjectInfoState |= ObjectInfoState.IsPkLite;
        }
    }
}

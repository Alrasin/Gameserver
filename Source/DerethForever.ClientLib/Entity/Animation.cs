/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CAnimation in the client
    /// </summary>
    public class Animation : DatabaseObject
    {
        /// <summary>
        /// +0x000 __VFN_table : Ptr32
        /// </summary>
        public uint AnimationId { get; set; }

        /// <summary>
        /// 0x038 pos_frames : Ptr32 AFrame
        /// </summary>
        public List<Frame> PosFrames { get; set; } = new List<Frame>();

        /// <summary>
        /// 0x03c part_frames : Ptr32 AnimFrame
        /// </summary>
        public List<AnimationFrame> PartFrames { get; set; } = new List<AnimationFrame>();

        /// <summary>
        /// 0x040 has_hooks : Int4B
        /// </summary>
        public bool HasHooks { get; set; }

        /// <summary>
        /// 0x044 num_parts : Uint4B
        /// </summary>
        public uint NumParts
        {
            get { return (uint) PartFrames.Count; }
        }

        /// <summary>
        /// 0x048 num_frames : Uint4B
        /// </summary>
        public uint NumFrames
        {
            get { return (uint) PosFrames.Count; }
        }

        /// <summary>
        /// ----- (0051FB50) --------------------------------------------------------
        /// int __thiscall CAnimation::UnPack(CAnimation*this, void** addr, unsigned int size)
        /// acclient.c 334086
        /// </summary>
        public static Animation Unpack(BinaryReader reader)
        {
            // TODO: Ask Behemoth - this looks to be loading right - but there are inherited fields
            // that are never assigned. ??
            Animation anim = new Animation();
            anim.AnimationId = reader.ReadUInt32();

            uint flags = reader.ReadUInt32();

            uint numParts = reader.ReadUInt32();
            uint numFrames = reader.ReadUInt32();

            bool hasPosFrames = ((flags & 1) != 0);
            if (hasPosFrames && numFrames > 0)
            {
                for (uint i = 0; i < numFrames; i++)
                {
                    // Origin
                    anim.PosFrames.Add(Frame.Unpack(reader));
                }
            }

            for (uint i = 0; i < numFrames; i++)
            {
                AnimationFrame f = AnimationFrame.Unpack(numParts, reader);
                anim.PartFrames.Add(f);
            }

            return anim;
        }
    }
}

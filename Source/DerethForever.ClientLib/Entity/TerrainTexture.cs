/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// TerrainTex in the client
    /// </summary>
    public class TerrainTexture
    {
        /// <summary>
        /// 0x000 tex_gid : IDClass
        /// </summary>
        public uint Id;

        /// <summary>
        /// 0x004 base_texture : Ptr32 ImgTex
        /// </summary>
        public uint BaseTexture;

        /// <summary>
        /// 0x008 min_slope : Float
        /// </summary>
        public float MinimumSlope;

        /// <summary>
        /// 0x00c tex_tiling : Uint4B
        /// </summary>
        public uint TextureTiling;

        /// <summary>
        /// 0x010 max_vert_bright : Uint4B
        /// </summary>
        public uint MaxVertexBrightness;

        /// <summary>
        /// 0x014 min_vert_bright : Uint4B
        /// </summary>
        public uint MinVertexBrightness;

        /// <summary>
        /// 0x018 max_vert_saturate : Uint4B
        /// </summary>
        public uint MaxVertexSaturation;

        /// <summary>
        /// 0x01c min_vert_saturate : Uint4B
        /// </summary>
        public uint MinVertexSaturation;

        /// <summary>
        /// 0x020 max_vert_hue : Uint4B
        /// </summary>
        public uint MaxVertexHue;

        /// <summary>
        /// 0x024 min_vert_hue : Uint4B
        /// </summary>
        public uint MinVertexHue;

        /// <summary>
        /// 0x028 detail_tex_tiling : Uint4B
        /// </summary>
        public uint DetailTextureTiling;

        /// <summary>
        /// 0x02c detail_tex_gid : IDClass
        /// </summary>
        public uint DetailTextureId;

        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private TerrainTexture()
        {
        }

        /// <summary>
        /// //----- (00503B40) --------------------------------------------------------
        /// int __thiscall TerrainTex::UnPack(TerrainTex *this, void **addr, unsigned int *size)
        /// acclient.c 305054
        /// </summary>
        public static TerrainTexture Unpack(BinaryReader reader)
        {
            TerrainTexture tt = new TerrainTexture();

            tt.Id = reader.ReadUInt32();
            tt.TextureTiling = reader.ReadUInt32();
            tt.MaxVertexBrightness = reader.ReadUInt32();
            tt.MinVertexBrightness = reader.ReadUInt32();
            tt.MaxVertexSaturation = reader.ReadUInt32();
            tt.MinVertexSaturation = reader.ReadUInt32();
            tt.MaxVertexHue = reader.ReadUInt32();
            tt.MinVertexHue = reader.ReadUInt32();
            tt.DetailTextureTiling = reader.ReadUInt32();
            tt.DetailTextureId = reader.ReadUInt32();

            return tt;
        }
    }
}

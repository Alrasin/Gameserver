/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    public class ShadowObject
    {
        /// <summary>
        /// +0x008 id               : Uint4B
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// +0x00c physobj          : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj { get; set; }

        /// <summary>
        ///  +0x010 cell_id          : Uint4B
        /// </summary>
        public uint CellId { get; set; }

        /// <summary>
        /// +0x014 cell             : Ptr32 CObjCell
        /// </summary>
        public ObjectCell Cell { get; set; }

        /// <summary>
        /// ----- (0050FF00) --------------------------------------------------------
        /// void __thiscall CShadowObj::CShadowObj(CShadowObj*this)
        /// acclient.c 317241
        /// </summary>
        public ShadowObject(PhysicsObject physicsObject, ObjectCell objectCell)
        {
            PhysicsObj = physicsObject;
            Cell = objectCell;
        }

        /// <summary>
        /// ----- (0051BB30) --------------------------------------------------------
        /// void __thiscall CShadowObj::set_physobj(CShadowObj*this, CPhysicsObj* _physobj)
        /// acclient.c 329719
        /// </summary>
        public void SetPhysicsObject(PhysicsObject physicsObject)
        {
            PhysicsObj = physicsObject;
            Id = physicsObject.Id;
        }
    }
}

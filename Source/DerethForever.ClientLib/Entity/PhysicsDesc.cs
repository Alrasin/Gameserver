/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.Numerics;
using DerethForever.ClientLib.Entity;
using DerethForever.ClientLib.Enum;

public class PhysicsDesc
{
    /// <summary>
    /// +0x004 bitfield         : Uint4B
    /// </summary>
    public uint Bitfield { get; set; }

    /// <summary>
    /// +0x008 movement_buffer  : Ptr32 Void
    /// </summary>
    public object MovementBuffer { get; set; }

    /// <summary>
    /// +0x010 autonomous_movement : Int4B
    /// </summary>
    public bool AutonomousMovement { get; set; }

    /// <summary>
    /// +0x014 animframe_id     : Uint4B
    /// </summary>
    public uint AnimFrameId { get; set; }

    /// <summary>
    /// +0x018 pos              : Position
    /// </summary>
    public Position Pos { get; set; }

    /// <summary>
    /// +0x060 state            : Uint4B
    /// </summary>
    public PhysicsState State { get; set; }

    /// <summary>
    /// +0x064 object_scale     : Float
    /// </summary>
    public float ObjectScale { get; set; }

    /// <summary>
    /// +0x068 friction         : Float
    /// </summary>
    public float Friction { get; set; }

    /// <summary>
    /// +0x06c elasticity       : Float
    /// </summary>
    public float Elasticity { get; set; }

    /// <summary>
    /// +0x070 translucency     : Float
    /// </summary>
    public float Translucency { get; set; }

    /// <summary>
    /// +0x074 velocity         : AC1Legacy::Vector3
    /// </summary>
    public Vector3 Velocity { get; set; }

    /// <summary>
    /// +0x080 acceleration     : AC1Legacy::Vector3
    /// </summary>
    public Vector3 Acceleration { get; set; }

    /// <summary>
    /// +0x08c omega            : AC1Legacy::Vector3
    /// </summary>
    public Vector3 Omega { get; set; }

    /// <summary>
    /// +0x098 num_children     : Uint4B
    /// </summary>
    public uint NumChildren { get; set; }

    /// <summary>
    /// +0x09c child_ids        : Ptr32 Uint4B
    /// </summary>
    public List<int> ChildIds { get; set; }

    /// <summary>
    /// +0x0a0 child_location_ids : Ptr32 Uint4B
    /// </summary>
    public List<int> ChildLocationIds { get; set; }

    /// <summary>
    /// +0x0a4 parent_id        : Uint4B
    /// </summary>
    public uint ParentId { get; set; }

    /// <summary>
    /// +0x0a8 location_id      : Uint4B
    /// </summary>
    public uint LocationId { get; set; }

    /// <summary>
    /// +0x0ac mtable_id        : IDClass<_tagDataID,32,0>
    /// </summary>
    public uint MotionTableId { get; set; }

    /// <summary>
    /// +0x0b0 stable_id        : IDClass<_tagDataID,32,0>
    /// </summary>
    public uint SoundTableId { get; set; }

    /// <summary>
    /// +0x0b4 phstable_id      : IDClass<_tagDataID,32,0>
    /// </summary>
    public uint PhsyicsTableId { get; set; }

    /// <summary>
    /// +0x0b8 default_script   : PScriptType
    /// </summary>
    public PlayScript DefaultScript { get; set; }

    /// <summary>
    /// +0x0bc default_script_intensity : Float
    /// </summary>
    public float DefaultScriptIntensity { get; set; }

    /// <summary>
    /// +0x0bc default_script_intensity : Float
    /// </summary>
    public uint SetupID { get; set; }

    /// <summary>
    /// +0x0c4 timestamps       : [9] Uint2B
    /// </summary>
    public int[] Timestamps;

    // This file looks very similar to world object - need to research Coral Golem.
    // TODO: RE the methods.

}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// represents both CVertex and CSWVertex.  The 2 types are interchangable
    /// for the most part.  CSWVertex was (speculatively) used for software
    /// rendering pre-ToD.  software rendering was dropped with ToD, which
    /// coincides with the appearance of CVertex in the client.  CVertex has
    /// dummy "Reserve" uints to take up the same data space as CSWVertex did,
    /// 32 bytes.  However, only the first 12 bytes are relevenat, at which
    /// point "Vertex" becomes synonymous with "Vector3".
    /// </summary>
    public class Vertex
    {
        /// <summary>
        /// +0x000 vertex           : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Vector = Vector3.Zero;

        /// <summary>
        /// +0x00c vert_id          : Int2B
        /// </summary>
        public short VertexId { get; set; }

        /// <summary>
        /// +0x014 normal           : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Normal { get; set; }

        /// <summary>
        /// +0x010 uvs              : Ptr32 CVec2Duv
        /// </summary>
        public List<UvMapping> UVs { get; set; } = new List<UvMapping>();

        /// <summary>
        /// //----- (0053DE50) --------------------------------------------------------
        /// int __thiscall CSWVertex::UnPack(CSWVertex *this, void **addr, unsigned int size)
        /// acclient.c 364861
        /// </summary>
        public static Vertex Unpack(BinaryReader reader)
        {
            Vertex v = new Vertex();

            v.VertexId = reader.ReadInt16();
            short uvCount = reader.ReadInt16();

            v.Vector = reader.ReadVector();
            v.Normal = reader.ReadVector();

            for (short i = 0; i < uvCount; i++)
                v.UVs.Add(UvMapping.Unpack(reader));

            return v;
        }
    }
}

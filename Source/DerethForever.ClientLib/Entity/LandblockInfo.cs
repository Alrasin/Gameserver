/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.DatUtil;
using log4net;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CLandBlockInfo from the client
    /// </summary>
    public class LandblockInfo : DatabaseObject
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// intentionally private.  LandblockInfo objects must be generated from the dat files
        /// </summary>
        private LandblockInfo()
        {   
        }

        /// <summary>
        /// 0x038 num_objects : Uint4B
        /// </summary>
        public uint NumObjects => (uint)ObjectIds.Count;

        /// <summary>
        /// 0x03c object_ids : Ptr32 IDClass&lt;_tagDataID,32,0&gt;
        /// </summary>
        public List<uint> ObjectIds { get; set; } = new List<uint>();

        /// <summary>
        /// +0x040 object_frames    : Ptr32 Frame+0x040 object_frames    : Ptr32 Frame
        /// </summary>
        public List<Frame> ObjectFrames { get; set; } = new List<Frame>();

        /// <summary>
        /// +0x044 num_buildings    : Uint4B
        /// </summary>
        public uint NumBuildings => (uint)Buildings.Count;

        /// <summary>
        /// +0x048 buildings        : Ptr32 Ptr32 BuildInfo
        /// </summary>
        public List<BuildingInfo> Buildings = new List<BuildingInfo>();

        /// <summary>
        /// +0x04c restriction_table : Ptr32 PackableHashTable&lt;unsigned long,unsigned long&gt;
        /// </summary>
        public List<Restriction> RestrictionTable = new List<Restriction>();

        /// <summary>
        /// +0x050 cell_ownership   : Ptr32 PackableHashTable&lt;unsigned long,PackableList&lt;unsigned long&gt; &gt;
        /// </summary>
        public uint CellOwnership;

        /// <summary>
        /// normally this would just check the list length, but this list applies to dungeons which
        /// were traditionally lazy-loaded via streaming content
        /// +0x054 num_cells        : Uint4B
        /// </summary>
        public uint NumCells { get; set; }

        /// <summary>
        /// +0x058 cell_ids         : Ptr32 Uint4B
        /// </summary>
        public List<uint> CellIds = new List<uint>();

        /// <summary>
        /// +0x05c cells            : Ptr32 Ptr32 CEnvCell
        /// </summary>
        public List<EnvironmentCell> Cells { get; set; } = new List<EnvironmentCell>();

        /// <summary>
        /// //----- (0052ECA0) --------------------------------------------------------
        /// int __thiscall CLandBlockInfo::UnPack(CLandBlockInfo*this, void** addr, unsigned int size)
        /// acclient.c 351049
        /// </summary>
        public static LandblockInfo Unpack(BinaryReader reader)
        {
            LandblockInfo info = new LandblockInfo();
            info.Id = reader.ReadUInt32();
            
            info.NumCells = reader.ReadUInt32();
            uint numObjects = reader.ReadUInt32();

            for (uint i = 0; i < numObjects; i++)
            {
                info.ObjectIds.Add(reader.ReadUInt32());
                info.ObjectFrames.Add(Frame.Unpack(reader));
            }

            ushort numBuildings = reader.ReadUInt16();
            ushort buildingFlags = reader.ReadUInt16();

            for (ushort i = 0; i < numBuildings; i++)
            {
                BuildingInfo bi = new BuildingInfo();
                info.Buildings.Add(bi);

                bi.Id = reader.ReadUInt32();
                bi.Frame = Frame.Unpack(reader);
                bi.NumLeaves = reader.ReadUInt32();

                uint numPortals = reader.ReadUInt32();

                for (int j = 0; j < numPortals; j++)
                    bi.Portals.Add(BuildingPortal.Unpack(reader, info.Id & 0xFFFF0000));
            }

            if ((buildingFlags & 1) != 0)
            {
                ushort numRestrictions = reader.ReadUInt16();
                reader.ReadUInt16(); // static 8, line 351225

                for (int i = 0; i < numRestrictions; i++)
                    info.RestrictionTable.Add(Restriction.Unpack(reader));
            }

            // this part isn't technically part of unpack, but we want to force it
            // on the server ahead of time.  only problem is, things may be missing
            // so we wrap it in a try block
            for (uint i = 0; i < info.NumCells; i++)
            {
                uint cellId = (info.Id & 0xFFFF0000) + 0x0100u + i;
                try
                {
                    var cell = CellDatReader.Current.Unpack<EnvironmentCell>(cellId);
                    info.Cells.Add(cell);
                }
                catch
                {
                    log.Error($"Unable to load cell {cellId:X4}");
                }
            }
            return info;
        }
    }
}

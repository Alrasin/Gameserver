/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;
using DerethForever.ClientLib.Enum.LandDefs;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// LScape in the client.  this class is not used in the same manner as
    /// the client, as the client uses it to manage the known and drawn
    /// landblocks for the player.  our uses for it are primarily to more
    /// accurately replicate the utility functions it contains in the client.
    /// more succinctly, this is the client version of a Landblock Manager.
    /// </summary>
    public class Landscape
    {
        /// <summary>
        /// radius from the center of the 2D array the client uses to cache landblocks
        /// 
        /// 0x000 mid_radius : Int4B
        /// </summary>
        private static int _midRadius = 5;

        /// <summary>
        /// size (x and y) of the 2D array the client uses to cache landblocks
        /// 
        /// 0x004 mid_width : Int4B
        /// </summary>
        private static int _midWidth = 11;
        
        /// <summary>
        /// gets the max single direction difference (in blocks) and direction of the specified
        /// indecis.  probably not relevant for server purposes, but translated before that was
        /// realized.  leaving it because there's no point in nuking it.
        /// 
        /// //----- (00504F90) --------------------------------------------------------
        /// void __thiscall LScape::get_block_orient(LScape *this, int ix, int iy, int *size, LandDefs::Direction *dir)
        /// acclient.c 306681
        /// </summary>
        public static void GetBlockOrientation(int x, int y, out int size, out Direction direction)
        {
            int deltaX = x - _midRadius;
            int deltaY = y - _midRadius;
            int absX = Math.Abs(deltaX);
            int absY = Math.Abs(deltaY);

            int absDist = Math.Max(absX, absY);

            if (absDist > 4)
            {
                size = 8;
                direction = Direction.InViewerBlock;
                return;
            }
            
            if (absDist > 2)
                size = 4;
            else if (absDist > 1)
                size = 2;
            else
                size = 1;

            // 306720
            if (deltaX == size)
            {
                if (deltaY == size)
                    direction = Direction.NorthEast;
                else if (deltaY == -size)
                    direction = Direction.SouthEast;
                else
                    direction = Direction.East;
            }
            else if (deltaX == -size)
            {
                if (deltaY == size)
                    direction = Direction.NorthWest;
                else if (deltaY == -size)
                    direction = Direction.SouthWest;
                else
                    direction = Direction.West;
            }
            else if (deltaY == size)
                direction = Direction.North;
            else if (deltaY == -size)
                direction = Direction.South;
            else
                direction = Direction.InViewerBlock;
        }
    }
}

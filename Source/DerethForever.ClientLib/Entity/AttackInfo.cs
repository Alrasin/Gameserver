/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;

namespace DerethForever.ClientLib.Entity
{
    public class AttackInfo
    {
        /// <summary>
        /// +0x000 attack_id        : Uint4B
        /// </summary>
        public uint AttackId { get; set; }

        /// <summary>
        /// +0x004 part_index       : Int4B
        /// </summary>
        public int PartIndex { get; set; }

        /// <summary>
        /// +0x008 attack_radius    : Float
        /// </summary>
        public float AttackRadius { get; set; }

        /// <summary>
        /// +0x00c waiting_for_cells : Uint4B
        /// </summary>
        public bool WaitingForCells { get; set; }

        /// <summary>
        /// +0x010 num_objects      : Uint4B
        /// </summary>
        public uint NumObjects
        {
            get { return (uint)ObjectList.Count; }
        }

        /// <summary>
        /// +0x014 object_list      : DArray<ObjectInfo>
        /// </summary>
        public List<AttackObjectInfo> ObjectList { get; set; }

        /// <summary>
        /// ----- (0051A190) --------------------------------------------------------
        /// void __thiscall AttackInfo::AddObject(AttackInfo*this, unsigned int object_id, unsigned int hit_location)
        /// </summary>
        public void AddObject(uint objectId, uint hitLocation)
        {
            ObjectList.Add(new AttackObjectInfo { ObjectId = objectId, HitLocation = hitLocation });
        }

        /// <summary>
        /// ----- (0051A140) --------------------------------------------------------
        /// void __thiscall AttackInfo::AttackInfo(AttackInfo*this, unsigned int _attack_id, int _part_index, float _attack_radius)
        /// acclient.c 327810
        /// </summary>
        public AttackInfo(uint attackId, int partIndex, float attackRadius)
        {
            AttackId = attackId;
            PartIndex = partIndex;
            AttackRadius = attackRadius;
            ObjectList = new List<AttackObjectInfo>();
        }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// TMTerrainDesc in the client
    /// </summary>
    public class TextureMergeTerrainDescription
    {
        /// <summary>
        /// 0x000 terrain_type : LandDefs::TerrainType
        /// </summary>
        public Enum.TerrainType TerrainType;

        /// <summary>
        /// technically a list in the client, but the dat files only ever have 1 entry.
        /// 0x004 terrain_tex : AC1Legacy::SmartArray(TerrainTex *)
        /// </summary>
        public List<TerrainTexture> TerrainTextures = new List<TerrainTexture>();

        /// <summary>
        /// private because Unpack is the only valid source of instances
        /// </summary>
        private TextureMergeTerrainDescription()
        {
        }

        /// <summary>
        /// //----- (00504260) --------------------------------------------------------
        /// int __thiscall TMTerrainDesc::UnPack(TMTerrainDesc *this, void **addr, unsigned int *size)
        /// acclient.c 305755
        /// </summary>
        public static TextureMergeTerrainDescription Unpack(BinaryReader reader)
        {
            TextureMergeTerrainDescription tmtd = new TextureMergeTerrainDescription();

            tmtd.TerrainType = (Enum.TerrainType)reader.ReadUInt32();
            tmtd.TerrainTextures.Add(TerrainTexture.Unpack(reader));

            return tmtd;
        }
    }
}

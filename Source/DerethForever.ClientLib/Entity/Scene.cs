/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// Scene in the client
    /// </summary>
    public class Scene : DatabaseObject
    {
        /// <summary>
        /// 0x038 version : Uint4B
        /// </summary>
        public uint Version;

        /// <summary>
        /// 0x03c num_objects : Uint4B
        /// </summary>
        public uint NumObjects => (uint)Objects.Count;

        /// <summary>
        /// 0x040 objects : Ptr32 ObjectDesc
        /// </summary>
        public List<ObjectDescription> Objects = new List<ObjectDescription>();

        /// <summary>
        /// //----- (005A6300) --------------------------------------------------------
        /// int __thiscall Scene::UnPack(Scene *this, void **addr, unsigned int size)
        /// acclient.c 463120
        /// </summary>
        public static Scene Unpack(BinaryReader reader)
        {
            Scene result = new Scene();
            result.Id = reader.ReadUInt32();

            uint numObjects = reader.ReadUInt32();

            for (int i = 0; i < numObjects; i++)
                result.Objects.Add(ObjectDescription.Unpack(reader));

            return result;
        }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BuildInfo from the client
    /// </summary>
    public class BuildingInfo
    {
        /// <summary>
        /// +0x000 building_id      : IDClass&lt;_tagDataID,32,0&gt;
        /// </summary>
        public uint Id;

        /// <summary>
        /// +0x004 building_frame   : Frame
        /// </summary>
        public Frame Frame;

        /// <summary>
        /// +0x044 num_leaves       : Uint4B
        /// </summary>
        public uint NumLeaves;

        /// <summary>
        /// +0x048 num_portals      : Uint4B
        /// </summary>
        public uint NumPortals => (uint)Portals.Count;

        /// <summary>
        /// +0x04c portals          : Ptr32 Ptr32 CBldPortal
        /// </summary>
        public List<BuildingPortal> Portals = new List<BuildingPortal>();

        /// <summary>
        /// this method is not explicitly defined in the client.  it is inlined in 
        /// CLandblockInfo::Unpack from lines 351158 - 351211
        /// </summary>
        public static BuildingInfo Unpack(BinaryReader reader)
        {
            throw new NotSupportedException();
        }
    }
}

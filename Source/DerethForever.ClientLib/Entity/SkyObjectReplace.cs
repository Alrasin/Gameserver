/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SkyObjectReplace in the client
    /// </summary>
    public class SkyObjectReplace
    {
        /// <summary>
        /// 0x000 object_index : Uint4B
        /// </summary>
        public uint Index;

        /// <summary>
        /// 0x004 object : Ptr32 SkyObject
        /// </summary>
        public SkyObject SkyObject;

        /// <summary>
        /// 0x008 gfx_obj_id : IDClass
        /// </summary>
        public uint GraphicsObjectId;

        /// <summary>
        /// 0x00c rotate : Float
        /// </summary>
        public float Rotate;

        /// <summary>
        /// 0x010 transparent : Float
        /// </summary>
        public float Transparency;

        /// <summary>
        /// 0x014 luminosity : Float
        /// </summary>
        public float Luminosity;

        /// <summary>
        /// 0x018 max_bright: Float
        /// </summary>
        public float MaxBright;

        /// <summary>
        /// //----- (00501180) --------------------------------------------------------
        /// int __thiscall SkyObjectReplace::UnPack(SkyObjectReplace *this, void **addr, unsigned int *size)
        /// acclient.c 301923
        /// </summary>
        public static SkyObjectReplace Unpack(BinaryReader reader)
        {
            SkyObjectReplace s = new SkyObjectReplace();

            s.Index = reader.ReadUInt32();
            s.GraphicsObjectId = reader.ReadUInt32();
            s.Rotate = reader.ReadSingle();
            s.Transparency = reader.ReadSingle();
            s.Luminosity = reader.ReadSingle();
            s.MaxBright = reader.ReadSingle();

            return s;
        }
    }
}

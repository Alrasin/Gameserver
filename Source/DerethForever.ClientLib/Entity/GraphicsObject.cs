/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.DatUtil;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CGfxObj in the client
    /// </summary>
    public class GraphicsObject : DatabaseObject
    {
        /// <summary>
        /// 0x030 material : Ptr32 CMaterial
        /// </summary>
        public Material Material { get; set; }

        /// <summary>
        /// 0x034 num_surfaces : Uint4B
        /// </summary>
        public uint NumSurfaces => (uint)Surfaces.Count;

        /// <summary>
        /// 0x038 m_rgSurfaces : Ptr32 Ptr32 CSurface
        /// </summary>
        public List<Surface> Surfaces { get; set; } = new List<Surface>();

        /// <summary>
        /// 0x03c vertex_array : CVertexArray
        /// </summary>
        public VertexArray VertexArray { get; set; }

        /// <summary>
        /// 0x064 num_physics_polygons : Uint4B
        /// </summary>
        public uint NumPhysicsPolygons => (uint)PhysicsPolygons.Count;

        /// <summary>
        /// 0x068 physics_polygons : Ptr32 CPolygon
        /// </summary>
        public List<Polygon> PhysicsPolygons { get; set; } = new List<Polygon>();

        /// <summary>
        /// 0x06c constructed_mesh : Ptr32 MeshBuffer
        /// </summary>
        public MeshBuffer ConstructedMesh { get; set; } = new MeshBuffer();

        /// <summary>
        /// 0x070 use_build_mesh : Int4B
        /// </summary>
        public bool UseBuiltMesh { get; set; } = false;

        /// <summary>
        /// 0x074 physics_sphere : Ptr32 CSphere
        /// </summary>
        public Sphere? PhysicsSphere { get; set; }

        /// <summary>
        /// 0x078 physics_bsp : Ptr32 BSPTREE
        /// </summary>
        public BspTree PhysicsBsp { get; set; }

        /// <summary>
        /// 0x07c sort_center : AC1Legacy::Vector3
        /// </summary>
        public Vector3 SortCenter { get; set; }

        /// <summary>
        /// 0x088 num_polygons : Uint4B
        /// </summary>
        public uint NumPolygons => (uint)Polygons.Count;

        /// <summary>
        /// 0x08c polygons : Ptr32 CPolygon
        /// </summary>
        public List<Polygon> Polygons { get; set; } = new List<Polygon>();

        /// <summary>
        /// 0x090 drawing_sphere : Ptr32 CSphere
        /// </summary>
        public Sphere? DrawingSphere { get; set; }

        /// <summary>
        /// 0x094 drawing_bsp : Ptr32 BSPTREE
        /// </summary>
        public BspTree DrawingBsp { get; set; }

        /// <summary>
        /// 0x098 gfx_bound_box : BBox
        /// </summary>
        public BoundingBox GraphicsBoundingBox { get; set; }

        /// <summary>
        /// 0x0b0 m_didDegrade
        /// </summary>
        public uint DegradeDid { get; set; }

        /// <summary>
        /// only valid source of these things is Unpack, as below
        /// </summary>
        private GraphicsObject()
        {
        }

        /// <summary>
        /// so, CGfxObj::Unpack doesn't exist in the client.  nor does any reasonably
        /// similar method.  instead, the best candidate found was CGfxObj::Serialize
        /// which does all of this stuff in reverse.  the logic there was reversed and
        /// relabeled as "Unpack" for consistency of use.
        /// 
        /// //----- (00534970) --------------------------------------------------------
        /// void __thiscall CGfxObj::Serialize(CGfxObj *this, Archive *io_archive)
        /// acclient.c 356683
        /// </summary>
        public static GraphicsObject Unpack(BinaryReader reader)
        {
            GraphicsObject go = new GraphicsObject();

            go.Id = reader.ReadUInt32();

            GraphicsObjectFlags flags = (GraphicsObjectFlags)reader.ReadUInt32();

            // serialize calls Archive::CheckAlignment(gfxObj, 4u) (line 70281) here.  
            // CheckAlignment does a lot of weird stuff i don't fully grok.  it didn't
            // seem necessary to do anything in testing, though.  the best guess is that
            // this is the pre-tod check.
            
            uint surfaceBytes;
            uint physicsBytes;
            uint drawingBytes;

            var surfaceCount = reader.ReadPackedUint32(out surfaceBytes);

            for (int i = 0; i < surfaceCount; i++)
                go.Surfaces.Add(PortalDatReader.Current.Unpack<Surface>(reader.ReadUInt32()));

            go.VertexArray = VertexArray.Unpack(reader);

            if (flags.HasFlag(GraphicsObjectFlags.PhysicsBsp))
            {
                var numPhysicsPolygons = reader.ReadPackedUint32(out physicsBytes);

                for (int i = 0; i < numPhysicsPolygons; i++)
                    go.PhysicsPolygons.Add(Polygon.Unpack(reader));

                go.PhysicsBsp = new BspTree();
                go.PhysicsBsp.Unpack(reader, BspTreeType.Physics, go.PhysicsPolygons);

                go.PhysicsSphere = go.PhysicsBsp.RootNode.Sphere;
            }

            go.SortCenter = reader.ReadVector();

            if (flags.HasFlag(GraphicsObjectFlags.DrawingBsp))
            {
                var numDrawingPolygons = reader.ReadPackedUint32(out drawingBytes);

                for (int i = 0; i < numDrawingPolygons; i++)
                    go.Polygons.Add(Polygon.Unpack(reader));

                go.DrawingBsp = new BspTree();
                go.DrawingBsp.Unpack(reader, BspTreeType.Drawing, go.Polygons);

                go.DrawingSphere = go.DrawingBsp.RootNode.Sphere;
            }

            return go;
        }
    }
}

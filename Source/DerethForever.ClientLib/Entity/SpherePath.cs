/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Numerics;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SPHEREPATH in the client
    /// </summary>
    public class SpherePath
    {
        /// <summary>
        /// +0x000 num_sphere       : Uint4B
        /// </summary>
        public uint NumberSpheres { get; set; }

        /// <summary>
        /// +0x004 local_sphere     : Ptr32 CSphere
        /// </summary>
        public List<Sphere> LocalSpheres { get; set; }

        /// <summary>
        /// +0x008 local_low_point  : AC1Legacy::Vector3
        /// </summary>
        public Vector3 LocalLowPoint;

        /// <summary>
        /// +0x014 global_sphere    : Ptr32 CSphere
        /// </summary>
        public List<Sphere> GlobalSpheres;

        /// <summary>
        /// +0x018 global_low_point : AC1Legacy::Vector3
        /// </summary>
        public Vector3 GlobalLowPoint;

        /// <summary>
        /// +0x024 localspace_sphere : Ptr32 CSphere
        /// </summary>
        public List<Sphere> LocalSpaceSpheres { get; set; }

        /// <summary>
        /// +0x028 localspace_low_point : AC1Legacy::Vector3
        /// </summary>
        public Vector3 LocalSpaceLowPoint;

        /// <summary>
        /// +0x034 localspace_curr_center : Ptr32 AC1Legacy::Vector3
        /// </summary>
        public List<Vector3> LocalSpaceCurrentCenter;

        /// <summary>
        /// +0x038 global_curr_center : Ptr32 AC1Legacy::Vector3
        /// </summary>
        public List<Vector3> GlobalCurrentCenter;

        /// <summary>
        /// +0x03c localspace_pos   : Position
        /// </summary>
        public Position LocalSpacePosition { get; set; }

        /// <summary>
        /// +0x084 localspace_z     : AC1Legacy::Vector3
        /// </summary>
        public Vector3 LocalSpaceZ;

        /// <summary>
        /// +0x090 begin_cell       : Ptr32 CObjCell
        /// </summary>
        public ObjectCell BeginCell { get; set; }

        /// <summary>
        /// +0x094 begin_pos        : Ptr32 Position
        /// </summary>
        public Position BeginPosition { get; set; }

        /// <summary>
        /// +0x098 end_pos          : Ptr32 Position
        /// </summary>
        public Position EndPosition { get; set; }

        /// <summary>
        /// +0x09c curr_cell        : Ptr32 CObjCell
        /// </summary>
        public ObjectCell CurrentCell { get; set; }

        /// <summary>
        /// +0x0a0 curr_pos         : Position
        /// </summary>
        public Position CurrentPosition { get; set; }

        /// <summary>
        /// +0x0e8 global_offset    : AC1Legacy::Vector3
        /// </summary>
        public Vector3 GlobalOffset;

        /// <summary>
        /// +0x0f4 step_up          : Int4B
        /// </summary>
        public int StepUp { get; set; } // possible boolean

        /// <summary>
        /// +0x0f8 step_up_normal   : AC1Legacy::Vector3
        /// </summary>
        public Vector3 StepUpNormal;

        /// <summary>
        /// 0x104 collide          : Int4B
        /// </summary>
        public int Collide { get; set; } // possible boolean

        /// <summary>
        ///  +0x108 check_cell       : Ptr32 CObjCell
        /// </summary>
        public ObjectCell CheckCell { get; set; }

        /// <summary>
        /// +0x10c check_pos        : Position
        /// </summary>
        public Position CheckPosition { get; set; }

        /// <summary>
        /// +0x154 insert_type      : SPHEREPATH::InsertType
        /// </summary>
        public SpherePathInsertType InsertType { get; set; }

        /// <summary>
        /// +0x158 step_down        : Int4B
        /// </summary>
        public int StepDown { get; set; } // possible boolean

        /// <summary>
        /// +0x15c backup           : SPHEREPATH::InsertType
        /// </summary>
        public SpherePathInsertType Backup { get; set; }

        /// <summary>
        /// +0x160 backup_cell      : Ptr32 CObjCell
        /// </summary>
        public ObjectCell BackupCell { get; set; }

        /// <summary>
        /// +0x164 backup_check_pos : Position
        /// </summary>
        public Position BackupCellPosition { get; set; }

        /// <summary>
        /// +0x1ac obstruction_ethereal : Int4B
        /// </summary>
        public int ObstructionEthereal { get; set; } // likely boolean

        /// <summary>
        /// +0x1b0 hits_interior_cell : Int4B
        /// </summary>
        public bool HitsInteriorCell { get; set; } // int in client

        /// <summary>
        /// +0x1b4 bldg_check       : Int4B
        /// </summary>
        public bool BuildingCheck { get; set; } // int in client

        /// <summary>
        /// +0x1b8 walkable_allowance : Float
        /// /// </summary>
        public float WalkableAllowance { get; set; }

        /// <summary>
        /// +0x1bc walk_interp      : Float
        /// </summary>
        public float WalkInterp { get; set; }

        /// <summary>
        /// +0x1c0 step_down_amt    : Float
        /// </summary>
        public float StepDownAmount { get; set; }

        /// <summary>
        /// +0x1c4 walkable_check_pos : CSphere
        /// </summary>
        public Sphere WalkableCheckPosition { get; set; }

        /// <summary>
        /// +0x1d4 walkable         : Ptr32 CPolygon
        /// </summary>
        public Polygon Walkable { get; set; }

        /// <summary>
        /// +0x1d8 check_walkable   : Int4B
        /// </summary>
        public int CheckWalkable { get; set; } // possible boolean

        /// <summary>
        /// +0x1dc walkable_up      : AC1Legacy::Vector3
        /// </summary>
        public Vector3 WalkableUp;

        /// <summary>
        /// +0x1e8 walkable_pos     : Position
        /// </summary>
        public Position WalkablePosition { get; set; }

        /// <summary>
        /// +0x230 walkable_scale   : Float
        /// </summary>
        public float WalkableScale { get; set; }

        /// <summary>
        /// +0x234 cell_array_valid : Int4B
        /// </summary>
        public bool CellArrayValid { get; set; }

        /// <summary>
        /// +0x238 neg_step_up      : Int4B
        /// </summary>
        public int NegStepUp { get; set; }

        /// <summary>
        /// +0x23c neg_collision_normal : AC1Legacy::Vector3
        /// </summary>
        public Vector3 NegCollisionNormal;

        /// <summary>
        /// +0x248 neg_poly_hit     : Int4B
        /// </summary>
        public int NegPolyHit { get; set; }

        /// <summary>
        ///  +0x24c placement_allows_sliding : Int4B
        /// </summary>
        public bool PlacementAllowsSliding { get; set; } // int in client

        /// <summary>
        /// //----- (0050C670) --------------------------------------------------------
        /// void __thiscall SPHEREPATH::init_sphere(SPHEREPATH *this, const unsigned int _num_sphere, CSphere *_sphere, const float _scale)
        /// acclient.c 313647
        /// </summary>
        public void InitializeSpheres(uint numSphere, List<Sphere> spheres, float scale)
        {
            if (numSphere <= 2)
                NumberSpheres = numSphere;
            else
                NumberSpheres = 2;

            for (int i = 0; i < NumberSpheres; i++)
                LocalSpheres.Add(new Sphere(spheres[i].Center * scale, spheres[i].Radius * scale));

            LocalLowPoint = LocalSpheres[0].Center;
            LocalLowPoint.Z -= LocalSpheres[0].Radius;
        }

        /// <summary>
        /// //----- (0050CE20) --------------------------------------------------------
        /// void __thiscall SPHEREPATH::init_path(SPHEREPATH*this, CObjCell* _begin_cell, Position* _begin_pos, Position* _end_pos)
        /// acclient.c 314043
        /// </summary>
        public void InitializePath(ObjectCell beginCell, Position beginPosition, Position endPosition)
        {
            BeginCell = beginCell;
            BeginPosition = beginPosition;
            EndPosition = endPosition;

            if (beginPosition != null)
            {
                CurrentPosition.CellId = beginPosition.CellId;
                CurrentPosition.Frame = new Frame(beginPosition.Frame);
                // client calls SPHEREPATH::cache_global_curr_center here
                InsertType = SpherePathInsertType.TransitionInsert;
            }
            else
            {
                CurrentPosition.CellId = endPosition.CellId;
                CurrentPosition.Frame = new Frame(endPosition.Frame);
                // client calls SPHEREPATH::cache_global_curr_center here
                InsertType = SpherePathInsertType.PlacementInsert;
            }
        }

        /// <summary>
        /// ----- (0050C3B0) --------------------------------------------------------
        /// signed int __thiscall SPHEREPATH::step_up_slide(SPHEREPATH*this, OBJECTINFO*object, COLLISIONINFO* collisions)
        /// acclient.c 313456
        /// </summary>
        public TransitionState StepUpSlide(ref ObjectInfo objectInfo, ref CollisionInfo collisionInfo)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ----- (00509D10) --------------------------------------------------------
        /// void __thiscall SPHEREPATH::add_offset_to_check_pos(SPHEREPATH*this, AC1Legacy::Vector3* offset)
        /// acclient.c 311557
        /// </summary>
        public void AddOffsetToCheckPosition(Vector3 offset)
        {
            CellArrayValid = false;
            CheckPosition.Frame.Origin += offset;
            CacheGlobalSphere(offset);
        }

        /// <summary>
        /// ----- (0050C810) --------------------------------------------------------
        /// void __thiscall SPHEREPATH::cache_global_sphere(SPHEREPATH*this, AC1Legacy::Vector3* offset)
        /// acclient.c 313748
        /// </summary>
        public void CacheGlobalSphere(Vector3 offset)
        {
            if (offset != Vector3.Zero)
            {
                List<Sphere> globalSpheres = new List<Sphere>();
                foreach (Sphere gs in GlobalSpheres)
                {
                    Sphere globalSphere = gs;
                    globalSphere.Center += offset;
                    globalSpheres.Add(globalSphere);
                }
                GlobalSpheres = globalSpheres;
                GlobalLowPoint += offset;
            }
            else
            {
                while (GlobalSpheres.Count < NumberSpheres)
                    GlobalSpheres.Add(new Sphere());

                List<Sphere> globalSpheres = new List<Sphere>();
                for (int i = 0; i < NumberSpheres; i++)
                {
                    Sphere globalSphere = GlobalSpheres[i];
                    globalSphere.Radius = LocalSpheres[i].Radius;
                    globalSphere.Center = CheckPosition.LocalToGlobal(LocalSpheres[i].Center);
                }
                GlobalSpheres = globalSpheres;
                GlobalLowPoint = CheckPosition.LocalToGlobal(LocalLowPoint);
            }
        }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// GameTime in client
    /// </summary>
    public class GameTime
    {
        /*
        0:000> dt acclient!GameTime
           +0x000 zero_time_of_year : Float
           +0x008 zero_year        : Int4B
           +0x00c day_length       : Float
           +0x010 days_per_year    : Int4B
           +0x014 times_of_day     : AC1Legacy::SmartArray<TimeOfDay *>
           +0x020 days_of_the_week : AC1Legacy::SmartArray<WeekDay *>
           +0x02c seasons          : AC1Legacy::SmartArray<Season *>
           +0x038 year_spec        : AC1Legacy::PStringBase<char>
           +0x040 year_length      : Float
           +0x048 present_time_of_day : Float
           +0x050 time_of_day_begin : Float
           +0x058 time_of_next_event : Float
           +0x060 present_time_in_day_unit : Float
           +0x064 current_year     : Int4B
           +0x068 current_day      : Int4B
           +0x06c current_season   : Int4B
           +0x070 current_week_day : Int4B
           +0x074 current_time_of_day : Int4B
           +0x078 clock_offset     : Float
           +0x080 time_zero_start_delta : Float
           =008ed9b8 current_game_time : (null) 
           =008ed9b0 global_next_event : Float           
        */

        public ulong ZeroTimeOfYear;
        public uint ZeroYear;
        public uint DayLength;
        public uint DaysPerYear;
        public List<TimeOfDay> TimesOfDay = new List<TimeOfDay>();
        public List<string> DaysOfTheWeek  = new List<string>(); // WeekDay is just a PString
        public List<Season> Seasons  = new List<Season>();
        public string YearSpec; // "P.Y."

        public static GameTime CurrentGameTime;

        /// <summary>
        /// //----- (005A6EC0) --------------------------------------------------------
        /// int __thiscall GameTime::UnPack(GameTime *this, void **addr, unsigned int *size)
        /// acclient.c 464014
        /// </summary>
        public static GameTime Unpack(BinaryReader reader)
        {
            GameTime gt = new GameTime();
            gt.ZeroTimeOfYear = reader.ReadUInt64();
            gt.ZeroYear = reader.ReadUInt32();
            gt.DayLength = reader.ReadUInt32();
            gt.DaysPerYear = reader.ReadUInt32();
            gt.YearSpec = reader.ReadPString();

            int numTimesOfDay = reader.ReadInt32();
            for (int i = 0; i < numTimesOfDay; i++)
                gt.TimesOfDay.Add(TimeOfDay.Unpack(reader));

            int numDaysOfWeek = reader.ReadInt32();
            for (int i = 0; i < numDaysOfWeek; i++)
                gt.DaysOfTheWeek.Add(reader.ReadPString());

            int numSeasons = reader.ReadInt32();
            for (int i = 0; i < numSeasons; i++)
                gt.Seasons.Add(Season.Unpack(reader));

            return gt;
        }
    }
}

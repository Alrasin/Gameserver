/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    public class NameFilterTable
    {
        public byte[] Unknown { get; set; }
        
        public List<NameTableFilterEntry> Table { get; set; } = new List<NameTableFilterEntry>();

        public static NameFilterTable Unpack(BinaryReader reader)
        {
            NameFilterTable nft = new NameFilterTable();

            // this seems totally arbitrary, and i'm not sure why this is an odd number
            // but there is a repeating structure of 5 bytes later on (the Entry), and
            // the DWORD starting at byte 23 is 0x00000003.
            nft.Unknown = reader.ReadBytes(23); 

            uint count = reader.ReadUInt32();

            for (int i = 0; i < count; i++)
                nft.Table.Add(NameTableFilterEntry.Unpack(reader));

            return nft;
        }
    }
}

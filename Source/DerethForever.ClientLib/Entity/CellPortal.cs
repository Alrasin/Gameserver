/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.IO;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CCellPortal from the client    
    /// </summary>
    public class CellPortal
    {
        public CellPortalState Flags { get; set; }

        /// <summary>
        /// CCellPortal->other_cell_id
        /// +0x000 other_cell_id    : Uint4B
        /// </summary>
        public uint OtherCellId { get; set; }

        /// <summary>
        /// +0x004 other_cell_ptr   : Ptr32 CEnvCell
        /// </summary>
        public EnvironmentCell OtherCell { get; set; }

        /// <summary>
        /// +0x008 portal           : Ptr32 CPolygon
        /// </summary>
        public Polygon Portal { get; set; }

        /// <summary>
        /// CCellPortal->portal_side
        /// +0x00c portal_side      : Int4B
        /// this->portal_side = ((unsigned int)(unsigned __int8)~(_BYTE)v5 >> 1) & 1;
        /// </summary>
        public bool PortalSide
        {
            get { return Flags.HasFlag(CellPortalState.PortalSide); }
        }

        /// <summary>
        /// CCellPortal->other_portal_id
        /// +0x010 other_portal_id  : Int4B
        /// </summary>
        public int OtherPortalId { get; set; }

        /// <summary>
        /// CCellPortal->exact_match
        /// +0x014 exact_match      : Int4B
        ///  this->exact_match = v5 & 1;
        /// </summary>
        public bool ExactMatch
        {
            get { return Flags.HasFlag(CellPortalState.ExactMatch); }
        }

        /// <summary>
        /// ----- (0053BAB0) --------------------------------------------------------
        /// int __thiscall CCellPortal::UnPack(CCellPortal*this, unsigned int block_mask, unsigned __int16 *poly_id, void** addr, unsigned int size)
        /// acclient.c 362379
        /// </summary>
        public static CellPortal Unpack(BinaryReader reader, uint blockMask)
        {
            CellPortal cp = new CellPortal();
            cp.Flags = (CellPortalState)reader.ReadUInt16();
            ushort envId = reader.ReadUInt16();
            cp.OtherCellId = reader.ReadUInt16();
            cp.OtherPortalId = reader.ReadUInt16();

            if ((envId & 4) > 0)
                cp.OtherCellId = uint.MaxValue; // client uses -1 here.  given 2s compliment, it's the same as MaxValue - thanks Mud!
            else
                cp.OtherCellId |= blockMask;

            return cp;
        }
    }
}

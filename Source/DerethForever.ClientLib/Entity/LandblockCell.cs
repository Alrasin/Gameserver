/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CLandCell in the client
    /// </summary>
    public class LandblockCell : SortCell
    {
        /// <summary>
        /// +0x100 polygons         : Ptr32 Ptr32 CPolygon
        /// </summary>
        public List<Polygon> Polygons { get; set; } = new List<Polygon>();

        /// <summary>
        /// +0x104 in_view          : BoundingType
        /// </summary>
        public BoundingType InView { get; set; }

        /// <summary>
        /// //----- (00532D60) --------------------------------------------------------
        /// signed int __thiscall CLandCell::find_collisions(CLandCell*this, CTransition* transition)
        /// acclient.c 354887
        /// </summary>
        public override TransitionState FindCollisions(Transition trans)
        {
            // TODO implement EnvironmentCell.FindCollisions
            return TransitionState.Ok;
        }

        /// <summary>
        /// //----- (00532F20) --------------------------------------------------------
        /// signed int __thiscall CLandCell::find_env_collisions(CLandCell *this, CTransition *transition)
        /// acclient.c 354992
        /// </summary>
        public override TransitionState FindEnvironmentCollisions(Transition trans)
        {
            // TODO implement EnvironmentCell.FindEnvironmentCollisions
            return TransitionState.Ok;
        }

        /// <summary>
        /// //----- (00532CF0) --------------------------------------------------------
        /// int __thiscall CLandCell::find_terrain_poly(CLandCell *this, AC1Legacy::Vector3 *origin, CPolygon **walkable)
        /// acclient.c 354859
        /// </summary>
        public bool FindTerrainPolygon(Vector3 origin, ref Polygon walkable)
        {
            if (Polygons?.Count < 1)
                return false;

            // client had a 2-iteration for loop.  unrolling it here.

            if (Polygons[0].PointInPoly2D(origin, Sidedness.Positive))
            {
                walkable = Polygons[0];
                return true;
            }

            if (Polygons[1].PointInPoly2D(origin, Sidedness.Positive))
            {
                walkable = Polygons[1];
                return true;
            }

            return false;
        }
    }
}

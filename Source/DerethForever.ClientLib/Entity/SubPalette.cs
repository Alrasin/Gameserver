/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// Subpalette in the client
    /// </summary>
    public class SubPalette
    {
        /// <summary>
        /// 0x004 subId : IDClass
        /// </summary>
        public uint Id;

        /// <summary>
        /// 0x008 Offset : Uint4B
        /// </summary>
        public uint Offset;

        /// <summary>
        /// 0x00c numcolors : Uint4B
        /// </summary>
        public uint NumColors;

        /// <summary>
        /// 0x010 prev : Ptr32 Subpalette
        /// </summary>
        public SubPalette Previous;

        /// <summary>
        /// 0x014 next : Ptr32 Subpalette
        /// </summary>
        public SubPalette Next;

        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private SubPalette()
        {
        }

        /// <summary>
        /// //----- (005ADAB0) --------------------------------------------------------
        /// int __thiscall Subpalette::UnPack(Subpalette *this, void **addr, unsigned int size)
        /// acclient.c 471744
        /// </summary>
        public static SubPalette Unpack(BinaryReader reader)
        {
            SubPalette sp = new SubPalette();

            // Unpack_AsDataIDOfKnownType in the client.  did not dig deep here.
            sp.Id = 0x04000000u | reader.ReadUInt16();

            // both offset and NumColors are bytes in the client, but uints in the object model

            sp.Offset = (uint)reader.ReadByte() * 8;
            sp.NumColors = reader.ReadByte();

            if (sp.NumColors == 0)
                sp.NumColors = 256;

            sp.NumColors *= 8;

            return sp;
        }
    }
}

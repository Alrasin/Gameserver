/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    public class TabooTable
    {
        private TabooTable()
        {   
        }

        public uint TableId { get; set; }

        public byte Unknown1 { get; set; }

        public List<TabooTableEntry> Tables { get; set; } = new List<TabooTableEntry>();

        /// <summary>
        /// no corresponding client function.  this one was just brute forced
        /// from data inspection
        /// </summary>
        public static TabooTable Unpack(BinaryReader reader)
        {
            TabooTable tt = new TabooTable();

            tt.TableId = reader.ReadUInt32();
            tt.Unknown1 = reader.ReadByte();

            ushort tableCount = reader.ReadByte();

            for (int i = 0; i < tableCount; i++)
                tt.Tables.Add(TabooTableEntry.Unpack(reader));
            
            return tt;
        }
    }
}

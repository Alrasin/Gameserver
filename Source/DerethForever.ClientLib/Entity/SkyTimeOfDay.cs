/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SkyTimeOfDay in the client
    /// </summary>
    public class SkyTimeOfDay
    {
        /// <summary>
        /// private because Unpack is the only valid source for this class
        /// </summary>
        private SkyTimeOfDay()
        {
        }

        /// <summary>
        /// +0x000 begin            : Float
        /// </summary>
        public float Begin { get; set; }

        /// <summary>
        /// +0x004 dir_bright       : Float
        /// </summary>
        public float DirBright { get; set; }

        /// <summary>
        /// +0x008 dir_heading      : Float
        /// </summary>
        public float DirHeading { get; set; }

        /// <summary>
        /// +0x00c dir_pitch        : Float
        /// </summary>
        public float DirPitch { get; set; }

        /// <summary>
        /// +0x010 dir_color        : RGBAUnion
        /// </summary>
        public uint DirColor { get; set; }

        /// <summary>
        /// +0x014 amb_bright       : Float
        /// </summary>
        public float AmbBright { get; set; }

        /// <summary>
        /// +0x018 amb_color        : RGBAUnion
        /// </summary>
        public uint AmbColor { get; set; }

        /// <summary>
        /// +0x01c world_fog        : Int4B
        /// </summary>
        public int WorldFog { get; set; }

        /// <summary>
        /// +0x024 max_world_fog    : Float
        /// </summary>
        public float MinWorldFog { get; set; }

        /// <summary>
        /// +0x024 max_world_fog    : Float
        /// </summary>
        public float MaxWorldFog { get; set; }

        /// <summary>
        /// +0x028 world_fog_color  : RGBAUnion
        /// </summary>
        public uint WorldFogColor { get; set; }

        /// <summary>
        /// +0x02c sky_obj_replace  : AC1Legacy::SmartArray<SkyObjectReplace *>
        /// </summary>
        public List<SkyObjectReplace> SkyObjReplace = new List<SkyObjectReplace>();

        /// <summary>
        /// //----- (005015B0) --------------------------------------------------------
        /// unsigned int __thiscall SkyTimeOfDay::UnPack(SkyTimeOfDay *this, void **addr, unsigned int *size)
        /// acclient.c 302257
        /// </summary>
        public static SkyTimeOfDay Unpack(BinaryReader reader)
        {
            SkyTimeOfDay s = new SkyTimeOfDay();

            s.Begin = reader.ReadSingle();
            s.DirBright = reader.ReadSingle();
            s.DirHeading = reader.ReadSingle();
            s.DirPitch = reader.ReadSingle();
            s.DirColor = reader.ReadUInt32();

            s.AmbBright = reader.ReadSingle();
            s.AmbColor = reader.ReadUInt32();

            s.MinWorldFog = reader.ReadSingle();
            s.MaxWorldFog = reader.ReadSingle();
            s.WorldFogColor = reader.ReadUInt32();
            s.WorldFog = reader.ReadInt32();

            uint numReplacements = reader.ReadUInt32();
            for (int i = 0; i < numReplacements; i++)
                s.SkyObjReplace.Add(SkyObjectReplace.Unpack(reader));

            return s;
        }
    }
}

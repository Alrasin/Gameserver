/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BSPNODE in the client
    /// </summary>
    public class BspNode
    {
        /// <summary>
        /// a field because of struct field vs property shenanigans
        /// 0x004 sphere : CSphere
        /// </summary>
        public Sphere Sphere;

        /// <summary>
        /// a field because of struct field vs property shenanigans
        /// 0x014 splitting_plane : Plane
        /// </summary>
        public Plane SplittingPlane;

        /// <summary>
        /// 0x024 type : Int4B
        /// </summary>
        public BspNodeType Type { get; set; }

        /// <summary>
        /// 0x028 num_polys : Uint4B
        /// </summary>
        public uint NumPolygons => (uint)InPolygons.Count;

        /// <summary>
        /// 0x02c in_polys : Ptr32 Ptr32 CPolygon
        /// </summary>
        public List<Polygon> InPolygons { get; set; } = new List<Polygon>();

        /// <summary>
        /// 0x030 pos_node : Ptr32 BSPNODE
        /// </summary>
        public BspNode PositiveNode { get; set; }

        /// <summary>
        /// 0x034 neg_node : Ptr32 BSPNODE
        /// </summary>
        public BspNode NegativeNode { get; set; }

        /// <summary>
        /// //----- (0053C770) --------------------------------------------------------
        /// int __cdecl BSPNODE::UnPackChild(BSPNODE **node, void **addr, unsigned int size)
        /// acclient.c 363344
        /// </summary>
        public static BspNode UnpackChild(BinaryReader reader, BspTreeType treeType, List<Polygon> polygons)
        {
            BspNode n;

            var nodeType = (BspNodeType)reader.ReadUInt32();

            if (nodeType == BspNodeType.Port)
            {
                n = new BspPortal();
                ((BspPortal)n).UnpackPort(reader, treeType, polygons);
            }
            else if (nodeType == BspNodeType.Leaf)
            {
                n = new BspLeaf();
                ((BspLeaf)n).UnpackLeaf(reader, treeType, polygons);
            }
            else
            {
                n = new BspNode();
                n.Type = nodeType;
                n.Unpack(reader, treeType, polygons);
            }

            return n;
        }

        /// <summary>
        /// //----- (0053C540) --------------------------------------------------------
        /// int __thiscall BSPNODE::UnPack(BSPNODE *this, void **addr, unsigned int size)
        /// acclient.c 363186
        /// </summary>
        public void Unpack(BinaryReader reader, BspTreeType treeType, List<Polygon> polygons)
        {
            SplittingPlane = reader.ReadPlane();

            // compiler optimizations were the devil here. just saying.

            switch (Type)
            {
                case BspNodeType.BpIN:
                case BspNodeType.BpnN:
                    NegativeNode = BspNode.UnpackChild(reader, treeType, polygons);
                    break;
                case BspNodeType.BPIn:
                case BspNodeType.BPnn:
                    PositiveNode = BspNode.UnpackChild(reader, treeType, polygons);
                    break;
                case BspNodeType.BPIN:
                case BspNodeType.BPnN:
                    PositiveNode = BspNode.UnpackChild(reader, treeType, polygons);
                    NegativeNode = BspNode.UnpackChild(reader, treeType, polygons);
                    break;
            }

            Sphere = reader.ReadSphere();

            if (treeType == BspTreeType.Physics)
                return;

            uint numPolys = reader.ReadUInt32();

            for (int i = 0; i < numPolys; i++)
            {
                ushort polyIndex = reader.ReadUInt16();
                InPolygons.Add(polygons[i]);
            }
        }

        /// <summary>
        /// ----- (0053C850) --------------------------------------------------------
        /// void __thiscall BSPNODE::LinkPortalNodeChain(BSPNODE*this, SmartArray<BSPNODE*,1>* _Portals)
        /// acclient.c 363409
        /// </summary>
        public void LinkPortalNodeChain(List<BspPortal> portals)
        {
            BspNode current = this;

            for (int i = portals.Count - 1; i >= 0; --i)
            {
                BspPortal nextPortal = portals[i];
                current.PositiveNode = nextPortal;
                if (i > 0)
                    nextPortal.PositiveNode = portals[i - 1];

                current = nextPortal;
            }
        }

        /// <summary>
        /// ----- (0053D340) --------------------------------------------------------
        /// void __thiscall BSPNODE::DetachPortalsAndPurgeNodes(BSPNODE*this, SmartArray<BSPNODE*,1>* io_PortalsToKeep)
        /// </summary>
        public List<BspPortal> PurgePortals()
        {
            var portals = new List<BspPortal>();

            if (PositiveNode != null)
            {
                portals.AddRange(PositiveNode.PurgePortals());
                PositiveNode = null;
            }

            if (NegativeNode != null)
            {
                portals.AddRange(NegativeNode.PurgePortals());
                NegativeNode = null;
            }
            return portals;
        }

        /// <summary>
        /// ----- (0053C310) --------------------------------------------------------
        /// int __thiscall BSPNODE::point_intersects_solid(BSPNODE*this, AC1Legacy::Vector3* point)
        /// acclient.c 363026
        /// </summary>
        public bool PointIntersectsSolid(Vector3 point)
        {
            if (Vector3.Dot(SplittingPlane.Normal, point) >= 0)
                return NegativeNode.PointIntersectsSolid(point);
            return PositiveNode.PointIntersectsSolid(point);
        }
    }
}

/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// ObjectDesc in the client
    /// </summary>
    public class ObjectDescription
    {
        /// <summary>
        /// +0x000 obj_id           : IDClass
        /// </summary>
        public uint ObjectId { get; set; }

        /// <summary>
        /// +0x004 base_loc         : Frame
        /// </summary>
        public Frame BaseLocation { get; set; }

        /// <summary>
        /// +0x044 freq             : Float
        /// </summary>
        public float Freq { get; set; }

        /// <summary>
        /// +0x048 displace_x       : Float
        /// </summary>
        public float DisplacementX { get; set; }

        /// <summary>
        /// +0x04c displace_y       : Float
        /// </summary>
        public float DisplacementY { get; set; }

        /// <summary>
        /// +0x050 min_scale        : Float
        /// </summary>
        public float MinScale { get; set; }

        /// <summary>
        /// +0x054 max_scale        : Float
        /// </summary>
        public float MaxScale { get; set; }

        /// <summary>
        /// +0x058 max_rot          : Float
        /// </summary>
        public float MaxRotation { get; set; }

        /// <summary>
        /// +0x05c min_slope        : Float
        /// </summary>
        public float MinSlope { get; set; }

        /// <summary>
        /// +0x060 max_slope        : Float
        /// </summary>
        public float MaxSlope { get; set; }

        /// <summary>
        /// +0x064 align            : Int4B
        /// </summary>
        public int Alignment { get; set; }

        /// <summary>
        /// +0x068 orient           : Int4B
        /// </summary>
        public int Orientation { get; set; }

        /// <summary>
        /// +0x06c weenie_obj       : Int4B
        /// </summary>
        public int WeenieObject { get; set; }

        /// <summary>
        /// //----- (005A6110) --------------------------------------------------------
        /// int __thiscall ObjectDesc::UnPack(ObjectDesc *this, void **addr, unsigned int size)
        /// acclient.c 462930
        /// </summary>
        public static ObjectDescription Unpack(BinaryReader reader)
        {
            ObjectDescription result = new ObjectDescription();

            result.ObjectId = reader.ReadUInt32();
            result.BaseLocation = Frame.Unpack(reader);
            result.Freq = reader.ReadSingle();
            result.DisplacementX = reader.ReadSingle();
            result.DisplacementY = reader.ReadSingle();
            result.MinScale = reader.ReadSingle();
            result.MaxScale = reader.ReadSingle();
            result.MaxRotation = reader.ReadSingle();
            result.MinSlope = reader.ReadSingle();
            result.MaxSlope = reader.ReadSingle();
            result.Alignment = reader.ReadInt32();
            result.Orientation = reader.ReadInt32();
            result.WeenieObject = reader.ReadInt32();
            return result;
        }

        /// <summary>
        /// //----- (005A5BB0) --------------------------------------------------------
        /// void __thiscall ObjectDesc::Place(ObjectDesc *this, unsigned int ix, unsigned int iy, unsigned int iq, AC1Legacy::Vector3 *obj_loc)
        /// acclient.c 462605
        /// </summary>
        public void Place(uint cellX, uint cellY, uint index, ref Vector3 location, double sceneRot)
        {
            location = BaseLocation.Origin;
            float newX = location.X;
            float newY = location.Y;

            if (DisplacementX >= 0)
                newX += ((float)Landblock.SceneryPsuedoRandom(cellX, cellY, Prng.Seeds.DisplacementX + index) * DisplacementX);

            if (DisplacementY >= 0)
                newY += ((float)Landblock.SceneryPsuedoRandom(cellX, cellY, Prng.Seeds.DisplacementY + index) * DisplacementY);
            
            if (sceneRot >= 0.75)
            {
                location.X = newY;
                location.Y = -newX;
            }
            else if (sceneRot >= 0.5)
            {
                location.X = -newX;
                location.Y = -newY;
            }
            else if (sceneRot >= 0.25)
            {
                location.X = -newY;
                location.Y = newX;
            }
            else
            {
                location.X = newX;
                location.Y = newY;
            }
        }

        /// <summary>
        /// //----- (0052F090) --------------------------------------------------------
        /// BOOL __thiscall ObjectDesc::CheckSlope(ObjectDesc *this, float z_val)
        /// acclient.c 351355
        /// </summary>
        public bool CheckSlope(float z)
        {
            return z <= MaxSlope && z >= MinSlope;
        }

        /// <summary>
        /// //----- (005A5E50) --------------------------------------------------------
        /// void __thiscall ObjectDesc::ObjAlign(ObjectDesc *this, Plane *plane, AC1Legacy::Vector3 *loc, Frame *obj_frame)
        /// acclient.c 462708
        /// </summary>
        public Frame Align(Plane plane, Vector3 loc)
        {
            var objectFrame = new Frame(BaseLocation); // copy
            objectFrame.Origin = loc;
            var normal = -1 * plane.Normal;
            var degrees = normal.GetHeading();
            objectFrame.SetHeading(degrees);
            return objectFrame;
        }


        /// <summary>
        /// //----- (005A5D50) --------------------------------------------------------
        /// void __thiscall ObjectDesc::GetObjFrame(ObjectDesc *this, unsigned int x, unsigned int y, unsigned int k, AC1Legacy::Vector3 *loc, Frame *obj_frame)
        /// acclient.c 462660
        /// </summary>
        public Frame GetRandomPlacement(uint x, uint y, uint sceneryIndex, ref Vector3 location)
        {
            Frame frame = new Frame(BaseLocation);
            frame.Origin = location;

            if (MaxRotation > 0.0f)
            {
                var factor = (float)Landblock.SceneryPsuedoRandom(x, y, Prng.Seeds.ObjectRotation + sceneryIndex);
                float rotation = MaxRotation * factor; // max rotation is in degrees
                frame.SetHeading(rotation);
            }

            return frame;
        }

        /// <summary>
        /// //----- (0052F0C0) --------------------------------------------------------
        /// double __thiscall ObjectDesc::ScaleObj(ObjectDesc *this, unsigned int x, unsigned int y, unsigned int k)
        /// acclient.c 351361
        /// </summary>
        public double RandomScale(uint x, uint y, uint seed)
        {
            double scale = this.MaxScale;

            if (MinScale != MaxScale)
            {
                var factor = MaxScale / MinScale;
                scale = Math.Pow(factor, Landblock.SceneryPsuedoRandom(x, y, Prng.Seeds.ObjectScale + seed));
            }

            return scale;
        }
    }
}

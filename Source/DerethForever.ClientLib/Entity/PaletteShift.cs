/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// PalShift in the client
    /// </summary>
    public class PaletteShift
    {
        /// <summary>
        /// 0x000 cur_tex : Uint4B
        /// </summary>
        public uint CurrentTexture;

        /// <summary>
        /// 0x004 land_tex : AC1Legacy::SmartArray(PalShiftTex *)
        /// </summary>
        public List<PaletteShiftTexture> LandTextures = new List<PaletteShiftTexture>();

        /// <summary>
        /// 0x010 sub_pals : Ptr32 Subpalette
        /// </summary>
        public List<SubPalette> SubPallets = new List<SubPalette>();

        /// <summary>
        /// 0x014 maxsubs : Uint4B
        /// </summary>
        public uint MaxSubPallets;

        /// <summary>
        /// private because Unpack is the only valid source of instances
        /// </summary>
        private PaletteShift()
        {
        }

        /// <summary>
        /// //----- (005007A0) --------------------------------------------------------
        /// int __thiscall PalShift::UnPack(PalShift *this, void **addr, unsigned int *size)
        /// acclient.c 301290
        /// </summary>
        public static PaletteShift Unpack(BinaryReader reader)
        {
            throw new NotImplementedException();
        }
    }
}

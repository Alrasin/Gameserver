/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// TexMerge in the client
    /// </summary>
    public class TextureMerge
    {
        /// <summary>
        /// 0x000 base_tex_size : Uint4B
        /// </summary>
        public uint BaseTextureSize;

        /// <summary>
        /// 0x004 corner_terrain_maps : AC1Legacy::SmartArray(TerrainAlphaMap *)
        /// </summary>
        public List<TerrainAlphaMap> CornerTerrainMaps = new List<TerrainAlphaMap>();

        /// <summary>
        /// 0x010 side_terrain_maps : AC1Legacy::SmartArray(TerrainAlphaMap *)
        /// </summary>
        public List<TerrainAlphaMap> SideTerrainMaps = new List<TerrainAlphaMap>();

        /// <summary>
        /// 0x01c road_maps : AC1Legacy::SmartArray(RoadAlphaMap *)
        /// </summary>
        public List<RoadAlphaMap> RoadMaps = new List<RoadAlphaMap>();

        /// <summary>
        /// 0x028 terrain_desc : AC1Legacy::SmartArray(TMTerrainDesc *)
        /// </summary>
        public List<TextureMergeTerrainDescription> TerrainDescriptions = new List<TextureMergeTerrainDescription>();

        /// <summary>
        /// private because Unpack is the only valid source of instances
        /// </summary>
        private TextureMerge()
        {
        }

        /// <summary>
        /// //----- (005045B0) --------------------------------------------------------
        /// int __thiscall TexMerge::UnPack(TexMerge *this, void **addr, unsigned int *size)
        /// acclient.c 305986
        /// </summary>
        public static TextureMerge Unpack(BinaryReader reader)
        {
            TextureMerge tm = new TextureMerge();

            tm.BaseTextureSize = reader.ReadUInt32();

            uint num = reader.ReadUInt32();
            for (int i = 0; i < num; i++)
            {
                TerrainAlphaMap m = new TerrainAlphaMap();
                m.TerrainCode = reader.ReadUInt32();
                m.TextureId = reader.ReadUInt32();
                tm.CornerTerrainMaps.Add(m);
            }

            num = reader.ReadUInt32();
            for (int i = 0; i < num; i++)
            {
                TerrainAlphaMap m = new TerrainAlphaMap();
                m.TerrainCode = reader.ReadUInt32();
                m.TextureId = reader.ReadUInt32();
                tm.SideTerrainMaps.Add(m);
            }

            num = reader.ReadUInt32();
            for (int i = 0; i < num; i++)
            {
                RoadAlphaMap m = new RoadAlphaMap();
                m.RoadCode = reader.ReadUInt32();
                m.RoadTextureId = reader.ReadUInt32();
                tm.RoadMaps.Add(m);
            }

            num = reader.ReadUInt32();
            for (int i = 0; i < num; i++)
                tm.TerrainDescriptions.Add(TextureMergeTerrainDescription.Unpack(reader));

            return tm;
        }
    }
}

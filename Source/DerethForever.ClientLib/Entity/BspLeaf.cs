/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BSPLEAF in the client
    /// </summary>
    public class BspLeaf : BspNode
    {
        internal BspLeaf()
        {
            Type = BspNodeType.Leaf;
        }

        /// <summary>
        /// 0x038 leaf_index : Uint4B
        /// </summary>
        public uint LeafIndex { get; set; }

        /// <summary>
        /// 0x03c solid : Int4B
        /// </summary>
        public int Solid { get; set; }

        /// <summary>
        /// //----- (0053D4A0) --------------------------------------------------------
        /// int __thiscall BSPLEAF::UnPackLeaf(BSPLEAF *this, void **addr, unsigned int size)
        /// acclient.c 364133
        /// </summary>
        public void UnpackLeaf(BinaryReader reader, BspTreeType treeType, List<Polygon> polygons)
        {
            LeafIndex = reader.ReadUInt32();

            if (treeType == BspTreeType.Physics)
            {
                Solid = reader.ReadInt32();
                Sphere = Sphere.Unpack(reader);

                var num = reader.ReadUInt32();

                for (int i = 0; i < num; i++)
                {
                    ushort polyIndex = reader.ReadUInt16();
                    InPolygons.Add(polygons[polyIndex]);
                }
            }
        }
    }
}

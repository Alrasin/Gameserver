/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using DerethForever.ClientLib.Entity;

namespace DerethForever.ClientLib.Managers
{
    public class ScriptManager
    {
        /// <summary>
        /// +0x000 physobj          : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObject { get; set; }

        /// <summary>
        /// +0x004 curr_data        : Ptr32 ScriptData
        /// </summary>
        public ScriptData CurrData { get; set; }

        /// <summary>
        /// +0x008 last_data        : Ptr32 ScriptData
        /// </summary>
        public ScriptData LastData { get; set; }

        /// <summary>
        /// +0x00c hook_index       : Int4B
        /// </summary>
        public int HookIndex { get; set; }

        /// <summary>
        /// +0x00c hook_index       : Int4B
        /// </summary>
        public double NextHookTime { get; set; }

        public ScriptManager()
        {
            CurrData = null;
            LastData = null;
            HookIndex = -1;
            NextHookTime = 0;
        }

        public ScriptManager(PhysicsObject physicsObject) : this()
        {
            PhysicsObject = physicsObject;
        }
    }
}

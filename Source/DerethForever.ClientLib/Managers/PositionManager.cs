/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using DerethForever.ClientLib.Entity;

namespace DerethForever.ClientLib.Managers
{
    public class PositionManager
    {
        /// <summary>
        /// +0x000 interpolation_manager : Ptr32 InterpolationManager
        /// </summary>
        public InterpolationManager InterpolationManager { get; set; }

        /// <summary>
        /// +0x004 sticky_manager   : Ptr32 StickyManager
        /// </summary>
        public StickyManager StickyManager { get; set; }

        /// <summary>
        /// +0x008 constraint_manager : Ptr32 ConstraintManager
        /// </summary>
        public ConstraintManager ConstraintManager { get; set; }

        /// <summary>
        /// +0x00c physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj { get; set; }


        /// <summary>
        /// ----- (005552D0) --------------------------------------------------------
        /// PositionManager* __cdecl PositionManager::Create(CPhysicsObj* _physics_obj)
        /// 388405
        /// </summary>
        public PositionManager(PhysicsObject physicsObject)
        {
            PhysicsObj = physicsObject;
            InterpolationManager = new InterpolationManager(physicsObject);
            StickyManager = new StickyManager(physicsObject);
            ConstraintManager = new ConstraintManager(physicsObject);
        }

        /// <summary>
        /// ----- (00555160) --------------------------------------------------------
        /// void __thiscall PositionManager::UseTime(PositionManager*this)
        /// </summary>
        public void UseTime(PhysicsObject physicsObject)
        {
            InterpolationManager?.UseTime();
            StickyManager?.UseTime();
        }

        /// <summary>
        /// ----- (00555190) --------------------------------------------------------
        /// void __thiscall PositionManager::adjust_offset(PositionManager*this, Frame* offset, long double quantum)
        /// acclient.c 388287
        /// </summary>
        public void AdjustOffset(ref Frame frame, double quantum)
        {
            if (InterpolationManager != null)
                InterpolationManager.AdjustOffset(ref frame, quantum);
            if (StickyManager != null)
                StickyManager.AdjustOffset(ref frame, quantum);
            if (ConstraintManager != null)
                ConstraintManager.AdjustOffset(frame, quantum);
        }
    }
}

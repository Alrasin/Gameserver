/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using DerethForever.ClientLib.Entity;

namespace DerethForever.ClientLib.Managers
{
    public class TargetManager
    {
        /// <summary>
        /// +0x000 physobj          : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObject { get; set; }

        /// <summary>
        /// +0x004 target_info      : Ptr32 TargetInfo
        /// </summary>
        public TargetInfo TargetInfo { get; set; }

        /// <summary>
        /// +0x008 voyeur_table     : Ptr32 LongNIHash<TargettedVoyeurInfo>
        /// </summary>
        public Dictionary<uint, TargettedVoyeurInfo> VoyeurTable { get; set; }

        public void HandleTargeting(PhysicsObject physicsObject)
        {
        }

        /// <summary>
        /// ----- (0051A7E0) --------------------------------------------------------
        /// void __thiscall TargetManager::ClearTarget(TargetManager*this)
        /// acclient.c 328271
        /// </summary>
        public void ClearTarget()
        {
            if (TargetInfo != null)
            {
                PhysicsObject physicsObject = PhysicsObject.GetObjectA(TargetInfo.ObjectId);
                if (physicsObject != null)
                    physicsObject.RemoveVoyeur(physicsObject.Id);

                TargetInfo = null;
            }
        }

        /// <summary>
        /// ----- (0051AD90) --------------------------------------------------------
        /// int __thiscall TargetManager::RemoveVoyeur(TargetManager*this, unsigned int object_id)
        /// acclient.c 328644
        /// </summary>
        public bool RemoveVoyeur(uint objectId)
        {
            if (VoyeurTable != null)
            {
                VoyeurTable.TryGetValue(objectId, out TargettedVoyeurInfo info);

                if (info != null)
                    return true;
            }
            return false;
        }
    }
}

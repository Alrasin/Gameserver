/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Numerics;
using DerethForever.ClientLib.Entity;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Managers
{
    public class ConstraintManager
    {
        /// <summary>
        /// +0x000 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj { get; set; }

        /// <summary>
        /// +0x004 is_constrained   : Int4B
        /// </summary>
        public bool IsConstrained { get; set; }

        /// <summary>
        /// +0x008 constraint_pos_offset : Float
        /// </summary>
        public float ConstraintPosOffset { get; set; }

        /// <summary>
        /// +0x00c constraint_pos   : Position
        /// </summary>
        public Position ConstraintPos { get; set; }

        /// <summary>
        /// +0x054 constraint_distance_start : Float
        /// </summary>
        public float ConstraintDistanceStart { get; set; }

        /// <summary>
        /// +0x058 constraint_distance_max : Float
        /// </summary>
        public float ConstraintDistanceMax { get; set; }

        public ConstraintManager(PhysicsObject physicsObject)
        {
            PhysicsObj = physicsObject;
        }

        /// <summary>
        /// ----- (00556180) --------------------------------------------------------
        /// void __thiscall ConstraintManager::adjust_offset(ConstraintManager*this, Frame* offset, long double quantum)
        /// acclient.c 389478
        /// </summary>
        public void AdjustOffset(Frame offset, double quantum)
        {
            if (PhysicsObj == null || !IsConstrained)
                return;
            if (PhysicsObj.TransientState.HasFlag(TransientState.Contact))
            {
                if (ConstraintPosOffset < ConstraintDistanceMax)
                {
                    if (ConstraintPosOffset > ConstraintDistanceStart)
                    {
                        float rhs = (ConstraintDistanceMax - ConstraintPosOffset) /
                                    (ConstraintDistanceMax - ConstraintDistanceStart);
                        offset.Origin *= rhs;
                    }
                }
                else
                    offset.Origin = Vector3.Zero;
            }
            ConstraintPosOffset = offset.Origin.Length();
        }
    }
}

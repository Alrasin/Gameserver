/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using DerethForever.ClientLib.Entity;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Managers
{
    public class InterpolationManager
    {
        /// <summary>
        /// +0x000 position_queue   : LList<InterpolationNode>
        /// </summary>
        public List<InterpolationNode> PositionQueue { get; set; }

        /// <summary>
        /// +0x008 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj { get; set; }

        /// <summary>
        /// +0x00c keep_heading     : Int4B
        /// </summary>
        public bool KeepHeading { get; set; }

        /// <summary>
        /// +0x010 frame_counter    : Uint4B
        /// </summary>
        public uint FrameCounter { get; set; }

        /// <summary>
        /// +0x014 original_distance : Float
        /// </summary>
        public float OriginalDistance { get; set; }

        /// <summary>
        /// +0x018 progress_quantum : Float
        /// </summary>
        public float ProgressQuantum { get; set; }

        /// <summary>
        /// +0x01c node_fail_counter : Int4B
        /// </summary>
        public int NodeFailCounter { get; set; }

        /// <summary>
        /// +0x020 blipto_position  : Position
        /// </summary>
        public Position BlipToPosition { get; set; }

        /// <summary>
        /// =0081f418 fUseAdjustedSpeed_ : Int4B
        /// </summary>
        public static bool UseAdjustedSpeed { get; set; }

        /// <summary>
        /// ----- (005558D0) --------------------------------------------------------
        /// void __thiscall InterpolationManager::InterpolationManager(InterpolationManager*this, CPhysicsObj* _physics_obj)
        /// acclient.c 388815
        /// </summary>
        public InterpolationManager(PhysicsObject physicsObject)
        {
            OriginalDistance = Constants.LargeDistance;
            PhysicsObj = physicsObject;
        }

        /// <summary>
        /// ----- (00555B20) --------------------------------------------------------
        /// void __thiscall InterpolationManager::InterpolateTo(InterpolationManager*this, Position* p, int _keep_heading)
        /// acclient.c 389017
        /// </summary>
        public void InterpolateTo(ref Position position, int keepHeading)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ----- (00555F20) --------------------------------------------------------
        /// void __thiscall InterpolationManager::UseTime(InterpolationManager*this)
        /// acclient.c 389278
        /// </summary>
        public void UseTime()
        {
            // TODO: Add me please
        }

        /// <summary>
        /// ----- (00555D30) --------------------------------------------------------
        /// void __thiscall InterpolationManager::adjust_offset(InterpolationManager*this, Frame* offset, long double quantum)
        /// acclient.c 389178
        /// </summary>
        public void AdjustOffset(ref Frame frame, double quantum)
        {
            // TODO: Add me please
        }
    }
}

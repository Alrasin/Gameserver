/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using DerethForever.ClientLib.Entity;

namespace DerethForever.ClientLib.Managers
{
    public class StickyManager
    {
        /// <summary>
        /// +0x000 target_id        : Uint4B
        /// </summary>
        public uint TargetId { get; set; }

        /// <summary>
        /// +0x004 target_radius    : Float
        /// </summary>
        public float TargetRadius { get; set; }

        /// <summary>
        /// +0x008 target_position  : Position
        /// </summary>
        public Position TargetPosition { get; set; }

        /// <summary>
        /// +0x050 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj { get; set; }

        /// <summary>
        /// +0x054 initialized      : Int4B
        /// </summary>
        public bool Initialized { get; set; }

        /// <summary>
        /// +0x058 sticky_timeout_time : Float
        /// </summary>
        public double StickyTimeoutTime { get; set; }

        public StickyManager(PhysicsObject physicsObject)
        {
            PhysicsObj = physicsObject;
        }

        /// <summary>
        /// ----- (00555610) --------------------------------------------------------
        /// void __thiscall StickyManager::UseTime(StickyManager*this)
        /// acclient.c 388605
        /// </summary>
        public void UseTime()
        {

        }

        /// <summary>
        /// ----- (00555430) --------------------------------------------------------
        /// void __thiscall StickyManager::adjust_offset(StickyManager*this, Frame* offset, long double quantum)
        /// acclient.c 388519
        /// </summary>
        public void AdjustOffset(ref Frame offset, double quantum)
        {
            // TODO: Add me please
        }
    }
}

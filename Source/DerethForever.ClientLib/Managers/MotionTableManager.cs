/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.Linq;
using DerethForever.ClientLib.Entity;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Managers
{
    public class MotionTableManager
    {
        /// <summary>
        /// +0x000 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj { get; set; }

        /// <summary>
        /// +0x004 table            : Ptr32 CMotionTable
        /// </summary>
        public MotionTable Table { get; set; }

        //+0x008 state            : MotionState
        public MotionState State { get; set; }

        /// <summary>
        /// +0x020 animation_counter : Int4B
        /// </summary>
        public uint AnimationCounter { get; set; }

        /// <summary>
        /// +0x024 pending_animations : DLList<MotionTableManager::AnimNode>
        /// </summary>
        public List<AnimationNode> PendingAnimations { get; set; }

        // TODO: WIP - need to finish implementing the methods.   Coral Golem
        public void UseTime()
        {

        }

        /// <summary>
        /// ----- (0051BDD0) --------------------------------------------------------
        /// void __thiscall MotionTableManager::HandleEnterWorld(MotionTableManager*this, CSequence* seq)
        /// acclient.c 329949
        /// </summary>
        public void HandleEnterWorld(Sequence sequence)
        {
            // TODO: Finish this - Coral Golem
        }

        /// <summary>
        /// ----- (0051C0B0) --------------------------------------------------------
        /// signed int __thiscall MotionTableManager::PerformMovement(MotionTableManager*this, MovementStruct* ms, CSequence* seq)
        /// acclient.c 330206
        /// </summary>
        public Sequence PerformMovement(MovementStructure movementStructure, Sequence seq)
        {
            // TODO: Finish this - Coral Golem
            if (Table == null)
                return new Sequence(0x7);

            switch (movementStructure.Type)
            {
                case MovementTypes.InterpretedCommand:
                    return null;
                case MovementTypes.StopInterpretedCommand:
                    return null;
                case MovementTypes.StopCompletely:
                    return null;
                default:
                    return seq;
            }
        }

        /// <summary>
        /// ----- (0051BE00) --------------------------------------------------------
        /// void __thiscall MotionTableManager::CheckForCompletedMotions(MotionTableManager*this)
        /// acclient.c 329961
        /// </summary>
        public void CheckForCompletedMotions()
        {
            // TODO: Need to implement this - Coral Golem
        }

        /// <summary>
        ///----- (0051C030) --------------------------------------------------------
        /// void __thiscall MotionTableManager::initialize_state(MotionTableManager*this, CSequence* seq)
        /// acclient.c 330172
        /// </summary>
        public void InitializeState(Sequence sequence)
        {
            uint numberOfAnimatons = 0;

            State = new MotionState();
            if (Table != null)
                Table.SetDefaultState(State, sequence, ref numberOfAnimatons);

            PendingAnimations.Add(new AnimationNode(MotionCommand.Ready, numberOfAnimatons));
            RemoveRedundantLinks(sequence);
        }

        /// <summary>
        /// ----- (0051BF20) --------------------------------------------------------
        /// void __thiscall MotionTableManager::remove_redundant_links(MotionTableManager*this, CSequence* seq)
        /// acclient.c 330079
        /// </summary>
        public void RemoveRedundantLinks(Sequence sequence)
        {
            for (int i = PendingAnimations.Count - 1; i >= 0; --i)
            {
                AnimationNode entry = PendingAnimations.ElementAt(i);
                if (entry == null)
                    break;

                if (entry.NumberOfAnimations == 0)
                    continue;

                if ((entry.Motion & MotionCommand.SubStateMask) != 0 || (entry.Motion & MotionCommand.ModifierMask) != 0)
                {
                    if ((entry.Motion & MotionCommand.StyleMask) != 0)
                        return;

                    for (int j = i - 1; j >= 0; --j)
                    {
                        AnimationNode prev = PendingAnimations.ElementAt(j);
                        if (prev == null)
                            break;

                        if (prev.Motion == entry.Motion)
                        {
                            TruncateAnimationList(prev, sequence);
                            return;
                        }
                        if (prev.NumberOfAnimations > 0 && (prev.Motion & (MotionCommand)0x70000000) != 0) // I might have these backwards
                            return;
                    }
                }
                else
                    for (int j = i - 1; j >= 0; --j)
                    {
                        AnimationNode prev = PendingAnimations.ElementAt(j);
                        if (prev == null)
                            break;

                        if (prev.Motion == entry.Motion)
                        {
                            TruncateAnimationList(prev, sequence);
                            return;
                        }
                        if (prev.NumberOfAnimations > 0 && (prev.Motion & (MotionCommand)0xB0000000) != 0) // I might have these backwards
                            return;
                    }
            }
        }

        /// <summary>
        /// ----- (0051BCA0) --------------------------------------------------------
        /// void __thiscall MotionTableManager::truncate_animation_list(MotionTableManager*this, MotionTableManager::AnimNode* node, CSequence* seq)
        /// acclient.c 329842
        /// </summary>
        public void TruncateAnimationList(AnimationNode node, Sequence sequence)
        {
            if (node == null) return;

            uint totalAnimations = 0;
            for (int i = PendingAnimations.Count - 1; i >= 0; --i)
            {
                AnimationNode entry = PendingAnimations.ElementAt(i);
                if (entry == node) break;

                totalAnimations += entry.NumberOfAnimations;
                entry.NumberOfAnimations = 0;
            }
            sequence.RemoveLinkAnimations(totalAnimations);
        }

    }
}

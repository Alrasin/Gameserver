/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib
{
    public static class Vector3Extentions
    {
        public static Vector3 Normalize(this Vector3 vector)
        {
            float mag = 1 / vector.Magnitude();
            vector.X *= mag;
            vector.Y *= mag;
            vector.Z *= mag;
            return vector;
        }

        public static bool IsInsignificant(this Vector3 normal)
        {
            float mag = normal.Magnitude();
            return (mag < Constants.TOLERANCE);
        }

        /// <summary>
        /// ----- (00452460) --------------------------------------------------------
        /// int __thiscall AC1Legacy::Vector3::normalize_check_small(AC1Legacy::Vector3*this)
        /// acclient.c 143622
        /// </summary>
        public static Vector3 NormalizeCheckSmall(this Vector3 normal)
        {
            if (!IsInsignificant(normal))
                return normal;
            return normal.Normalize();
        }

        public static float Magnitude(this Vector3 vector)
        {
            return (float)Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z);
        }

        /// <summary>
        /// ----- (004575D0) --------------------------------------------------------
        /// double __thiscall AC1Legacy::Vector3::get_heading(AC1Legacy::Vector3*this)
        /// </summary>
        /// <param name="vector">Input vector3</param>
        /// <param name="returnRadians">Do you want the result in radians (default) or in degrees</param>
        /// <returns>Heading in radians or degrees</returns>
        public static float GetHeading(this Vector3 vector, bool returnRadians = true)
        {
            Vector3 heading = new Vector3(vector.X, vector.Y, 0);

            if (heading.IsInsignificant())
                return 0f;

            heading.Normalize();

            double radians = ((450d - Math.Atan2(heading.X, heading.Y) * Constants.DEGREES_PER_RADIAN) % 360d) / Constants.DEGREES_PER_RADIAN;
            if (returnRadians)
                return (float)radians;

            return (float)RadiansToDegrees(radians);
        }

        private static double RadiansToDegrees(double angle)
        {
            return angle * Constants.DEGREES_PER_RADIAN;
        }

        public static Vector3 CreateVelocity(this Vector3 vector, float timeToUse)
        {
            if (timeToUse < Constants.TOLERANCE)
                throw new ArgumentException("can not scale a vector by 0.");

            Vector3 result;

            result.X = vector.X / timeToUse;
            result.Y = vector.Y / timeToUse;
            result.Z = vector.Z / timeToUse;

            return result;
        }
    }
}

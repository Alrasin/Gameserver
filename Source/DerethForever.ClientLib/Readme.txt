ClientLib class patterns
* dat classes (Entity/*) should have no public constructors
* unpack must be public static <type> Unpack(BinaryReader reader)
* if possible, reference the client Unpack method signature and line of code
* object name should follow C# standards, but reference the client object name in the xml summary
* other methods should reference the client method signature and line of code whenever possible

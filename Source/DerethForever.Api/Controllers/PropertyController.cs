﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DerethForever.Api.Common;
using DerethForever.Api.Models;
using DerethForever.Entity.Enum.Properties;

using Swashbuckle.Swagger.Annotations;

namespace DerethForever.Api.Controllers
{
    /// <summary>
    /// lookups for all the property values.  note, descriptions are subject to change as we prettify them.  the ID values
    /// and their logical means will not.
    /// </summary>
    public class PropertyController : BaseController
    {
        /// <summary>
        /// gets a list of all the available string properties
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<AceProperty>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Strings()
        {
            var t = Enum.GetValues(typeof(PropertyString)).Cast<PropertyString>().ToList();
            var result = t.Select(v => new AceProperty() { PropertyId = (uint)v, PropertyName = v.GetDescription() }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Gets a list of all the available double properties
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<AceProperty>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Doubles()
        {
            var t = Enum.GetValues(typeof(PropertyDouble)).Cast<PropertyDouble>().ToList();
            var result = t.Select(v => new AceProperty() { PropertyId = (uint)v, PropertyName = v.GetDescription() }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Gets a list of all the available data id properties
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<AceProperty>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage DataIds()
        {
            var t = Enum.GetValues(typeof(PropertyDataId)).Cast<PropertyDataId>().ToList();
            var result = t.Select(v => new AceProperty() { PropertyId = (uint)v, PropertyName = v.GetDescription() }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Gets a list of all the available isntance id properties
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<AceProperty>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage InstanceIds()
        {
            var t = Enum.GetValues(typeof(PropertyInstanceId)).Cast<PropertyInstanceId>().ToList();
            var result = t.Select(v => new AceProperty() { PropertyId = (uint)v, PropertyName = v.GetDescription() }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Gets a list of all the available attribute properties
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<AceProperty>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Attributes()
        {
            var t = Enum.GetValues(typeof(PropertyAttribute)).Cast<PropertyAttribute>().ToList();
            var result = t.Select(v => new AceProperty() { PropertyId = (uint)v, PropertyName = v.GetDescription() }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Gets a list of all the available boolean properties
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<AceProperty>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Bools()
        {
            var t = Enum.GetValues(typeof(PropertyBool)).Cast<PropertyBool>().ToList();
            var result = t.Select(v => new AceProperty() { PropertyId = (uint)v, PropertyName = v.GetDescription() }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Gets a list of all the available integer properties
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<AceProperty>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Ints()
        {
            var t = Enum.GetValues(typeof(PropertyInt)).Cast<PropertyInt>().ToList();
            var result = t.Select(v => new AceProperty() { PropertyId = (uint)v, PropertyName = v.GetDescription() }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Gets a list of all the available 64-bit integer properties
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<AceProperty>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Int64s()
        {
            var t = Enum.GetValues(typeof(PropertyInt64)).Cast<PropertyInt64>().ToList();
            var result = t.Select(v => new AceProperty() { PropertyId = (uint)v, PropertyName = v.GetDescription() }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}

/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Net.Http;

using Swashbuckle.Swagger.Annotations;
using System.Net;

using DerethForever.Api.Common;
using DerethForever.Api.Models;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Database;

namespace DerethForever.Api.Controllers
{
    /// <summary>
    ///
    /// </summary>
    public class ContentController : BaseController
    {
        /// <summary>
        ///
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<Content>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage GetAll()
        {
            return Request.CreateResponse(HttpStatusCode.OK, DatabaseManager.World.GetAllContent());
        }

        /// <summary>
        ///
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Request invalid.", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.OK, "Update successful", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "An error occurred.  Details will be in the message.", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Create([FromBody]Content content)
        {
            if (string.IsNullOrWhiteSpace(content?.ContentName))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "ContentName is required" });

            try
            {
                DatabaseManager.World.CreateContent(content);
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "creation successful." });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = ex.ToString() });
            }

        }

        /// <summary>
        ///
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Request invalid.", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.OK, "Update successful", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "An error occurred.  Details will be in the message.", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Update([FromBody]Content content)
        {
            if (content?.ContentGuid == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "cannot update null content or content with a null ContentGuid." });

            if (string.IsNullOrWhiteSpace(content?.ContentName))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "ContentName is required" });

            try
            {
                DatabaseManager.World.UpdateContent(content);
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "Update successful." });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = ex.ToString() });
            }

        }

        /// <summary>
        ///
        /// </summary>
        [HttpDelete]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Request invalid.", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.OK, "Delete successful", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "An error occurred.  Details will be in the message.", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Delete(Guid? contentGuid)
        {
            if (contentGuid == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "cannot delete with a null ContentGuid." });

            try
            {
                DatabaseManager.World.DeleteContent(contentGuid.Value);
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "Delete successful." });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = ex.ToString() });
            }

        }
    }
}

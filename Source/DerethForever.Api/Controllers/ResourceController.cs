/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DerethForever.Api.Common;
using DerethForever.Api.Models;
using DerethForever.Common;
using DerethForever.Database;
using DerethForever.DatLoader;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;

namespace DerethForever.Api.Controllers
{
    /// <summary>
    /// Methods for accessing game resources such as Icons, Models, etc.
    /// </summary>
    public class ResourceController : BaseController
    {
        /// <summary>
        /// Returns a byte array containing a Png image of the Primary Icon.
        /// </summary>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Byte[]", typeof(byte[]))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "unable to get icon.")]
        public HttpResponseMessage GetPrimaryPngIcon(uint iconId)
        {
            if (iconId > 0)
            {
                var image = ImageBuilder.GetPrimaryPngIcon(iconId);
                if (image != null)
                    return Request.CreateResponse(HttpStatusCode.OK, image.GetByteArray());
                else
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = "Icon not found in dat!" });
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = "Invalid iconId" });
        }

        /// <summary>
        /// Returns a byte array containing a Png image of a Background.
        /// </summary>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Byte[]", typeof(byte[]))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "unable to get icon.")]
        public HttpResponseMessage GetBackground(uint itemType)
        {
            if (itemType > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ImageBuilder.GetBackgroundPngIcon((ItemType)itemType).GetByteArray());
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = "Invalid ItemType" });
        }

        /// <summary>
        /// Returns a byte array containing a Png image of a fully layered Icon.
        /// </summary>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "Byte[]", typeof(byte[]))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "unable to get icon.")]
        public HttpResponseMessage GetFullyLayeredPngIcon(uint weenieClassId)
        {
            if (weenieClassId > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ImageBuilder.GetFullyLayeredPngIcon(weenieClassId).GetByteArray());
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = "Not Implemented" });
        }

        /// <summary>
        /// gets the 3d model
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AceModel))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Model(uint modelId)
        {
            return Request.CreateResponse(HttpStatusCode.NotImplemented);
        }

        /// <summary>
        /// Returns the current world release
        /// </summary>
        [HttpGet]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(WorldRelease))]
        [SwaggerResponse(HttpStatusCode.NoContent, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Troubles processing the server request.")]
        public HttpResponseMessage GetCurrentWorldReleaseInfo()
        {
            string currentRelease = Path.GetFullPath(Path.Combine(ConfigManager.Config.Database.World.StoredReleaseFolder, @"current_release.json"));

            if (!File.Exists(currentRelease))
                return Request.CreateResponse(HttpStatusCode.NoContent,
                    new SimpleMessage("There was trouble locating the current release information."));

            var releaseInfo = JsonConvert.DeserializeObject<WorldRelease>(File.ReadAllText(currentRelease));
            return Request.CreateResponse(HttpStatusCode.OK, releaseInfo);
        }

        /// <summary>
        /// Returns the specified world release information
        /// </summary>
        [HttpGet]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(WorldRelease))]
        [SwaggerResponse(HttpStatusCode.NoContent, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Troubles processing the server request.")]
        public HttpResponseMessage GetWorldReleaseInfo(string releaseName)
        {
            string currentRelease = Path.GetFullPath(Path.Combine(ConfigManager.Config.Database.World.StoredReleaseFolder, releaseName));

            if (!File.Exists(currentRelease))
                return Request.CreateResponse(HttpStatusCode.NoContent,
                    new SimpleMessage("There was trouble locating the requested release information."));

            var releaseInfo = JsonConvert.DeserializeObject<WorldRelease>(File.ReadAllText(currentRelease));
            return Request.CreateResponse(HttpStatusCode.OK, releaseInfo);
        }

        /// <summary>
        /// Returns the specified World Release, if present on the filesystem.
        /// </summary>
        [HttpGet]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, "Byte[]", typeof(byte[]))]
        [SwaggerResponse(HttpStatusCode.NoContent, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Troubles processing the server request.")]
        public HttpResponseMessage GetWorldRelease(string fileName)
        {
            string filePath = Path.GetFullPath(Path.Combine(ConfigManager.Config.Database.World.StoredReleaseFolder, fileName));

            if (!File.Exists(filePath))
                return Request.CreateResponse(HttpStatusCode.NoContent,
                    new SimpleMessage("There was trouble locating the requested release."));

            var release = DatabaseManager.World.GetWorldRelease(filePath);
            return Request.CreateResponse(HttpStatusCode.OK, release);
        }

        /// <summary>
        /// Returns the Current World Release, if present.
        /// </summary>
        [HttpGet]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, "Byte[]", typeof(byte[]))]
        [SwaggerResponse(HttpStatusCode.NoContent, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "Troubles processing the server request.")]
        public HttpResponseMessage GetCurrentWorldRelease()
        {
            string currentRelease = Path.GetFullPath(Path.Combine(ConfigManager.Config.Database.World.StoredReleaseFolder, @"current_release.json"));

            if (!File.Exists(currentRelease))
                return Request.CreateResponse(HttpStatusCode.NoContent,
                    new SimpleMessage("There was trouble locating the current release."));

            var releaseInfo = JsonConvert.DeserializeObject<WorldRelease>(File.ReadAllText(currentRelease));
            var release = DatabaseManager.World.GetWorldRelease(releaseInfo.FileName);
            return Request.CreateResponse(HttpStatusCode.OK, release);
        }
    }
}

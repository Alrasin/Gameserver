/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DerethForever.ClientLib.DatUtil;

namespace DerethForever.DatExplorer
{
    /// <summary>
    /// Windows UI Form to view dat contents.
    /// </summary>
    public partial class DatReaderForm : Form
    {
        /// <summary>
        /// The collection for the dats. Includes a DatReader object.
        /// </summary>
        public List<DatFile> Dats { get; private set; }

        /// <summary>
        /// Max records per Step.
        /// </summary>
        private int MaxDatFilesPerStep = 10000;

        /// <summary>
        /// Currently set File System Path, for the Dat Folder.
        /// </summary>
        private string datFolderPath = string.Empty;

        /// <summary>
        /// Last/Current read datFileId
        /// </summary>
        private uint currentFileId = 0;

        /// <summary>
        /// State Variable showing if the background process is Active.
        /// </summary>
        public bool ReadingDats { get; private set; } = false;

        /// <summary>
        /// Max Bytes per row.
        /// </summary>
        private int HexViewByteWidth = 16;

        /// <summary>
        /// Background process that loads the Dat files.
        /// </summary>
        private BackgroundWorker bwLoadData = new BackgroundWorker();

        /// <summary>
        /// Simple UI for reading Dat contents.
        /// </summary>
        public DatReaderForm()
        {
            bwLoadData.DoWork += new DoWorkEventHandler(BwLoadData_DoWork);
            bwLoadData.WorkerReportsProgress = true;
            bwLoadData.WorkerSupportsCancellation = true;
            bwLoadData.RunWorkerCompleted += BwLoadData_RunWorkerCompleted;
            bwLoadData.ProgressChanged += BwLoadData_ProgressChanged;
            LoadDatFolderFromAppConfig();
            InitializeComponent();

            for (int i = 1; i <= HexViewByteWidth; i++)
            {
                listFileView.Columns.Add($"{i}", " ", 20);
            }
        }

        /// <summary>
        /// Resets the TreeView
        /// </summary>
        /// <remarks>
        /// TODO:
        ///     1. Create Virtual methods and collections for the TreeViewItems
        ///     2. Create a dat mapper and wire in a map.
        ///     3. Create a dropdown for the dat mapper.
        /// </remarks>
        private void LoadDatTree()
        {
            // Slow and gross clear, replace with virtual methods:
            datTree.BeginUpdate();
            datTree.Nodes.Clear();

            int datNodeLevel = 0;
            int fileNodeLevel = 0;

            datTree.Nodes.Add("DatFiles: ");
            
            foreach (var Dat in Dats)
            {
                TreeNode datNode = new TreeNode
                {
                    Tag = new DatNodeTag(Dat.Id, Dat.FilePath, Dat.DatReader.TableOfContents.Count),
                    Text = $"Dat {Dat.Id}: {Path.GetFileName(Dat.FilePath)} : {Dat.DatReader.TableOfContents.Count.ToString()}"
                };
                datTree.Nodes[0].Nodes.Add(datNode);
                datTree.Nodes[0].Nodes[datNodeLevel].Nodes.Add($"Files:");
                int fileCount = 0;
                
                if (Dat.TreeViewStep > 0)
                {
                    TreeNode previousStepNode = new TreeNode
                    {
                        Tag = new DatFileTag(0),
                        Text = $"Load previous {MaxDatFilesPerStep.ToString("N0")} records."
                    };
                    datTree.Nodes[0].Nodes[datNodeLevel].Nodes[0].Nodes.Add(previousStepNode);
                }

                toolStripProgressBar1.PerformStep();

                foreach (var file in Dat.DatReader.TableOfContents)
                {
                    fileCount++;

                    if (fileCount < (Dat.TreeViewStep * MaxDatFilesPerStep))
                    {
                        continue;
                    }

                    TreeNode fileNode = new TreeNode
                    {
                        Tag = new DatFileTag(file.Key),
                        Text = "0x" + file.Key.ToString("X8")
                    };
                    datTree.Nodes[0].Nodes[datNodeLevel].Nodes[0].Nodes.Add(fileNode);
                    fileNode = null;
                    fileNodeLevel++;

                    if (fileNodeLevel == MaxDatFilesPerStep)
                    {
                        TreeNode listMoreNode = new TreeNode
                        {
                            Text = $"Load next {MaxDatFilesPerStep.ToString("N0")} records.",
                            Tag = new DatFileTag(file.Key)
                        };
                        datTree.Nodes[0].Nodes[datNodeLevel].Nodes[0].Nodes.Add(listMoreNode);
                        break;
                    }
                }

                datTree.Nodes[0].Expand();

                datNodeLevel++;
                fileNodeLevel = 0;
            }

            datTree.EndUpdate();
            toolStripProgressBar1.Value = 0;
        }

        /// <summary>
        /// Reads a Dat Folder and Loads the appropriate Dat files, if they exist.
        /// </summary>
        private void LoadDatDirectory()
        {
            using (var folderSelection = new FolderBrowserDialog())
            {
                if (datFolderPath?.Length > 0)
                    folderSelection.SelectedPath = datFolderPath;

                if (folderSelection.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(folderSelection.SelectedPath))
                {
                    List<DatFile> datFiles = new List<DatFile>();

                    foreach (var file in Directory.GetFiles(folderSelection.SelectedPath))
                    {
                        if (datFiles?.Count <= 3)
                        {
                            // get client_portal.dat
                            if (file.ToLower().Contains("client_portal.dat"))
                            {
                                datFiles.Add(new DatFile() { DatType = ClientLib.DatUtil.DatTypes.Portal, FilePath = file, Id = datFiles.Count });
                                continue;
                            }
                            // get client_cell_1.dat
                            if (file.ToLower().Contains("client_cell_1.dat"))
                            {
                                datFiles.Add(new DatFile() { DatType = ClientLib.DatUtil.DatTypes.Cell, FilePath = file, Id = datFiles.Count });
                                continue;
                            }
                            // get client_highres.dat
                            if (file.ToLower().Contains("client_highres.dat"))
                            {
                                datFiles.Add(new DatFile() { DatType = ClientLib.DatUtil.DatTypes.Highres, FilePath = file, Id = datFiles.Count });
                                continue;
                            }
                            // get client_local_English.dat
                            if (file.ToLower().Contains("client_local_english.dat"))
                            {
                                datFiles.Add(new DatFile() { DatType = ClientLib.DatUtil.DatTypes.local_Lang, FilePath = file, Id = datFiles.Count });
                            }
                        }
                        else
                            break;
                    }

                    if (CheckDat(datFiles))
                    {
                        UpdateStatusText("Processing...");
                        Dats = datFiles;
                        datFolderPath = folderSelection.SelectedPath;
                        SaveDatFolderToAppConfig();
                        ReadingDats = true;
                        if (bwLoadData.IsBusy != true)
                        {
                            // Start the asynchronous operation.
                            bwLoadData.RunWorkerAsync();
                        }
                    }
                    else
                        UpdateStatusText("Issue! Could not load any dat files.");
                    datFiles = null;
                }
            }
        }

        /// <summary>
        /// Read the saved Dat Path from the App Config
        /// </summary>
        private void LoadDatFolderFromAppConfig()
        {
            try
            {
                string key = "DatFolderPath";
                var appSettings = ConfigurationManager.AppSettings;
                datFolderPath = appSettings[key] ?? string.Empty;
            }
            catch (ConfigurationErrorsException)
            {
                UpdateStatusText("Error reading app settings");
            }
        }

        /// <summary>
        /// Save the Dat Path to the App Config
        /// </summary>
        private void SaveDatFolderToAppConfig()
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                string key = "DatFolderPath";
                if (settings[key] == null)
                    settings.Add(key, datFolderPath);
                else
                    settings[key].Value = datFolderPath;
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                UpdateStatusText("Error writing app settings");
            }
        }

        /// <summary>
        /// Attemps too update the satus text on the bottom of the app.
        /// </summary>
        /// <remarks>
        /// 1. Cannot get this to refresh. About to abort and use a different control.
        /// </remarks>
        /// <param name="statusMessage">Message to display on the status bar.</param>
        private void UpdateStatusText(string statusMessage)
        {
            statusStrip.Text = statusMessage;
            statusStrip.Refresh();
            statusStrip.Update();
            Refresh();
        }

        /// <summary>
        /// Verifies that the correct Dat's are present in a collection.
        /// </summary>
        private bool CheckDat(List<DatFile> datFiles)
        {
            // Default too invalid state.
            bool isValid = false;

            // Check file count
            if (datFiles?.Count == 4 || datFiles?.Count == 2)
            {
                var cell = datFiles.FirstOrDefault(s => s.DatType == DatTypes.Cell);
                var portal = datFiles.FirstOrDefault(s => s.DatType == DatTypes.Portal);

                // Verify that cell and portal are available:
                if ((cell != null && File.Exists(cell.FilePath))
                    &&
                    (portal != null && File.Exists(portal.FilePath)))
                {
                    isValid = true;
                }

                cell = null;
                portal = null;
            }
            return isValid;
        }

        /// <summary>
        /// Reads the content of an individual dat file and displays relevant information in the Main form.
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="datId"></param>
        private void LoadDatFileContent(uint fileId, int datId)
        {
            currentFileId = fileId;
            var fileToc = Dats[datId].DatReader.TableOfContents[fileId];
            var hexString = ByteArrayToHexString(Dats[datId].DatReader.GetRawFile(fileId));
            string[] hex = new string[HexViewByteWidth];
            int charIndex = 0;

            listFileView.Items.Clear();

            foreach (char c in hexString)
            {                
                if (charIndex == HexViewByteWidth)
                {
                    listFileView.Items.Add(new ListViewItem(hex));
                    hex = new string[HexViewByteWidth];
                    charIndex = 0;
                }
                else
                {
                    hex[charIndex] = c.ToString();
                    charIndex++;
                }
            }

            if (charIndex != 0)
                listFileView.Items.Add(new ListViewItem(hex));

            Refresh();
        }

        /// <summary>
        /// Launches background process to Load dat files.
        /// </summary>
        private void BwLoadData_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            if (!ReadingDats)
                worker.CancelAsync();

            if (worker.CancellationPending == true)
                e.Cancel = true;
            else
            {
                foreach (var dat in Dats)
                {
                    bwLoadData.ReportProgress(0);
                    dat.DatReader = new DatReader(dat.FilePath);
                }
            }
        }

        /// <summary>
        /// Event fired after Dats have been loaded.
        /// </summary>
        private void BwLoadData_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripProgressBar1.Value = 0;
            ReadingDats = false;
            LoadDatTree();
        }

        /// <summary>
        /// Event fired when indicating progress durring the dat reading process.
        /// </summary>
        private void BwLoadData_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            toolStripProgressBar1.PerformStep();
        }

        /// <summary>
        /// DatTree selection event
        /// </summary>
        private void DatTree_AfterSelect(System.Object sender, TreeViewEventArgs e)
        {
            if (e.Node.Parent != null && e.Node.Parent.Text == "Files:")
            {
                if (e.Node.Text.Contains("Load"))
                {
                    // reload dat at count?
                    if (e.Node.Tag != null && e.Node.Parent.Parent.Tag != null)
                    {
                        if (e.Node.Text.Contains("Load next"))
                        {
                            var fileInfo = (DatFileTag)e.Node.Tag;
                            var datInfo = (DatNodeTag)e.Node.Parent.Parent.Tag;
                            Dats[datInfo.DatId].TreeViewStep++;
                        }
                        else if (e.Node.Text.Contains("Load previous"))
                        {
                            var fileInfo = (DatFileTag)e.Node.Tag;
                            var datInfo = (DatNodeTag)e.Node.Parent.Parent.Tag;
                            Dats[datInfo.DatId].TreeViewStep--;
                        }
                        LoadDatTree();
                        return;
                    }
                }

                // Load the file in the tab and save currentId
                if (e.Node.Tag != null && e.Node.Parent.Parent.Tag != null)
                {
                    var fileInfo = (DatFileTag)e.Node.Tag;
                    var datInfo = (DatNodeTag)e.Node.Parent.Parent.Tag;
                    LoadDatFileContent(fileInfo.DatFileId, datInfo.DatId);
                }
            }
        }

        private void DirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadDatDirectory();
        }

        private void InitialiseDatTreeView()
        {

        }

        /// <summary>
        /// Converts a byteArray into a Hex string.
        /// </summary>
        /// <param name="byteArray"></param>
        /// <returns>string of hex</returns>
        public string ByteArrayToHexString(byte[] byteArray)
        {
            return BitConverter.ToString(
                byteArray, 0).Replace("-",  string.Empty);
        }
    }
}

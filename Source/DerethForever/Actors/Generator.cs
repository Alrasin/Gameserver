/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Actors
{
    public class Generator : WorldObject
    {
        public Generator(ObjectGuid guid, DataObject baseDataObject)
            : base(guid, baseDataObject)
        {
            DescriptionFlags = ObjectDescriptionFlag.None;
            Stuck = true; Attackable = true; HiddenAdmin = true;
            IgnoreCollision = true; Hidden = true; Ethereal = true;
            RadarBehavior = Entity.Enum.RadarBehavior.ShowNever;
            RadarColor = Entity.Enum.RadarColor.Admin;
            Usable = Entity.Enum.Usable.No;
        }

        public bool GeneratorStatus
        {
            get { return DataObject.GeneratorStatus ?? false; }
            set { DataObject.GeneratorStatus = value; }
        }

        public bool GeneratorEnteredWorld
        {
            get { return DataObject.GeneratorEnteredWorld ?? false; }
            set { DataObject.GeneratorEnteredWorld = value; }
        }

        public bool GeneratorDisabled
        {
            get { return DataObject.GeneratorDisabled ?? false; }
            set { DataObject.GeneratorDisabled = value; }
        }

        public bool GeneratedTreasureItem
        {
            get { return DataObject.GeneratedTreasureItem ?? false; }
            set { DataObject.GeneratedTreasureItem = value; }
        }

        public bool GeneratorAutomaticDestruction
        {
            get { return DataObject.GeneratorAutomaticDestruction ?? false; }
            set { DataObject.GeneratorAutomaticDestruction = value; }
        }

        public bool CanGenerateRare
        {
            get { return DataObject.CanGenerateRare ?? false; }
            set { DataObject.CanGenerateRare = value; }
        }

        public bool CorpseGeneratedRare
        {
            get { return DataObject.CorpseGeneratedRare ?? false; }
            set { DataObject.CorpseGeneratedRare = value; }
        }

        public bool SuppressGenerateEffect
        {
            get { return DataObject.SuppressGenerateEffect ?? false; }
            set { DataObject.SuppressGenerateEffect = value; }
        }

        public bool ChestRegenOnClose
        {
            get { return DataObject.ChestRegenOnClose ?? false; }
            set { DataObject.ChestRegenOnClose = value; }
        }

        public bool ChestClearedWhenClosed
        {
            get { return DataObject.ChestClearedWhenClosed ?? false; }
            set { DataObject.ChestClearedWhenClosed = value; }
        }

        public int GeneratorTimeType
        {
            get { return DataObject.GeneratorTimeType ?? 0; }
            set { DataObject.GeneratorTimeType = value; }
        }

        public int GeneratorProbability
        {
            get { return DataObject.GeneratorProbability ?? 0; }
            set { DataObject.GeneratorProbability = value; }
        }

        public int MaxGeneratedObjects
        {
            get { return DataObject.MaxGeneratedObjects ?? 0; }
            set { DataObject.MaxGeneratedObjects = value; }
        }

        public int GeneratorType
        {
            get { return DataObject.GeneratorType ?? 0; }
            set { DataObject.GeneratorType = value; }
        }

        public int ActivationCreateClass
        {
            get { return DataObject.ActivationCreateClass ?? 0; }
            set { DataObject.ActivationCreateClass = value; }
        }
    }
}

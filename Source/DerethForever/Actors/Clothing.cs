/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.DatLoader.FileTypes;
using DerethForever.DatLoader.Entity;

namespace DerethForever.Actors
{
    public class Clothing : WorldObject
    {
        public CoverageMask? VisualClothingPriority
        {
            get
            {
                return GetVisualClothingPriority();
            }
            set
            {
                visualClothingPriority = value;
            }
        }
        private CoverageMask? visualClothingPriority;

        public Clothing(DataObject dataO)
            : base(dataO)
        {
            GetVisualClothingPriority();
        }

        // This will help to properly display tailored (reduced) items and special items such as Over-Robes that look like they cover more areas than they do.
        private CoverageMask? GetVisualClothingPriority()
        {
            if (visualClothingPriority != null || ClothingBase == null) 
                return visualClothingPriority;

            // Under Clothes aren't an issue, so we'll ignore it
            if ((ValidLocations & EquipMask.ChestWear) == EquipMask.ChestWear ||
                 (ValidLocations & EquipMask.AbdomenWear) == EquipMask.AbdomenWear ||
                  (ValidLocations & EquipMask.UpperArmWear) == EquipMask.UpperArmWear ||
                   (ValidLocations & EquipMask.LowerArmWear) == EquipMask.LowerArmWear ||
                    (ValidLocations & EquipMask.UpperLegWear) == EquipMask.UpperLegWear ||
                     (ValidLocations & EquipMask.LowerLegWear) == EquipMask.LowerLegWear)
            {
                visualClothingPriority = Priority;
                return visualClothingPriority;
            }

            ClothingTable item = ClothingTable.ReadFromDat((uint)ClothingBase);

            const uint HUMAN_MALE_SETUP_DID = 0x02000001;

            // If there is no human male setup val in the ClothingBaseEffects, it's likely not clothing
            CoverageMask clothingTableDerivedPriority = CoverageMask.None;
            if (item.ClothingBaseEffects.ContainsKey(HUMAN_MALE_SETUP_DID))
            {
                ClothingBaseEffect cb = item.ClothingBaseEffects[HUMAN_MALE_SETUP_DID];

                for (var i = 0; i < cb.CloObjectEffects.Count; i++)
                {
                    HumanParts part = (HumanParts)cb.CloObjectEffects[i].Index;

                    switch (part)
                    {
                        case HumanParts.Abdomen: // This is the "girth" area
                            clothingTableDerivedPriority |= CoverageMask.OuterwearAbdomen;
                            break;
                        case HumanParts.LeftUpperLeg:
                        case HumanParts.RightUpperLeg:
                            clothingTableDerivedPriority |= CoverageMask.OuterwearUpperLegs;
                            break;
                        case HumanParts.LeftLowerLeg:
                        case HumanParts.RightLowerLeg:
                            clothingTableDerivedPriority |= CoverageMask.OuterwearLowerLegs;
                            break;
                        case HumanParts.LeftUpperArm:
                        case HumanParts.RightUpperArm:
                            clothingTableDerivedPriority |= CoverageMask.OuterwearUpperArms;
                            break;
                        case HumanParts.LeftLowerArm:
                        case HumanParts.RightLowerArm:
                            clothingTableDerivedPriority |= CoverageMask.OuterwearLowerArms;
                            break;
                        case HumanParts.LeftFoot:
                        case HumanParts.LeftToe:
                        case HumanParts.RightFoot:
                        case HumanParts.RightToe:
                            clothingTableDerivedPriority |= CoverageMask.Feet;
                            break;
                        case HumanParts.Head:
                        case HumanParts.HeadHair:
                        case HumanParts.HeadHelmet:
                            clothingTableDerivedPriority |= CoverageMask.Head;
                            break;
                        case HumanParts.Chest:
                            clothingTableDerivedPriority |= CoverageMask.OuterwearChest;
                            break;
                    }
                }
            }

            if (clothingTableDerivedPriority != CoverageMask.None)
                visualClothingPriority = clothingTableDerivedPriority;

            // If we still have nothing, set it to whatever the Priority is...
            if (visualClothingPriority == null)
                visualClothingPriority = Priority;

            return visualClothingPriority;
        }
    }
}

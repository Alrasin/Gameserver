/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Network.Enum;
using System;
using System.Security.Cryptography;
using System.Text;

namespace DerethForever.Actors
{
    /// <summary>
    /// Confirmations are used to verify the target player would like to participate in what they are being invited to
    /// Invite Types: found in DerethForever.Network.Enum.ConfirmationType
    /// </summary>
    public class Confirmation
    {
        /// <summary>
        /// Value sent in 0x0274 Character_ConfirmationRequest so the response can be tied back to correct confirmation
        /// </summary>
        public uint Context { get; set; }

        /// <summary>
        /// DerethForever.Network.Enum.ConfirmationType
        /// </summary>
        public ConfirmationType Type { get; set; }

        /// <summary>
        /// Message sent to recipient
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Used by Confirmation Manager to time out requests
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Guid of the player whose action is triggering a confirmation
        /// </summary>
        public uint Initiator { get; set; }

        /// <summary>
        /// Guid of the player being targeted for the action
        /// </summary>
        public uint Target { get; set; }

        /// <summary>
        /// Create a confirmation to be used in requests for actions like Fellowships, Allegiances, Crafting, and Augmenting
        /// Skill and attribute purchase are handled by client
        /// </summary>
        /// <param name="confirmationType">Confirmation Type found in DerethForever.Network.Enum.ConfirmationType</param>
        /// <param name="message">Message to be displayed in message box to recipient</param>
        /// <param name="initiatorPlayerGuid">Guid of player initiating the request being confirmed</param>
        /// <param name="targetPlayerGuid">Guid of the player confirmation is sent to</param>
        public Confirmation(ConfirmationType confirmationType, string message, uint initiatorPlayerGuid, uint targetPlayerGuid)
        {
            Context = GenerateContextId();
            Type = confirmationType;
            Message = message;
            Initiator = initiatorPlayerGuid;
            Target = targetPlayerGuid;
        }

        /// <summary>
        /// Generic constructor
        /// </summary>
        public Confirmation()
        {
            Context = GenerateContextId();
        }

        /// <summary>
        /// Taken from https://stackoverflow.com/questions/32932679/using-rngcryptoserviceprovider-to-generate-random-string
        /// Generates unique numbers without dups in 10k numbers
        /// </summary>
        /// <returns></returns>
        private uint GenerateContextId()
        {
            int maxLength = 9;
            const string valid = "1234567890";
            StringBuilder res = new StringBuilder();
            using (RNGCryptoServiceProvider rnd = new RNGCryptoServiceProvider())
            {
                while (maxLength-- > 0)
                {
                    res.Append(valid[GetInt(rnd, valid.Length)]);
                }
            }
            return Convert.ToUInt32(res.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rnd">Reference to crypto provider</param>
        /// <param name="max">Length of the string used for valid values in GenerateContextId</param>
        /// <returns></returns>
        private static int GetInt(RNGCryptoServiceProvider rnd, int max)
        {
            byte[] r = new byte[4];
            int value;
            do
            {
                rnd.GetBytes(r);
                value = BitConverter.ToInt32(r, 0) & Int32.MaxValue;
            } while (value >= max * (Int32.MaxValue / max));
            return value % max;
        }
    }
}

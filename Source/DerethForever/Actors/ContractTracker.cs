/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;
using DerethForever.DatLoader.FileTypes;

namespace DerethForever.Actors
{
    /// <summary>
    /// This class is the base of contracts for quest tracking.
    /// </summary>
    public class ContractTracker
    {
        protected DataContractTracker DataContractTracker { get; set; }

        public DatLoader.Entity.Contract ContractDetails { get; }

        public uint DataObjectId
        {
            get { return DataContractTracker.DataObjectId; }
            set { DataContractTracker.DataObjectId = value; }
        }
        /// <summary>
        /// Version of the contract.   So far I have only seen 0, but I have not done an exhaustive search.
        /// </summary>
        public uint Version
        {
            get { return DataContractTracker.Version; }
            set { DataContractTracker.Version = value; }
        }

        /// <summary>
        /// Id of the contract.   This is the index into the contract table in the portal.dat file
        /// </summary>
        public uint ContractId
        {
            get { return DataContractTracker.ContractId; }
            set { DataContractTracker.ContractId = value; }
        }

        /// <summary>
        /// here are we in the quest - progress.   Starts at 0 on initial add to quest tracker.   Examples kill task kill 10 this marks you place.
        /// </summary>
        public uint Stage
        {
            get { return DataContractTracker.Stage; }
            set { DataContractTracker.Stage = value; }
        }

        /// <summary>
        /// I believe this is used for timed quests - kill so many within an hour - have not found it populated in pcaps.
        /// This value is stored in seconds.   ie 1 hour = 3600 seconds.
        /// </summary>
        public ulong TimeWhenDone
        {
            get { return DataContractTracker.TimeWhenDone; }
            set { DataContractTracker.TimeWhenDone = value; }
        }

        /// <summary>
        /// When is my quest timer up so I can do the quest again.  This will be tracked in the greater quest system. Time is stored in seconds
        /// until I can rerun the quest
        /// </summary>
        public ulong TimeWhenRepeats
        {
            get { return DataContractTracker.TimeWhenRepeats; }
            set { DataContractTracker.TimeWhenRepeats = value; }
        }

        /// <summary>
        /// delete flag 0 is false, 1 is true - delete the contract
        /// </summary>
        public uint DeleteContract
        {
            get { return DataContractTracker.DeleteContract; }
            set { DataContractTracker.DeleteContract = value; }
        }

        /// <summary>
        /// flag to display details of the quest and not just the list of contracts in the quest panel
        /// </summary>
        public uint SetAsDisplayContract
        {
            get { return DataContractTracker.SetAsDisplayContract; }
            set { DataContractTracker.SetAsDisplayContract = value; }
        }

        /// <summary>
        /// Initialization method.   Loads up information about the contract
        /// </summary>
        /// <param name="contractId">Id of the contract.   This is the index into the contract table in the portal.dat file</param>
        /// <param name="dataObjectId">This is the guid of the player to which we are adding this contract tracker</param>
        public ContractTracker(uint contractId, uint dataObjectId)
        {
            DataContractTracker = new DataContractTracker();
            ContractId = contractId;
            DataObjectId = dataObjectId;
            ContractDetails = GetContractDetails();
            Version = ContractDetails.Version;
        }

        /// <summary>
        /// This is called in initialization and gets us all of the information from the dat file about this quest.
        /// </summary>
        /// <returns>Contract - this class has all of the information from the dat file about the quest.</returns>
        public DatLoader.Entity.Contract GetContractDetails()
        {
            DatLoader.Entity.Contract contractData;
            ContractTable contractTable = ContractTable.ReadFromDat();
            return contractTable.Contracts.TryGetValue(ContractId, out contractData) ? contractData : null;
        }

        public DataContractTracker SnapShotOfDataContractTracker()
        {
            DataContractTracker snapshot = (DataContractTracker)DataContractTracker.Clone();
            return snapshot;
        }
    }
}

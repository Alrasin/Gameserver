/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Entity.Enum;
using DerethForever.Managers;
using DerethForever.Network;
using DerethForever.Network.GameMessages.Messages;

namespace DerethForever.Command.Handlers
{
    public static class ShardCommands
    {
        // // commandname parameters
        // [CommandHandler("commandname", AccessLevel.Admin, CommandHandlerFlag.RequiresWorld, 0)]
        // public static void HandleHelp(Session session, params string[] parameters)
        // {
        //     //TODO: output
        // }
   
        /// <summary>
        /// Cancels an in-progress shutdown event.
        /// </summary>
        [CommandHandler("cancel-shutdown", AccessLevel.Admin, CommandHandlerFlag.None, 0,
            "Stops an active server shutdown.",
            "")]
        public static void HandleCancelShutdown(Session session, params string[] parameters)
        {
            ServerManager.CancelShutdown();
        }

        /// <summary>
        /// Increase or decrease the server shutdown interval in seconds
        /// </summary>
        [CommandHandler("set-shutdown-interval", AccessLevel.Admin, CommandHandlerFlag.None, 1,
            "Changes the delay before the server will shutdown.",
            "< 0-99999 >")]
        public static void HandleSetShutdownInterval(Session session, params string[] parameters)
        {
            if (parameters?.Length > 0)
            {
                uint newShutdownInterval = 0;
                // delay server shutdown for up to x minutes
                // limit to uint length 65535
                string parseInt = parameters[0].Length > 5 ? parameters[0].Substring(0, 5) : parameters[0];
                if (uint.TryParse(parseInt, out newShutdownInterval))
                {
                    // newShutdownInterval is represented as a time element
                    if (newShutdownInterval > uint.MaxValue) newShutdownInterval = uint.MaxValue;

                    // set the interval
                    ServerManager.SetShutdownInterval(Convert.ToUInt32(newShutdownInterval));

                    // message the admin
                    ChatPacket.SendServerMessage(session, $"Shutdown Interval (seconds to shutdown server) has been set to {ServerManager.ShutdownInterval}.", ChatMessageType.Broadcast);
                    return;
                }
            }
            ChatPacket.SendServerMessage(session, "Usage: /change-shutdown-interval <00000>", ChatMessageType.Broadcast);
        }

        /// <summary>
        /// Immediately begins the restart process by setting the shutdown interval to 0 before executing the shutdown method, with restart enabled
        /// </summary>
        [CommandHandler("restart-now", AccessLevel.Admin, CommandHandlerFlag.None, -1,
            "Restart the server down, immediately!",
            "\nThis command will attempt to safely logoff all players, before restarting the server.")]
        public static void RestartServerNow(Session session, params string[] parameters)
        {
            ServerManager.SetShutdownInterval(0);
            ServerManager.RestartServer();
        }

        /// <summary>
        /// Immediately begins the shutdown process by setting the shutdown interval to 0 before executing the shutdown method
        /// </summary>
        [CommandHandler("stop-now", AccessLevel.Admin, CommandHandlerFlag.None, -1,
            "Shuts the server down, immediately!",
            "\nThis command will attempt to safely logoff all players, before shutting down the server.")]
        public static void ShutdownServerNow(Session session, params string[] parameters)
        {
            ServerManager.SetShutdownInterval(0);
            ShutdownServer(session, parameters);
        }

        /// <summary>
        /// Function to shutdown the server from console or in-game.
        /// </summary>
        [CommandHandler("shutdown", AccessLevel.Admin, CommandHandlerFlag.None, 0,
            "Begins the server shutdown process. Optionally displays a shutdown message, if a string is passed.",
            "< Optional Shutdown Message >\n" +
            "\tUse @cancel-shutdown too abort an active shutdown!\n" +
            "\tSet the shutdown delay with @set-shutdown-interval < 0-99999 >")]
        public static void ShutdownServer(Session session, params string[] parameters)
        {
            // inform the world that a shutdown is about to take place
            string shutdownInitiator = (session == null ? "Server" : session.Player.Name.ToString());
            string shutdownText = "";
            string adminShutdownText = "";
            TimeSpan timeTillShutdown = TimeSpan.FromSeconds(ServerManager.ShutdownInterval);
            string timeRemaining = (timeTillShutdown.TotalSeconds > 120 ? $"The server will go down in {(int)timeTillShutdown.TotalMinutes} minutes."
                : $"The server will go down in {timeTillShutdown.TotalSeconds} seconds.");

            // add admin shutdown text
            if (parameters?.Length > 0)
            {
                foreach (var word in parameters)
                {
                    if (adminShutdownText.Length > 0)
                        adminShutdownText += " " + (string)word;
                    else
                        adminShutdownText += (string)word;
                }
            }

            shutdownText += $"{shutdownInitiator} initiated a complete server shutdown @ {DateTime.UtcNow} UTC";

            // output to console (log in the future)
            Console.WriteLine(shutdownText);
            Console.WriteLine(timeRemaining);

            if (adminShutdownText.Length > 0)
                Console.WriteLine("Admin message: " + adminShutdownText);

            // send a message to each player that the server will go down in x interval
            foreach (var player in WorldManager.GetAll())
            {
                // send server shutdown message and time remaining till shutdown
                player.Network.EnqueueSend(new GameMessageSystemChat(shutdownText + "\n" + timeRemaining, ChatMessageType.Broadcast));

                if (adminShutdownText.Length > 0)
                    player.Network.EnqueueSend(new GameMessageSystemChat($"Message from {shutdownInitiator}: {adminShutdownText}", ChatMessageType.Broadcast));
            }
            ServerManager.BeginShutdown(false);
        }

        /// <summary>
        /// Function to shutdown the server from console or in-game.
        /// </summary>
        [CommandHandler("restart", AccessLevel.Admin, CommandHandlerFlag.None, 0,
            "Begins the server restart process. Optionally displays a restart message, if a string is passed.",
            "< Optional Restart Message >\n" +
            "\tUse @cancel-shutdown too abort an active restart!\n" +
            "\tSet the shutdown delay with @set-shutdown-interval < 0-99999 >")]
        public static void RestartServer(Session session, params string[] parameters)
        {
            // inform the world that a shutdown is about to take place
            string shutdownInitiator = (session == null ? "Server" : session.Player.Name.ToString());
            string shutdownText = "";
            string adminShutdownText = "";
            TimeSpan timeTillShutdown = TimeSpan.FromSeconds(ServerManager.ShutdownInterval);
            string timeRemaining = (timeTillShutdown.TotalSeconds > 120 ? $"The server will restart in {(int)timeTillShutdown.TotalMinutes} minutes."
                : $"The server will restart in {timeTillShutdown.TotalSeconds} seconds.");

            // add admin shutdown text
            if (parameters?.Length > 0)
            {
                foreach (var word in parameters)
                {
                    if (adminShutdownText.Length > 0)
                        adminShutdownText += " " + (string)word;
                    else
                        adminShutdownText += (string)word;
                }
            }

            shutdownText += $"{shutdownInitiator} initiated a server restart @ {DateTime.UtcNow} UTC";

            // output to console (log in the future)
            Console.WriteLine(shutdownText);
            Console.WriteLine(timeRemaining);

            if (adminShutdownText.Length > 0)
                Console.WriteLine("Admin message: " + adminShutdownText);

            // send a message to each player that the server will go down in x interval
            foreach (var player in WorldManager.GetAll())
            {
                // send server shutdown message and time remaining till shutdown
                player.Network.EnqueueSend(new GameMessageSystemChat(shutdownText + "\n" + timeRemaining, ChatMessageType.Broadcast));

                if (adminShutdownText.Length > 0)
                    player.Network.EnqueueSend(new GameMessageSystemChat($"Message from {shutdownInitiator}: {adminShutdownText}", ChatMessageType.Broadcast));
            }
            // Begin restart process Restart
            ServerManager.BeginShutdown(true);
        }
    }
}

/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Entity.Enum;

namespace DerethForever.Command
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class CommandHandlerAttribute : Attribute
    {
        public string Command { get; }

        public AccessLevel Access { get; }

        public CommandHandlerFlag Flags { get; }

        public int ParameterCount { get; }

        public string Description { get; }

        public string Usage { get; }

        public CommandHandlerAttribute(string command, AccessLevel access, CommandHandlerFlag flags = CommandHandlerFlag.None, int parameterCount = -1, string description = "", string usage = "")
        {
            Command        = command;
            Access         = access;
            Flags          = flags;
            ParameterCount = parameterCount;
            Description    = description;
            Usage          = usage;
        }

        public CommandHandlerAttribute(string command, AccessLevel access, CommandHandlerFlag flags = CommandHandlerFlag.None, string description = "", string usage = "")
        {
            Command = command;
            Access = access;
            Flags = flags;
            ParameterCount = -1;
            Description = description;
            Usage = usage;
        }

        public CommandHandlerAttribute(string command, AccessLevel access, CommandHandlerFlag flags = CommandHandlerFlag.None)
        {
            Command = command;
            Access = access;
            Flags = flags;
            ParameterCount = -1;
            Description = "";
            Usage = "";
        }
    }
}

/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.Common;
using DerethForever.Api.Common;
using Microsoft.Owin.Hosting;
using System.Threading;
using log4net;
using System.Reflection;

namespace DerethForever.Managers
{
    public class ApiManager
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static volatile bool _run = true;
        private static Thread mainApi;

        public static void Initialize()
        {
            mainApi = new Thread(RunMainApi);
            mainApi.Start();
        }

        private static void RunMainApi()
        {
            if (!string.IsNullOrWhiteSpace(ConfigManager.Config.ApiServer?.ListenUrl))
            {
                // register an observer for changes
                ApiObserver apiObs = new ApiObserver();
                Api.Controllers.BaseController.ApiObservers.Add(apiObs);

                // Get the bind address and port from config:
                var server = WebApp.Start<Api.Startup>(url: ConfigManager.Config.ApiServer.ListenUrl);
                log.Info($"Dereth Forever API listening at {ConfigManager.Config.ApiServer.ListenUrl}");
            }
            else
            {
                log.Info("There was an error in your API configuration.");
            }

            while (_run)
            {
                Thread.Sleep(100);
            }
        }
        
        public static void ShutdownApi()
        {
            _run = false;
            mainApi.Join();
        }
    }
}

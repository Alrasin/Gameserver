/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.Physics;
using DerethForever.Entity;
using System.Drawing;
using BulletSharp;
using DerethForever.Actors;
using DerethForever.Network.Motion;
using DerethForever.Entity.Enum;
using System.Collections.Concurrent;
using log4net;

namespace DerethForever.Managers
{
    public class PhysicsManager
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static volatile DerethPhysics _physics;
        private static Dictionary<uint, RigidBody> _physicsObjects = new Dictionary<uint, RigidBody>();
        private static readonly float TOLERANCE = 0.0001f;

        /// <summary>
        /// this list is a temporary hack for automatic portal usage.  bullet needs to be ripped out,
        /// but this quick 'n dirty thing will allow us to use portals automatically.
        /// </summary>
        private static List<WorldObject> _portals = new List<WorldObject>();
            
        // multiple stack overflow articles document that a List with manual locking is faster than
        // a "ConcurrentBag".  don't be tempted to refactor this.
        private static volatile List<uint> _loadedLandblocks = new List<uint>();
        private static object _landblockListMutex = new object();

        static PhysicsManager()
        {
            _physics = new DerethPhysics();
            _physics.Start();

            // start a thread that loads landblocks in the background
            // maybe?
        }

        public static void UseTime()
        {
            _physics.Step(); // step ignores the timeTick and handles timing internally
        }

        public static void LoadLandblock(uint landblockId)
        {
            if (landblockId < 0x10000)
                landblockId = landblockId << 16;

            if (_loadedLandblocks.Contains(landblockId))
                return;

            lock (_landblockListMutex)
            {
                if (_loadedLandblocks.Contains(landblockId))
                    return;

                _physics.LoadLandlock(landblockId);

                _loadedLandblocks.Add(landblockId);
            }
        }

        public static void LaunchViewer()
        {
            _physics.LaunchViewer();
        }
        
        public static void MoveCamera(uint landblockId)
        {
            if (landblockId < 0x10000)
                landblockId = landblockId << 16;

            _physics.MoveCamera(landblockId);
        }

        public static void MoveCamera(Position p)
        {
            _physics.MoveCamera(p);
        }

        /// <summary>
        /// used for NPCs, portals, generic stuff that just exists.  not for use with anything that moves or changes
        /// physics state.  Note: moveToGround will be removed once we have real physics - we'll just check the gravity flag
        /// </summary>
        public static void AddStaticObject(WorldObject wo, Position position, bool moveToGround = true)
        {
            if (_physicsObjects.ContainsKey(wo.Guid.Full))
            {
                // already in the physics system, just move it
                _physics.MoveObject(_physicsObjects[wo.Guid.Full], position, position, moveToGround);
            }
            else
            {
                // examples:
                // portals are Ethereal, ReportCollision, Gravity, LightingOn

                // because we're not doing our own collision detection, we have to report a lot more than we'd want

                CollisionFlags flags = CollisionFlags.None;
                if ((wo.PhysicsState & PhysicsState.Static) > 0)
                    flags |= CollisionFlags.StaticObject;

                // ReportCollision is true for almost any dynamically loaded object.  The exceptions are temporary:
                //   loot on the ground, corpses, etc.  This check may have to be expanded.

                if ((wo.PhysicsState & PhysicsState.ReportCollision) > 0)
                    flags |= CollisionFlags.CustomMaterialCallback;

                // hacky hacky hack
                if (wo is Portal)
                    _portals.Add(wo);

                var shape = _physics.LoadObject(wo.SetupTableId.Value, position, wo.PhysicsState, Color.Yellow, 0f, flags, moveToGround: moveToGround);
                if (shape != null)
                    _physicsObjects.Add(wo.Guid.Full, shape);
            }
        }

        /// <summary>
        /// primary use case is for players, monsters, and projectiles
        /// </summary>
        public static void AddMovableObject(WorldObject wo, Position position)
        {
            if (_physicsObjects.ContainsKey(wo.Guid.Full))
            {
                // already in the physics system, just move it
                _physics.MoveObject(_physicsObjects[wo.Guid.Full], position, position);
            }
            else
            {
                CollisionFlags flags = CollisionFlags.None;
                if ((wo.PhysicsState & PhysicsState.Static) > 0)
                    flags |= CollisionFlags.StaticObject;

                // ReportCollision is true for almost any dynamically loaded object.  The exceptions are temporary:
                //   loot on the ground, corpses, etc.  This check may have to be expanded.

                if ((wo.PhysicsState & PhysicsState.ReportCollision) > 0)
                    flags |= CollisionFlags.CustomMaterialCallback;

                var body = _physics.LoadObject(wo.SetupTableId.Value, position, wo.PhysicsState, Color.White, 1f, flags);

                if (body != null)
                    _physicsObjects.Add(wo.Guid.Full, body);
            }
        }

        /// <summary>
        /// removes an object.  primary use cases: logoff, monster death, and object decay
        /// </summary>
        public static void RemoveObject(uint objectId)
        {
            if (_physicsObjects.ContainsKey(objectId))
            {
                _physics.RemoveObject(_physicsObjects[objectId]);
                _physicsObjects.Remove(objectId);
            }

            // clean up the nasty nasty bad evil hack
            _portals.RemoveAll(p => p.Guid.Full == objectId);
        }

        /// <summary>
        /// does not work, do not use.  leaving for documentation sake.
        /// </summary>
        private static void SetVelocity(uint objectId, UniversalMotion motion)
        {
            if (!_physicsObjects.ContainsKey(objectId))
                return;

            if (motion?.Position == null)
                return;

            System.Numerics.Quaternion nq = new System.Numerics.Quaternion(motion.Position.RotationX, motion.Position.RotationY, motion.Position.RotationZ, motion.Position.RotationW);
            var heading = System.Numerics.Vector3.Transform(new System.Numerics.Vector3(1f, 0, 0), nq) * motion.Speed;

            // LinearVelocity had no visual impact
            _physicsObjects[objectId].LinearVelocity = new Vector3(heading.X, heading.Z, heading.Y);
            // _physicsObjects[objectId].
        }

        public static Position UpdatePosition(WorldObject wo, Position oldPosition, Position newPosition)
        {
            Position calculatedPosition = null;

            if (!_physicsObjects.ContainsKey(wo.Guid.Full))
            {
                AddMovableObject(wo, newPosition);
                calculatedPosition = newPosition; // TODO: validate the position is legal
            }
            else
                calculatedPosition = _physics.MoveObject(_physicsObjects[wo.Guid.Full], oldPosition, newPosition);

            if (wo is Player)
            {
                // total hack - to be removed on physics rewrite.  static portals ought not be so close the landblock boundary.
                // naive assumption, but it's a temporary hack
                var nearby = _portals.Where(p => p.Location.LandblockId == calculatedPosition.LandblockId).ToList();

                foreach (var p in nearby)
                {
                    float x = wo.CSetup.Radius + p.CSetup.Radius;
                    float squaredDist = wo.Location.SquaredDistanceTo(p.Location);

                    if (squaredDist <= x * x)
                    {
                        log.Info($"colliding {wo.Name} with {p.Name}");
                        p.HandleActionOnCollide(wo.Guid);
                        break;
                    }
                }
            }

            return calculatedPosition;
        }
        
        public static void ProcessMovement(WorldObject wo, double ticks)
        {
            if (wo.CurrentMovement == null)
                return;

            bool updatePhysics = false;
            Position oldPosition = wo.Location;

            double forwardSpeed = wo.CurrentMovement.ForwardSpeed;

            if (forwardSpeed > 0)
            {
                // TODO: account for and/or verify "walking"

                double distance = forwardSpeed * ticks * 4d;
                wo.Location = wo.Location.InFrontOf(distance);
                updatePhysics = true;
            }
            else if (forwardSpeed < 0)
            {
                double distance = forwardSpeed * ticks * 0.65d;
                wo.Location = wo.Location.InFrontOf(distance);
                updatePhysics = true;
            }

            if (Math.Abs(wo.CurrentMovement.SideStepSpeed) > TOLERANCE)
            {
                // by observation, sidestep speed appears to be ~2.5 ingame units per second
                double distance = 2.5d * ticks * (wo.CurrentMovement.SideStepSpeed > 0 ? 1 : -1);
                wo.Location = wo.Location.ToLeftOf(distance);
                updatePhysics = true;
            }

            if (Math.Abs(wo.CurrentMovement.TurnSpeed) > TOLERANCE)
            {
                // the client normally sends +/- 1.5f here, and takes about 2.75 seconds to turn around
                // this implies ~2.25 radians of rotation per second, which is conveniently 1.5 squared
                double rotationSpeed = wo.CurrentMovement.TurnSpeed * Math.Abs(wo.CurrentMovement.TurnSpeed);
                double rotationAmount = rotationSpeed * ticks; // TimeSpan.TicksPerSecond;
                Quaternion q = new Quaternion(wo.Location.RotationX, wo.Location.RotationY, wo.Location.RotationZ, wo.Location.RotationW);
                var q2 = q * new Quaternion(new Vector3(0, 0, 1), (float)-rotationAmount);
                wo.Location.RotationW = q2.W;
                wo.Location.RotationZ = q2.Z;
                updatePhysics = true;
            }
            
            if (updatePhysics)
                wo.Location = UpdatePosition(wo, oldPosition, wo.Location);
        }
    }
}

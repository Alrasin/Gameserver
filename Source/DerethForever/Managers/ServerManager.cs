/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using log4net;
using System;
using System.Threading;

using DerethForever.Common;
using System.Diagnostics;
using System.IO;

namespace DerethForever.Managers
{
    /// <summary>
    /// Servermanager handles unloading the server application properly.
    /// </summary>
    /// <remarks>
    ///   Possibly useful for:
    ///     1. Monitor for errors and performance issues in LandblockManager, GuidManager, WorldManager,
    ///         DatabaseManager, or AssetManager
    ///   Known issue:
    ///     1. No method to verify that everything unloaded properly.
    /// </remarks>
    public static class ServerManager
    {
        /// <summary>
        /// Indicates advanced warning if the applcation will unload.
        /// </summary>
        public static bool ShutdownInitiated { get; private set; }

        /// <summary>
        /// The amount of seconds that the server will wait before unloading the application.
        /// </summary>
        public static uint ShutdownInterval { get; private set; }

        /// <summary>
        /// Tells the server too restart upon shutdown.
        /// </summary>
        public static bool RestartAfterShutdown { get; private set; }

        /// <summary>
        /// Sets the Shutdown Interval in Seconds
        /// </summary>
        /// <param name="interval">postive value representing seconds</param>
        public static void SetShutdownInterval(uint interval)
        {
            log.Warn($"Server shutdown interval reset: {interval}");
            ShutdownInterval = interval;
        }

        public static void Initialize()
        {
            if (AnotherServerIsRunning)
            {
                Console.WriteLine($"Another server is running, please close any other instances of the application too continue...");
                // Prevent server load until previous server process has unloaded
                while (AnotherServerIsRunning)
                    Thread.Sleep(1000);
                Console.WriteLine($"No other processes are found, server will now start.");
            }
            // Loads the configuration for ShutdownInterval from the settings file.
            ShutdownInterval = ConfigManager.Config.Server.ShutdownInterval;
        }

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Starts the shutdown wait thread.
        /// </summary>
        public static void BeginShutdown(bool restartAfterShutdown)
        {
            RestartAfterShutdown = restartAfterShutdown;
            if (!ShutdownInitiated)
            {
                ShutdownInitiated = true;
                var shutdownThread = new Thread(ShutdownServer);
                shutdownThread.Start();
            }
        }

        /// <summary>
        /// Calling this function will always cancel an in-progress shutdown (application unload). This will also
        /// stop the shutdown wait thread and alert users that the server will stay in operation.
        /// </summary>
        public static void CancelShutdown()
        {
            ShutdownInitiated = false;
        }

        private static bool AnotherServerIsRunning
        {
            get
            {
                Process[] runningServerProcesses = Process.GetProcessesByName(Path.GetFileName(System.Windows.Forms.Application.ExecutablePath).Replace(".exe", string.Empty));

                if (runningServerProcesses?.Length >= 2)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Restarts the application by launching a new process and initiating an immediate shutdown.
        /// </summary>
        public static void RestartServer()
        {
            ProcessStartInfo newProcess = new ProcessStartInfo();
            newProcess.WindowStyle = ProcessWindowStyle.Normal;
            newProcess.CreateNoWindow = true;
            newProcess.FileName = System.Windows.Forms.Application.ExecutablePath;
            Process.Start(newProcess);
            // Had success after process start:
            if (ShutdownInterval > 0)
                SetShutdownInterval(0);

            if (!ShutdownInitiated)
                BeginShutdown(false);
        }

        /// <summary>
        /// Threaded task created when performing a server shutdown
        /// </summary>
        private static void ShutdownServer()
        {
            DateTime shutdownTime = DateTime.UtcNow.AddSeconds(ShutdownInterval);

            // wait for shutdown interval to expire
            while (shutdownTime != DateTime.MinValue && shutdownTime >= DateTime.UtcNow)
            {
                // this allows the server shutdown to be canceled
                if (!ShutdownInitiated)
                {
                    // reset shutdown details
                    string shutdownText = $"The server has canceled the shutdown procedure @ {DateTime.UtcNow} UTC";
                    log.Warn(shutdownText);
                    // special text
                    foreach (var player in WorldManager.GetAll())
                    {
                        player.WorldBroadcast(shutdownText);
                    }
                    // break function
                    return;
                }
            }

            // logout each player
            foreach (var player in WorldManager.GetAll(false))
            {
                player.LogOffPlayer();
            }

            // wait 6 seconds for log-off
            Thread.Sleep(6000);

            // TODO: Make sure that the landblocks unloads properly.

            // TODO: Make sure that the databasemanager unloads properly.

            // disabled thread update loop and halt application
            WorldManager.StopWorld();
            // wait for world to end
            while (WorldManager.WorldActive)
            {
                // no nothing
            }

            // write exit to console/log
            log.Warn($"Exiting at {DateTime.UtcNow}");

            if (RestartAfterShutdown)
            {
                log.Warn("Restarting.");
                RestartServer();
            }

            // system exit
            Environment.Exit(Environment.ExitCode);
        }
    }
}

/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using DerethForever.Actors;
using DerethForever.Actors.Actions;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Entity.Enum.Properties;
using DerethForever.Factories;
using DerethForever.Network;
using DerethForever.Network.GameEvent.Events;
using DerethForever.Network.GameMessages.Messages;

// Big shout out to the developers that did so much work on emulators past to help us get to this point.   Coral Golem.

namespace DerethForever.Managers
{
    public static class EmoteManager
    {
        private static double _emoteEndTime { get; set; } // to be used later.   Coral Golem

        /// <summary>
        /// This class is used to get all of the emote actions, and keep a running total of the execution time.   I still have work to do to finish this out.
        /// </summary>
        private class QueuedEmote
        {
            public Emote Emote { get; set; } = new Emote();
            public double ExecutionTime { get; set; }
        }

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static Random _random = new Random();

        /// <summary>
        /// This is where we do the work.   This still has a lot of functionality to be added.   We will queue up all of the emotes
        /// </summary>
        /// <param name="target">What object that has an emote table has been invoked by various methods, use, handing items etc that will now react to the player</param>
        /// <param name="player">Who is the player that has kicked off a series of emotes i.e. who are we telling, advancing a quest or granting xp as examples</param>
        /// <param name="chain">Action Chain to control timing</param>
        /// <param name="category"></param>
        /// <param name="wcid"></param>
        private static void ExecuteEmoteChain(Player player, Creature target, ActionChain chain, EmoteCategory category, uint wcid = 0, string msg = null)
        {
            EmoteSet emoteSet;
            if (wcid == 0)
                if (msg == null)
                {
                    emoteSet = target.EmoteSets.Find(x => x.EmoteCategoryId == (uint)category);
                }
                else
                {
                    emoteSet = target.EmoteSets.Find(x => x.EmoteCategoryId == (uint)category && x.Quest == msg);
                }
            else
            {
                if (msg == null)
                {
                    emoteSet = target.EmoteSets.Find(x => x.EmoteCategoryId == (uint)category && x.ClassId == wcid);
                }
                else
                {
                    emoteSet = target.EmoteSets.Find(x => x.EmoteCategoryId == (uint)category && x.ClassId == wcid && x.Quest == msg);
                }
            }

            if (emoteSet == null || emoteSet.Emotes.Count <= 0)
                return;

            // Initialize
            float totalEmoteSetTime = 0.0F;
            List<QueuedEmote> _emoteQueue = new List<QueuedEmote>();

            // Load up the emoteQueue
            foreach (Emote emote in emoteSet.Emotes.OrderBy(em => em.SortOrder))
            {
                totalEmoteSetTime += emote.Delay;
                // TODO: add current_time
                _emoteQueue.Add(new QueuedEmote { Emote = emote, ExecutionTime = totalEmoteSetTime });
            }

            if (_emoteQueue.Count == 0)
                return;
            foreach (QueuedEmote queuedEmote in _emoteQueue.OrderBy(x => x.Emote.SortOrder))
            {
                // Put in busy and time check here.
                chain.AddAction(target, () => ExecuteEmote(queuedEmote.Emote, player, target, chain));
            }
            _emoteQueue.Clear();
        }

        public static void OnUse(Player player, Creature target)
        {
            ActionChain onUseChain = new ActionChain();
            ExecuteEmoteChain(player, target, onUseChain, EmoteCategory.Use);
            onUseChain.EnqueueChain();
        }

        public static void ReceiveItem(Player player, Creature target, WorldObject item, uint amount = 1)
        {
            ActionChain receiveItemChain = new ActionChain();
            ExecuteEmoteChain(player, target, receiveItemChain, EmoteCategory.Give, item.WeenieClassId);
            item.ContainerId = target.Guid.Full;
            player.Session.Network.EnqueueSend(
                new GameMessagePutObjectInContainer(player.Session, target.Guid, item, 0),
                new GameMessageUpdateInstanceId(player.Session.Player.Sequences, item.Guid, PropertyInstanceId.Container, target.Guid));
            receiveItemChain.EnqueueChain();
        }

        /// <summary>
        /// Big old switch case - we process by emote type.   They can also link to other emote types to allow for more complex interactions and quest progression.
        /// </summary>
        /// <param name="emote">the emote we are doing</param>
        /// <param name="player">What player are we reacting to?</param>
        /// <param name="target">Target of the emote. NPC or item that has an emote table</param>
        /// <param name="chain">Action Chain to control timing and concurrency </param>

        private static void ExecuteEmote(Emote emote, Player player, Creature target, ActionChain chain)
        {
            const float tellDelay = 0.75f;
            const float giveDelay = 0.75f;
            const double radiansPerSecond = 1.5;

            switch (emote.EmoteType)
            {
                case EmoteType.Invalid:
                    break;
                case EmoteType.Act:
                    break;
                case EmoteType.AwardXP:
                    {
                        ulong reward = emote.Amount64 ?? 0;
                        chain.AddAction(target, () => player.GrantXp((ulong)reward));
                    }
                    break;
                case EmoteType.Give:
                    {
                        // TODO Once the other code is in place we need to add an overload method to create by CreateNewWorldObject passing in Palette and Shade
                        chain.AddDelaySeconds(Math.Max(emote.Delay, giveDelay));
                        WorldObject reward = WorldObjectFactory.CreateNewWorldObject(emote.CreationProfile.WeenieClassId.Value);
                        reward.ContainerId = player.Guid.Full;

                        chain.AddAction(player, () => reward.InitializeDataObjectForSave());
                        chain.AddAction(player, () => player.AddToInventory(reward));

                        string message = target.Name + " gives you " + reward.Name;
                        chain.AddAction(player, () => player.Session.Network.EnqueueSend(
                            // new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.EncumbranceVal, UpdateBurden()),
                            new GameMessageCreateObject(reward),
                            new GameMessageSound(player.Guid, Sound.ReceiveItem, 1.0f),
                            new GameMessageUpdateInstanceId(reward.Guid, player.Guid, PropertyInstanceId.Container),
                            new GameMessagePutObjectInContainer(player.Session, player.Guid, reward, 0),
                            new GameMessagePickupEvent(reward),
                            new GameMessageSystemChat(message, ChatMessageType.Broadcast)));
                        // TODO I still need to update burden, if coin, coin value.
                    }
                    break;
                case EmoteType.MoveHome:
                    break;
                case EmoteType.Motion:
                    break;
                case EmoteType.Move:
                    break;
                case EmoteType.PhysScript:
                    break;
                case EmoteType.Say:
                    break;
                case EmoteType.Sound:
                    break;
                case EmoteType.Tell:
                    {
                        chain.AddDelaySeconds(Math.Max(emote.Delay, tellDelay));
                        GameEventTell tell = new GameEventTell(player.Session, emote.Message, target.Name, target.Guid.Full, player.Guid.Full, ChatMessageType.Tell);
                        chain.AddAction(player, () => player.Session.Network.EnqueueSend(tell));
                    }
                    break;
                case EmoteType.Turn:
                    break;
                case EmoteType.TurnToTarget:
                    {
                        double rotateDelay = target.Location.DegreeRotation(player.Location) / radiansPerSecond;
                        chain.AddAction(target, () => target.OnAutonomousMove(player.Location, target.Sequences, MovementTypes.TurnToObject, player.Guid));
                        chain.AddDelaySeconds(emote.Delay + rotateDelay);
                    }
                    break;
                case EmoteType.TextDirect:
                    break;
                case EmoteType.CastSpell:
                    break;
                case EmoteType.Activate:
                    break;
                case EmoteType.WorldBroadcast:
                    break;
                case EmoteType.LocalBroadcast:
                    break;
                case EmoteType.DirectBroadcast:
                    break;
                case EmoteType.CastSpellInstant:
                    break;
                case EmoteType.UpdateQuest:
                    break;
                case EmoteType.InqQuest:
                    break;
                case EmoteType.StampQuest:
                    break;
                case EmoteType.StartEvent:
                    break;
                case EmoteType.StopEvent:
                    break;
                case EmoteType.BLog:
                    break;
                case EmoteType.AdminSpam:
                    break;
                case EmoteType.TeachSpell:
                    {
                        if (emote.SpellId != null)
                        {
                            chain.AddDelaySeconds(emote.Delay);
                            chain.AddAction(player, () => player.HandleActionLearnSpell((uint)emote.SpellId));
                        }
                        else
                            log.Debug(
                                $"Tried to learn spell via emote system - error null spell ID. Target {target.Guid.Full:X} - EmoteId: {emote.EmoteGuid} - EmoteTypeId {emote.EmoteTypeId}");
                    }
                    break;
                case EmoteType.AwardSkillXP:
                    break;
                case EmoteType.AwardSkillPoints:
                    break;
                case EmoteType.InqQuestSolves:
                    break;
                case EmoteType.EraseQuest:
                    break;
                case EmoteType.DecrementQuest:
                    break;
                case EmoteType.IncrementQuest:
                    break;
                case EmoteType.AddCharacterTitle:
                    break;
                case EmoteType.InqBoolStat:
                    break;
                case EmoteType.InqIntStat:
                    {
                        // Let's get the int property they are asking for in the emote
                        int value = player.InquireIntStat((int)(emote.Stat ?? 0)) ?? 0;

                        // Does the target meet the criteria?
                        bool success = value >= emote.Minimum && value <= emote.Maximum;

                        // Get the correct category based on our test
                        EmoteCategory emoteCategory = success ? EmoteCategory.TestSuccess : EmoteCategory.TestFailure;

                        chain.AddAction(target, () => ExecuteEmoteChain(player, target, chain, emoteCategory, 0, emote.Message));
                    }
                    break;
                case EmoteType.InqFloatStat:
                    {
                        // Let's get the float stat they are asking for in the emote
                        double value = player.InquireFloatStat((int)(emote.Stat ?? 0)) ?? 0.0;

                        // Does the target meet the criteria?
                        bool success = (emote.Minimum == null || value >= emote.Minimum) && (emote.Maximum == null || value <= emote.Maximum);

                        // Get the correct category based on our test
                        EmoteCategory emoteCategory = success ? EmoteCategory.TestSuccess : EmoteCategory.TestFailure;
                        
                        chain.AddAction(target, () => ExecuteEmoteChain(player, target, chain, emoteCategory, 0, emote.Message));
                    }
                    break;
                case EmoteType.InqStringStat:
                    {
                        // Let's get the string stat they are asking for in the emote
                        string value = player.InquireStringStat((int)(emote.Stat ?? 0)) ?? "";

                        // Does the target meet the criteria?
                        bool success = value == emote.TestString;

                        // Get the correct category based on our test
                        EmoteCategory emoteCategory = success ? EmoteCategory.TestSuccess : EmoteCategory.TestFailure;

                        chain.AddAction(target, () => ExecuteEmoteChain(player, target, chain, emoteCategory, 0, emote.Message));
                    }
                    break;
                case EmoteType.InqAttributeStat:
                    break;
                case EmoteType.InqRawAttributeStat:
                    break;
                case EmoteType.InqSecondaryAttributeStat:
                    break;
                case EmoteType.InqRawSecondaryAttributeStat:
                    break;
                case EmoteType.InqSkillStat:
                    break;
                case EmoteType.InqRawSkillStat:
                    break;
                case EmoteType.InqSkillTrained:
                    break;
                case EmoteType.InqSkillSpecialized:
                    break;
                case EmoteType.AwardTrainingCredits:
                    {
                        int amount = (int)emote.Amount;
                        chain.AddAction(target, () => player.GrantSkillCredits((amount)));
                    }
                    break;
                case EmoteType.InflictVitaePenalty:
                    break;
                case EmoteType.AwardLevelProportionalXP:
                    {
                        XpTable xpTable = XpTable.ReadFromDat();
                        LevelingChart chart = xpTable.LevelingXpChart;
                        int currentLevel = player.InquireIntStat((int)(PropertyInt.Level)) ?? 0;
                        long reward = 0;
                        if (currentLevel < 275)
                            reward = Math.Min(emote.Maximum64 ?? 0, (long)(chart.Levels[currentLevel].FromPreviousLevel * emote.Percent ?? 0.00));
                        else
                            reward = emote.Maximum64 ?? 0;
                        chain.AddAction(target, () => player.GrantXp((ulong)reward));
                    }
                    break;
                case EmoteType.AwardLevelProportionalSkillXP:
                    break;
                case EmoteType.InqEvent:
                    break;
                case EmoteType.ForceMotion:
                    break;
                case EmoteType.SetIntStat:
                    break;
                case EmoteType.IncrementIntStat:
                    break;
                case EmoteType.DecrementIntStat:
                    break;
                case EmoteType.CreateTreasure:
                    break;
                case EmoteType.ResetHomePosition:
                    break;
                case EmoteType.InqFellowQuest:
                    break;
                case EmoteType.InqFellowNum:
                    break;
                case EmoteType.UpdateFellowQuest:
                    break;
                case EmoteType.StampFellowQuest:
                    break;
                case EmoteType.AwardNoShareXP:
                    break;
                case EmoteType.SetSanctuaryPosition:
                    break;
                case EmoteType.TellFellow:
                    break;
                case EmoteType.FellowBroadcast:
                    break;
                case EmoteType.LockFellow:
                    break;
                case EmoteType.Goto:
                    break;
                case EmoteType.PopUp:
                    break;
                case EmoteType.SetBoolStat:
                    break;
                case EmoteType.SetQuestCompletions:
                    break;
                case EmoteType.InqNumCharacterTitles:
                    break;
                case EmoteType.Generate:
                    break;
                case EmoteType.PetCastSpellOnOwner:
                    break;
                case EmoteType.TakeItems:
                    break;
                case EmoteType.InqYesNo:
                    break;
                case EmoteType.InqOwnsItems:
                    break;
                case EmoteType.DeleteSelf:
                    break;
                case EmoteType.KillSelf:
                    break;
                case EmoteType.UpdateMyQuest:
                    break;
                case EmoteType.InqMyQuest:
                    break;
                case EmoteType.StampMyQuest:
                    break;
                case EmoteType.InqMyQuestSolves:
                    break;
                case EmoteType.EraseMyQuest:
                    break;
                case EmoteType.DecrementMyQuest:
                    break;
                case EmoteType.IncrementMyQuest:
                    break;
                case EmoteType.SetMyQuestCompletions:
                    break;
                case EmoteType.MoveToPos:
                    break;
                case EmoteType.LocalSignal:
                    break;
                case EmoteType.InqPackSpace:
                    break;
                case EmoteType.RemoveVitaePenalty:
                    break;
                case EmoteType.SetEyeTexture:
                    break;
                case EmoteType.SetEyePalette:
                    break;
                case EmoteType.SetNoseTexture:
                    break;
                case EmoteType.SetNosePalette:
                    break;
                case EmoteType.SetMouthTexture:
                    break;
                case EmoteType.SetMouthPalette:
                    break;
                case EmoteType.SetHeadObject:
                    break;
                case EmoteType.SetHeadPalette:
                    break;
                case EmoteType.TeleportTarget:
                    break;
                case EmoteType.TeleportSelf:
                    break;
                case EmoteType.StartBarber:
                    break;
                case EmoteType.InqQuestBitsOn:
                    break;
                case EmoteType.InqQuestBitsOff:
                    break;
                case EmoteType.InqMyQuestBitsOn:
                    break;
                case EmoteType.InqMyQuestBitsOff:
                    break;
                case EmoteType.SetQuestBitsOn:
                    break;
                case EmoteType.SetQuestBitsOff:
                    break;
                case EmoteType.SetMyQuestBitsOn:
                    break;
                case EmoteType.SetMyQuestBitsOff:
                    break;
                case EmoteType.UntrainSkill:
                    break;
                case EmoteType.SetAltRacialSkills:
                    break;
                case EmoteType.SpendLuminance:
                    break;
                case EmoteType.AwardLuminance:
                    break;
                case EmoteType.InqInt64Stat:
                    break;
                case EmoteType.SetInt64Stat:
                    break;
                case EmoteType.OpenMe:
                    break;
                case EmoteType.CloseMe:
                    break;
                case EmoteType.SetFloatStat:
                    break;
                case EmoteType.AddContract:
                    break;
                case EmoteType.RemoveContract:
                    break;
                case EmoteType.InqContractsFull:
                    break;
            }
        }
    }
}

﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.Network.Sequence
{
    public class UIntSequence : ISequence
    {
        private uint value;
        private uint maxValue = UInt32.MaxValue;

        public UIntSequence(uint startingValue, uint maxValue = UInt32.MaxValue)
        {
            value = startingValue;
            this.maxValue = maxValue;
        }

        /// <summary>
        /// Creates an instance without a starting value
        /// </summary>
        /// <param name="clientPrimed">Whether the value gets sent to client before first increment</param>
        public UIntSequence(bool clientPrimed = true, uint maxValue = UInt32.MaxValue)
        {
            this.maxValue = maxValue;
            if (clientPrimed)
                value = 0;
            else
                value = this.maxValue;
        }

        public uint CurrentValue
        {
            get
            {
                return value;
            }
        }
        public uint NextValue
        {
            get
            {
                if (value == maxValue)
                {
                    value = 0;
                    return value;
                }
                return ++value;
            }
        }

        public byte[] CurrentBytes
        {
            get
            {
                return BitConverter.GetBytes(CurrentValue);
            }
        }

        public byte[] NextBytes
        {
            get
            {
                return BitConverter.GetBytes(NextValue);
            }
        }
    }
}

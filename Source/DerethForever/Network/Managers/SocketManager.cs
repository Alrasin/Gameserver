﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System;

using DerethForever.Common;

using log4net;

namespace DerethForever.Network.Managers
{
    public static class SocketManager
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly List<ConnectionListener> Listeners = new List<ConnectionListener>();

        public static void Initialize()
        {
            IPAddress host;

            try
            {
                host = IPAddress.Parse(ConfigManager.Config.Server.Network.Host);
            }
            catch (Exception ex)
            {
                log.Error($"Unable to use {ConfigManager.Config.Server.Network.Host} as host due to: {ex.ToString()}");
                log.Error("Using IPAddress.Any as host instead.");
                host = IPAddress.Any;
            }

            Listeners.Add(new ConnectionListener(host, ConfigManager.Config.Server.Network.Port));
            log.Info($"Binding ConnectionListener to {host}:{ConfigManager.Config.Server.Network.Port}");
            Listeners.Add(new ConnectionListener(host, ConfigManager.Config.Server.Network.Port + 1));
            log.Info($"Binding ConnectionListener to {host}:{ConfigManager.Config.Server.Network.Port + 1}");

            foreach (var listener in Listeners)
                listener.Start();
        }

        public static Socket GetSocket(int id = 1)
        {
            return Listeners[id].Socket;
        }
    }
}

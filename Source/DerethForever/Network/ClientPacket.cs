﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using log4net;
using System.IO;

namespace DerethForever.Network
{
    public class ClientPacket : Packet
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public BinaryReader Payload { get; }
        public PacketHeaderOptional HeaderOptional { get; private set; }

        public ClientPacket(byte[] data)
        {
            using (var stream = new MemoryStream(data))
            {
                using (var reader = new BinaryReader(stream))
                {
                    Header = new PacketHeader(reader);
                    Data = new MemoryStream(reader.ReadBytes(Header.Size), 0, Header.Size, false, true);
                    Payload = new BinaryReader(Data);
                    HeaderOptional = new PacketHeaderOptional(Payload, Header);
                }
            }

            ReadFragments();
        }

        private void ReadFragments()
        {
            if (Header.HasFlag(PacketHeaderFlags.BlobFragments))
            {
                while (Payload.BaseStream.Position != Payload.BaseStream.Length)
                    Fragments.Add(new ClientPacketFragment(Payload));
            }
        }

        public bool VerifyChecksum(uint issacXor)
        {
            uint fragmentChecksum = 0u;
            foreach (ClientPacketFragment fragment in Fragments)
            {
                fragmentChecksum += fragment.CalculateHash32();
            }

            uint payloadChecksum = HeaderOptional.CalculateHash32() + fragmentChecksum;

            uint finalChecksum = Header.CalculateHash32() + (payloadChecksum ^ issacXor);
            log.DebugFormat("Checksum is calculated as {0} and is {1} in header", finalChecksum, Header.Checksum);
            return finalChecksum == Header.Checksum;
        }
    }
}

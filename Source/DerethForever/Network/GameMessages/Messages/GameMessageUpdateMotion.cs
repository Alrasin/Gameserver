/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;
using DerethForever.Network.Motion;
using DerethForever.Network.Sequence;

namespace DerethForever.Network.GameMessages.Messages
{
    public class GameMessageUpdateMotion : GameMessage
    {
        public GameMessageUpdateMotion(ObjectGuid animationTargetGuid, byte[] instance_timestamp, SequenceManager sequence, MotionState newState)
             : base(GameMessageOpcode.Motion, GameMessageGroup.Group0A)
        {
            if (newState == null)
                return;

            Writer.WriteGuid(animationTargetGuid);
            // who is getting the message - the rest of the sequences are the target objects sequences -may be the same
            Writer.Write(instance_timestamp);
            Writer.Write(sequence.GetNextSequence(SequenceType.ObjectMovement));
            ushort autonomous = newState.IsAutonomous ? (ushort)1 : (ushort)0;
            if (autonomous == 0)
                Writer.Write(sequence.GetNextSequence(SequenceType.ObjectServerControl));
            else
                Writer.Write(sequence.GetCurrentSequence(SequenceType.ObjectServerControl));
            Writer.Write(autonomous);
            var movementData = newState.GetPayload(animationTargetGuid, sequence);
            Writer.Write(movementData);
            Writer.Align();
        }
    }
}

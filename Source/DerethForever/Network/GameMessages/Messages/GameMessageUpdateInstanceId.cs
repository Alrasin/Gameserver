/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Actors;
using DerethForever.Entity;
using DerethForever.Entity.Enum.Properties;
using DerethForever.Network.Sequence;

namespace DerethForever.Network.GameMessages.Messages
{
    public class GameMessageUpdateInstanceId : GameMessage
    {
        public GameMessageUpdateInstanceId(ObjectGuid containerGuid, ObjectGuid itemGuid, PropertyInstanceId iidPropertyId)
            : base(GameMessageOpcode.UpdateInstanceId, GameMessageGroup.Group09)
        {
            // TODO: research - could these types of sends be generalized by payload type?   for example GameMessageInt
            Writer.Write((byte)1);  // wts
            Writer.Write(containerGuid.Full); // sender
            Writer.Write((uint)iidPropertyId);
            Writer.Write(itemGuid.Full); // new value of the container id
            Writer.Align(); // not sure that I need this - can someone explain when to use this?
        }

        public GameMessageUpdateInstanceId(SequenceManager sequences, ObjectGuid senderGuid, PropertyInstanceId idPropertyId, ObjectGuid value)
            : base(GameMessageOpcode.UpdateInstanceId, GameMessageGroup.Group09)
        {
            Writer.Write(sequences.GetNextSequence(SequenceType.PublicUpdatePropertyInstanceId));  // wts
            Writer.Write(senderGuid.Full);
            Writer.Write((uint)idPropertyId);
            Writer.Write(value.Full);
        }
    }
}

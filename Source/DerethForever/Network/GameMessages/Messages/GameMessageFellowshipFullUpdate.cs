/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Actors;
using DerethForever.Actors.Actions;

namespace DerethForever.Network.GameMessages.Messages
{
    public class GameMessageFellowshipFullUpdate : GameMessage
    {
        public GameMessageFellowshipFullUpdate(Session session)
            : base(GameMessageOpcode.GameEvent, GameMessageGroup.Group09)
        {
            // This is a naive, bare-bones implementation of 0x02BE, FullFellowshipUpdate.
            // 0x02BE is fairly complicated, so the following code is at least valuable as an example of a valid server response.

            // todo: The current implementation has race conditions,
            // and there are questions that must be answered before it can be fixed.
            // We need to figure out who "owns" the fellowship data.
            // Does everyone get a turn to read from and modify the fellowship data, and if so, how is this managed?

            // Currently, creating and leaving a fellowship is supported.
            // Any other fellowship function is not yet supported.
            
            Fellowship fellowship = session.Player.Fellowship;
            if (fellowship == null)
                return;

            Writer.Write(session.Player.Guid.Full);
            Writer.Write(session.GameEventSequence++);
            Writer.Write((uint)GameEvent.GameEventType.FellowshipFullUpdate);

            // the current number of fellowship members
            Writer.Write((UInt16)fellowship.Fellows.Count);

            // todo: figure out what these two bytes are for
            Writer.Write((byte)0x10);
            Writer.Write((byte)0x00);

            // --- FellowInfo ---
            foreach (Player fellow in fellowship.Fellows)
            {
                // Write data associated with each fellowship member
                WriteFellow(fellow, fellowship.ShareLoot);
            }

            Writer.WriteString16L(fellowship.FellowshipName);
            Writer.Write(fellowship.FellowshipLeaderGuid);
            Writer.Write(Convert.ToUInt32(fellowship.ShareXP));
            Writer.Write(Convert.ToUInt32(fellowship.EvenXpShare));
            Writer.Write(Convert.ToUInt32(fellowship.Open));

            // todo: fellows departed?
            Writer.Write(0u);

            // End of meaningful data?
            Writer.Write((uint)0x00200000);
            Writer.Write((uint)0x00200000);
        }

        public void WriteFellow(Player fellow, bool shareLoot)
        {
            Writer.Write(fellow.Guid.Full);

            Writer.Write(0u);
            Writer.Write(0u);

            Writer.Write(fellow.Level);

            Writer.Write(fellow.Health.MaxValue);
            Writer.Write(fellow.Stamina.MaxValue);
            Writer.Write(fellow.Mana.MaxValue);

            Writer.Write(fellow.Health.Current);
            Writer.Write(fellow.Stamina.Current);
            Writer.Write(fellow.Mana.Current);

            // todo: share loot with this fellow?
            if (shareLoot)
            {
                Writer.Write(1u);
            }
            else
            {
                Writer.Write(0u);
            }

            Writer.WriteString16L(fellow.Name);
        }
    }
}

/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;

namespace DerethForever.Network.GameEvent.Events
{
    public class GameEventStartBarber : GameEventMessage
    {
        public GameEventStartBarber(Session session)
            : base(GameEventType.StartBarber, GameMessageGroup.Group09, session)
        {
            // These are the motion tables for Empyrean float and not-float (one for each gender). They are hard-coded into the client.
            const uint EmpyreanMaleMotionDID = 0x0900020Eu;
            const uint EmpyreanFemaleMotionDID = 0x0900020Du;

            // We will use this function to get the current player's appearance values.
            DataCharacter characterClone = (DataCharacter)Session.Player.GetDataObject();

            Writer.Write((uint)session.Player.PaletteBaseId); // base palette for character
            Writer.Write(characterClone.HeadObject); // Default Hair Model
            Writer.Write(characterClone.HairTexture); // Hair Texture
            Writer.Write(characterClone.DefaultHairTexture); // Default Hair Texture

            Writer.Write(characterClone.EyesTexture); // Eyes Texture
            Writer.Write(characterClone.DefaultEyesTexture); // Default Eyes Texture

            Writer.Write(characterClone.NoseTexture); // Nose Texture
            Writer.Write(characterClone.DefaultNoseTexture); // Default Nose Texture

            Writer.Write(characterClone.MouthTexture); // Mouth Texture
            Writer.Write(characterClone.DefaultMouthTexture); // Default Mouth Texture

            Writer.Write(characterClone.SkinPalette); // Skin Palette
            Writer.Write(characterClone.HairPalette); // Hair Palette
            Writer.Write(characterClone.EyesPalette); // Eyes Palette

            Writer.Write((uint)characterClone.SetupTableId); // Setup Model

            // Check for Empyrean "Float" option
            if (characterClone.MotionTableId == EmpyreanFemaleMotionDID || characterClone.MotionTableId == EmpyreanMaleMotionDID)
                Writer.Write(0x0000001); // Currently using the "bound/running" animation
            else
                Writer.Write(0x0000000); // Current "default" animation (normal running for most, float for Empyrean)

            Writer.Write(0x0u); // Unknown - Client seems to have this hard-coded as 0, so likely was just a TBD for potential future use
        }
    }
}

﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Network.GameEvent.Events
{
    public class GameEventChannelIndex : GameEventMessage
    {
        public GameEventChannelIndex(Session session) : base(GameEventType.ChannelIndex, GameMessageGroup.Group09, session)
        {
            WriteEventBody();
        }

        private void WriteEventBody()
        {
            // TODO: this probably could be done better but I'm not sure if there's a point, it's not like the client out of the box supports making up new channels

            // Admin
            // Abuse
            // Audit
            // Av1
            // Av2
            // Av3
            // Sentinel
            // Help

            // uint channelCount = 0;

            if (Session.Player.IsAdmin)
            {
                Writer.Write(8u);
                Writer.WriteString16L("Abuse");
                Writer.WriteString16L("Admin");
                Writer.WriteString16L("Audit");
                Writer.WriteString16L("Av1");
                Writer.WriteString16L("Av2");
                Writer.WriteString16L("Av3");
                Writer.WriteString16L("Sentinel");
                Writer.WriteString16L("Help");
            }

            if (Session.Player.IsArch || Session.Player.IsEnvoy || Session.Player.IsPsr)
            {
                Writer.Write(7u);
                Writer.WriteString16L("Abuse");
                Writer.WriteString16L("Audit");
                Writer.WriteString16L("Av1");
                Writer.WriteString16L("Av2");
                Writer.WriteString16L("Av3");
                Writer.WriteString16L("Sentinel");
                Writer.WriteString16L("Help");
            }

            // TODO: Needs an IsAdvocate thing to work like it should
            // if (Session.Player.IsAdvocate)
            // {
            //    Writer.Write(5u);
            //    Writer.WriteString16L("Abuse");
            //    Writer.WriteString16L("Av1");
            //    Writer.WriteString16L("Av2");
            //    Writer.WriteString16L("Av3");
            //    Writer.WriteString16L("Help");
            // }
        }
    }
}
﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Network.GameEvent.Events
{
    public class GameEventInscriptionResponse : GameEventMessage
    {
        /// <summary>
        /// Sends our response message to the client once we recieve a Game Event (F7B0) Writing_SetInscription (0x00BF) Og II
        /// </summary>
        /// <param name="session">Player Session - used by the base for squence and guid to target</param>
        /// <param name="objectID">This is the object we are inscribing</param>
        /// <param name="inscriptionText">This is the inscription - I am sure it is something profound.</param>
        /// <param name="scribeName">Who is inscribing the object.</param>
        /// <param name="scribeAccount">This is the scribe account - not sure how it works and passing empty string if null</param>
        public GameEventInscriptionResponse(Session session, uint objectID, string inscriptionText, string scribeName, string scribeAccount)
                : base(GameEventType.GetInscriptionResponse, GameMessageGroup.Group09, session)
        {
            Writer.Write(objectID);
            Writer.WriteString16L(inscriptionText);
            Writer.Write(session.Player.Guid.Full);
            Writer.WriteString16L(scribeName);
            Writer.WriteString16L(scribeAccount);
            Writer.Align();
        }
    }
}
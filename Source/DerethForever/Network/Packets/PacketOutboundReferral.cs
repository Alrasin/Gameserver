﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Network.Packets
{
    public class PacketOutboundReferral : ServerPacket
    {
        public PacketOutboundReferral(ulong worldConnectionKey, string[] sessionIPAddress, byte[] host, ushort port, bool sendInternalHostOnLocalNetwork, byte[] internalHost) : base()
        {
            Header.Flags = PacketHeaderFlags.EncryptedChecksum | PacketHeaderFlags.Referral;

            BodyWriter.Write(worldConnectionKey);
            BodyWriter.Write((ushort)2);
            BodyWriter.WriteUInt16BE(port);

            if (sendInternalHostOnLocalNetwork &&
                (sessionIPAddress[0] == "10"
                || (sessionIPAddress[0] == "172" && System.Convert.ToInt16(sessionIPAddress[1]) >= 16 && System.Convert.ToInt16(sessionIPAddress[1]) <= 31)
                || (sessionIPAddress[0] == "192" && sessionIPAddress[1] == "168")))
                BodyWriter.Write(internalHost);
            else
                BodyWriter.Write(host);

            BodyWriter.Write(0ul);
            BodyWriter.Write((ushort)0x18); // This value is currently the hard coded Server ID. It can be something different...
            BodyWriter.Write((ushort)0);
            BodyWriter.Write(0u);
        }
    }
}

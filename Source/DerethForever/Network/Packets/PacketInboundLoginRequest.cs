﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common.Extensions;

namespace DerethForever.Network.Packets
{
    public class PacketInboundLoginRequest
    {
        public uint Timestamp { get; }

        public string ClientAccountString { get; }

        /// <summary>
        /// formerly the GLS ticket.  We don't have GLS, so we're going to use Jwt.
        /// </summary>
        public string JwtToken { get; }

        public PacketInboundLoginRequest(ClientPacket packet)
        {
            string someString = packet.Payload.ReadString16L(); // always "1802"
            uint len = packet.Payload.ReadUInt32(); // data length left in packet including ticket
            uint unknown1 = packet.Payload.ReadUInt32();
            uint unknown2 = packet.Payload.ReadUInt32();
            Timestamp = packet.Payload.ReadUInt32();
            ClientAccountString = packet.Payload.ReadString16L();
            string unknown3 = packet.Payload.ReadString16L();
            
            // int consumed = (someString.Length + 2) + 4 * sizeof(uint) + (ClientAccountString.Length + 2) + (unknown3.Length + 2);
            
            // this packet header has 2 bytes that are proving hard to decipher.  sometimes they occur
            // before the length DWORD at the start of the 32L string, sometimes they are after, in which
            // case the string len is increased by 2 and the 2 bytes are prepended to the string (as
            // garbage and possibly invalid input)

            JwtToken = packet.Payload.ReadString32L();
        }
    }
}

﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class CBldPortal
    {
        public ushort Flags { get; set; }

        // Not sure what these do. They are both calculated from the flags.
        public ushort ExactMatch { get; set; } 
        public ushort PortalSide { get; set; }

        // Basically the cells that connect both sides of the portal
        public uint OtherCellId { get; set; }
        public uint OtherPortalId { get; set; }

        // List of cells used in this structure. (Or possibly just those visible through it.)
        public List<uint> StabList { get; set; } = new List<uint>();
    }
}

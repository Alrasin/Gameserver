﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    /// <summary>
    /// This is actually different than just a "Vertex" class.
    /// </summary>
    public class SWVertex
    {
        public short VertId { get; set; } // referenced by a Polygon
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public float NormalX { get; set; }
        public float NormalY { get; set; }
        public float NormalZ { get; set; }
        public List<Vec2Duv> UVs { get; set; } = new List<Vec2Duv>();

        public static SWVertex Read(DatReader datReader)
        {
            SWVertex obj = new SWVertex();

            short num_uvs = datReader.ReadInt16();

            obj.X = datReader.ReadSingle();
            obj.Y = datReader.ReadSingle();
            obj.Z = datReader.ReadSingle();
            obj.NormalX = datReader.ReadSingle();
            obj.NormalY = datReader.ReadSingle();
            obj.NormalZ = datReader.ReadSingle();

            for (short i = 0; i < num_uvs; i++)
                obj.UVs.Add(Vec2Duv.Read(datReader));
            
            return obj;
        }
    }
}

﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class Polygon
    {
        public byte NumPts { get; set; }
        public byte Stippling { get; set; } // Whether it has that textured/bumpiness to it
        public int SidesType { get; set; }
        public short PosSurface { get; set; }
        public short NegSurface { get; set; }
        public List<short> VertexIds { get; set; } = new List<short>(); 
        public List<byte> PosUVIndices { get; set; } = new List<byte>();
        public List<byte> NegUVIndices { get; set; } = new List<byte>();

        public static Polygon Read(DatReader datReader)
        {
            Polygon obj = new Polygon();

            obj.NumPts = datReader.ReadByte();
            obj.Stippling = datReader.ReadByte();
            obj.SidesType = datReader.ReadInt32();
            obj.PosSurface = datReader.ReadInt16();
            obj.NegSurface = datReader.ReadInt16();

            for (short i = 0; i < obj.NumPts; i++)
                obj.VertexIds.Add(datReader.ReadInt16());

            if ((obj.Stippling & 4) == 0)
            {
                for (short i = 0; i < obj.NumPts; i++)
                    obj.PosUVIndices.Add(datReader.ReadByte());
            }

            if (obj.SidesType == 2 && ((obj.Stippling & 8) == 0))
            {
                for (short i = 0; i < obj.NumPts; i++)
                    obj.NegUVIndices.Add(datReader.ReadByte());
            }

            if (obj.SidesType == 1)
            {
                obj.NegSurface = obj.PosSurface;
                obj.NegUVIndices = obj.PosUVIndices;
            }

            return obj;
        }
    }
}

﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class TexMerge
    {
        public uint BaseTexSize { get; set; }
        public List<TerrainAlphaMap> CornerTerrainMaps { get; set; } = new List<TerrainAlphaMap>();
        public List<TerrainAlphaMap> SideTerrainMaps { get; set; } = new List<TerrainAlphaMap>();
        public List<RoadAlphaMap> RoadMaps { get; set; } = new List<RoadAlphaMap>();
        public List<TMTerrainDesc> TerrainDesc { get; set; } = new List<TMTerrainDesc>();

        public static TexMerge Read(DatReader datReader)
        {
            TexMerge obj = new TexMerge();

            obj.BaseTexSize = datReader.ReadUInt32();

            uint num_corner_terrain_maps = datReader.ReadUInt32();
            for (uint i = 0; i < num_corner_terrain_maps; i++)
                obj.CornerTerrainMaps.Add(TerrainAlphaMap.Read(datReader));

            uint num_side_terrain_maps = datReader.ReadUInt32();
            for (uint i = 0; i < num_side_terrain_maps; i++)
                obj.SideTerrainMaps.Add(TerrainAlphaMap.Read(datReader));

            uint num_road_maps = datReader.ReadUInt32();
            for (uint i = 0; i < num_road_maps; i++)
                obj.RoadMaps.Add(RoadAlphaMap.Read(datReader));

            uint num_terrain_desc = datReader.ReadUInt32();
            for (uint i = 0; i < num_terrain_desc; i++)
                obj.TerrainDesc.Add(TMTerrainDesc.Read(datReader));

            return obj;
        }
    }
}

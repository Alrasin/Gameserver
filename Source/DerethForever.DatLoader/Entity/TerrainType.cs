/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class TerrainType
    {
        public string TerrainName { get; set; }

        public uint TerrainColor { get; set; }

        public List<uint> SceneTypes { get; set; } = new List<uint>();

        public static TerrainType Read(DatReader datReader)
        {
            TerrainType obj = new TerrainType();

            obj.TerrainName = datReader.ReadPString();
            datReader.AlignBoundary();

            obj.TerrainColor = datReader.ReadUInt32();

            uint num_stypes = datReader.ReadUInt32();
            for (uint i = 0; i < num_stypes; i++)
                obj.SceneTypes.Add(datReader.ReadUInt32());

            return obj;
        }
    }
}

﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.DatLoader.Entity
{
    public class SkyObjectReplace
    {
        public uint ObjectIndex { get; set; }
        public uint GFXObjId { get; set; }
        public float Rotate { get; set; }
        public float Transparent { get; set; }
        public float Luminosity { get; set; }
        public float MaxBright { get; set; }
    
        public static SkyObjectReplace Read(DatReader datReader)
        {
            SkyObjectReplace obj = new SkyObjectReplace();
            obj.ObjectIndex = datReader.ReadUInt32();
            obj.GFXObjId = datReader.ReadUInt32();
            obj.Rotate = datReader.ReadSingle();
            obj.Transparent = datReader.ReadSingle();
            obj.Luminosity = datReader.ReadSingle();
            obj.MaxBright = datReader.ReadSingle();
            return obj;
        }
    }
}

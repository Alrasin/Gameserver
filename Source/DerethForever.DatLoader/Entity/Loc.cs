/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Numerics;

namespace DerethForever.DatLoader.Entity
{
#pragma warning disable 660,661
    public class Loc
#pragma warning restore 660,661
    {
        public Vector3 Origin;
        public Quaternion Quaternion;

        public const float TOLERANCE = 0.0002f;
        public const float DEGREES_PER_RADIAN = (float)Math.PI / 180f;

        public uint Cell;

        public Loc()
        {
            Origin = Vector3.Zero;
            Quaternion = new Quaternion(0f, 0f, 0f, 1f);
        }

        public Loc(Loc source)
        {
            Cell = source.Cell;
            Origin = source.Origin;
            Quaternion = source.Quaternion;
        }

        public Loc(uint cell, Vector3 origin, Quaternion quat)
        {
            Cell = cell;
            Origin = origin;
            Quaternion = quat;
        }

        public static Loc Unpack(uint cell, DatReader reader)
        {
            Loc loc = new Loc();
            loc.Cell = cell;
            loc.Origin = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            float qw = reader.ReadSingle(); // packed quats are WXYZ, native constructor is XYZW
            loc.Quaternion = new Quaternion(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), qw);
            return loc;
        }

        public static bool operator ==(Loc p1, Loc p2)
        {
            if (ReferenceEquals(p1, null))
                return ReferenceEquals(p2, null);

            if (Math.Abs(p1.Origin.X - p2.Origin.X) > TOLERANCE)
                return false;
            if (Math.Abs(p1.Origin.Y - p2.Origin.Y) > TOLERANCE)
                return false;
            if (Math.Abs(p1.Origin.Z - p2.Origin.Z) > TOLERANCE)
                return false;

            return true;
        }

        public static bool operator !=(Loc p1, Loc p2)
        {
            return !(p1 == p2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Loc p2 = (Loc)obj;

            return (this == p2);
        }

        /// <summary>
        /// //----- (00535DB0) --------------------------------------------------------
        /// void __thiscall Frame::set_vector_heading(Frame*this, AC1Legacy::Vector3* heading)
        /// acclient.c 357668
        /// </summary>
        public void SetVectorHeading(Vector3 heading)
        {
            if (heading.IsInsignificant())
                return;

            heading.Normalize();

            float zRotation = (-1 * (450f - (float)Math.Atan2(heading.X, heading.Y) * DEGREES_PER_RADIAN) % 360f) / DEGREES_PER_RADIAN;
            float xRotation = (float)Math.Asin(heading.Z);

            SetHeading(zRotation, xRotation);
        }

        /// <summary>
        /// //----- (00509F60) --------------------------------------------------------
        /// AC1Legacy::Vector3* __thiscall Position::get_offset(Position*this, AC1Legacy::Vector3* result, Position* p)
        /// acclient.c 311690
        /// </summary>
        /// <param name="target"></param>
        public Vector3 GetOffset(Loc target)
        {
            Vector3 offset = Vector3.Zero;

            if (this.Cell != target.Cell)
                offset = GetBlockOffset(this.Cell, target.Cell);

            offset.X += target.Origin.X - this.Origin.X;
            offset.Y += target.Origin.Y - this.Origin.Y;
            offset.Z += target.Origin.Z - this.Origin.Z;

            return offset;
        }

        /// <summary>
        /// calculates distances between the specified landcells.  the client takes makes 2 functions
        /// out of this, one left shifting the result 3 bits and the other multiplying by 24.  instead,
        /// we will just multiply by 192 and save the shift operation.
        ///
        /// //----- (0043E630) --------------------------------------------------------
        /// AC1Legacy::Vector3* __cdecl LandDefs::get_block_offset(AC1Legacy::Vector3* result, unsigned int cell_from, unsigned int cell_to)
        /// acclient.c 123110
        /// </summary>
        public static Vector3 GetBlockOffset(uint originBlock, uint targetBlock)
        {
            uint originX = (originBlock & 0xFF000000) >> 24;
            uint originY = (originBlock & 0x00FF0000) >> 16;
            uint targetX = (targetBlock & 0xFF000000) >> 24;
            uint targetY = (targetBlock & 0x00FF0000) >> 16;
            float deltaX = (targetX - originX) * 192.0f;
            float deltaY = (targetY - originY) * 192.0f;

            return new Vector3(deltaX, deltaY, 0f);
        }

        /// <summary>
        /// client optimizations have been removed from this function in favor of calling stock
        /// quaternion functions in the System.Numerics library.
        ///
        /// //----- (00525180) --------------------------------------------------------
        /// void __thiscall Frame::combine(Frame*this, Frame* _f1, AFrame* _f2)
        /// acclient.c 340420
        /// </summary>
        public static Loc Combine(Loc f1, Loc f2)
        {
            Loc result = new Loc();
            result.Origin = f1.Origin + Vector3.Transform(f2.Origin, f1.Quaternion);
            result.Quaternion = Quaternion.Multiply(f1.Quaternion, f2.Quaternion);
            return result;
        }

        /// <summary>
        /// client optimizations have been removed from this function in favor of calling stock
        /// quaternion functions in the System.Numerics library.
        ///
        /// //----- (00518FD0) --------------------------------------------------------
        /// void __thiscall Frame::combine(Frame*this, Frame* _f1, AFrame* _f2, AC1Legacy::Vector3* scale)
        /// acclient.c 323563
        /// </summary>
        public static Loc Combine(Loc f1, Loc f2, Vector3 scale)
        {
            Loc result = new Loc();
            result.Origin = f1.Origin + Vector3.Transform(f2.Origin * scale, f1.Quaternion);
            result.Quaternion = Quaternion.Multiply(f1.Quaternion, f2.Quaternion);
            return result;
        }

        public void SetHeading(float zRotation, float xRotation = 0)
        {
            /*
            w = c1 c2 c3 - s1 s2 s3
            x = s1 s2 c3 + c1 c2 s3
            y = s1 c2 c3 + c1 s2 s3
            z = c1 s2 c3 - s1 c2 s3

            where:

            c1 = cos(heading / 2)
            c2 = cos(attitude / 2)
            c3 = cos(bank / 2)
            s1 = sin(heading / 2)
            s2 = sin(attitude / 2)
            s3 = sin(bank / 2)

            for us:

            heading = zRotation
            bank = 0
            attitude = xRotation

             */

            double c1 = Math.Cos(zRotation / 2);
            double c2 = Math.Cos(xRotation / 2);
            // double c3 = 1; // Math.Cos(0 / 2) == 1;
            double s1 = Math.Sin(zRotation / 2);
            double s2 = Math.Sin(xRotation / 2);
            // double s3 = 0; // Math.Sin(0 / 2) == 0;

            Quaternion.W = (float)(c1 * c2);  // reduced from c1 * c2 * 1 - s1 * s2 * 0
            Quaternion.X = (float)(s1 * s2);  // reduced from s1 * s2 * 1 + c1 * c2 * 0
            Quaternion.Y = (float)(s1 * c2);  // reduced from s1 * c2 * 1 + c1 * s2 * 0
            Quaternion.Z = (float)(c1 * s2);  // reduced from c1 * s2 * 1 - s1 * c2 * 0
        }

        /// <summary>
        /// rotates the current frame by the XYZ angles specified in the vector.
        ///
        /// //----- (005357A0) --------------------------------------------------------
        /// void __thiscall Frame::grotate(Frame*this, AC1Legacy::Vector3* w)
        /// acclient.c 357422
        /// </summary>
        public void Rotate(Vector3 w)
        {
            float mag = w.Magnitude();
            if (mag < TOLERANCE)
                return;

            w *= 1 / mag; // normalize

            // yay system libraries!
            var q1 = Quaternion.CreateFromAxisAngle(w, mag * 0.5f);
            var q2 = Quaternion.Multiply(q1, this.Quaternion);
            this.Quaternion = Quaternion.Normalize(q2);
        }

        /// <summary>
        /// //----- (00534ED0) --------------------------------------------------------
        /// int __thiscall Frame::IsValid(Frame*this)
        /// acclient.c 357025
        /// </summary>
        public bool IsValid()
        {
            if (!IsValidExceptForHeading())
                return false;

            float sumSquares = Quaternion.LengthSquared();

            // why 5?  no clue, it's what the client used
            return float.IsNaN(sumSquares) || sumSquares > (5 * TOLERANCE);
        }

        /// <summary>
        /// //----- (00534FE0) --------------------------------------------------------
        /// BOOL __thiscall Frame::IsValidExceptForHeading(Frame*this)
        /// acclient.c 357051
        /// </summary>
        /// <returns></returns>
        public bool IsValidExceptForHeading()
        {
            bool anyNaN = float.IsNaN(Origin.X) || float.IsNaN(Origin.Y) || float.IsNaN(Origin.Z) || float.IsNaN(Quaternion.W) || float.IsNaN(Quaternion.X) || float.IsNaN(Quaternion.Y) || float.IsNaN(Quaternion.Z);
            return anyNaN;
        }
    }
}

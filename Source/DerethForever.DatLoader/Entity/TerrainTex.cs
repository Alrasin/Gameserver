﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.DatLoader.Entity
{
    public class TerrainTex
    {
        public uint TexGID { get; set; }
        public uint TexTiling { get; set; }
        public uint MaxVertBright { get; set; }
        public uint MinVertBright { get; set; }
        public uint MaxVertSaturate { get; set; }
        public uint MinVertSaturate { get; set; }
        public uint MaxVertHue { get; set; }
        public uint MinVertHue { get; set; }
        public uint DetailTexTiling { get; set; }
        public uint DetailTexGID { get; set; }

        public static TerrainTex Read(DatReader datReader)
        {
            TerrainTex obj = new TerrainTex();

            obj.TexGID = datReader.ReadUInt32();
            obj.TexTiling = datReader.ReadUInt32();
            obj.MaxVertBright = datReader.ReadUInt32();
            obj.MinVertBright = datReader.ReadUInt32();
            obj.MaxVertSaturate = datReader.ReadUInt32();
            obj.MinVertSaturate = datReader.ReadUInt32();
            obj.MaxVertHue = datReader.ReadUInt32();
            obj.MinVertHue = datReader.ReadUInt32();
            obj.DetailTexTiling = datReader.ReadUInt32();
            obj.DetailTexGID = datReader.ReadUInt32();

            return obj;
        }
    }
}

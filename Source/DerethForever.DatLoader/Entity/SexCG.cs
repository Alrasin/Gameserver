﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class SexCG
    {
        public string Name { get; set; }
        public uint Scale { get; set; }
        public uint SetupID { get; set; }
        public uint SoundTable { get; set; }
        public uint IconImage { get; set; }
        public uint BasePalette { get; set; }
        public uint SkinPalSet { get; set; }
        public uint PhysicsTable { get; set; }
        public uint MotionTable { get; set; }
        public uint CombatTable { get; set; }
        public ObjDesc BaseObjDesc { get; set; } = new ObjDesc();
        public List<uint> HairColorList { get; set; } = new List<uint>();
        public List<HairStyleCG> HairStyleList { get; set; } = new List<HairStyleCG>();
        public List<uint> EyeColorList { get; set; } = new List<uint>();
        public List<EyeStripCG> EyeStripList { get; set; } = new List<EyeStripCG>();
        public List<FaceStripCG> NoseStripList { get; set; } = new List<FaceStripCG>();
        public List<FaceStripCG> MouthStripList { get; set; } = new List<FaceStripCG>();
        public List<GearCG> HeadgearList { get; set; } = new List<GearCG>();
        public List<GearCG> ShirtList { get; set; } = new List<GearCG>();
        public List<GearCG> PantsList { get; set; } = new List<GearCG>();
        public List<GearCG> FootwearList { get; set; } = new List<GearCG>();
        public List<uint> ClothingColorsList { get; set; } = new List<uint>();

        // Eyes
        public uint GetEyeTexture(uint eyesStrip, bool isBald)
        {
            ObjDesc eyes;
            if (isBald)
                eyes = EyeStripList[Convert.ToInt32(eyesStrip)].ObjDescBald;
            else
                eyes = EyeStripList[Convert.ToInt32(eyesStrip)].ObjDesc;
            return eyes.TextureChanges[0].NewTexture;
        }
        public uint GetDefaultEyeTexture(uint eyesStrip, bool isBald)
        {
            ObjDesc eyes;
            if (isBald)
                eyes = EyeStripList[Convert.ToInt32(eyesStrip)].ObjDescBald;
            else
                eyes = EyeStripList[Convert.ToInt32(eyesStrip)].ObjDesc;
            return eyes.TextureChanges[0].OldTexture;
        }

        // Nose
        public uint GetNoseTexture(uint noseStrip)
        {
            ObjDesc nose = NoseStripList[Convert.ToInt32(noseStrip)].ObjDesc;
            return nose.TextureChanges[0].NewTexture;
        }
        public uint GetDefaultNoseTexture(uint noseStrip)
        {
            ObjDesc nose = NoseStripList[Convert.ToInt32(noseStrip)].ObjDesc;
            return nose.TextureChanges[0].OldTexture;
        }

        // Mouth
        public uint GetMouthTexture(uint mouthStrip)
        {
            ObjDesc mouth = MouthStripList[Convert.ToInt32(mouthStrip)].ObjDesc;
            return mouth.TextureChanges[0].NewTexture;
        }
        public uint GetDefaultMouthTexture(uint mouthStrip)
        {
            ObjDesc mouth = MouthStripList[Convert.ToInt32(mouthStrip)].ObjDesc;
            return mouth.TextureChanges[0].OldTexture;
        }

        // Hair (Head)
        public uint GetHeadObject(uint hairStyle)
        {
            HairStyleCG hairstyle = HairStyleList[Convert.ToInt32(hairStyle)];
            return hairstyle.ObjDesc.AnimPartChanges[0].PartID;
        }
        public uint GetHairTexture(uint hairStyle)
        {
            HairStyleCG hairstyle = HairStyleList[Convert.ToInt32(hairStyle)];
            return hairstyle.ObjDesc.TextureChanges[0].NewTexture;
        }
        public uint GetDefaultHairTexture(uint hairStyle)
        {
            HairStyleCG hairstyle = HairStyleList[Convert.ToInt32(hairStyle)];
            return hairstyle.ObjDesc.TextureChanges[0].OldTexture;
        }

        // Headgear
        public uint GetHeadgearWeenie(uint headgearStyle)
        {
            return HeadgearList[Convert.ToInt32(headgearStyle)].WeenieDefault;
        }
        public uint GetHeadgearClothingTable(uint headgearStyle)
        {
            return HeadgearList[Convert.ToInt32(headgearStyle)].ClothingTable;
        }

        // Shirt
        public uint GetShirtWeenie(uint shirtStyle)
        {
            return ShirtList[Convert.ToInt32(shirtStyle)].WeenieDefault;
        }
        public uint GetShirtClothingTable(uint shirtStyle)
        {
            return ShirtList[Convert.ToInt32(shirtStyle)].ClothingTable;
        }

        // Pants
        public uint GetPantsWeenie(uint pantsStyle)
        {
            return PantsList[Convert.ToInt32(pantsStyle)].WeenieDefault;
        }
        public uint GetPantsClothingTable(uint pantsStyle)
        {
            return PantsList[Convert.ToInt32(pantsStyle)].ClothingTable;
        }

        // Footwear
        public uint GetFootwearWeenie(uint footwearStyle)
        {
            return FootwearList[Convert.ToInt32(footwearStyle)].WeenieDefault;
        }
        public uint GetFootwearClothingTable(uint footwearStyle)
        {
            return FootwearList[Convert.ToInt32(footwearStyle)].ClothingTable;
        }
    }
}
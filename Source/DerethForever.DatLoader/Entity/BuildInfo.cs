/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

using DerethForever.Entity;

namespace DerethForever.DatLoader.Entity
{
    public class BuildInfo
    {
        /// <summary>
        /// 0x01 or 0x02 model of the building - sometimes called MeshId in other AC rendering tools
        /// </summary>
        public uint ModelId { get; set; }

        public Position Frame { get; set; } = new Position(); // specific @loc of the model
        public uint NumLeaves { get; set; } // unsure what this is used for
        public List<CBldPortal> Portals { get; set; } = new List<CBldPortal>(); // portals are things like doors, windows, etc.
    }
}

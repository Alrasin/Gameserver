﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

using DerethForever.DatLoader.Entity;

namespace DerethForever.DatLoader.FileTypes
{
    /// <summary>
    /// This is the client_portal.dat file 0x0E00001D
    /// </summary>
    public class ContractTable
    {
        private const uint CONTRACT_TABLE_ID = 0x0E00001D;

        public uint Id { get; set; } // This should match CONTRACT_TABLE_ID
        public Dictionary<uint, Contract> Contracts { get; set; } = new Dictionary<uint, Contract>(); 

        public static ContractTable ReadFromDat()
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(CONTRACT_TABLE_ID))
            {
                return (ContractTable)DatManager.PortalDat.FileCache[CONTRACT_TABLE_ID];
            }
            else
            {
                DatReader datReader = DatManager.PortalDat.GetReaderForFile(CONTRACT_TABLE_ID);
                ContractTable obj = new ContractTable();

                obj.Id = datReader.ReadUInt32();

                ushort num_contracts = datReader.ReadUInt16();
                ushort table_size = datReader.ReadUInt16(); // We don't need this since C# handles it's own memory
                for (ushort i = 0; i < num_contracts; i++)
                {
                    uint key = datReader.ReadUInt32();
                    Contract value = Contract.Read(datReader);
                    obj.Contracts.Add(key, value);
                }

                DatManager.PortalDat.FileCache[CONTRACT_TABLE_ID] = obj;
                return obj;
            }
        }
    }
}
/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.DatLoader.Entity;

namespace DerethForever.DatLoader.FileTypes
{
    /// <summary>
    /// This is the client_portal.dat file starting with 0x13 -- There is only one of these, which is why REGION_ID is a constant.
    /// </summary>
    public class RegionDesc
    {
        private const uint REGION_ID = 0x13000000;

        public uint FileId { get; set; }

        public uint BLoaded { get; set; }

        public uint TimeStamp { get; set; }

        public string RegionName { get; set; }

        public uint PartsMask { get; set; }

        public LandDefs LandDefs { get; set; }

        public GameTime GameTime { get; set; }

        public uint PNext { get; set; }

        public SkyDesc SkyInfo { get; set; }

        public SoundDesc SoundInfo { get; set; }

        public SceneDesc SceneInfo { get; set; }

        public TerrainDesc TerrainInfo { get; set; }

        public RegionMisc RegionMisc { get; set; }

        public static RegionDesc ReadFromDat()
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(REGION_ID))
            {
                return (RegionDesc)DatManager.PortalDat.FileCache[REGION_ID];
            }

            DatReader datReader = DatManager.PortalDat.GetReaderForFile(REGION_ID);
            RegionDesc region = new RegionDesc();

            region.FileId = datReader.ReadUInt32();
            region.BLoaded = datReader.ReadUInt32();
            region.TimeStamp = datReader.ReadUInt32();
            region.RegionName = datReader.ReadPString(); // "Dereth"
            datReader.AlignBoundary();
            region.PartsMask = datReader.ReadUInt32();

            // There are 7 x 4 byte entries here that are "unknown". We will just skip them.
            datReader.Offset += (7 * 4);

            region.LandDefs = LandDefs.Read(datReader);
            region.GameTime = GameTime.Read(datReader);

            region.PNext = datReader.ReadUInt32();

            if ((region.PNext & 0x10) > 0)
                region.SkyInfo = SkyDesc.Read(datReader);

            if ((region.PNext & 0x01) > 0)
                region.SoundInfo = SoundDesc.Read(datReader);

            if ((region.PNext & 0x02) > 0)
                region.SceneInfo = SceneDesc.Read(datReader);

            region.TerrainInfo = TerrainDesc.Read(datReader);

            if ((region.PNext & 0x0200) > 0)
                region.RegionMisc = RegionMisc.Read(datReader);

            DatManager.PortalDat.FileCache[REGION_ID] = region;
            return region;
        }
    }
}

﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity.Enum;

namespace DerethForever.DatLoader.FileTypes
{
    /// <summary>
    /// These are client_portal.dat files starting with 0x08.
    /// As the name implies this contains surface info for an object. Either texture reference or color and whatever effects applied to it.
    /// </summary>
    public class Surface
    {
        public SurfaceType Type { get; set; }
        public uint OrigTextureId { get; set; }
        public uint OrigPaletteId { get; set; }
        public uint ColorValue { get; set; }
        public float Translucency { get; set; }
        public float Luminosity { get; set; }
        public float Diffuse { get; set; }

        public static Surface ReadFromDat(uint fileId)
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(fileId))
            {
                return (Surface)DatManager.PortalDat.FileCache[fileId];
            }
            else
            {
                DatReader datReader = DatManager.PortalDat.GetReaderForFile(fileId);
                Surface obj = new Surface();

                obj.Type = (SurfaceType)datReader.ReadUInt32();

                if (((uint)obj.Type & 6) > 0)
                {
                    obj.OrigTextureId = datReader.ReadUInt32();
                    obj.OrigPaletteId = datReader.ReadUInt32();
                }
                else
                {
                    obj.ColorValue = datReader.ReadUInt32();
                }

                obj.Translucency = datReader.ReadSingle();
                obj.Luminosity = datReader.ReadSingle();
                obj.Diffuse = datReader.ReadSingle();

                // Store this object in the FileCache
                DatManager.PortalDat.FileCache[fileId] = obj;

                return obj;
            }
        }
    }
}
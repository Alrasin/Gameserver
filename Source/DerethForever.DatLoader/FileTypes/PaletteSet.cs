﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

namespace DerethForever.DatLoader.FileTypes
{
    /// <summary>
    /// These are client_portal.dat files starting with 0x0F. 
    /// They contain, as the name may imply, a set of palettes (0x04 files)
    /// </summary>
    public class PaletteSet
    {
        public uint PaletteSetId { get; set; }
        public List<uint> PaletteList { get; set; } = new List<uint>();

        public static PaletteSet ReadFromDat(uint fileId)
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(fileId))
            {
                return (PaletteSet)DatManager.PortalDat.FileCache[fileId];
            }
            else
            {
                DatReader datReader = DatManager.PortalDat.GetReaderForFile(fileId);
                PaletteSet p = new PaletteSet();
                p.PaletteSetId = datReader.ReadUInt32();

                uint numpalettesets = datReader.ReadUInt32();
                for (int i = 0; i < numpalettesets; i++)
                    p.PaletteList.Add(datReader.ReadUInt32());

                // Store this object in the FileCache
                DatManager.PortalDat.FileCache[fileId] = p;

                return p;
            }
        }

        /// <summary>
        /// Returns the palette ID (uint, 0x04 file) from the Palette list based on the corresponding hue
        /// Hue is mostly (only?) used in Character Creation data.
        /// "Hue" referred to as "shade" in acclient.c
        /// </summary>
        public uint GetPaletteID(double hue)
        {
            // Make sure the PaletteList has valid data and the hue is within valid ranges
            if (this.PaletteList.Count == 0 || hue < 0 || hue > 1)
                return 0;

            // Hue is stored in DB as a percent of the total, so do some math to figure out the int position
            int palIndex = Convert.ToInt32(Convert.ToDouble(this.PaletteList.Count - 0.000001) * hue); // Taken from acclient.c (PalSet::GetPaletteID)
                                                                                                       // Since the hue numbers are a little odd, make sure we're in the bounds.
            if (palIndex < 0)
                palIndex = 0;
            if (palIndex > this.PaletteList.Count - 1)
                palIndex = this.PaletteList.Count - 1;
            return this.PaletteList[palIndex];
        }
    }
}

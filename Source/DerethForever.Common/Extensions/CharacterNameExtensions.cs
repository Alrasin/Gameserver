﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Common.Extensions
{
    /// <summary>
    /// This is a Helper Extension to service common String functions for Names
    /// </summary>
    public static class CharacterNameExtensions
    {
        /// <summary>
        /// This is a Player Name Helper that converts a string array into a string with necessary spaces throughout. This is used by server commands.
        /// </summary>
        /// <param name="nameStrings">Array of strings, may contain spaces.</param>
        /// <param name="startingElement">The starting position of the array where the character name begins. This is usually 1 when working with server commands.</param>
        /// <returns>string containing a character or account name</returns>
        public static string StringArrayToCharacterName(string[] nameStrings, int startingElement = 0)
        {
            // Store the first part of the player name.
            string characterName = nameStrings[startingElement];
            // Determine if the name has a space
            if (nameStrings.Length > 2)
            {
                // Build the name
                // characterName should already contain one element, so we start at the next position:
                for (int i = (startingElement + 1); i < nameStrings.Length; i++)
                {
                    characterName = characterName + " " + nameStrings[i];
                }
            }
            return characterName;
        }
    }
}

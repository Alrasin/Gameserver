﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Common
{
    public class ApiServerConfiguration
    {
        /// <summary>
        /// This is the URL that the AuthServer will listen to requests on.
        /// By default, http://*:8001 will listen on all addresses and would be best left unchanged.
        /// </summary>
        public string ListenUrl { get; set; }

        /// <summary>
        /// This is the URL that anyone connecting to your server for authorization uses.
        /// Tickets issued by this AuthServer will point to this address.
        /// If this address is not publically accessible on the internet, you're going to have a bad time.
        /// </summary>
        public string PublicUrl { get; set; }
    }
}